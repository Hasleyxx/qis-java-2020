import { Component, OnInit, Inject, Input, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialog, MatPaginator, MatTableDataSource, MatDialogRef } from '@angular/material';
import { TransactionService } from 'src/app/services/transaction.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DoctorService } from 'src/app/services/doctor.service';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import * as moment from 'moment';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';
import { EditPrintMedicationComponent } from '../edit-print-medication/edit-print-medication.component';
import { AddMedicationDialogComponent } from '../add-medication-dialog/add-medication-dialog.component';

@Component({
  selector: 'app-medicine-dialog',
  templateUrl: './medicine-dialog.component.html',
  styleUrls: ['./medicine-dialog.component.scss']
})
export class MedicineDialogComponent implements OnInit {

  displayedColumns = ['select', 'genericName', 'brandName', 'quantity', 'dosage', 'sched', 'duration', 'notes', 'dateCreated'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() itemUrl: string;
  public patItem: any;
  public ids = [];

  public patientId: any;

  public patientDatas: any = [];
  public addMedicationForm: FormGroup;
  public dataRef: any;

  public medications: string[] = [];
  public units = ["MG", "G", "ML"];
  public forms = ["TABLET", "CAPSULE", "IV", "IM"];
  public unitDefault = this.units[0];
  public formDefault = this.forms[0];

  public medhisString = "";
  public medhis = [
    { "label": "Hypertension", "control": "hyper" },
    { "label": "Diabetes", "control": "dia" },
    { "label": "Bronchial Asthma", "control": "bron" },
    { "label": "CAD/IHD", "control": "cid" },
    { "label": "Smoking Hx", "control": "smoke" },
    { "label": "Alcohol Hx", "control": "alcohol" },
    { "label": "Allergies", "control": "alle" },
    { "label": "Others", "control": "others" }
  ];

  public rosString = "";
  public ros = [
    { "label": "Easy Fatigability", "control": "fatig" },
    { "label": "PND", "control": "pnd" },
    { "label": "Syncope", "control": "syn" },
    { "label": "Anginal Pain", "control": "anginal" },
    { "label": "Dizziness", "control": "diz" },
    { "label": "Palpitation", "control": "palp" },
    { "label": "Dyspnea on Exertion", "control": "dysp" },
    { "label": "Bipedal Edema", "control": "bipedal" },
    { "label": "Orthopnea", "control": "ortho" },
    { "label": "Polyuria", "control": "poly" },
    { "label": "Tremors", "control": "trem" },
    { "label": "Cough", "control": "cough" }
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private doctor: DoctorService,
    private formBuilder: FormBuilder,
    private PS: PatientService,
    private TS: TransactionService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private math: MathService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<MedicineDialogComponent>
  ) {
    this.patientId = this.data.pid;
  }

  ngOnInit() {
    this.doctor.getDocPes().subscribe(
      pes => {
        this.dataRef = this.math.dataRef(pes);
      }
    );

    this.addMedicationForm = this.formBuilder.group({
      patientID: [this.patientId],
      genericName: ['', Validators.required],
      quantity: ['', Validators.required],
      dosage_amount: ['', Validators.required],
      dosage_unit: [this.unitDefault, Validators.required],
      dosage: [''],
      form: [this.formDefault],
      brandName: ['', Validators.required],
      sched: ['', Validators.required],
      duration: ['', Validators.required],
      notes: [''],
      dateCreated: ['']
    });

    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => this.patientDatas = data[0]
    );

    this.getMedications();
  }

  getMedications() {
    this.doctor.getDocMedId(this.patientId).subscribe(
      results => {
        this.dataSource = new MatTableDataSource<any>(results);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  getItem(value) {
    // this.addMedicationForm.get("itemName").setValue(value);
    if (value !== undefined) {
      const dialogRef = this.dialog.open(AddMedicationDialogComponent, {
        data: {
          data: value,
          pid: this.patientId
        },
        width: '25vw'
      });

      dialogRef.afterClosed().subscribe(
        result => {
          if (result == "added") {
            this.openSnackBar("Added new medication", "close");
            this.getMedications();
          } else if (result == "error") {
            this.openSnackBar("Failed to add new medication", "close");
          }
        }
      );
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  addMedication() {
    this.addMedicationForm.patchValue({
      dosage: this.addMedicationForm.get('dosage_amount').value + " / " + this.addMedicationForm.get('dosage_unit').value,
      dateCreated: moment().format("YYYY-MM-DD h:mm:ss")
    });

    this.doctor.addDocMed(this.addMedicationForm.value).subscribe(
      response => {
        // console.log(response);
        if (response == 1) {
          this.getMedications();
          let element: HTMLElement = document.getElementById('resetFormButton') as HTMLElement;
          element.click();
        }
      }
    );
  }

  tickMe(id) {
    let check = this.ids.includes(id);
    if (check == false) {
      this.ids.push(id);
    } else {
      this.ids.splice(this.ids.indexOf(id), 1);
    }
  }

  deleteMedication() {
    if (this.ids.length < 1) {
      this.openSnackBar("Please select at least 1 medication.", "close");
    } else {
      const dialogRef = this.dialog.open(ConfirmComponent, {
        data: {
          Title: "Are you sure?",
          Content: "Medication will be deleted!"
        },
        width: '20%',
      });

      dialogRef.afterClosed().subscribe(
        result => {
          if (result == "ok") {
            this.ids.forEach(id => {
              this.doctor.deleteDocMed(id).subscribe(
                response => {
                  console.log(response);
                }
              );
            });

            this.openSnackBar("Medication has been deleted", "close");
            this.getMedications();
          }
        }
      );
    }
  }

  printMedication() {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(EditPrintMedicationComponent, {
      data: {
        patientId: this.patientId,
        ids: this.ids
      },
      width: '70%'
    });

    /* dialogRef.afterClosed().subscribe(result => {
      if (result == "print") {
        alert("print");
      }
    }); */
    return;
    if (this.ids.length < 1) {
      this.openSnackBar("Please select at least 1 medication.", "close");
    } else {
      /* let suffix = [this.patientId];
      this.ids.forEach(id => {
        suffix.push(id);
      }); */

      this.dialogRef.close();
      const dialogRef = this.dialog.open(EditPrintMedicationComponent, {
        data: {
          patientId: this.patientId,
          ids: this.ids
        },
        width: '70%'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == "print") {
          alert("print");
        }
      });

      /* this.dialogRef.close();
      this.math.printDocMedication('',  suffix);
      setTimeout(() => {
        const dialogDialog = this.dialog.open(MedicineDialogComponent, {
          data: {
            tid: this.transactionId,
            pid: this.patientId
          }
        });
        this.getMedications();
        this.ids = [];
      }, 3000); */
    }
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId
      },
      disableClose: true
    });
  }

}
