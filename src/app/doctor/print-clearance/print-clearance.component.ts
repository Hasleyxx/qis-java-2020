import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'print-clearance',
  templateUrl: './print-clearance.component.html',
  styleUrls: ['./print-clearance.component.scss']
})
export class PrintClearanceComponent implements OnInit {
  public medHis: any[] = [];
  public pe: any[] = [];
  public ros: any[] = [];

  public allergies: any;
  public workUp: any;
  public prevSurgery: any;
  public assessmentPlan: any;

  constructor() {
    this.medHis = JSON.parse(sessionStorage.getItem("pastMedHis"));
    this.pe = JSON.parse(sessionStorage.getItem("pe"));
    this.ros = JSON.parse(sessionStorage.getItem("ros"));

    this.allergies = sessionStorage.getItem("allergies");
    this.workUp = sessionStorage.getItem("workUp");
    this.prevSurgery = sessionStorage.getItem("prevSurgery");
    this.assessmentPlan = sessionStorage.getItem("assessmentPlan");

    setTimeout(() => {
      sessionStorage.removeItem("pastMedHis");
      sessionStorage.removeItem("pe");
      sessionStorage.removeItem("ros");

      sessionStorage.removeItem("allergies");
      sessionStorage.removeItem("workUp");
      sessionStorage.removeItem("prevSurgery");
      sessionStorage.removeItem("assessmentPlan");
    });
  }

  ngOnInit() {
    
  }
}
