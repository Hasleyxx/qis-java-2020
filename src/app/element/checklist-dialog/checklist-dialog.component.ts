import { Component, OnInit, Inject } from '@angular/core';
import { transaction } from 'src/app/services/service.interface';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PeService } from 'src/app/services/pe.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ItemService } from 'src/app/services/item.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogPeSummaryComponent } from '../dialog-pe-summary/dialog-pe-summary.component';
import { ConfirmComponent } from '../confirm/confirm.component';

@Component({
  selector: 'app-checklist-dialog',
  templateUrl: './checklist-dialog.component.html',
  styleUrls: ['./checklist-dialog.component.scss']
})
export class ChecklistDialogComponent implements OnInit {

  id: any;
  transaction: transaction;
  public patientId: any;
  public transactionId: any;
  public transactionDatas: any;
  public patientDatas: any = [];
  dialog: any;
  public haves = [];
  public types = ["blood", "stool", "urine", "xray", "medical", "vital"];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private peService: PeService,
    private labService: LaboratoryService,
    private itemService: ItemService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DialogPeSummaryComponent>
  ) {
    this.transactionId = this.data.tid;
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.haves = [];
    // Transaction
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientId = data[0]['patientId'];
        this.types.forEach(element => {
          if (data[0]['have_' + element] !== "0") {
            let temp = {
              label: element,
              check: true,
              value: 0
            };
            this.haves.push(temp);
          } else {
            let temp = {
              label: element,
              check: false,
              value: 1
            };
            this.haves.push(temp);
          }
        });

        // console.log(this.haves);

        // Patient
        if (this.patientDatas.length < 1) {
          this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
            data => this.patientDatas = data[0]
          );
        }
      }
    );
  }

  tickMe(tid, type, value) {
    let data = {
      transactionId: tid,
      type: "have_"+type,
      status: value
    }

    this.transactionService.updateChecklist(data).subscribe(
      result => {
        // console.log(result);
        this.getData();
      }
    );
  }

}
