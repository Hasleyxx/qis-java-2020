import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { PatientService } from 'src/app/services/patient.service';
import { PeService } from 'src/app/services/pe.service';
import * as moment from 'moment';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: 'app-display-pe-medical',
  templateUrl: './display-pe-medical.component.html',
  styleUrls: ['./display-pe-medical.component.scss']
})
export class DisplayPeMedicalComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];
  public date = moment().format("YYYY-MM-DD h:mm:ss");
  public patientId: any;
  public transactionId: any;
  public patientDatas: any = [];
  public hemaDatas: any = [];
  public seroDatas: any = [];
  public microDatas: any = [];
  public toxiDatas: any = [];
  public classification: any = [];
  public pes: any = [];

  public class: any;
  public classColor: any = 'text-dark';
  public notes: any;
  public antiHav: any;
  public hbsag: any;
  public cbcot: any = 'text-dark';
  public pt: any;
  public meth: any;
  public tetra: any;
  public urinotes: any;

  public medical: any = [];
  public quality: any = [];
  public path: any = [];
  public rad: any = [];

  constructor(
    route: ActivatedRoute,
    public math: MathService,
    private patientService: PatientService,
    private peService: PeService,
    private labService: LaboratoryService,
    private XS: XrayService,
    private transactionService: TransactionService
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.transactionId = this.data[0];
    this.dataDetails = this.data.map(data => this.getPeMedicalReady(data));

    /* if (this.router.url == '/nurse/medical/' + this.patientId + "/" + this.transactionId) {
      this.math.navSubs("nurse");

    } else if (this.router.url == '/imaging/medical/' + this.patientId + "/" + this.transactionId) {
      this.math.navSubs("imaging");

    } else {
      this.math.navSubs("pe");
    } */
  }

  ngOnInit() {

    this.getData();
  }

  getData() {
    // Transaction
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.patientId = data[0]['patientId'];
        // Patient
        this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );

    // PE
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        if (data == null ) {
          this.pes['doctor'] = 'FROILAN A. CANLAS, M.D.';
          this.pes['license'] = '82498';
        }else {
          this.pes = data;
        }
      }
    );

    // Hematology
    this.labService.getHematology(this.transactionId).subscribe(
      data => {
        if (data.length !== 0) {
          this.hemaDatas = data[0];
          if (data[0]['cbcot'] == 'REACTIVE') {
            this.cbcot = 'text-danger';
          }
        }
      }
    );

    // Microscopy
    this.labService.getMicroscopy(this.transactionId).subscribe(
      data => {
        this.microDatas = data[0];

        if (data[0] !== undefined) {
          if (this.microDatas.pregTest == 'POSITIVE') {
            this.pt = 'text-danger';
          }

          if (this.microDatas.uriOt !== 'NORMAL' && this.microDatas.uriOt !== '' && this.microDatas.uriOt !== 'N/A') {
            this.urinotes = 'text-danger';
          }

          this.labService.getPersonnel(this.microDatas.medID).subscribe(
            data => {
              this.medical.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
              this.medical = this.medical[0];
            }
          );
          this.labService.getPersonnel(this.microDatas.qualityID).subscribe(
            data => {
              if (data.length !== 0) {
                this.quality.push({
                  'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                  'license': data[0].licenseNO
                });
                this.quality = this.quality[0];
              }
            }
          );
          this.labService.getPersonnel(this.microDatas.pathID).subscribe(
            data => {
              this.path.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
              this.path = this.path[0];
            }
          );

        } else {
          this.pt = 'text-dark';
          this.urinotes = 'text-dark';
          this.medical.push({
            'name': '-',
            'license': '-'
          });

          this.quality.push({
            'name': '-',
            'license': '-'
          });

          this.path.push({
            'name': '-',
            'license': '-'
          });
        }
      }
    );

    // Serology
    this.labService.getSerology(this.transactionId).subscribe(
      data => {
        this.seroDatas = data[0];
        console.log(data[0]);
        if (data[0] !== undefined) {
          if (this.seroDatas.hbsAG == 'REACTIVE') {
            this.hbsag = 'text-danger';
          }
          if (this.seroDatas.antiHav == 'REACTIVE') {
            this.antiHav = 'text-danger';
          }

        } else {
          this.hbsag = 'text-dark';
        }
      }
    );

    // Toxicology
    this.labService.getToxicology(this.transactionId).subscribe(
      data => {
        this.toxiDatas = data[0];

        if (data[0] !== undefined) {
          if (this.toxiDatas.meth == 'POSITIVE') {
            this.meth = 'text-danger';
          }

          if (this.toxiDatas.tetra == 'POSITIVE') {
            this.tetra = 'text-danger';
          }

        } else {
          this.meth = 'text-dark';
          this.tetra = 'text-dark';
        }
      }
    );

    // Class
    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        this.classification = data;

        if (data !== null) {
          if (this.classification.medicalClass == 'CLASS A - Physically Fit') {
            this.class = 'Class A - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency') {
            this.class = 'Class B - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.class = 'CLASS C - With abnormal findings generally not accepted for employment.';
          }
          else if (this.classification.medicalClass == 'CLASS D - Unemployable') {
            this.class = 'Class D - UNEMPLOYABLE';
          }
          else {
            this.class = 'PENDING';
          }

          if (this.classification.notes != 'NORMAL' && this.classification.notes != '' && this.classification.notes != 'N/A') {
            this.notes = 'text-danger';
          }

          if (this.class == 'Class D - UNEMPLOYABLE' || this.class == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.classColor = 'text-danger';
          }

        } else {
          this.class = 'PENDING';
          this.notes = 'text-dark';
          this.classColor = 'text-dark';
        }
      }
    );

    // Radiology
    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        if (data == null) {
          this.rad.push({
            'qa': '-',
            'radiologist': '-'
          });

        } else {
          this.rad = data;
        }

      }
    );

    Promise.all(this.dataDetails).then(() => {
      this.math.onPeMedicalReady();
    });

  }

  getPeMedicalReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

}
