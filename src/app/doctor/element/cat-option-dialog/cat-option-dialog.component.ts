import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { DoctorService } from 'src/app/services/doctor.service';
import { MathService } from 'src/app/services/math.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-cat-option-dialog',
  templateUrl: './cat-option-dialog.component.html',
  styleUrls: ['./cat-option-dialog.component.scss']
})
export class CatOptionDialogComponent implements OnInit {

  public form: FormGroup;

  public type: string;
  public submitText: string;
  public labelText: string;
  public successText: string;
  public failedText: string;

  public categories: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private DS: DoctorService,
    private math: MathService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CatOptionDialogComponent>,
    private formBuilder: FormBuilder
  ) { 
    this.type = this.dialogData.type;
  }

  ngOnInit() {
    if (this.type == "option") {
      this.form = this.formBuilder.group({
        category: ['', Validators.required],
        option: ['', Validators.required]
      });

      this.labelText = "Add new option for category";
      this.submitText = "New option for selected category will be added!";
      this.successText = "New option has been added to the selected category!";
      this.failedText = "Failed to add new option to the selected category!";
    } else {
      this.form = this.formBuilder.group({
        category: ['', Validators.required]
      });

      this.labelText = "Add new category";
      this.submitText = "New category will be added!";
      this.successText = "New category has been added!";
      this.failedText = "Failed to add new category!";
    }

    this.DS.getDocRequestCategory().subscribe(data => {
      if (data.length > 0) {
        data.forEach((element, index) => {
          this.categories.push(element);
        });
      }
    });
  }

  cancel() {
    this.dialogRef.close();
  }

  submit() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: this.submitText
      },
      width: "20%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        if (this.type == "category") {
          this.DS.addDocRequestCategory(this.form.value).subscribe(data => {
            if (data == 1) {
              this.dialogRef.close({
                text: this.successText,
                type: this.type
              });
            } else {
              this.math.openSnackBar(this.failedText, "close");
            }
          });
        } else {
          this.DS.addDocRequestOption(this.form.value).subscribe(data => {
            if (data == 1) {
              this.dialogRef.close({
                text: this.successText,
                type: this.type,
                id: this.form.get('category').value
              });
            } else {
              this.math.openSnackBar(this.failedText, "close");
            }
          });
        }
      }
    });
  }

}
