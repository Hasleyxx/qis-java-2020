import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientRecComponent } from './patient-rec.component';

describe('PatientRecComponent', () => {
  let component: PatientRecComponent;
  let fixture: ComponentFixture<PatientRecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientRecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientRecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
