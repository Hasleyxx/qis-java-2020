import { Component, OnInit, Inject } from '@angular/core';
import { patient } from 'src/app/services/service.interface';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ItemService } from 'src/app/services/item.service';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { ConfirmComponent } from '../confirm/confirm.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: "patient-form",
  templateUrl: "./patient-form.component.html",
  styleUrls: ["./patient-form.component.scss"]
})
export class PatientFormComponent implements OnInit {
  patientForm: any;
  transaction: any;
  transForm: any;
  title;
  patient: any;
  _confirm: any;
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 10;
  };
  d = new DatePipe("en-US");
  patCompany: any;
  maxDate = new Date();

  constructor(
    private fb: FormBuilder,
    private iS: ItemService,
    public math: MathService,
    private pat: PatientService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PatientFormComponent>,
    @Inject(MAT_DIALOG_DATA) public patInfo: any
  ) {
    //console.log(this.patInfo)
    if (this.patInfo.lastName || this.patInfo.firstName) {
      this.iS
        .getCompanyByID(this.patInfo.companyID)
        .subscribe(data => (this.patCompany = data[0]));


      this.patientForm = this.fb.group({
        patientID: [this.patInfo.patientID],
        companyName: [this.patInfo.companyName],
        position: [this.patInfo.position],
        firstName: [this.patInfo.firstName, Validators.required],
        middleName: [this.patInfo.middleName, Validators.required],
        lastName: [this.patInfo.lastName, Validators.required],
        address: [this.patInfo.address, Validators.required],
        gender: [this.patInfo.gender, Validators.required],
        birthdate: [this.patInfo.birthdate, Validators.required],
        age: [this.patInfo.age, Validators.required],
        contactNo: [this.patInfo.contactNo, Validators.required],
        email: [this.patInfo.email],
        sid: [this.patInfo.sid],
        patientRef: [this.patInfo.patientRef],
        dateUpdate: [this.patInfo.dateUpdate],
        creationDate: [this.patInfo.creationDate],
        patientType: [this.patInfo.patientType],
        notes: [this.patInfo.notes]


      });

      this.title = "Edit Patient Information";
    } else {
      this.patientForm = this.fb.group({
        companyName: [""],
        position: [""],
        firstName: ["", Validators.required],
        middleName: ["", Validators.required],
        lastName: ["", Validators.required],
        address: ["", Validators.required],
        gender: ["", Validators.required],
        birthdate: ["", Validators.required],
        age: ["", Validators.required],
        contactNo: ["", Validators.required],
        email: ["", Validators.email],
        sid: [""],
        patientRef: [""],
        dateUpdate: this.math.getDateNow(),
        creationDate: [this.d.transform(new Date(), "yyyy-MM-dd")],
        patientType: [""],
        notes: [""]
      });



      this.title = "Add Patient";
    }
  }

  ngOnInit() {
    if (!this.patInfo.patientRef) {
      //Generate random numbers and check patient ref for duplicate
      this.pat
        .getPatient("getPatient")
        .subscribe(data => this.getRN(this.math.patcheckRef(data)));
    }
    this.patientForm.get("birthdate").valueChanges.subscribe(date => {
      if (date == null) {
        this.patientForm.get("age").setValue("");
      } else {
        const bday = this.math.convertDate(date);
        const age = this.math.computeAge(bday);
        this.patientForm.get("age").setValue(age);
      }
    });
  }
  getRN(data) {
    this.patientForm.get("patientRef").setValue(data);
  }
  //get company select emited value
  getCompany(value) {
    this.patientForm.get("companyName").setValue(value.nameCompany);
  }
  savePatient() {
    this.patientForm
      .get("firstName")
      .setValue(this.patientForm.get("firstName").value.toUpperCase());
    this.patientForm
      .get("middleName")
      .setValue(this.patientForm.get("middleName").value.toUpperCase());
    this.patientForm
      .get("lastName")
      .setValue(this.patientForm.get("lastName").value.toUpperCase());
    this.patientForm
      .get("address")
      .setValue(this.patientForm.get("address").value.toUpperCase());

    // convert date to default date string
    if (this.patientForm.get("birthdate").value.toDateString) {
      const bdate = this.patientForm.get("birthdate").value;
      var bday = this.math.convertDate(bdate);
      this.patientForm.get("birthdate").setValue(bday);
    }
    if (this.patInfo.patientID) {
      //open confirm dialog
      const dialogRef = this.dialog.open(ConfirmComponent, {
        width: "20%",
        data: {
          Title: "Are you sure?",
          Content: "This Patient will be updated!!!"
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          // save new patient
          this.patientForm.patchValue({
            dateUpdate: this.math.getDateNow()
          });

          /* if (this.patientForm.get("companyName").value == "CALFURN") {
            this.trans.getOneTrans("getTransaction/" + this.patientForm.get('patientID').value).subscribe(
              result => {
                let transTrans: transaction = {
                  transactionId: result[0].transactionId,
                  transactionRef: result[0].transactionRef,
                  patientId: result[0].patientId,
                  userId: result[0].userId,
                  transactionType: result[0].transactionType,
                  biller: result[0].biller,
                  totalPrice: result[0].totalPrice,
                  paidIn: result[0].paidIn,
                  paidOut: result[0].paidOut,
                  grandTotal: result[0].grandTotal,
                  status: result[0].status,
                  salesType: result[0].salesType,
                  loe: result[0].loe,
                  an: result[0].an,
                  ac: result[0].ac,
                  notes: result[0].notes,
                  currency: result[0].currency,
                  transactionDate: this.math.getDateNow()
                };

                this.trans.updateTransaction(transTrans).subscribe(
                  value => {
                    console.log(value);
                  }
                );
              }
            );
          } */

          this.pat.updatePatient(this.patientForm.value).subscribe(
            (data: patient) => {
              console.log(data);
            },
            (error: any) => console.log(error)
          );
          this.dialogRef.close({
            message: "Patient information updated successfully",
            status: "ok",
            data: this.patientForm.value
          });
        }
      });
    } else {
      //open confirm dialog
      const dialogRef = this.dialog.open(ConfirmComponent, {
        width: "20%",
        data: {
          Title: "Are you sure?",
          Content: "New Patient will be save to database."
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          // save new patient
          this.pat.addPatient(this.patientForm.value).subscribe(
            (data: patient) => {
              console.log(data);
            },
            (error: any) => console.log(error)
          );
          this.dialogRef.close({
            message: "Patient Successfully Added",
            status: "ok",
            data: this.patientForm.value
          });
        }
      });
    }
  }

  isNumber(event) {
    //console.log(event);
  }
}
