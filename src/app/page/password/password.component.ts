import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { PasswordService } from 'src/app/password.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  public form: FormGroup;
  public validityForm: FormGroup;
  public currentPassword: any = "";
  public validityValue: any = "";

  public validities = [
    { "value": "One (1) month" },
    { "value": "Two (2) months" },
    { "value": "Three (3) months" }
  ];

  constructor(
    private math: MathService,
    private PS: PasswordService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog
  ) {
    this.math.navSubs("admin");
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      password: ['', Validators.required]
    });

    this.validityForm = this.formBuilder.group({
      id: [1],
      validity: ['', Validators.required]
    });

    this.getData();
  }

  getData() {
    this.PS.getPassword().subscribe(data => {
      if (data !== null) {
        this.form.patchValue({
          password: data.password
        });
        this.currentPassword = data.password;
      }
    });

    this.PS.getValidity().subscribe(data => {
      if (data !== null) {
        this.validityValue = data.validity;
        this.validityForm.get('validity').setValue(data.validity);
      }
    });
  }

  submitForm() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Password will be saved and cannot be reversed!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "ok") {
          if (this.currentPassword == this.form.get('password').value) {
            this.math.openSnackBar("The Password you provided is not allowed or has been already used.", "close");
          } else {
            this.PS.addPassword(this.form.value).subscribe((data: any) => {
              if (data == 1) {
                this.math.openSnackBar("Password has been updated!", "close");
                this.currentPassword = this.form.get('password').value;
              } else {
                this.math.openSnackBar("Failed to update password", "close");
              }
            });
          }
        }
      }
    );
  }

  validityFormSubmit() {
    let validity = this.validityForm.get('validity').value;

    if (validity == "") {
      this.math.openSnackBar("Please select password validity", "close");
    } else {
      this.PS.updateValidity(this.validityForm.value).subscribe((data: any) => {
        if (data == 1) {
          this.math.openSnackBar("Password validity has been updated to " + validity + ".", "close");
        } else {
          this.math.openSnackBar("Failed to update password validity", "close");
        }
      });
    }
  }
}
