import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { WebcamImage } from "ngx-webcam";
import { ConfirmComponent } from '../confirm/confirm.component';
import { DatePipe } from '@angular/common';
import { UserService } from 'src/app/services/user.service';

export interface tempPatInfo {
  companyName: any,
  position: any,
  firstName: any,
  middleName: any,
  lastName: any,
  address: any,
  gender: any,
  birthdate: any,
  age: any,
  contactNo: any,
  email: any,
  sid: any,
  patientRef: any,
  dateUpdate: any,
  creationDate: any,
  patientType: any,
  notes: any,
  image: any,
  itemId?: number
}

@Component({
  selector: "app-camera-dialog",
  templateUrl: "./camera-dialog.component.html",
  styleUrls: ["./camera-dialog.component.scss"]
})
export class CameraDialogComponent implements OnInit {
  public type: string;
  public patientID: any;
  public patInfo: FormGroup;
  public tempPatInfo: tempPatInfo;
  public webcamImage: WebcamImage = null;
  public patientRef: any;
  d = new DatePipe("en-US");

  @ViewChild("noImageImg") noImageImg: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CameraDialogComponent>,
    private formBuider: FormBuilder,
    private math: MathService,
    private PS: PatientService,
    private US: UserService
  ) {
    this.tempPatInfo = this.dialogData.patInfo;
    this.type = this.dialogData.type;
    this.patientID = this.dialogData.patientID;
  }

  ngOnInit() {
    if (this.type == "new") {
      this.patInfo = this.formBuider.group({
        companyName: [""],
        position: [""],
        firstName: [""],
        middleName: [""],
        lastName: [""],
        address: [""],
        gender: [""],
        birthdate: [""],
        age: [""],
        contactNo: [""],
        email: [""],
        sid: [""],
        patientRef: [""],
        dateUpdate: [""],
        creationDate: [""],
        patientType: [""],
        notes: [""],
        image: [""]
      });
    } else {
      this.patInfo = this.formBuider.group({
        patientID: [""],
        imageName: [""],
        image: [""]
      });
    }

    this.patchData();
  }

  patchData() {
    if (this.type == "new") {
      this.patientRef = this.tempPatInfo.patientRef;

      this.patInfo.patchValue({
        companyName: this.tempPatInfo.companyName,
        position: this.tempPatInfo.position,
        firstName: this.tempPatInfo.firstName,
        middleName: this.tempPatInfo.middleName,
        lastName: this.tempPatInfo.lastName,
        address: this.tempPatInfo.address,
        gender: this.tempPatInfo.gender,
        birthdate: this.d.transform(new Date(this.tempPatInfo.birthdate), "yyyy-MM-dd"),
        age: this.tempPatInfo.age,
        contactNo: this.tempPatInfo.contactNo,
        email: this.tempPatInfo.email,
        sid: this.tempPatInfo.sid,
        patientRef: this.tempPatInfo.patientRef,
        dateUpdate: this.math.getDateNow(),
        creationDate: this.d.transform(new Date(), "yyyy-MM-dd"),
        patientType: this.tempPatInfo.patientType,
        notes: this.tempPatInfo.notes
      });
    } else {
      this.patInfo.patchValue({
        patientID: this.patientID,
        imageName: this.math.getDateNow() + "_" + this.patientID,
        image: ''
      });
    }
  }

  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;

    this.patInfo.get("image").setValue(this.webcamImage.imageAsDataUrl);
  }

  savePatient() {
    let content: any;
    if (this.webcamImage == null) {
      this.US.getBase64FromImage().subscribe(data => {
        let base64 = "data:image/jpeg;base64," + data;
        this.patInfo.get("image").setValue(base64);
      });
    }

    if (this.type == "new") {
      content = "Doc Patient will be added!";
    } else {
      content = "Doc Patient data will be updated!";
    }

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: content
      },
      width: "25%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        if (this.type == "new") {
          this.PS.addPatient(this.patInfo.value).subscribe(data => {
            this.PS.getPatientRef(this.tempPatInfo.patientRef).subscribe(patient => {
              let patientData = patient[0];
              this.dialogRef.close({
                status: "added",
                data: patientData
              });
            });
          });

        } else {
          this.PS.updatePatientImage(this.patInfo.value).subscribe(data => {
            this.PS.getOnePatient("/getPatient/" + this.patientID).subscribe(patient => {
              let patientData = patient[0];
              this.dialogRef.close({
                status: "updated",
                data: patientData
              });
            });
          });
        }
      }
    });
  }
}
