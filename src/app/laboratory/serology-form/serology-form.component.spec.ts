import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerologyFormComponent } from './serology-form.component';

describe('SerologyFormComponent', () => {
  let component: SerologyFormComponent;
  let fixture: ComponentFixture<SerologyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerologyFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerologyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
