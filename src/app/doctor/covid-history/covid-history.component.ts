import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef } from '@angular/material';
import { MathService } from 'src/app/services/math.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { PatientService } from 'src/app/services/patient.service';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { CovidDialogComponent } from '../covid-dialog/covid-dialog.component';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';

@Component({
  selector: 'app-covid-history',
  templateUrl: './covid-history.component.html',
  styleUrls: ['./covid-history.component.scss']
})
export class CovidHistoryComponent implements OnInit {

  displayedColumns: string[] = ['date', 'result', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  heldData: any[] = [];

  public patientId: any;
  public type: any;

  public patientDatas: any = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private math: MathService,
    private PS: PatientService,
    private doctor: DoctorService,
    public state: ActivatedRoute,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CovidHistoryComponent>
  ) {
    this.patientId = this.data.pid;
    if (this.data.status) {
      this.type = this.data.status;
    }
  }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData() {
    // Patient
    this.PS.getOnePatient("getPatient/" + this.patientId).subscribe(data => {
      this.patientDatas = data[0];
    });

    // Covid History
    this.doctor.getCovidHistory(this.patientId).subscribe(
      data => {
        if (data.length > 0) {
          this.heldData = [];
          data.forEach(element => {
            let transData: any = {
              date: moment(element['creationDate']).format("LLL"),
              result: element['result'],
              docCovidId: element['docCovidId']
            }
            this.heldData.push(transData);
          });
          this.dataSource = new MatTableDataSource(this.heldData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    )
  }

  addCovid(pid) {
    this.dialogRef.close();

    const dialogRef = this.dialog.open(CovidDialogComponent, {
      data: {
        pid: pid,
        docCovidId: undefined
      }
    });
  }

  updateResult(docCovidId, pid) {
    this.dialogRef.close();

    const dialogRef = this.dialog.open(CovidDialogComponent, {
      data: {
        pid: pid,
        docCovidId: docCovidId
      }
    });
  }

  printResult(docCovidId) {
    this.dialogRef.close();
    sessionStorage.setItem("docCovidId", docCovidId);
    const suffix = [this.patientId, 'covid'];
    this.math.printDoc('', suffix);
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId,
      }
    });
  }

}
