import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'print-request',
  templateUrl: './print-request.component.html',
  styleUrls: ['./print-request.component.scss']
})
export class PrintRequestComponent implements OnInit {

  public hemas: any;
  public micros: any;
  public chems: any;
  public xrays: any;
  public others: any;

  constructor(
  ) {
    this.hemas = JSON.parse(sessionStorage.getItem("hemas"));
    this.micros = JSON.parse(sessionStorage.getItem("micros"));
    this.chems = JSON.parse(sessionStorage.getItem("chems"));
    this.xrays = JSON.parse(sessionStorage.getItem("xrays"));
    this.others = JSON.parse(sessionStorage.getItem("others"));
  }

  ngOnInit() {

  }
}
