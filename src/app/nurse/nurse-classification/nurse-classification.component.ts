import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-nurse-classification',
  templateUrl: './nurse-classification.component.html',
  styleUrls: ['./nurse-classification.component.scss']
})
export class NurseClassificationComponent implements OnInit {

  public nurseClassification = "nurseClassification";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("nurse");
  }

  ngOnInit() {
  }

}
