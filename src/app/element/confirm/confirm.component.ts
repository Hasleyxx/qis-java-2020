import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  Title = "Are you sure?";
  Content = "";
  cancelBTN = "Cancel";
  okBTN = "OK";
  barCodeBTN = "";
  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public config: any) {
      if(config.Title){
        this.Title = config.Title;
        this.Content = config.Content;
      }
      if(config.cancel){
        this.cancelBTN = config.cancel;
      }
      if(config.ok){
        this.okBTN = config.ok;
      }
      if(config.barcode){
        this.barCodeBTN = config.barcode;
      }
     }

  ngOnInit() {
    // this.Title = this.config.Title;
    // this.Content = this.config.Content;
    // this.cancelBTN = this.config
  }
  ok(){
    this.dialogRef.close("ok");
  }
  cancel(rel: number = 0){
    this.dialogRef.close("cancel");
    if(rel == 1){
      location.reload();
    }
  }
  barcode(rel: number = 0){
    this.dialogRef.close("barcode");
    if(rel == 1){
      location.reload();
    }
  }
}
