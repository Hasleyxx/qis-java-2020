import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { PeService } from 'src/app/services/pe.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-pe-update-pe',
  templateUrl: './pe-update-pe.component.html',
  styleUrls: ['./pe-update-pe.component.scss']
})
export class PeUpdatePeComponent implements OnInit {

  public transactionId: any;

  public peForm: FormGroup;

  public areas = [
    { "area": "Skin", "control": "skin" },
    { "area": "Head and Neck", "control": "hn" },
    { "area": "Chest/Breast/Lungs", "control": "cbl" },
    { "area": "Cardiac/Heart", "control": "ch" },
    { "area": "Abdomen", "control": "abdo" },
    { "area": "Extremities", "control": "extre" }
  ]

  public doctors = [
    { "doctor": "FROILAN A. CANLAS, M.D.", "license": "82498" },
    { "doctor": "JOHN TANGLAO, M.D.", "license": "79549" },
    { "doctor": "MARIGOLD A. SIBAL, M.D.", "license": "104200" }
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private math: MathService,
    private peService: PeService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<PeUpdatePeComponent>,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.transactionId = this.data.tid;

    this.peForm = this.formBuilder.group({
      skin: [''],
      hn: [''],
      cbl: [''],
      ch: [''],
      abdo: [''],
      extre: [''],
      ot: [''],
      find: [''],
      doctor: [''],
      license: [''],
      userID: [''],
      creationDate: [''],
      dateUpdate: [''],
      pexamID: ['']
    });

    this.getPe();
  }

  getPe() {
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        this.peForm.patchValue({
          skin: data.skin,
          hn: data.hn,
          cbl: data.cbl,
          ch: data.ch,
          abdo: data.abdo,
          extre: data.extre,
          ot: data.ot,
          find: data.find,
          doctor: data.doctor,
          license: data.license,
          userID: parseInt(sessionStorage.getItem('token')),
          creationDate: data.creationDate,
          dateUpdate: data.dateUpdate,
          pexamID: data.pexamID,
        });
      }
    );
  }

  updatePe() {
    this.peForm.patchValue({
      dateUpdate: moment().format("YYYY-MM-DD h:mm:ss"),
      userID: parseInt(sessionStorage.getItem('token')),
    });

    this.peService.updatePe(this.peForm.value).subscribe(
      data => {
        if (data == 1) {
          this.dialogRef.close(this.peForm.value);
          this.openSnackBar("Physical examination has been updated!", "close");
        }
      }
    );
  }

  changeDoctor(event: any) {
    let doctor = event;
    for (let index = 0; index < this.doctors.length; index++) {
      if (this.doctors[index]['doctor'] === doctor) {
        this.peForm.patchValue({
          license: this.doctors[index]['license']
        });
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
