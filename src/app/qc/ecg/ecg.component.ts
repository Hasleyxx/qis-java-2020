import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-ecg',
  templateUrl: './ecg.component.html',
  styleUrls: ['./ecg.component.scss']
})
export class EcgComponent implements OnInit {

  public ecg = "ecg";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("qc");
  }
  ngOnInit() {
  }

}
