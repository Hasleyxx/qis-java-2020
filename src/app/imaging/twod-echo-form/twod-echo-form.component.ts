import { Component, OnInit, Inject } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { transaction, personnel } from 'src/app/services/service.interface';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TwodService } from 'src/app/services/twod.service';
import { ColorFlowService } from 'src/app/services/color-flow.service';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { PersonnelService } from 'src/app/services/personnel.service';

@Component({
  selector: 'app-twod-echo-form',
  templateUrl: './twod-echo-form.component.html',
  styleUrls: ['./twod-echo-form.component.scss']
})
export class TwodEchoFormComponent implements OnInit {

  public personels = ["Febie Dane C. Buada, RRT"];
  public cardiographers = ["Febie Dane C. Buada, RRT"];

  tid: any;
  personnels: personnel[];
  transaction: transaction;
  update: boolean = false;

  form: FormGroup;
  public echoCarDefault: string;
  public qualityDefault: string;
  public cardioDefault: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private math: MathService,
    private TS: TransactionService,
    private router: Router,
    private formBuilder: FormBuilder,
    private TwoS: TwodService,
    private CSF: ColorFlowService,
    private PersonnelS: PersonnelService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<TwodEchoFormComponent>
  ) {
    this.math.navSubs("imaging");
    this.tid = this.dialogData.tid;

    if (isNaN(this.tid)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      echoCarID: ['', [Validators.required]],
      qualityID: ['', [Validators.required]],
      cardioID: ['', [Validators.required]],
      patientIdRef: [''],
      echoForm: this.formBuilder.group({
        echoID: [''],
        transactionID: [''],
        patientID: [''],
        echoCarID: ['-'],
        qualityID: ['-'],
        cardioID: ['-'],
        userID: [parseInt(sessionStorage.getItem("token"))],

        lvedd: [''],
        lvedv: [''],
        laap: [''],
        lvesd: [''],
        lvesv: [''],
        larl: [''],
        lveddbsa: [''],
        sv: [''],
        labsa: [''],
        lvesdbsa: [''],
        co: [''],
        a1: [''],
        ivsd: [''],
        efm: [''],
        a2: [''],
        ivss: [''],
        efs: [''],
        lavi: [''],
        pwd: [''],
        fs: [''],
        aorta: [''],
        pws: [''],
        epss: [''],
        lvot: [''],
        lvmi: [''],
        lvet: [''],
        mva: [''],
        rwt: [''],
        rvm: [''],
        tva: [''],
        rarl: [''],
        rvfac: [''],
        rvot: [''],
        rabsa: [''],
        tapse: [''],
        mpa: [''],
        rvwt: [''],

        creationDate: [''],
        dateUpdate: ['']
      }),
      colorForm: this.formBuilder.group({
        colorID: [''],
        transactionID: [''],
        patientID: [''],
        userID: [parseInt(sessionStorage.getItem("token"))],

        lvelocity: [''],
        lpeakgrad: [''],
        lvti: [''],
        lvalve: [''],
        lratio: [''],
        ljetarea: [''],
        lvc: [''],

        avvelocity: [''],
        avpeakgrad: [''],
        avvti: [''],
        avvalve: [''],
        avratio: [''],
        avjetarea: [''],
        avvc: [''],

        mvvelocity: [''],
        mvpeakgrad: [''],
        mvvti: [''],
        mvvalve: [''],
        mvratio: [''],
        mvjetarea: [''],
        mvvc: [''],

        tvvelocity: [''],
        tvpeakgrad: [''],
        tvvti: [''],
        tvvalve: [''],
        tvratio: [''],
        tvjetarea: [''],
        tvvc: [''],

        pvvelocity: [''],
        pvpeakgrad: [''],
        pvvti: [''],
        pvvalve: [''],
        pvratio: [''],
        pvjetarea: [''],
        pvvc: [''],


        pat: [''],
        rvat: [''],
        trjet: [''],

        ewave: [''],
        ivrt: [''],

        late: [''],
        lata: [''],

        mede: [''],
        meda: [''],

        e: [''],
        a: [''],

        eeratio: [''],

        creationDate: [''],
        dateUpdate: ['']
      })
    });

    this.PersonnelS.getRadtech().subscribe(personnel => {
      this.personnels = personnel;
    });

    this.getData();
  }

  getData() {
    this.TS.getOneTrans("getTransaction/" + this.tid).subscribe(data => {
      if (data[0].length == 0) {
        this.router.navigate(['error/404']);
      } else {
        this.transaction = data[0];
        this.form.get('echoForm.transactionID').setValue(data[0].transactionId);
        this.form.get('echoForm.patientID').setValue(data[0].patientId);

        this.form.get('colorForm.transactionID').setValue(data[0].transactionId);
        this.form.get('colorForm.patientID').setValue(data[0].patientId);

        this.form.get('patientIdRef').setValue(data[0].patientIdRef);
      }
    });

    this.TwoS.getTwod(this.tid).subscribe((echoData) => {
      
      if (this.personnels.length > 0) {
        this.form.get('echoCarID').setValue(this.personnels[0].personnelID.toString());
        this.form.get('qualityID').setValue(this.personnels[0].personnelID.toString());
        this.form.get('cardioID').setValue(this.personnels[0].personnelID.toString());
      }

      if (echoData !== null) {

        this.update = true;

        this.form.get('echoCarID').setValue(echoData.echoCarID.toString());
        this.form.get('qualityID').setValue(echoData.qualityID.toString());
        this.form.get('cardioID').setValue(echoData.cardioID.toString());

        Object.keys(echoData).forEach((key: string) => {
          let temp = "echoForm." + key;
          this.form.get(temp).setValue(echoData[key]);
        });

        this.CSF.getColorFlow(this.tid).subscribe(colorData => {
          if (colorData !== null) {

            Object.keys(colorData).forEach((key: string) => {
              let temp = "colorForm." + key;
              this.form.get(temp).setValue(colorData[key]);
            });

          }
        });
      }
    });
  }

  submit() {
    if (this.update) {
      this.updateTwod();
    } else {
      this.addTwod();
    }
  }

  addTwod() {
    let personelsTemp = ["echoCarID", "qualityID", "cardioID"];
    let echoForm = this.form.get('echoForm').value;
    let colorForm = this.form.get('colorForm').value;

    personelsTemp.forEach(element => {
      let value = this.form.get(element).value;
      echoForm[element] = value;
    });

    Object.keys(echoForm).forEach((key: string) => {
      if (echoForm[key] == "" || echoForm[key] == null) {
        if (key !== "echoID") {
          if (key !== "colorID") {
            if (key !== "userID") {
              if (key == "creationDate") {
                echoForm[key] = this.math.getDateNow();

              } else if (key == "dateUpdate") {
                echoForm[key] = "0000-00-00 00:00:00";

              } else {
                echoForm[key] = "N/A";
              }
            }
          }
        }
      }
    });

    Object.keys(colorForm).forEach((key: string) => {
      if (colorForm[key] == "" || colorForm[key] == null) {
        if (key !== "colorID") {
          if (key !== "userID") {
            if (key == "creationDate") {
              colorForm[key] = this.math.getDateNow();

            } else if (key == "dateUpdate") {
              colorForm[key] = "0000-00-00 00:00:00";

            } else {
              colorForm[key] = "N/A";
            }
          }
        }
      }
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?", Content: "2-D Echo Data will be saved!"
      },
      width: '25%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.TwoS.addTwod(echoForm).subscribe(data => {
          // console.log(data);
        });

        this.CSF.addColorFlow(colorForm).subscribe(data => {
          // console.log(data);
        });

        this.dialogRef.close("added");
      }
    });
  }

  updateTwod() {
    let personelsTemp = ["echoCarID", "qualityID", "cardioID"];
    let echoForm = this.form.get('echoForm').value;
    let colorForm = this.form.get('colorForm').value;

    personelsTemp.forEach(element => {
      let value = this.form.get(element).value;
      echoForm[element] = value;
    });

    Object.keys(echoForm).forEach((key: string) => {
      if (echoForm[key] == "" || echoForm[key] == null) {
        if (key !== "echoID") {
          if (key !== "colorID") {
            if (key !== "userID") {
              if (key == "creationDate") {
                echoForm[key] = this.math.getDateNow();

              } else if (key == "dateUpdate") {
                echoForm[key] = "0000-00-00 00:00:00";

              } else {
                echoForm[key] = "N/A";
              }
            }
          }
        }
      }
    });

    Object.keys(colorForm).forEach((key: string) => {
      if (colorForm[key] == "" || colorForm[key] == null) {
        if (key !== "colorID") {
          if (key !== "userID") {
            if (key == "creationDate") {
              colorForm[key] = this.math.getDateNow();

            } else if (key == "dateUpdate") {
              colorForm[key] = "0000-00-00 00:00:00";

            } else {
              colorForm[key] = "N/A";
            }
          }
        }
      }
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?", Content: "2-D Echo Data will be updated!"
      },
      width: '25%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.TwoS.updateTwod(echoForm).subscribe(data => {
          console.log(data);
        });

        this.CSF.updateColorFlow(colorForm).subscribe(data => {
          console.log(data);
        });

        this.dialogRef.close("updated");
      }
    });
  }
}
