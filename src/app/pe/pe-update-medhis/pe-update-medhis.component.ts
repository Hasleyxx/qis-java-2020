import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { PeService } from 'src/app/services/pe.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SnackSuccessComponent } from 'src/app/element/snack-success/snack-success.component';
import * as moment from 'moment';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-pe-update-medhis',
  templateUrl: './pe-update-medhis.component.html',
  styleUrls: ['./pe-update-medhis.component.scss']
})
export class PeUpdateMedhisComponent implements OnInit {

  public medHisID: any;
  public transactionId: any;
  public medicals = [
    { "category": "Asthma", "control": "asth" },
    { "category": "Tuberculosis", "control": "tb" },
    { "category": "Diabetes Mellitus", "control": "dia" },
    { "category": "High Blood Pressure", "control": "hb" },
    { "category": "Heart Problem", "control": "hp" },
    { "category": "Kidney Problem", "control": "kp" },
    { "category": "Abdominal/Hernia", "control": "ab" },
    { "category": "Joint/Back/Scoliosis", "control": "jbs" },
    { "category": "Psychiatric Problem", "control": "pp" },
    { "category": "Migraine/Headache", "control": "mh" },
    { "category": "Fainting/Seizures", "control": "fs" },
    { "category": "Allergies", "control": "alle" },
    { "category": "Cancer/Tumor", "control": "ct" },
    { "category": "Hepatitis", "control": "hep" },
    { "category": "STD/PLHIV", "control": "std" }
  ];
  public medicalData: any;

  public medhisForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private math: MathService,
    private peService: PeService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<PeUpdateMedhisComponent>,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.transactionId = this.data.tid;

    this.medhisForm = this.formBuilder.group({
      asth: [''],
      tb: [''],
      dia: [''],
      hb: [''],
      hp: [''],
      kp: [''],
      ab: [''],
      jbs: [''],
      pp: [''],
      mh: [''],
      fs: [''],
      alle: [''],
      ct: [''],
      hep: [''],
      std: [''],
      medHisID: [''],
      userID: [''],
      creationDate: [''],
      dateUpdate: ['']
    });

    this.medhisFormPatch();
  }

  updateMedhis() {
    this.medhisForm.patchValue({
      dateUpdate: moment().format("YYYY-MM-DD h:mm:ss")
    });
    this.peService.updateMedhis(this.medhisForm.value).subscribe(
      data => {
        if (data == 1) {
          this.dialogRef.close(this.medhisForm.value);
          this.openSnackBar("Medical history has been updated!", "close");
        }
      }
    );
  }

  medhisFormPatch() {
    this.peService.getMedhis(this.transactionId).subscribe(
      data => {
        this.medHisID = data.medHisID;
        this.medhisForm.patchValue({
          asth: data.asth,
          tb: data.tb,
          dia: data.dia,
          hb: data.hb,
          hp: data.hp,
          kp: data.kp,
          ab: data.ab,
          jbs: data.jbs,
          pp: data.pp,
          mh: data.mh,
          fs: data.fs,
          alle: data.alle,
          ct: data.ct,
          hep: data.hep,
          std: data.std,
          medHisID: this.medHisID,
          userID: parseInt(sessionStorage.getItem('token')),
          creationDate: data.creationDate,
          dateUpdate: data.dateUpdate
        });
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
