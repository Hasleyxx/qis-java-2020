import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { patient, addDocTrans } from 'src/app/services/service.interface';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ItemService } from 'src/app/services/item.service';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { DatePipe } from '@angular/common';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { CameraDialogComponent } from 'src/app/element/camera-dialog/camera-dialog.component';
import { DocTransactionService } from 'src/app/services/doc-transaction.service';

@Component({
  selector: "app-registration",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.scss"]
})
export class RegistrationComponent implements OnInit {
  public patientDatas: any = [];
  public patientId: any;

  displayedColumns = ["fullname", "contact", "gender", "action"];
  dataSource: any;

  patientForm: any;
  transaction: any;
  transForm: any;
  title: any;
  patient: patient;
  _confirm: any;
  maxDate = new Date();
  d = new DatePipe("en-US");
  patCompany: any;
  imgSrc: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 10;
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public patInfo,
    private formBuilder: FormBuilder,
    private IS: ItemService,
    private math: MathService,
    private PS: PatientService,
    private dialog: MatDialog,
    private DTS: DocTransactionService,
    private router: Router
  ) {
    this.math.navSubs("secretary");

    if (this.patInfo.lastName || this.patInfo.firstName) {
      this.IS.getCompanyByID(this.patInfo.companyID).subscribe(
        data => (this.patCompany = data[0])
      );
      this.patientForm = this.formBuilder.group({
        patientID: [this.patInfo.patientID],
        companyName: [this.patInfo.companyName],
        position: [this.patInfo.position],
        firstName: [this.patInfo.firstName, Validators.required],
        middleName: [this.patInfo.middleName, Validators.required],
        lastName: [this.patInfo.lastName, Validators.required],
        address: [this.patInfo.address, Validators.required],
        gender: [this.patInfo.gender, Validators.required],
        birthdate: [this.patInfo.birthdate, Validators.required],
        age: [this.patInfo.age, Validators.required],
        contactNo: [this.patInfo.contactNo, Validators.required],
        email: [this.patInfo.email],
        sid: [this.patInfo.sid],
        patientRef: [this.patInfo.patientRef],
        dateUpdate: [this.patInfo.dateUpdate],
        creationDate: [this.patInfo.creationDate],
        patientType: [this.patInfo.patientType],
        notes: [this.patInfo.notes]
      });

      this.title = "Edit Patient Information";
    } else {
      this.patientForm = this.formBuilder.group({
        companyName: [""],
        position: [""],
        firstName: ["", Validators.required],
        middleName: ["", Validators.required],
        lastName: ["", Validators.required],
        address: ["", Validators.required],
        gender: ["", Validators.required],
        birthdate: ["", Validators.required],
        age: ["", Validators.required],
        contactNo: ["", Validators.required],
        email: ["", Validators.email],
        sid: [""],
        patientRef: [""],
        dateUpdate: this.math.getDateNow(),
        creationDate: [this.d.transform(new Date(), "yyyy-MM-dd")],
        patientType: ["DocPatient"],
        notes: [""]
      });
      this.title = "Add Patient";
    }
  }

  ngOnInit() {
    if (!this.patInfo.patientRef) {
      //Generate random numbers and check patient ref for duplicate
      this.PS.getPatient("getPatient").subscribe(data =>
        this.getRN(this.math.patcheckRef(data))
      );
    }

    this.patientForm.get("birthdate").valueChanges.subscribe(date => {
      if (date == null) {
        this.patientForm.get("age").setValue("");
      } else {
        const bday = this.math.convertDate(date);
        const age = this.math.computeAge(bday);
        this.patientForm.get("age").setValue(age);
      }
    });
  }

  getPatient(value) {
    this.patient = value;
    if (value.image == null || value.image == "") {
      this.imgSrc = "/assets/no_image.png";
    } else {
      this.imgSrc = "/assets/patients/" + value.image;
    }
  }

  getRN(data) {
    this.patientForm.get("patientRef").setValue(data);
  }

  //get company select emited value
  getCompany(value) {
    this.patientForm.get("companyName").setValue(value.nameCompany);
  }

  savePatient() {
    this.patientForm
      .get("firstName")
      .setValue(this.patientForm.get("firstName").value.toUpperCase());
    this.patientForm
      .get("middleName")
      .setValue(this.patientForm.get("middleName").value.toUpperCase());
    this.patientForm
      .get("lastName")
      .setValue(this.patientForm.get("lastName").value.toUpperCase());
    this.patientForm
      .get("address")
      .setValue(this.patientForm.get("address").value.toUpperCase());

    if (this.patInfo.patientID) {
      //open confirm dialog
      const dialogRef = this.dialog.open(ConfirmComponent, {
        width: "20%",
        data: {
          Title: "Are you sure?",
          Content: "This Patient will be updated!!!"
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          // save new patient
          this.patientForm.patchValue({
            dateUpdate: this.math.getDateNow()
          });
          this.PS.updatePatient(this.patientForm.value).subscribe(
            (data: patient) => {
              console.log(data);
            },
            (error: any) => console.log(error)
          );
        }
      });
    } else {
      //open confirm dialog
      const dialogRef = this.dialog.open(ConfirmComponent, {
        width: "20%",
        data: {
          Title: "Are you sure?",
          Content: "New Patient will be save to database."
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          // save new patient

          const cameraDialog = this.dialog.open(CameraDialogComponent, {
            data: {
              type: "new",
              patientID: undefined,
              patInfo: this.patientForm.value
            },
            width: "100%"
          });

          cameraDialog.afterClosed().subscribe(result => {
            if (result.status == "added") {
              /* this.clear();
              this.router.navigateByUrl('element/consultation', { skipLocationChange: true }).then(() => {
                this.router.navigate(['doctor/registration']);
              }); */
              setTimeout(() => {
                this.patient = result.data;
                this.imgSrc = "/assets/patients/" + result.data.image;
              }, 1500);
            }
          });
        }
      });
    }
  }

  clear() {
    this.patientForm.reset();
  }

  openCamera(pid: number) {
    const dialogRef = this.dialog.open(CameraDialogComponent, {
      data: {
        type: "image",
        patientID: pid
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == "updated") {
        /* this.clear();
              this.router.navigateByUrl('element/consultation', { skipLocationChange: true }).then(() => {
                this.router.navigate(['doctor/registration']);
              }); */
        setTimeout(() => {
          this.patient = result.data;
          this.imgSrc = "/assets/patients/" + result.data.image;
        }, 1000);
      }
    });
  }

  addConsulation(patientID: number) {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?", Content: "Patient will be added to today's consultation?"
      },
      width: '25%'
    });

    let addConsulation: addDocTrans = {
      transactionRef: patientID + ":" + this.math.randomNumber(),
      userId: parseInt(sessionStorage.getItem("token")),
      patientId: patientID,
      totalPrice: "0",
      paidIn: 0,
      paidOut: 0,
      grandTotal: 0
    };

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.DTS.addDocTransaction(addConsulation).subscribe((data: any) => {
          if (data == 1) {
            this.math.openSnackBar("Patient has been added to today's consultation!", "close");

            this.router.navigateByUrl("element/consultation", { skipLocationChange: true }).then(() => {
              this.router.navigate(["doctor/registration"]);
            });
          }
        });
      }
    });
  }
}

