import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
  selector: 'app-snack-success',
  templateUrl: './snack-success.component.html',
  styleUrls: ['./snack-success.component.scss']
})
export class SnackSuccessComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data) { }

  ngOnInit() {
  }

}
