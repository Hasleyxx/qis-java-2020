import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef } from '@angular/material';
import { DoctorService } from 'src/app/services/doctor.service';
import { PatientRecComponent } from '../../patient-rec/patient-rec.component';
import { PatientService } from 'src/app/services/patient.service';
import { LabHistoryContainerComponent } from '../lab-history-container/lab-history-container.component';

@Component({
  selector: 'app-lab-history-dialog',
  templateUrl: './lab-history-dialog.component.html',
  styleUrls: ['./lab-history-dialog.component.scss']
})
export class LabHistoryDialogComponent implements OnInit {

  public patientId: number;
  public type: string;
  
  public patientDatas: any;

  public displayedColumns: string[] = ['transaction', 'date', 'action'];
  heldData: any[] = [];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private DS: DoctorService,
    private patientService: PatientService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<LabHistoryDialogComponent>
  ) {
    this.patientId = this.dialogData.pid;
    this.type = this.dialogData.type;
  }

  ngOnInit() {
    this.getData();

    this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => {
        this.patientDatas = data[0];
      }
    );
  }

  getData() {
    this.heldData = [];

    let urlType: any;
    if (this.type == "microscopy") {
      urlType = "getMicroPid/" + this.patientId;

    } else if (this.type == "hematology") {
      urlType = "getHemaPid/" + this.patientId;

    } else if (this.type == "chemistry") {
      urlType = "getChemPid/" + this.patientId;

    } else if (this.type == "serology") {
      urlType = "getSeroPid/" + this.patientId;
    
    } else {
      urlType = "getXrayPid/" + this.patientId;
    }

    this.DS.getLabPid(urlType).subscribe(data => {
      if (data.length > 0) {
        data.forEach(element => {
          let tempData: any = {
            transaction: element.transactionID,
            pid: element.patientID,
            date: element.creationDate
          };

          this.heldData.push(tempData);
          this.dataSource = new MatTableDataSource(this.heldData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  close() {
    this.dialogRef.close();

    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId
      }
    });
  }

  view(tid) {
    this.dialogRef.close();

    const dialogRef = this.dialog.open(LabHistoryContainerComponent, {
      data: {
        patientData: this.patientDatas,
        patientId: this.patientId,
        tid: tid,
        type: this.type
      },
      width: "80%",
      disableClose: true,
      autoFocus: false
    });
  }
}
