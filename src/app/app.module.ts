import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { ClarityModule } from '@clr/angular';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MAT_DIALOG_DATA,
} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CdkTableModule } from '@angular/cdk/table';
import { SelectItemComponent } from './element/select-item/select-item.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ItemService } from './services/item.service';
import { ErrorService } from './services/error.service';
import { TransactionComponent } from './page/transaction/transaction.component';
import { ItemBoxComponent } from './element/item-box/item-box.component';
import { Global } from './global.variable';
import { SelectPatientComponent } from './element/select-patient/select-patient.component';
import { PatientService } from './services/patient.service';
import { PatientFormComponent } from './element/patient-form/patient-form.component';
import { CompanySelectComponent } from './element/company-select/company-select.component';
import { ConfirmComponent } from './element/confirm/confirm.component';
import { CompanyFormComponent } from './element/company-form/company-form.component';
import { ReceiptComponent } from './element/receipt/receipt.component';
import { NgxPrintModule } from 'ngx-print';
import { ClickOutsideModule } from 'ng-click-outside';
import { MomentModule } from 'ngx-moment';
import { HeldTransactionComponent } from './element/held-transaction/held-transaction.component';
import { MatDialogRef} from '@angular/material/dialog';
import { TransactionListComponent } from './element/transaction-list/transaction-list.component';
import { LoadingComponent } from './element/loading/loading.component';
import { EditHMOComponent } from './element/edit-hmo/edit-hmo.component';
import { ErrorPageComponent } from './page/error-page/error-page.component';
import { HMOListComponent } from './page/hmo-list/hmo-list.component';
import { ReportListComponent } from './page/report-list/report-list.component';
import { UserService } from './services/user.service';
import { ItemListComponent } from './element/item-list/item-list.component';
import { ManagePackageComponent } from './page/manage-package/manage-package.component';
import { CreateItemComponent } from './element/create-item/create-item.component';
import { PackageListComponent } from './element/package-list/package-list.component';
import { CreatePackageComponent } from './element/create-package/create-package.component';
import { SalesPdfComponent } from './element/sales-pdf/sales-pdf.component';
import { DateTimePickerModule } from "@syncfusion/ej2-angular-calendars";
import { AuthenticationComponent } from './page/authentication/authentication.component';
import { SigninComponent } from './element/signin/signin.component';
import { SignupComponent } from './element/signup/signup.component';
import { PrivErrorComponent } from './page/priv-error/priv-error.component';
import { RefundComponent } from './page/refund/refund.component';
import { PendingAccountComponent } from './element/pending-account/pending-account.component';
import { ManageUserComponent } from './admin/manage-user/manage-user.component';
import { UserListComponent } from './admin/element/user-list/user-list.component';
import { UserFormComponent } from './admin/element/user-form/user-form.component';
import { UserAccessComponent } from './admin/element/user-access/user-access.component';
import { AccountPaymentComponent } from './admin/account-payment/account-payment.component';
import { BillingComponent } from './admin/billing/billing.component';
import { BillingPdfComponent } from './admin/element/billing-pdf/billing-pdf.component';
import { SoaListComponent } from './admin/soa-list/soa-list.component';
import { DatePipe } from '@angular/common';
import { PaymentComponent } from './admin/element/payment/payment.component';
import { MicroscopyComponent } from './laboratory/microscopy/microscopy.component';
import { MicroscopyFormComponent } from './laboratory/microscopy-form/microscopy-form.component';
import { PatientInfoComponent } from './laboratory/patient-info/patient-info.component';
import { LabResultComponent } from './laboratory/lab-result/lab-result.component';
import { MicroscopyResultComponent } from './laboratory/lab-result/microscopy-result/microscopy-result.component';
import { HematologyResultComponent } from './laboratory/lab-result/hematology-result/hematology-result.component';
import { ChemistryResultComponent } from './laboratory/lab-result/chemistry-result/chemistry-result.component';
import { SerologyyResultComponent } from './laboratory/lab-result/serologyy-result/serologyy-result.component';
import { ToxicologyResultComponent } from './laboratory/lab-result/toxicology-result/toxicology-result.component';
import { LabHeaderComponent } from './laboratory/lab-result/lab-header/lab-header.component';
import { LabFooterComponent } from './laboratory/lab-result/lab-footer/lab-footer.component';
import { HematologyComponent } from './laboratory/hematology/hematology.component';
import { HematologyFormComponent } from './laboratory/hematology-form/hematology-form.component';
import { ChemistryComponent } from './laboratory/chemistry/chemistry.component';
import { ChemistryFormComponent } from './laboratory/chemistry-form/chemistry-form.component';
import { PeSummaryComponent } from './element/pe-summary/pe-summary.component';
import { PeDataComponent } from './element/pe-data/pe-data.component';
import { RegistrationComponent } from './doctor/registration/registration.component';
import { MarkersComponent } from './imaging/markers/markers.component';
import { MarkerListComponent } from './element/marker-list/marker-list.component';
import { RegistrationFormComponent } from './element/registration-form/registration-form.component';
import { PeAddComponent } from './pe/pe-add/pe-add.component';
import { DialogConfirmComponent } from './element/dialog-confirm/dialog-confirm.component';
import { SnackSuccessComponent } from './element/snack-success/snack-success.component';
import { PeUpdateComponent } from './pe/pe-update/pe-update.component';
import { ImagingMarkersComponent } from './imaging/imaging-markers/imaging-markers.component';
import { ImagingSummaryFormComponent } from './imaging/imaging-summary-form/imaging-summary-form.component';
import { ImagingPrintMarkerComponent } from './imaging/imaging-print-marker/imaging-print-marker.component';
import { ImagingSummaryComponent } from './imaging/imaging-summary/imaging-summary.component';
import { ImagingUltrasoundReportComponent } from './imaging/imaging-ultrasound-report/imaging-ultrasound-report.component';
import { ImagingUltrasoundFormComponent } from './imaging/imaging-ultrasound-form/imaging-ultrasound-form.component';
import { ImagingPeFormComponent } from './imaging/imaging-pe-form/imaging-pe-form.component';
import { ImagingPeComponent } from './imaging/imaging-pe/imaging-pe.component';
import { XrayInventoryComponent } from './imaging/xray-inventory/xray-inventory.component';
import { ImagingXrayInventoryComponent } from './imaging/imaging-xray-inventory/imaging-xray-inventory.component';
import { FilmInventoryComponent } from './element/film-inventory/film-inventory.component';
import { StickerInventoryComponent } from './element/sticker-inventory/sticker-inventory.component';
import { ImagingItemsComponent } from './imaging/imaging-items/imaging-items.component';
import { DialogPeSummaryComponent } from './element/dialog-pe-summary/dialog-pe-summary.component';
import { PeListComponent } from './pe/pe-list/pe-list.component';
import { DialogPeDataComponent } from './element/dialog-pe-data/dialog-pe-data.component';
import { PeRecordComponent } from './pe/pe-record/pe-record.component';
import { PeCertificateComponent } from './pe/pe-certificate/pe-certificate.component';
import { DialogPeFormComponent } from './element/dialog-pe-form/dialog-pe-form.component';
import { DialogPeMedicalComponent } from './element/dialog-pe-medical/dialog-pe-medical.component';
import { PeUpdateMedhisComponent } from './pe/pe-update-medhis/pe-update-medhis.component';
import { PeUpdateVitalComponent } from './pe/pe-update-vital/pe-update-vital.component';
import { PeUpdatePeComponent } from './pe/pe-update-pe/pe-update-pe.component';
import { DialogFilmInventoryComponent } from './imaging/dialog-film-inventory/dialog-film-inventory.component';
import { SalesComponent } from './doctor/sales/sales.component';
import { SalesDialogComponent } from './doctor/sales-dialog/sales-dialog.component';
import { IndustrialComponent } from './laboratory/industrial/industrial.component';
import { IndustrialFormComponent } from './laboratory/industrial-form/industrial-form.component';
import { ViewSummaryDialogComponent } from './imaging/view-summary-dialog/view-summary-dialog.component';
import { ImagingSummaryUpdateComponent } from './imaging/imaging-summary-update/imaging-summary-update.component';
import { DisplayPeMedicalComponent } from './pe/display-pe-medical/display-pe-medical.component';
import { NursePatientSummaryComponent } from './nurse/nurse-patient-summary/nurse-patient-summary.component';
import { NurseClassificationComponent } from './nurse/nurse-classification/nurse-classification.component';
import { ImagingPrintStickerComponent } from './imaging/imaging-print-sticker/imaging-print-sticker.component';
import { PackagesComponent } from './laboratory/packages/packages.component';
import { SummaryReportComponent } from './nurse/summary-report/summary-report.component';
import { DialogSummaryReportComponent } from './nurse/dialog-summary-report/dialog-summary-report.component';
import { IndustrialSummaryComponent } from './laboratory/industrial-summary/industrial-summary.component';
import { BasicTwoComponent } from './laboratory/basic-two/basic-two.component';
import { BasicTwoFormComponent } from './laboratory/basic-two-form/basic-two-form.component';
import { BasicTwoSummaryComponent } from './laboratory/basic-two-summary/basic-two-summary.component';
import { EmailResultsComponent } from './qc/email-results/email-results.component';
import { SendDialogComponent } from './qc/send-dialog/send-dialog.component';
import { DisplayPeFormComponent } from './pe/display-pe-form/display-pe-form.component';
import { SerologyComponent } from './laboratory/serology/serology.component';
import { SerologyFormComponent } from './laboratory/serology-form/serology-form.component';
import { ToxicologyComponent } from './laboratory/toxicology/toxicology.component';
import { ToxicologyFormComponent } from './laboratory/toxicology-form/toxicology-form.component';
import { DocpatientListComponent } from './doctor/docpatient-list/docpatient-list.component';
import { PatientRecComponent } from './doctor/patient-rec/patient-rec.component';
import { SerotoxicResultComponent } from './laboratory/lab-result/serotoxic-result/serotoxic-result.component';
import { ReportMainComponent } from './admin/reports/report-main/report-main.component';
import { ChatLogComponent } from './chat/chat-log/chat-log.component';
import { PatientRecordsComponent } from './qc/patient-records/patient-records.component';
import { EcgComponent } from './qc/ecg/ecg.component';
import { EcgFormComponent } from './qc/ecg-form/ecg-form.component';
import { CertificatesComponent } from './qc/certificates/certificates.component';
import { ImportComponent } from './admin/import/import.component';
import { ChecklistDialogComponent } from './element/checklist-dialog/checklist-dialog.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { ClearanceDialogComponent } from './doctor/clearance-dialog/clearance-dialog.component';
import { ReportPdfComponent } from './qc/report-pdf/report-pdf.component';
import { PrintClearanceComponent } from './doctor/print-clearance/print-clearance.component';
import { QcComponent } from './qc/qc/qc.component';
import { QcFormComponent } from './qc/qc-form/qc-form.component';
import { PdfSummaryReportComponent } from './qc/pdf-summary-report/pdf-summary-report.component';
import { MedcertDialogComponent } from './doctor/medcert-dialog/medcert-dialog.component';
import { PrintMedcertComponent } from './doctor/print-medcert/print-medcert.component';
import { ExportAsModule } from "ngx-export-as";
import { LoadingCssComponent } from './element/loading-css/loading-css.component';
import { ClearanceHistoryComponent } from './doctor/clearance-history/clearance-history.component';
import { MedcertHistoryComponent } from './doctor/medcert-history/medcert-history.component';
import { MedicineDialogComponent } from './doctor/medicine-dialog/medicine-dialog.component';
import { SelectDocItemComponent } from './element/select-doc-item/select-doc-item.component';
import { AddMedicationDialogComponent } from './doctor/add-medication-dialog/add-medication-dialog.component';
import { RequestDialogComponent } from './doctor/request-dialog/request-dialog.component';
import { PrintMedicationComponent } from './doctor/print-medication/print-medication.component';
import { SelectItemAccountComponent } from './element/select-item-account/select-item-account.component';
import { ReleasedResultsComponent } from './element/released-results/released-results.component';
import { XraySummaryComponent } from './imaging/xray-summary/xray-summary.component';
import { XrayResultComponent } from './imaging/xray-result/xray-result.component';
import { ChangeComponent } from './element/change/change.component';
import { QueueComponent } from './page/queue/queue.component';
import { QueueDialogComponent } from './page/queue-dialog/queue-dialog.component';
import { PasswordDialogComponent } from './element/password-dialog/password-dialog.component';
import { ApprovalDialogComponent } from './element/approval-dialog/approval-dialog.component';
import { ImageDialogComponent } from './imaging/image-dialog/image-dialog.component';
import { BarcodeComponent } from "./element/barcode/barcode.component";
import { NgxBarcodeModule } from "ngx-barcode";
import { PasswordComponent } from './page/password/password.component';
import { ImageDialogPdfComponent } from './imaging/image-dialog-pdf/image-dialog-pdf.component';
import { CameraDialogComponent } from './element/camera-dialog/camera-dialog.component';
import { AccountSettingComponent } from './element/account-setting/account-setting.component';
import { LockedAccountComponent } from './element/locked-account/locked-account.component';
import { BlankPageComponent } from './doctor/blank-page/blank-page.component';
import { ImageDialogPdfPrintComponent } from './imaging/image-dialog-pdf-print/image-dialog-pdf-print.component';
import { CameraComponent } from './element/camera/camera.component';
import { WebcamModule } from 'ngx-webcam';
import { TwodEchoFormComponent } from './imaging/twod-echo-form/twod-echo-form.component';
import { TwodEchoSummaryComponent } from './imaging/twod-echo-summary/twod-echo-summary.component';
import { TwodEchoPrintComponent } from './imaging/twod-echo-print/twod-echo-print.component';
import { ConsultationComponent } from './element/consultation/consultation.component';
import { BlankPagePrintComponent } from './doctor/blank-page-print/blank-page-print.component';
import { DocHeadComponent } from './doctor/element/doc-head/doc-head.component';
import { DocFootComponent } from './doctor/element/doc-foot/doc-foot.component';
import { DocBodyComponent } from './doctor/element/doc-body/doc-body.component';
import { PrintRequestComponent } from './doctor/print-request/print-request.component';
import { EditPrintMedicationComponent } from './doctor/edit-print-medication/edit-print-medication.component';
import { EditPrintMedcertComponent } from './doctor/edit-print-medcert/edit-print-medcert.component';
import { EditPrintClearanceComponent } from './doctor/edit-print-clearance/edit-print-clearance.component';
import { EditPrintRequestComponent } from './doctor/edit-print-request/edit-print-request.component';
import { ReplacePipe } from './replace.pipe';
import { RequestHistoryComponent } from './doctor/request-history/request-history.component';
import { PersonnelComponent } from './admin/personnel/personnel.component';
import { PersonnelFormComponent } from './admin/personnel-form/personnel-form.component';
import { CovidHistoryComponent } from './doctor/covid-history/covid-history.component';
import { CovidDialogComponent } from './doctor/covid-dialog/covid-dialog.component';
import { PrintCovidComponent } from './doctor/print-covid/print-covid.component';
import { CovidPrintTwoComponent } from './doctor/covid-print-two/covid-print-two.component';
import { DocHeadEditComponent } from './doctor/element/doc-head-edit/doc-head-edit.component';
import { LabHistoryDialogComponent } from './doctor/element/lab-history-dialog/lab-history-dialog.component';
import { LabHistoryContainerComponent } from './doctor/element/lab-history-container/lab-history-container.component';
import { RequestValueComponent } from './doctor/request-value/request-value.component';
import { CatOptionDialogComponent } from './doctor/element/cat-option-dialog/cat-option-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectItemComponent,
    TransactionComponent,
    ItemBoxComponent,
    SelectPatientComponent,
    PatientFormComponent,
    CompanySelectComponent,
    ConfirmComponent,
    CompanyFormComponent,
    ReceiptComponent,
    HeldTransactionComponent,
    TransactionListComponent,
    LoadingComponent,
    EditHMOComponent,
    ErrorPageComponent,
    HMOListComponent,
    ReportListComponent,
    ItemListComponent,
    ManagePackageComponent,
    CreateItemComponent,
    PackageListComponent,
    CreatePackageComponent,
    SalesPdfComponent,
    AuthenticationComponent,
    SigninComponent,
    SignupComponent,
    PrivErrorComponent,
    RefundComponent,
    PendingAccountComponent,
    ManageUserComponent,
    UserListComponent,
    UserFormComponent,
    UserAccessComponent,
    AccountPaymentComponent,
    BillingComponent,
    BillingPdfComponent,
    SoaListComponent,
    PaymentComponent,
    MicroscopyComponent,
    MicroscopyFormComponent,
    PatientInfoComponent,
    LabResultComponent,
    MicroscopyResultComponent,
    HematologyResultComponent,
    ChemistryResultComponent,
    SerologyyResultComponent,
    ToxicologyResultComponent,
    LabHeaderComponent,
    LabFooterComponent,
    HematologyComponent,
    HematologyFormComponent,
    ChemistryComponent,
    ChemistryFormComponent,
    PeSummaryComponent,
    PeDataComponent,
    RegistrationComponent,
    MarkersComponent,
    MarkerListComponent,
    RegistrationFormComponent,
    PeAddComponent,
    DialogConfirmComponent,
    SnackSuccessComponent,
    PeUpdateComponent,
    ImagingMarkersComponent,
    ImagingSummaryFormComponent,
    ImagingPrintMarkerComponent,
    ImagingSummaryComponent,
    ImagingUltrasoundReportComponent,
    ImagingUltrasoundFormComponent,
    ImagingPeFormComponent,
    ImagingPeComponent,
    XrayInventoryComponent,
    ImagingXrayInventoryComponent,
    FilmInventoryComponent,
    StickerInventoryComponent,
    ImagingItemsComponent,
    DialogPeSummaryComponent,
    PeListComponent,
    DialogPeDataComponent,
    PeRecordComponent,
    PeCertificateComponent,
    DialogPeFormComponent,
    DialogPeMedicalComponent,
    PeUpdateMedhisComponent,
    PeUpdateVitalComponent,
    PeUpdatePeComponent,
    DialogFilmInventoryComponent,
    SalesComponent,
    SalesDialogComponent,
    IndustrialComponent,
    IndustrialFormComponent,
    ViewSummaryDialogComponent,
    ImagingSummaryUpdateComponent,
    DisplayPeMedicalComponent,
    NursePatientSummaryComponent,
    NurseClassificationComponent,
    ImagingPrintStickerComponent,
    IndustrialComponent,
    IndustrialFormComponent,
    PackagesComponent,
    SummaryReportComponent,
    DialogSummaryReportComponent,
    IndustrialSummaryComponent,
    BasicTwoComponent,
    BasicTwoFormComponent,
    BasicTwoSummaryComponent,
    EmailResultsComponent,
    SendDialogComponent,
    DisplayPeFormComponent,
    SerologyComponent,
    SerologyFormComponent,
    ToxicologyComponent,
    ToxicologyFormComponent,
    DocpatientListComponent,
    PatientRecComponent,
    SerotoxicResultComponent,
    ReportMainComponent,
    ImagingPrintStickerComponent,
    ChatLogComponent,
    PatientRecordsComponent,
    EcgComponent,
    EcgFormComponent,
    CertificatesComponent,
    ImportComponent,
    ChecklistDialogComponent,
    ClearanceDialogComponent,
    ReportPdfComponent,
    PrintClearanceComponent,
    QcComponent,
    QcFormComponent,
    PdfSummaryReportComponent,
    MedcertDialogComponent,
    PrintMedcertComponent,
    LoadingCssComponent,
    ClearanceHistoryComponent,
    MedcertHistoryComponent,
    MedicineDialogComponent,
    SelectDocItemComponent,
    AddMedicationDialogComponent,
    RequestDialogComponent,
    PrintMedicationComponent,
    SelectItemAccountComponent,
    ReleasedResultsComponent,
    XraySummaryComponent,
    XrayResultComponent,
    ChangeComponent,
    QueueComponent,
    QueueDialogComponent,
    PasswordDialogComponent,
    ApprovalDialogComponent,
    ImageDialogComponent,
    BarcodeComponent,
    PasswordComponent,
    ImageDialogPdfComponent,
    CameraDialogComponent,
    AccountSettingComponent,
    LockedAccountComponent,
    BlankPageComponent,
    ImageDialogPdfPrintComponent,
    CameraComponent,
    TwodEchoFormComponent,
    TwodEchoSummaryComponent,
    TwodEchoPrintComponent,
    ConsultationComponent,
    BlankPagePrintComponent,
    DocHeadComponent,
    DocFootComponent,
    DocBodyComponent,
    PrintRequestComponent,
    EditPrintMedicationComponent,
    EditPrintMedcertComponent,
    EditPrintClearanceComponent,
    EditPrintRequestComponent,
    ReplacePipe,
    RequestHistoryComponent,
    PersonnelComponent,
    PersonnelFormComponent,
    CovidHistoryComponent,
    CovidDialogComponent,
    PrintCovidComponent,
    CovidPrintTwoComponent,
    DocHeadEditComponent,
    LabHistoryDialogComponent,
    LabHistoryContainerComponent,
    RequestValueComponent,
    CatOptionDialogComponent
  ],
  exports: [
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    FlexLayoutModule
  ],
  imports: [
    ClarityModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    FlexLayoutModule,
    NgxMatSelectSearchModule,
    NgxPrintModule,
    ClickOutsideModule,
    MomentModule,
    DateTimePickerModule,
    MatFileUploadModule,
    ExportAsModule,
    NgxBarcodeModule,
    WebcamModule
  ],
  providers: [
    DatePipe,
    ItemService,
    ErrorService,
    PatientService,
    UserService,
    Global,
    HeldTransactionComponent,
    { provide: MatDialogRef, useValue: {} },
    PatientFormComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    EditHMOComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    CreateItemComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    CreatePackageComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    UserAccessComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    SoaListComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    PaymentComponent,
    { provide: MAT_DIALOG_DATA, useValue: {} }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    UserAccessComponent,
    PatientFormComponent,
    ConfirmComponent,
    ChangeComponent,
    CompanyFormComponent,
    HeldTransactionComponent,
    EditHMOComponent,
    CreateItemComponent,
    CreatePackageComponent,
    SoaListComponent,
    PaymentComponent,
    PeSummaryComponent,
    ImagingSummaryFormComponent,
    ImagingPrintMarkerComponent,
    ImagingUltrasoundFormComponent,
    SnackSuccessComponent,
    FilmInventoryComponent,
    StickerInventoryComponent,
    DialogConfirmComponent,
    DialogPeSummaryComponent,
    DialogPeDataComponent,
    DialogPeFormComponent,
    DialogPeMedicalComponent,
    PeUpdateMedhisComponent,
    PeUpdateVitalComponent,
    PeUpdatePeComponent,
    DialogFilmInventoryComponent,
    SalesDialogComponent,
    ImagingItemsComponent,
    ViewSummaryDialogComponent,
    ImagingSummaryUpdateComponent,
    ImagingPrintStickerComponent,
    DialogSummaryReportComponent,
    DisplayPeMedicalComponent,
    SendDialogComponent,
    PatientRecComponent,
    EcgFormComponent,
    ChecklistDialogComponent,
    ClearanceDialogComponent,
    ReportPdfComponent,
    EcgFormComponent,
    QcFormComponent,
    MedcertDialogComponent,
    LoadingCssComponent,
    ClearanceHistoryComponent,
    MedcertHistoryComponent,
    MedicineDialogComponent,
    AddMedicationDialogComponent,
    RequestDialogComponent,
    QueueDialogComponent,
    PasswordDialogComponent,
    ApprovalDialogComponent,
    ImageDialogComponent,
    CameraDialogComponent,
    AccountSettingComponent,
    BlankPageComponent,
    TwodEchoPrintComponent,
    EditPrintMedicationComponent,
    EditPrintMedcertComponent,
    EditPrintClearanceComponent,
    EditPrintRequestComponent,
    RequestHistoryComponent,
    TwodEchoFormComponent,
    PersonnelFormComponent,
    CovidHistoryComponent,
    CovidDialogComponent,
    LabHistoryDialogComponent,
    LabHistoryContainerComponent,
    CatOptionDialogComponent
  ]
})
export class AppModule {}
