import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MathService } from 'src/app/services/math.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-send-dialog',
  templateUrl: './send-dialog.component.html',
  styleUrls: ['./send-dialog.component.scss']
})
export class SendDialogComponent implements OnInit {

  public loading: boolean = false;
  public fileInput: File;
  public sendDialogForm: FormGroup;
  public files: any[] = [];

  constructor(
    private math: MathService,
    private formBuilder: FormBuilder,
    private TS: TransactionService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<SendDialogComponent>
  ) {
    this.math.navSubs("qc");
  }

  ngOnInit() {
    this.sendDialogForm = this.formBuilder.group({
      file: ['', Validators.required],
      subject: ['', Validators.required],
      recipients: this.formBuilder.array([
        this.addMoreRecipient()
      ])
    });
  }

  onChange(event) {
    for (let index = 0; index < event.target.files.length; index++) {
      const element = <File>event.target.files[index];
      this.files.push(element);
    }
    // this.fileInput = <File>event.target.files[0];
  }

  sendDialog() {
    this.loading = true;
    const formData = new FormData();

    this.files.forEach(element => {
      formData.append('file', element);
    });
    const recipients = this.sendDialogForm.get('recipients').value;
    recipients.forEach(element => {
      formData.append('email', element['recipient_email']);
    });

    formData.append('subject', this.sendDialogForm.get('subject').value);
    formData.append('body', "Dear Patient, \n\nYou may now look to the attached file of your result in PDF format.\n\nFor any question/classification, please contact us at 0917-535-9471 for Globe and 0925-577-8378 for Smart or send us an email at questphil.corpresult@gmail.com\n\nThank you for your concern!\n\nYours truly, \n\nQuest Diagnostics Team.");

    this.TS.sendEmail(formData).subscribe(data => {
      this.dialogRef.close("close");
    });
  }

  addMore(): void {
    (<FormArray>this.sendDialogForm.get('recipients')).push(this.addMoreRecipient());
  }

  addMoreRecipient(): FormGroup {
    return this.formBuilder.group({
      recipient_email: ['', Validators.required]
    });
  }

  removeRecipient(index) {
    (<FormArray>this.sendDialogForm.get('recipients')).removeAt(index);
  }

}
