import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { patient, transaction, medtech, microscopy, hematology, chemistry, serology, toxicology } from 'src/app/services/service.interface';
import { LaboratoryService } from 'src/app/services/laboratory.service';

@Component({
  selector: 'lab-result',
  templateUrl: './lab-result.component.html',
  styleUrls: ['./lab-result.component.scss']
})
export class LabResultComponent implements OnInit {
  id: string[];
  idDetails: Promise<any>[];
  patient: patient;
  transaction: transaction;
  path: medtech;
  qc: medtech;
  mt: medtech;
  microscopy: microscopy;
  hematology: hematology;
  chemistry1: chemistry;
  chemistry2: chemistry;
  serology: serology;
  toxicology: toxicology;

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private LS: LaboratoryService
  ) {
    this.id = route.snapshot.params['ids'].split(',');
  }

  ngOnInit() {
    this.idDetails = this.id
      .map(id => this.getInvoiceDetails(id));

    Promise.all(this.idDetails)
      .then(() => {
        this.math.onLabReady();
      });
    if (this.id[0]) {
      this.TS.getOneTrans("getTransaction/" + this.id[0]).subscribe(
        trans => {
          this.transaction = trans[0];
          this.PS.getOnePatient("getPatient/" + trans[0].patientId).subscribe(
            pat => {
              this.patient = pat[0];

              let copy = this.id[0] + "_" + pat[0].fullName;
              // Set TID + name to Clipboard
              const selBox = document.createElement('textarea');
              selBox.style.position = 'fixed';
              selBox.style.left = '0';
              selBox.style.top = '0';
              selBox.style.opacity = '0';
              selBox.value = copy;
              document.body.appendChild(selBox);
              selBox.focus();
              selBox.select();
              document.execCommand('copy');
              document.body.removeChild(selBox);
            }
          )
          if (this.id[1] == "microscopy") {
            this.LS.getMicroscopy(this.id[0]).subscribe(
              mic => {
                this.microscopy = mic[0];

                // medtech
                this.LS.getPersonnel(mic[0].medID).subscribe(
                  med => {
                    this.mt = med[0];
                  }
                )
                // Quality Control
                this.LS.getPersonnel(mic[0].qualityID).subscribe(
                  med => {
                    this.qc = med[0];
                  }
                )
                // pathology
                this.LS.getPersonnel(mic[0].pathID).subscribe(
                  med => {
                    this.path = med[0];
                  }
                )

              }
            )
          }
          if (this.id[1] == "hematology") {
            this.LS.getHematology(this.id[0]).subscribe(
              hema => {
                this.hematology = hema[0];

                // medtech
                this.LS.getPersonnel(hema[0].medID).subscribe(
                  med => {
                    this.mt = med[0];
                  }
                )
                // Quality Control
                this.LS.getPersonnel(hema[0].qualityID).subscribe(
                  med => {
                    this.qc = med[0];
                  }
                )
                // pathology
                this.LS.getPersonnel(hema[0].pathID).subscribe(
                  med => {
                    this.path = med[0];
                  }
                )

              }
            )
          }
          if (this.id[1] == "chemistry") {
            this.LS.getChemistry(this.id[0]).subscribe(
              chem => {
                // this.chemistry1 = chem[0];
                // console.log(this.chemistry);
                this.chemLength(chem[0]);

                // medtech
                this.LS.getPersonnel(chem[0].medID).subscribe(
                  med => {
                    this.mt = med[0];
                  }
                )
                // Quality Control
                this.LS.getPersonnel(chem[0].qualityID).subscribe(
                  med => {
                    this.qc = med[0];
                  }
                )
                // pathology
                this.LS.getPersonnel(chem[0].pathID).subscribe(
                  med => {
                    this.path = med[0];
                  }
                )

              }
            )
          }
          if (this.id[1] == "serology" || this.id[1] == "toxicology") {
            this.LS.getSerology(this.id[0]).subscribe(
              sero => {
                if(sero.length > 0) {
                  this.serology = sero[0];

                  // medtech
                  this.LS.getPersonnel(sero[0].medID).subscribe(
                    med => {
                      this.mt = med[0];
                    }
                  )
                  // Quality Control
                  this.LS.getPersonnel(sero[0].qualityID).subscribe(
                    med => {
                      this.qc = med[0];
                    }
                  )
                  // pathology
                  this.LS.getPersonnel(sero[0].pathID).subscribe(
                    med => {
                      this.path = med[0];
                    }
                  )
                }

              }
            )

            this.LS.getToxicology(this.id[0]).subscribe(
              toxic => {
                if (toxic.length > 0) {
                  this.toxicology = toxic[0];

                  // medtech
                  this.LS.getPersonnel(toxic[0].medID).subscribe(
                    med => {
                      this.mt = med[0];
                    }
                  )
                  // Quality Control
                  this.LS.getPersonnel(toxic[0].qualityID).subscribe(
                    med => {
                      this.qc = med[0];
                    }
                  )
                  // pathology
                  this.LS.getPersonnel(toxic[0].pathID).subscribe(
                    med => {
                      this.path = med[0];
                    }
                  )
                }
              }
            );
          }
        }
      )
    }

  }

  getInvoiceDetails(invoiceId) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

  chemLength(chem: chemistry) {
    let count = 0;
    this.chemistry1 = {
      chemID  : null,
      patientID : null,
      transactionID : null,
      pathID : null,
      medID : null,
      qualityID : null,
      creationDate : null,
      dateUpdate : null,

      chol : null,
      cholcon : null,
      trig : null,
      trigcon : null,
      hdl : null,
      hdlcon : null,
      ldl : null,
      ldlcon : null,
      ch : null,
      vldl : null,

      na : null,
      k : null,
      cl : null,

      alt : null,
      ast : null,
      amylase : null,
      lipase : null,

      ogtt1 : null,
      ogtt1con : null,
      ogtt2 : null,
      ogtt2con : null,

      cpkmb : null,
      totalCPK : null,
      cpkmm : null,

      biltotal : null,
      bildirect : null,
      bilindirect : null,

      agratio : null,
      buncon : null,
      bun : null,
      fbs : null,
      fbscon : null,

      crea : null,
      creacon : null,

      bua : null,

      buacon : null,

      hb : null,

      alp : null,
      psa : null,

      calcium : null,
      albumin : null,
      ldh : null,
      rbs : null,
      globulin : null,
      ggtp : null,
      ionCalcium : null,
      protein : null,
      ogct : null,
      ogctcon : null,
      rbscon : null,
      magnesium : null,
      inPhos : null,

      chemNotes: null
    }

    for (var i in this.chemistry1) {
      if (i == "chemID" || i == "transactionID" || i == "patientID" || i == "medID"
        || i == "qualityID" || i == "creationDate" || i == "dateUpdate" || i == "userID") {

      } else {
        if (count <= 24 && this.math.hideRes(chem[i])) {
          this.chemistry1[i] = chem[i];
          count++;
        } else {
          // console.log(count);

          // Previous: 26
          if(count == 25){
            // console.log(count);
            this.chemistry2 = {
              chemID  : null, patientID : null, transactionID : null, pathID : null, medID : null, qualityID : null,
              creationDate : null, dateUpdate : null, chol : null, cholcon : null, trig : null, trigcon : null,
              hdl : null, hdlcon : null, ldl : null, ldlcon : null, ch : null, vldl : null,  na : null, k : null,
              cl : null, alt : null, ast : null, amylase : null, lipase : null, ogtt1 : null, ogtt1con : null,
              ogtt2 : null, ogtt2con : null, cpkmb : null, totalCPK : null, cpkmm : null, biltotal : null,
              bildirect : null, bilindirect : null, agratio : null, buncon : null, bun : null, fbs : null,
              fbscon : null, crea : null, creacon : null, bua : null, buacon : null, hb : null, alp : null,
              psa : null, calcium : null, albumin : null, ldh : null, rbs : null, globulin : null, ggtp : null,
              ionCalcium : null, protein : null, ogct : null, ogctcon : null, rbscon : null, magnesium : null,
              inPhos : null, chemNotes : null
            }
          }

          if (this.math.hideRes(chem[i])) {
            this.chemistry2[i] = chem[i];
            count++;
          }
        }

      }
    }
    //console.log(this.chemistry2);
  }
}
