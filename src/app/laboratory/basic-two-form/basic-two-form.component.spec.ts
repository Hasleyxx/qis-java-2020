import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicTwoFormComponent } from './basic-two-form.component';

describe('BasicTwoFormComponent', () => {
  let component: BasicTwoFormComponent;
  let fixture: ComponentFixture<BasicTwoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicTwoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicTwoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
