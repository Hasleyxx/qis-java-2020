import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PeService } from 'src/app/services/pe.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { medtech } from 'src/app/services/service.interface';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: 'app-qc-form',
  templateUrl: './qc-form.component.html',
  styleUrls: ['./qc-form.component.scss']
})
export class QcFormComponent implements OnInit {

  public type: any;
  public typeLabel: any;
  public patientId: any;
  public transactionId: any;
  public patientDatas: any = [];
  public hemaDatas: any = [];
  public seroDatas: any = [];
  public microDatas: any = [];
  public toxiDatas: any = [];
  public classification: any = [];
  public pes: any = [];

  public class: any;
  public classColor: any = 'text-dark';
  public notes: any;
  public cbcot: any = 'text-dark';
  public hbsag: any;
  public pt: any;
  public meth: any;
  public tetra: any;
  public urinotes: any;

  public medical: any = [];
  public quality: any = [];
  public path: any = [];
  public rad: any = [];

  public qcForm: FormGroup;
  medtech: medtech[];

  public date = moment().format("YYYY-MM-DD");

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private peService: PeService,
    private labService: LaboratoryService,
    private XS: XrayService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<QcFormComponent>
  ) {
    this.transactionId = this.data.tid;
    this.type = this.data.type;

    if (this.type == "add") {
      this.typeLabel = "Add data";
    } else {
      this.typeLabel = "Update data";
    }
  }

  ngOnInit() {
    this.getData();

    this.qcForm = this.formBuilder.group({
      transactionID: [''],
      patientID: [''],
      medicalClass: [''],
      notes: [''],
      qc: ['-', Validators.required],
      qclicense: [''],
      creationDate: [''],
      classId: ['']
    });
  }

  getData() {
    // Lab Personels
    this.labService.getMedtech().subscribe(
      medtech => {
        this.medtech = medtech;
      }
    )

    // Transaction
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.patientId = data[0]['patientId'];
        this.qcForm.patchValue({
          transactionID: this.transactionId,
          patientID: data[0]['patientId']
        });
        // Patient
        this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );

    // PE
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        this.pes = data;
      }
    );

    // Hematology
    this.labService.getHematology(this.transactionId).subscribe(
      data => {
        if (data.length > 0) {
          this.hemaDatas = data[0];
          if (data[0].cbcot !== "NORMAL") {
            this.cbcot = "text-danger";
          }
        }
      }
    );

    // Microscopy
    this.labService.getMicroscopy(this.transactionId).subscribe(
      data => {
        this.microDatas = data[0];

        if (data[0] !== undefined) {
          if (this.microDatas.pregTest == 'POSITIVE') {
            this.pt = 'text-danger';
          }

          if (this.microDatas.uriOt != 'NORMAL') {
            this.urinotes = 'text-danger';
          }

          this.labService.getPersonnel(this.microDatas.medID).subscribe(
            data => {
              this.medical.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
            }
          );
          this.labService.getPersonnel(this.microDatas.qualityID).subscribe(
            data => {
              this.quality.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
            }
          );
          this.labService.getPersonnel(this.microDatas.pathID).subscribe(
            data => {
              this.path.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
            }
          );

        } else {
          this.pt = 'text-dark';
          this.urinotes = 'text-dark';
          this.medical.push({
            'name': '-',
            'license': '-'
          });

          this.quality.push({
            'name': '-',
            'license': '-'
          });

          this.path.push({
            'name': '-',
            'license': '-'
          });
        }
      }
    );

    // Serology
    this.labService.getSerology(this.transactionId).subscribe(
      data => {
        this.seroDatas = data[0];

        if (data[0] !== undefined) {
          if (this.seroDatas.hbsAG == 'REACTIVE') {
            this.hbsag = 'text-danger';
          }

        } else {
          this.hbsag = 'text-dark';
        }
      }
    );

    // Toxicology
    this.labService.getToxicology(this.transactionId).subscribe(
      data => {
        this.toxiDatas = data[0];

        if (data[0] !== undefined) {
          if (this.toxiDatas.meth == 'POSITIVE') {
            this.meth = 'text-danger';
          }

          if (this.toxiDatas.tetra == 'POSITIVE') {
            this.tetra = 'text-danger';
          }

        } else {
          this.meth = 'text-dark';
          this.tetra = 'text-dark';
        }
      }
    );

    // Class
    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        this.classification = data;

        if (data !== null) {
          console.log(data);
          if (this.classification.medicalClass == 'CLASS A - Physically Fit') {
            this.class = 'Class A - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency') {
            this.class = 'Class B - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.class = 'CLASS C - With abnormal findings generally not accepted for employment.';
          }
          else if (this.classification.medicalClass == 'CLASS D - Unemployable') {
            this.class = 'Class D - UNEMPLOYABLE';
          }
          else {
            this.class = 'PENDING';
          }

          if (this.classification.notes != 'NORMAL' && this.classification.notes != '') {
            this.notes = 'text-danger';
          }

          if (this.class == 'Class D - UNEMPLOYABLE' || this.class == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.classColor = 'text-danger';
          }
          this.qcForm.patchValue({
            medicalClass: this.classification.medicalClass,
            notes: this.classification.notes,
            qc: this.classification.qc,
            qclicense: this.classification.qclicense,
            creationDate: this.classification.creationDate,
            classId: this.classification.classID
          });
        } else {
          this.class = 'PENDING';
          this.notes = 'text-dark';
          this.classColor = 'text-dark';
        }
      }
    );

    // Radiology
    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        if (data == null) {
          this.rad.push({
            'qa': '-',
            'radiologist': '-'
          });

        } else {
          this.rad = data;
        }

      }
    );

  }

  qc() {
    if (this.type == "add") {
      this.qcForm.patchValue({
        medicalClass: "",
        notes: "",
        qclicense: 0,
        creationDate: moment().format("YYYY-MM-DD h:mm:ss")
      });
      this.labService.addClass(this.qcForm.value).subscribe(
        data => {
          if (data == 1) {
            this.dialogRef.close("added");
          } else {
            this.dialogRef.close("error");
          }
        }
      );
    } else {
      this.labService.updateClass(this.qcForm.value).subscribe(
        data => {
          if (data == 1) {
            this.dialogRef.close("updated");
          } else {
            this.dialogRef.close("error");
          }
        }
      );
    }

  }

}
