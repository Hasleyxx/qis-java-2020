import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingCssComponent } from './loading-css.component';

describe('LoadingCssComponent', () => {
  let component: LoadingCssComponent;
  let fixture: ComponentFixture<LoadingCssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingCssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingCssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
