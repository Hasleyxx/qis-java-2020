import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { navList } from './service.interface';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { PasswordService } from '../password.service';
import { DoctorService } from './doctor.service';

@Injectable({
  providedIn: "root"
})
export class MathService {
  isPrinting = false;
  userID: number = parseInt(sessionStorage.getItem("token"));

  public cats: any[] = ["HEMATOLOGY", "MICROSCOPY", "CHEMISTRY", "IMAGING"];

  public hemas: any[] = ["COMPLETE BLOOD COUNT", "WBC & DIFF. COUNT", "HB & HCT", "PLATELET COUNT", "ESR", "PT", "APTT", "MALARIAL SMEAR", "PERIPHERAL BLOOD SMEAR", "CT/BT", "BLOOD TYPING"];

  public micros: any[] = ["URINALYSIS", "MICRAL TEST", "PREGNANCY TEST", "SEMEN ANALYSIS", "FECALYSIS", "OCCULT BLOOD", "AMOEBA CONGC. TECH"];

  public chems: any[] = ["FBS", "2HR POST PRANDIAL", "OGCT", "OGTT", "HBA1C", "LIPID PROFILE", "CHOLESTEROL", "TRIGLYCERIDES", "HDL", "BUN", "CREATININE", "URIC ACID", "TOTAL PROTEIN", "ALBUMIN", "TPAG RATIO", "B1B2", "SGPT (ALT)", "SGOT(AST)", "GGT", "ALKALINE PHOSPHATASE", "ACP", "PROSTATIC ACP", "CK TOTAL", "CK MB", "CKMM", "LDH", "SODIUM (NA)", "POTASSIUM (K)", "CHLORIDE", "TOTAL CALCIUM", "IONIZED CALCIUM", "INORGANIC PHOS", "MAGNESIUM", "IRON\/TIBC", "CO2", "AMYLASE", "LIPASE", "AMMONIA", "HIGH SENSITIVE CRP"];
        
  public imagings: any[] = ["X-RAY", "ULTRASOUND", "2D ECHO", "CT SCAN", "ECG"];

  constructor(
    private router: Router,
    public _snackBar: MatSnackBar,
    private PS: PasswordService,
    private DocS: DoctorService
  ) {}

  convertDate(date) {
    var dateString = new Date(date.getTime() - date.getTimezoneOffset() * 60000)
      .toISOString()
      .split("T")[0];
    return dateString;
  }

  randomNumber(min: number = 0, max: number = 100000000): number {
    var rn = Math.floor(Math.random() * (max - min)) + min;
    return rn;
  }
  patcheckRef(data) {
    let pat = data;
    let rn;
    do {
      rn = this.randomNumber();
      var found = pat.find(data => data.patientRef == rn);
    } while (found !== undefined);
    return rn;
  }
  transcheckRef(data) {
    let trans = data;
    let rn;
    do {
      rn = this.randomNumber();
      var found = trans.find(data => data.transactionRef == rn);
    } while (found !== undefined);
    return rn;
  }
  dataRef(pes) {
    let pe = pes;
    let rn;
    do {
      rn = this.randomNumber();
      var found = pe.find(data => data.dataRef == rn);
    } while (found !== undefined);
    return rn;
  }
  getDateNow() {
    let d = new DatePipe("en-US");
    return d.transform(new Date(), "yyyy-MM-dd H:mm:ss");
  }

  //2Decho
  printEcho(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          echo: ["print", documentData.join()]
        }
      }
    ]);
  }


  printDocument(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          print: ["print", documentData.join()]
        }
      }
    ]);
  }
  printReport(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          salesReport: ["print", documentData.join()]
        }
      }
    ]);
  }

  printBilling(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          billing: ["print", documentData.join()]
        }
      }
    ]);
  }

  printLab(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          lab: ["print", documentData.join()]
        }
      }
    ]);
  }

  printBasic(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          basicTwo: ["print", documentData.join()]
        }
      }
    ]);
  }

  printMarker(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          imaging: ["print", documentData.join()]
        }
      }
    ]);
  }

  printSticker(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          imagingSticker: ["print", documentData.join()]
        }
      }
    ]);
  }

  printPeForm(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          peForm: ["print", documentData.join()]
        }
      }
    ]);
  }

  printPeMedical(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          peMedical: ["print", documentData.join()]
        }
      }
    ]);
  }

  printReportPdf(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          reportPdf: ["print", documentData.join()]
        }
      }
    ]);
  }

  printEcgForm(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          ecgForm: ["print", documentData.join()]
        }
      }
    ]);
  }

  printXrayResult(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          xrayResult: ["print", documentData.join()]
        }
      }
    ]);
  }

  printPdfSummaryReport(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          pdfSummaryReport: ["print", documentData.join()]
        }
      }
    ]);
  }

  printBarcode(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          barCode: ["print", documentData.join()]
        }
      }
    ]);
  }

  printXrayScan(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          xrayScan: ["print", documentData.join()]
        }
      }
    ]);
  }

  printXrayScanPrint(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          xrayScanPrint: ["print", documentData.join()]
        }
      }
    ]);
  }

  printDoc(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      "/",
      {
        outlets: {
          doc: ["print", documentData.join()]
        }
      }
    ]);
  }

  onEchoRready() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { echo: null } }]);
    });
  }

  onDataReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { print: null } }]);
    });
  }
  onReportReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { salesReport: null } }]);
    });
  }

  onBillingReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { billing: null } }]);
    });
  }

  onLabReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { lab: null } }]);
    });
  }

  onMarkerReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { imaging: null } }]);
    });
  }

  onStickerReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { imagingSticker: null } }]);
    });
  }

  onPeFormReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { peForm: null } }]);
    });
  }

  onPeMedicalReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { peMedical: null } }]);
    });
  }

  onReportPdfReady() {
    setTimeout(() => {
      // window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { reportPdf: null } }]);
    });
  }

  onBasictwoReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { basicTwo: null } }]);
    });
  }

  onEcgFormReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { ecgForm: null } }]);
    });
  }

  onPdfSummaryReportReady() {
    setTimeout(() => {
      // window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { pdfSummaryReport: null } }]);
    });
  }

  onXrayResultReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { xrayResult: null } }]);
    });
  }

  onBarcodeReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { barCode: null } }]);
    });
  }

  onXrayScanReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { xrayScan: null } }]);
    });
  }

  onXrayScanPrintReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { xrayScanPrint: null } }]);
    });
  }

  onDocReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { doc: null } }]);
    });
  }

  getReady(name, data, header) {
    new Angular5Csv(data, name, {
      headers: header
    });
  }

  protected navObs = new Subject<navList[]>();

  changeEmitted$ = this.navObs.asObservable();

  public navSubs(change: any) {
    let list: navList[];
    if (change == "cashier") {
      list = [
        { name: "Transact", route: "cashier/transact", icon: "store" },
        {
          name: "Transaction List",
          route: "cashier/transactions",
          icon: "shop"
        },
        {
          name: "HMO Transaction",
          route: "cashier/hmo",
          icon: "shopping_cart"
        },
        { name: "Sales Report", route: "cashier/sales", icon: "assessment" },
        {
          name: "Manage Items",
          route: "cashier/manage-items",
          icon: "settings_applications"
        },
        {
          name: "Refund",
          route: "cashier/refund-exchange",
          icon: "compare_arrows"
        }
        // {name: "", route: "", icon: ""},
      ];
    }
    if (change == "admin") {
      list = [
        { name: "Manage User",route: "admin/manage-user", icon: "assignment_ind"},
        { name: "Personels", route: "admin/personel", icon: "supervised_user_circle"},
        { name: "Billing", route: "admin/billing", icon: "library_add" },
        { name: "Account Payment", route: "admin/account-payment", icon: "library_books"},
        { name: "Reports", route: "admin/reports", icon: "timeline" },
        { name: "Imports", route: "admin/import", icon: "publish" },
        { name: "Passwords", route: "admin/passwords", icon: "lock"}
      ];
    }
    if (change == "lab") {
      list = [
        {
          name: "Clinical Microscopy",
          route: "laboratory/microscopy",
          icon: "visibility"
        },
        {
          name: "Hematology",
          route: "laboratory/hematology",
          icon: "linear_scale"
        },
        { name: "Chemistry", route: "laboratory/chemistry", icon: "share" },
        {
          name: "Serology",
          route: "laboratory/serology",
          icon: "scatter_plot"
        },
        { name: "Toxicology", route: "laboratory/toxicology", icon: "grain" },
        {
          name: "Industrial",
          route: "laboratory/industrial",
          icon: "timeline"
        },
        {
          name: "Packages",
          route: "laboratory/packages",
          icon: "drag_indicator"
        }
      ];
    }
    if (change == "imaging") {
      list = [
        { name: "Markers", route: "imaging/markers", icon: "print" },
        {
          name: "X-ray Inventory",
          route: "imaging/inventory",
          icon: "perm_data_setting"
        },
        {
          name: "X-ray Summary",
          route: "imaging/xray-summary",
          icon: "theaters"
        },
        {
          name: "Radiology Report",
          route: "imaging/summary",
          icon: "radio_button_checked"
        },
        {
          name: "2DEcho Report",
          route: "imaging/2decho-summary",
          icon: "album"
        },
        {
          name: "Ultrasound Report",
          route: "imaging/ultrasound-report",
          icon: "airline_seat_flat"
        },
        {
          name: "Certificates",
          route: "imaging/certificate",
          icon: "recent_actors"
        }
        //{name: "Med Cert", route: "imaging/", icon: "recent_actors"},
      ];
    }

    if (change == "qc") {
      list = [
        { name: "Email", route: "qc/email-results", icon: "mail" },
        {
          name: "APE Summary",
          route: "qc/summary-report",
          icon: "description"
        },
        {
          name: "Patient Records",
          route: "qc/patient-records",
          icon: "assignment_ind"
        },
        { name: "QC", route: "qc/qc", icon: "dvr" },
        { name: "ECG", route: "qc/ecg", icon: "nfc" },
        {
          name: "Certificates",
          route: "qc/certificates",
          icon: "recent_actors"
        }
      ];
    }

    if (change == "pe") {
      list = [
        { name: "Summary", route: "pe/summary", icon: "library_books" },
        { name: "MH & VS", route: "pe/list", icon: "list" },
        { name: "Patient Record", route: "pe/record", icon: "people" },
        { name: "Certificate", route: "pe/certificate", icon: "recent_actors" }
      ];
    }
    if (change == "nurse") {
      list = [
        {
          name: "Classification",
          route: "nurse/classification",
          icon: "monetization_on"
        },
        {
          name: "Summary",
          route: "nurse/patient-summary",
          icon: "library_books"
        },
        {
          name: "Certificate",
          route: "nurse/certificate",
          icon: "recent_actors"
        }
      ];
    }
    if (change == "inventory") {
      list = [
        {
          name: "Results",
          route: "element/released-results",
          icon: "monetization_on"
        }
      ];
    }
    if (change == "secretary") {
      list = [
        { name: "Register", route: "doctor/registration", icon: "person_add" },
        { name: "Sales", route: "doctor/sales", icon: "monetization_on" },
        { name: "Request Value", route: "doctor/request-value", icon: "layers" }
      ];
    }
    if (change == "doctor") {
      list = [
        { name: "Patients", route: "doctor/docpatient-list", icon: "person" }
      ];
    }
    this.navVis.next(true);
    this.navObs.next(list);
  }

  protected navVis = new Subject<boolean>();

  changeVis = this.navVis.asObservable();

  public navVisibility(change: boolean) {
    this.navVis.next(change);
  }

  computeDisc(price: number, disc: number, qty: number) {
    let discount: number = disc / 100;
    discount = price * discount;
    let total = price - discount;
    total = total * qty;
    return total.toFixed(2);
  }

  dateNow() {
    let d = new DatePipe("en-US");
    let datenow = d.transform(new Date(), "yyyy-MM-dd HH:mm:ss");
    return datenow;
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 4000
    });
  }

  computeAge(dob: any): number {
    return moment().diff(dob, "years");
  }

  hideRes(res): boolean {
    if (
      res == "" ||
      res == "N/A" ||
      res == "NaN" ||
      res == "null" ||
      res == null ||
      res == undefined ||
      res == "-"
    ) {
      return false;
    } else {
      return true;
    }
    // if (res == "" || res == "null" || res == null || res == undefined) {
    //   return false;
    // } else {
    //   return true;
    // }
  }

  checkDefaultValues() {
    this.PS.getValidity().subscribe(validityData => {
      if (validityData == null) {
        let temp = { "validity": "One (1) month"};

        this.PS.addValidity(temp).subscribe(data => {
          console.log(data);
        });
      }
    });

    this.DocS.getDocRequestCategory().subscribe(categoryData => {
      if (categoryData.length < 1) {
        this.cats.forEach((element, index) => {
          let temp = { "category": element };

          this.DocS.addDocRequestCategory(temp).subscribe(data => {
          });
        });
      }
    });

    setTimeout(() => {
      this.addCatOption();
    }, 1500);
  }

  addCatOption() {
    setTimeout(() => {
      this.DocS.getDocRequestCategory().subscribe(catsData => {
        if (catsData.length > 0) {
          catsData.forEach(catsElement => {
            let tempId = catsElement.docReqCatId;
            let tempCategory = catsElement.category;

            this.DocS.getDocRequestOptionId(tempId).subscribe(catsOptionData => {
              if (catsOptionData.length < 1) {
                if (tempCategory == this.cats[0]) {
                  this.addCatOptionParam(this.hemas, tempId);
                } else if (tempCategory == this.cats[1]) {
                  this.addCatOptionParam(this.micros, tempId);
                } else if (tempCategory == this.cats[2]) {
                  this.addCatOptionParam(this.chems, tempId);
                } else if (tempCategory == this.cats[3]) {
                  this.addCatOptionParam(this.imagings, tempId);
                }
              }
            });
          });
        }
      });
    });
  }

  addCatOptionParam(data: any[], id) {
    setTimeout(() => {
      let temp = {
        option: "",
        category: ""
      };
      data.forEach(elementHema => {
        temp.option = elementHema;
        temp.category = id;

        this.DocS.addDocRequestOption(temp).subscribe(addData => {
        });
      });
    });
  }
}
