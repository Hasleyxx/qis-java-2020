import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { PersonnelService } from 'src/app/services/personnel.service';
import { MathService } from 'src/app/services/math.service';
import { PersonnelFormComponent } from '../personnel-form/personnel-form.component';

export interface personnelTemp {
  no: number,
  name: string,
  license: string,
  personnelID: number
}

@Component({
  selector: 'app-personnel',
  templateUrl: './personnel.component.html',
  styleUrls: ['./personnel.component.scss']
})
export class PersonnelComponent implements OnInit {

  public type: string = "all";
  public typeFinal: string = "";
  public types = [
    {"label" : "LABORATORY", "value": "lab"},
    {"label" : "IMAGING", "value": "imaging"},
  ];
  personnelTemp: personnelTemp[] = [];

  dataSource: MatTableDataSource<personnelTemp>;
  displayedColumns: string[] = ["no", "name", "license", "action"];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private PersonnS: PersonnelService,
    private dialog: MatDialog,
    private math: MathService
  ) {
    this.math.navSubs("admin");
  }

  ngOnInit() {
    this.getData(this.type);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData(type: string) {
    this.personnelTemp = [];
    let url: string = undefined;
    this.type = type;

    if (type == "all") {
      url = "getPersonnel";
      this.typeFinal = "all";

    } else if (type == "lab") {
      url = "getPersonnelDep/LAB";
      this.typeFinal = "LABORATORY";

    } else {
      this.typeFinal = "IMAGING";
      url = "getPersonnelDep/IMAGING";
    }

    this.PersonnS.getPersonnelUrl(url).subscribe(data => {
      if (data.length > 0) {
        data.forEach((element, index) => {
          let temp = {
            "no": index + 1,
            "name": element.lastName + ", " + element.firstName + " " + element.middleName + " - " + element.positionEXT,
            "license": element.licenseNO,
            "personnelID": element.personnelID
          };

          this.personnelTemp.push(temp);
          this.dataSource = new MatTableDataSource(this.personnelTemp);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      } else {
        this.dataSource = new MatTableDataSource([]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  selectDep(value) {
    this.getData(value);
  }

  addPersonnel() {
    const dialogRef = this.dialog.open(PersonnelFormComponent, {
      data: {
        personnelID: 0
      },
      width: '40%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == "added") {
        this.math.openSnackBar("Added new Personnel.", "close");
        this.getData(result.dept);
      }
    });
  }
  updatePersonnel(personnelID: number) {
    const dialogRef = this.dialog.open(PersonnelFormComponent, {
      data: {
        personnelID: personnelID
      },
      width: '40%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == "updated") {
        this.math.openSnackBar("Personnel has been updated.", "close");
        this.getData(result.dept);
      }
    });
  }
}
