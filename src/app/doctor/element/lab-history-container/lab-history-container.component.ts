import { Component, OnInit, Inject } from '@angular/core';
import { DoctorService } from 'src/app/services/doctor.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { microscopy, hematology, chemistry, serology, toxicology } from 'src/app/services/service.interface';
import { MathService } from 'src/app/services/math.service';
import { LabHistoryDialogComponent } from '../lab-history-dialog/lab-history-dialog.component';

@Component({
  selector: 'app-lab-history-container',
  templateUrl: './lab-history-container.component.html',
  styleUrls: ['./lab-history-container.component.scss']
})
export class LabHistoryContainerComponent implements OnInit {

  public patientData: any;
  public patientId: number;
  public transactionData: any;
  public transactionId: any;
  public type: string;

  microscopy: microscopy;
  hematology: hematology;
  chemistry1: chemistry;
  chemistry2: chemistry;
  serology: serology;
  toxicology: toxicology;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private math: MathService,
    private LS: LaboratoryService,
    private TS: TransactionService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<LabHistoryContainerComponent>
  ) {
    console.log(this.dialogData);
    this.patientData = this.dialogData.patientData;
    this.patientId = this.dialogData.patientId;
    this.transactionId = this.dialogData.tid;
    this.type = this.dialogData.type;
  }

  ngOnInit() {
    this.TS.getOneTrans("getTransaction/" + this.transactionId).subscribe(transData => {
      this.transactionData = transData[0];

      if (this.type == "microscopy") {
        this.LS.getMicroscopy(this.transactionId).subscribe(
          mic => {
            this.microscopy = mic[0];
          }
        )
      }
      if (this.type == "hematology") {
        this.LS.getHematology(this.transactionId).subscribe(
          hema => {
            this.hematology = hema[0];
          }
        )
      }
      if (this.type == "chemistry") {
        this.LS.getChemistry(this.transactionId).subscribe(
          chem => {
            this.chemLength(chem[0]);
          }
        )
      }
      if (this.type == "serology" || this.type == "toxicology") {
        this.LS.getSerology(this.transactionId).subscribe(
          sero => {
            if (sero.length > 0) {
              this.serology = sero[0];
            }
          }
        )

        this.LS.getToxicology(this.transactionId).subscribe(
          toxic => {
            if (toxic.length > 0) {
              this.toxicology = toxic[0];
            }
          }
        );
      }
    });
  }

  chemLength(chem: chemistry) {
    let count = 0;
    this.chemistry1 = {
      chemID: null,
      patientID: null,
      transactionID: null,
      pathID: null,
      medID: null,
      qualityID: null,
      creationDate: null,
      dateUpdate: null,

      chol: null,
      cholcon: null,
      trig: null,
      trigcon: null,
      hdl: null,
      hdlcon: null,
      ldl: null,
      ldlcon: null,
      ch: null,
      vldl: null,

      na: null,
      k: null,
      cl: null,

      alt: null,
      ast: null,
      amylase: null,
      lipase: null,

      ogtt1: null,
      ogtt1con: null,
      ogtt2: null,
      ogtt2con: null,

      cpkmb: null,
      totalCPK: null,
      cpkmm: null,

      biltotal: null,
      bildirect: null,
      bilindirect: null,

      agratio: null,
      buncon: null,
      bun: null,
      fbs: null,
      fbscon: null,

      crea: null,
      creacon: null,

      bua: null,

      buacon: null,

      hb: null,

      alp: null,
      psa: null,

      calcium: null,
      albumin: null,
      ldh: null,
      rbs: null,
      globulin: null,
      ggtp: null,
      ionCalcium: null,
      protein: null,
      ogct: null,
      ogctcon: null,
      rbscon: null,
      magnesium: null,
      inPhos: null,

      chemNotes: null
    }

    for (var i in this.chemistry1) {
      if (i == "chemID" || i == "transactionID" || i == "patientID" || i == "medID"
        || i == "qualityID" || i == "creationDate" || i == "dateUpdate" || i == "userID") {

      } else {
        if (count <= 24 && this.math.hideRes(chem[i])) {
          this.chemistry1[i] = chem[i];
          count++;
        } else {
          // console.log(count);

          // Previous: 26
          if (count == 25) {
            // console.log(count);
            this.chemistry2 = {
              chemID: null, patientID: null, transactionID: null, pathID: null, medID: null, qualityID: null,
              creationDate: null, dateUpdate: null, chol: null, cholcon: null, trig: null, trigcon: null,
              hdl: null, hdlcon: null, ldl: null, ldlcon: null, ch: null, vldl: null, na: null, k: null,
              cl: null, alt: null, ast: null, amylase: null, lipase: null, ogtt1: null, ogtt1con: null,
              ogtt2: null, ogtt2con: null, cpkmb: null, totalCPK: null, cpkmm: null, biltotal: null,
              bildirect: null, bilindirect: null, agratio: null, buncon: null, bun: null, fbs: null,
              fbscon: null, crea: null, creacon: null, bua: null, buacon: null, hb: null, alp: null,
              psa: null, calcium: null, albumin: null, ldh: null, rbs: null, globulin: null, ggtp: null,
              ionCalcium: null, protein: null, ogct: null, ogctcon: null, rbscon: null, magnesium: null,
              inPhos: null, chemNotes: null
            }
          }

          if (this.math.hideRes(chem[i])) {
            this.chemistry2[i] = chem[i];
            count++;
          }
        }

      }
    }
  }

  close() {
    this.dialogRef.close();

    const dialogRef = this.dialog.open(LabHistoryDialogComponent, {
      data: {
        pid: this.patientId,
        type: this.type
      },
      width: "80%",
      disableClose: true
    });
  }
}
