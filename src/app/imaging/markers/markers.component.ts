import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-markers',
  templateUrl: './markers.component.html',
  styleUrls: ['./markers.component.scss']
})
export class MarkersComponent implements OnInit {

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("imaging");
  }


  ngOnInit() {
  }

}
