import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Global } from '../global.variable';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class DocpeService {

  constructor(
    private httpClient: HttpClient,
    private ehs: ErrorService,
    private global: Global
  ) { }

  getDocPes(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocPe")
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
      );
  };

  addDocPe(pe: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addDocPe', pe)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocPe(dataRef: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getDocPe/' + dataRef)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }


  updateDocPe(pe): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updateDocPe', pe)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }
}
