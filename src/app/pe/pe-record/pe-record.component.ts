import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-pe-record',
  templateUrl: './pe-record.component.html',
  styleUrls: ['./pe-record.component.scss']
})
export class PeRecordComponent implements OnInit {

  public peRecord = "peRecord";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("pe");
  }

  ngOnInit() {
  }

}
