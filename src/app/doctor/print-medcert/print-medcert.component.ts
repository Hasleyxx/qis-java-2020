import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: 'print-medcert',
  templateUrl: './print-medcert.component.html',
  styleUrls: ['./print-medcert.component.scss']
})
export class PrintMedcertComponent implements OnInit {

  data: string[];
  public patientData: any;
  public value1: string;
  public value2: string;
  public value3: string;
  public value4: string;

  constructor(
    route: ActivatedRoute,
    private patientService: PatientService
  ) {
    this.data = route.snapshot.params['data'].split(',');
  }

  ngOnInit() {
    this.patientService.getOnePatient('getPatient/' + this.data[0]).subscribe(
      data => this.patientData = data[0]
    );

    this.value1 = JSON.parse(sessionStorage.getItem("value1"));
    this.value2 = JSON.parse(sessionStorage.getItem("value2"));
    this.value3 = JSON.parse(sessionStorage.getItem("value3"));
    this.value4 = JSON.parse(sessionStorage.getItem("value4"));

    sessionStorage.removeItem("value1");
    sessionStorage.removeItem("value2");
    sessionStorage.removeItem("value3");
    sessionStorage.removeItem("value4");
  }
}
