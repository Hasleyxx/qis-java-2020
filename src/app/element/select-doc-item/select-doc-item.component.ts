import { Component, OnInit, AfterViewInit, OnDestroy, Output, Input, EventEmitter, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { company } from 'src/app/services/service.interface';
import { MatSelect } from '@angular/material';
import { DoctorService } from 'src/app/services/doctor.service';
import { takeUntil, take } from 'rxjs/operators';

@Component({
  selector: 'app-select-doc-item',
  templateUrl: './select-doc-item.component.html',
  styleUrls: ['./select-doc-item.component.scss']
})
export class SelectDocItemComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() selectTrig = new EventEmitter();
  @Input() itemURL: string;
  @Input() placeholder: string;
  @Input() selected: any;
  items = [];

  /** control for the selected bank */
  public itemCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public itemFilterCtrl: FormControl = new FormControl();

  /** list of item filtered by search keyword */
  public filteredItems: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  @ViewChild('singleSelect') singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  getSelectedValue() {
    this.selectTrig.emit(this.itemCtrl.value);
  }

  constructor(public doctorService: DoctorService) { }

  ngOnInit() {
    //get item list and pass the data to items variable
    this.doctorService.getDocItems()
      .subscribe(data => {
        this.items = data;
        // load the initial bank list
        this.filteredItems.next(this.items.slice());
      });

    // set initial selection
    this.itemCtrl.setValue("a");

    // listen for search field value changes
    this.itemFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterItems();
      });
    // emit value
    this.itemCtrl.valueChanges.subscribe(
      value => this.selectTrig.emit(value)
    );
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * Sets the initial value after the filteredBanks are loaded initially
   */
  protected setInitialValue() {
    this.filteredItems
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredBanks are loaded initially
        // and after the mat-option elements are available
        this.singleSelect.compareWith =
          (a: company, b: company) => a && b && a.companyID === b.companyID;
      });
  }

  protected filterItems() {
    if (!this.items) {
      return;
    }
    // get the search keyword
    let search = this.itemFilterCtrl.value;
    if (!search) {
      this.filteredItems.next(this.items.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the items
    this.filteredItems.next(
      this.items.filter(items => items.generic.toLowerCase().indexOf(search) > -1)
    );
  }

}
