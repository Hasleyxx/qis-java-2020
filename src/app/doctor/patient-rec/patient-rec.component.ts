import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { ItemService } from 'src/app/services/item.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';
import { ClearanceHistoryComponent } from '../clearance-history/clearance-history.component';
import { MedcertHistoryComponent } from '../medcert-history/medcert-history.component';
import { MedicineDialogComponent } from '../medicine-dialog/medicine-dialog.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { DoctorService } from 'src/app/services/doctor.service';
import { MathService } from 'src/app/services/math.service';
import { BlankPageComponent } from '../blank-page/blank-page.component';
import { RequestHistoryComponent } from '../request-history/request-history.component';
import { CovidHistoryComponent } from '../covid-history/covid-history.component';
import { LabHistoryDialogComponent } from '../element/lab-history-dialog/lab-history-dialog.component';

@Component({
  selector: 'app-patient-rec',
  templateUrl: './patient-rec.component.html',
  styleUrls: ['./patient-rec.component.scss']
})

export class PatientRecComponent implements OnInit {

  private snapshot: ActivatedRouteSnapshot;
  public patientId: any;
  public type: any;
  public patientDatas: any = [];
  public transDatas: any = [];
  public availed: any;
  public description: any;
  public transactionType: any;
  public docForm: FormGroup;
  public docPatientStatus: boolean = false;
  public docPatientData: any;
  public dates: any = [];

  @ViewChild('findingsText') public findingsText: ElementRef;
  @ViewChild('diagnosisText') public diagnosisText: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private itemService: ItemService,
    private docService: DoctorService,
    public state: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<PatientRecComponent>,
    private formBuilder: FormBuilder,
    private math: MathService
  ) {
    this.patientId = this.data.pid;
  }

  ngOnInit() {
    if (this.data.status) {
      this.type = this.data.status;
    }

    this.docForm = this.formBuilder.group({
      famHis: [''],
      pastmedHis: [''],
      allergies: [''],
      medication: [''],
      CreationDate: [moment().format("YYYY-MM-DD h:mm:ss")],
      findings: [''],
      diagnosis: ['']
    });

    this.getData();
  }

  getData() {
    // Patient
    this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => {
        this.patientDatas = data[0];
      }
    );

    // Doc patient
    this.docService.getDocPatient(this.patientId).subscribe(data => {
      if(data.length > 0) {
        this.docPatientStatus = true;
        this.docPatientData = data[0];
        this.docForm.patchValue({
          patientID: data[0]['patientID'],
          famHis: data[0]['famHis'],
          pastmedHis: data[0]['pastmedHis'],
          allergies: data[0]['allergies'],
          medication: data[0]['medication']
        });
      }
    });

    this.getFindDiagData();
  }

  clearance(pid) {
    this.dialogRef.close();
    this.dialog.open(ClearanceHistoryComponent, {
      data: {
        pid: pid
      },
      disableClose: true
    });

  }

  medcert(pid) {
    this.dialogRef.close();
    this.dialog.open(MedcertHistoryComponent, {
      data: {
        pid: pid
      },
      disableClose: true
    });
  }

  medicine(pid) {
    this.dialogRef.close();
    this.dialog.open(MedicineDialogComponent, {
      data: {
        pid: pid
      },
      disableClose: true
    });
  }

  request(pid) {
    this.dialogRef.close();

    this.dialog.open(RequestHistoryComponent, {
      data: {
        pid: pid
      },
      disableClose: true
    });
  }

  blank(pid) {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '40%',
      data: {
        Title: "BLANK OR VIEW?",
        Content: "What to do?",
        cancel: "Cancel",
        prev: "Previously Printed",
        ok: "Blank Page",
        pid: pid
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.dialog.open(BlankPageComponent, {
          data: {
            pid: pid
          },
          width: '50vw',
        });
      }
    });
  }


  /* clearance(pid) {
    this.router.navigate(['/doctor/clearance-dialog']);
  } */

  addClearance(tid) {
    this.snapshot = this.state.snapshot;
    this.router.navigate([this.snapshot.routeConfig.path + "/form", tid]);
  }

  docFormSubmit() {
    let famHis = this.docForm.get('famHis').value;
    let pastmedHis = this.docForm.get('pastmedHis').value;
    let allergies = this.docForm.get('allergies').value;
    let medication = this.docForm.get('medication').value;
    let CreationDate = this.docForm.get('CreationDate').value;
    let findings = this.docForm.get('findings').value;
    let diagnosis = this.docForm.get('diagnosis').value;

    let docPatient = {
      docPatientID: "",
      patientID: this.patientId,
      famHis: famHis,
      pastmedHis: pastmedHis,
      allergies: allergies,
      medication: medication,
      dateCreated: moment().format("YYYY-MM-DD h:mm:ss")
    };

    let docFindDiag = {
      patientID: this.patientId,
      CreationDate: CreationDate,
      findings: findings,
      diagnosis: diagnosis
    };

    if (this.docPatientData) {
      docPatient['docPatientID'] = this.docPatientData['docPatientID'];
    }

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Patient records will be saved!" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        if (famHis !== "" || pastmedHis !== "" || allergies !== "" || medication !== "") {
          if (this.docPatientStatus == true) {
            this.docService.updateDocPatient(docPatient).subscribe(data => {
              // console.log("UPDATE DOC PATIENT: " + data);
              if (data == 1) {
                this.math.openSnackBar("Patient data has been updated", "close");
              } else {
                this.math.openSnackBar("Failed to update patient data", "close");
              }
            });
          } else {
            this.docService.addDocPatient(docPatient).subscribe(data => {
              // console.log("ADD DOC PATIENT: " + data);
              if (data == 1) {
                this.math.openSnackBar("Patient data has been added", "close");
              } else {
                this.math.openSnackBar("Failed to add patient data", "close");
              }
            });
          }
        }

        if (findings !== "" || diagnosis !== "") {
          this.docService.addFindDiag(docFindDiag).subscribe(data => {
            // console.log("ADD DOC FINDINGS DIAGNOSIS: " + data);
            this.docForm.patchValue({
              findings: '',
              diagnosis: ''
            });

            this.getFindDiagData();
            if (data == 1) {
              this.math.openSnackBar("Patient data has been added", "close");
            } else {
              this.math.openSnackBar("Failed to add patient data", "close");
            }
          });
        }
      }
    });
  }

  viewFindDiag(findings, diagnosis) {
    this.findingsText.nativeElement.innerHTML = findings;
    this.diagnosisText.nativeElement.innerHTML = diagnosis;
  }

  getFindDiagData() {
    // Doc Findings Dignosis
    this.dates = [];
    this.docService.getFindDiag(this.patientId).subscribe(data => {
      if (data.length > 0) {
        data.forEach(element => {
          this.dates.push(element);
        });
      }
    });
  }

  covid(pid) {
    this.dialogRef.close();

    this.dialog.open(CovidHistoryComponent, {
      data: {
        pid: pid
      },
      width: "60%",
      disableClose: true
    });
  }

  labs(pid: number, type: string) {
    this.dialogRef.close();

    const dialogRef = this.dialog.open(LabHistoryDialogComponent, {
      data: {
        pid: pid,
        type: type
      },
      width: "80%",
      disableClose: true
    });
  }

}
