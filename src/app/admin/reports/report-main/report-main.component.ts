import { Component, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-report-main',
  templateUrl: './report-main.component.html',
  styleUrls: ['./report-main.component.scss']
})
export class ReportMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    // Create chart instance
    let chart = am4core.create("chartdiv", am4charts.XYChart);

    // Add data
    chart.data = [{
      "item": 'Basic 5',
      "income": 19000,
      "soldCount": 50
    },{
      "item": 'PSA',
      "income": 14625,
      "soldCount": 15
    },{
      "item": 'Lipid Profile',
      "income": 10375,
      "soldCount": 25
    },{
      "item": 'CHEST LAT (11X14)',
      "income": 10000,
      "soldCount": 50
    },{
      "item": 'KUB UTZ',
      "income": 9720,
      "soldCount": 6
    },{
      "item": 'Drug Test',
      "income": 9000,
      "soldCount": 30
    },{
      "item": 'Fasting Blood Sugar',
      "income": 8000,
      "soldCount": 100
    },{
      "item": 'Basic 3',
      "income": 7500,
      "soldCount": 20
    },{
      "item": 'CBC',
      "income": 6000,
      "soldCount": 20
    },{
      "item": 'COMPLETE URINALYSIS',
      "income": 3750,
      "soldCount": 50
    },{
      "item": 'FECALYSIS',
      "income": 1950,
      "soldCount": 30
    },];

    // Create axes
    let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "item";
    categoryAxis.numberFormatter.numberFormat = "#";
    categoryAxis.renderer.inversed = true;
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.cellStartLocation = 0.1;
    categoryAxis.renderer.cellEndLocation = 0.9;

    let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.opposite = true;

    // Create series
    function createSeries(field, name) {
      let series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueX = field;
      series.dataFields.categoryY = "item";
      series.name = name;
      series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
      series.columns.template.height = am4core.percent(80);
      series.sequencedInterpolation = true;

      let valueLabel = series.bullets.push(new am4charts.LabelBullet());
      valueLabel.label.text = "{valueX}";
      valueLabel.label.horizontalCenter = "left";
      valueLabel.label.dx = 10;
      valueLabel.label.hideOversized = false;
      valueLabel.label.truncate = false;

      let categoryLabel = series.bullets.push(new am4charts.LabelBullet());
      categoryLabel.label.text = "{name}";
      categoryLabel.label.horizontalCenter = "right";
      categoryLabel.label.dx = -10;
      categoryLabel.label.fill = am4core.color("#fff");
      categoryLabel.label.hideOversized = false;
      categoryLabel.label.truncate = false;
    }

    createSeries("income", "Income");
    // createSeries("soldCount", "Sold Count");
   
  }


}
