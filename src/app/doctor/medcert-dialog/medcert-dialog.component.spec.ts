import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedcertDialogComponent } from './medcert-dialog.component';

describe('MedcertDialogComponent', () => {
  let component: MedcertDialogComponent;
  let fixture: ComponentFixture<MedcertDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedcertDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedcertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
