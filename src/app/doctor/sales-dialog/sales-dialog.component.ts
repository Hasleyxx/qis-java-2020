import { Component, OnInit, Inject } from '@angular/core';
import { DocTransactionService } from 'src/app/services/doc-transaction.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { patient, docTrans } from 'src/app/services/service.interface';
import { ItemService } from 'src/app/services/item.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

export interface dialogData {
  tid: number,
  pid: number,
  type: string
}

@Component({
  selector: "app-sales-dialog",
  templateUrl: "./sales-dialog.component.html",
  styleUrls: ["./sales-dialog.component.scss"]
})
export class SalesDialogComponent implements OnInit {
  public tid: number;
  public pid: number;
  public type: string;

  public patientData: patient;
  public docTrans: docTrans;

  public itemName: string = undefined;
  public items: any[] = [];
  public form: FormGroup;
  public totalPrice: number = 0;
  public change: number = 0;

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: dialogData,
    private math: MathService,
    private PS: PatientService,
    private DTS: DocTransactionService,
    private IS: ItemService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SalesDialogComponent>
  ) {
    this.tid = this.dialogData.tid;
    this.pid = this.dialogData.pid;
    this.type = this.dialogData.type;
  }

  ngOnInit() {
    this.getData();

    if (this.type == "payment") {
      this.form = this.formBuilder.group({
        transactionDocId: ["", Validators.required],
        item: ["", Validators.required],
        totalPrice: ["", Validators.required],
        paidIn: ["", Validators.required],
        paidOut: ["", Validators.required],
        grandTotal: ["", Validators.required],
        date_paid: [""]
      });

      this.form.get("paidIn").valueChanges.subscribe(value => {
        let paidIn: number = parseInt(value);

        setTimeout(() => {
          if (paidIn > this.totalPrice) {
            let changeTemp = paidIn - this.totalPrice;
            this.change = changeTemp;
          } else {
            this.change = 0;
          }
        }, 500);
      });
    }
  }

  getData() {
    this.PS.getPatient("/getPatient/" + this.pid).subscribe(patient => {
      this.patientData = patient[0];
    });

    this.DTS.getOneDocTransaction(this.tid).subscribe(trans => {
      this.docTrans = trans;

      if (trans.itemId !== null) {
        let itemIdTemp = parseInt(trans.itemId);
        this.IS.getItemByID(itemIdTemp).subscribe(data => {
          this.itemName = data[0].itemName;
        });
      }
    });

    this.IS.getItemType("MEDICAL SERVICE").subscribe(itemData => {
      itemData.forEach(element => {
        this.items.push(element);
      });
    });
  }

  selectItem(value: string) {
    let itemTemp = value.split("|");
    let itemPrice = parseInt(itemTemp[0]);
    let itemId = itemTemp[1];

    this.form.patchValue({
      item: itemId,
      paidIn: itemPrice,
      date_paid: this.math.dateNow()
    });

    this.totalPrice = itemPrice;
  }

  submit() {
    let updateDocTransaction = {
      transactionDocId: this.tid,
      totalPrice: this.totalPrice,
      paidIn: this.form.get('paidIn').value,
      paidOut: this.change,
      grandTotal: this.totalPrice,
      itemId: this.form.get('item').value,
      date_paid: this.math.dateNow()
    };

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?", Content: "Patient payment will be saved!"
      },
      width: '25%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.DTS.updateDocTransaction(updateDocTransaction).subscribe((data: any) => {
          if (data == 1) {
            this.math.openSnackBar("Payment has been succesfully made!", "close");
            this.dialogRef.close("paid");
          }
        });
      }
    });
  }

  cancel() {
    this.dialogRef.close();
  }
}
