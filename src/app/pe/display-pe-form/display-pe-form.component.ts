import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeService } from 'src/app/services/pe.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-display-pe-form',
  templateUrl: './display-pe-form.component.html',
  styleUrls: ['./display-pe-form.component.scss']
})
export class DisplayPeFormComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];
  public patientID: any;
  public transactionID: any;
  public patientDatas: any;
  public transactionDatas: any;
  public vitalDatas: any;
  public medHisDatas = [];
  public vitals = [];
  public pes = [];
  public findings: any;
  public phy: any;
  public phyLic: any;

  public vitalNotes: any;
  public vitalNotesColor: any = "text-dark";

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private peService: PeService,
    private patientService: PatientService,
    private transactionService: TransactionService,
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.transactionID = this.data[0];
    this.dataDetails = this.data.map(data => this.getPeFormReady(data));

    /* if (this.router.url == '/nurse/form/' + this.patientID + "/" + this.transactionID) {
      this.math.navSubs("nurse");

    } else if (this.router.url == '/imaging/form/' + this.patientID + "/" + this.transactionID) {
      this.math.navSubs("imaging");

    } else {
      this.math.navSubs("pe");
    } */
  }

  ngOnInit() {
    this.getPatientdata();
  }


  getPatientdata() {

    this.peService.getMedhis(this.transactionID).subscribe(
      data => {
        if (data !== null) {
          this.medHisDatas.push(
            { "label": "Asthma: ", "value": data.asth },
            { "label": "Tuberculosis: ", "value": data.tb },
            { "label": "Diabetes Mellitus: ", "value": data.dia },
            { "label": "High Blood Pressure: ", "value": data.hb },
            { "label": "Heart Problem: ", "value": data.hp },
            { "label": "Kidney Problem: ", "value": data.kp },
            { "label": "Abdominal/Hernia: ", "value": data.ab },
            { "label": "Joint/Back/Scoliosis: ", "value": data.jbs },
            { "label": "Psychiatric Problem: ", "value": data.pp },
            { "label": "Migraine/Headache: ", "value": data.mh },
            { "label": "Fainting/Seizures: ", "value": data.fs },
            { "label": "Allergies: ", "value": data.alle },
            { "label": "Cancer/Tumor: ", "value": data.ct },
            { "label": "Hepatitis: ", "value": data.hep },
            { "label": "STD/PLHIV: ", "value": data.std }
          );

        } else {
          this.medHisDatas.push(
            { "label": "Asthma: ", "value": '-' },
            { "label": "Tuberculosis: ", "value": '-' },
            { "label": "Diabetes Mellitus: ", "value": '-' },
            { "label": "High Blood Pressure: ", "value": '-' },
            { "label": "Heart Problem: ", "value": '-' },
            { "label": "Kidney Problem: ", "value": '-' },
            { "label": "Abdominal/Hernia: ", "value": '-' },
            { "label": "Joint/Back/Scoliosis: ", "value": '-' },
            { "label": "Psychiatric Problem: ", "value": '-' },
            { "label": "Migraine/Headache: ", "value": '-' },
            { "label": "Fainting/Seizures: ", "value": '-' },
            { "label": "Allergies: ", "value": '-' },
            { "label": "Cancer/Tumor: ", "value": '-' },
            { "label": "Hepatitis: ", "value": '-' },
            { "label": "STD/PLHIV: ", "value": '-' }
          );
        }
      }
    );
    this.peService.getVital(this.transactionID).subscribe(
      data => {
        this.vitalDatas = data;
        if (data !== null) {
          this.vitals.push(
            { "label": "Ishihara Test: ", "value": data.cv, "color": "text-dark" },
            { "label": "Hearing: ", "value": data.hearing, "color": "text-dark" },
            { "label": "Hospitalization: ", "value": data.hosp, "color": "text-dark" },
            { "label": "Operations: ", "value": data.opr, "color": "text-dark" },
            { "label": "Medications: ", "value": data.pm, "color": "text-dark" },
            { "label": "Smoker: ", "value": data.smoker, "color": "text-dark" },
            { "label": "Alcoholic: ", "value": data.ad, "color": "text-dark" },
            { "label": "Last Menstrual: ", "value": data.lmp, "color": "text-dark" }
          );
          this.vitalNotes = data.notes;
          this.vitalNotesColor = "text-danger";

        } else {
          this.vitals.push(
            { "label": "Ishihara Test: ", "value": '-', "color": "text-dark" },
            { "label": "Hearing: ", "value": '-', "color": "text-dark" },
            { "label": "Hospitalization: ", "value": '-', "color": "text-dark" },
            { "label": "Operations: ", "value": '-', "color": "text-dark" },
            { "label": "Medications: ", "value": '-', "color": "text-dark" },
            { "label": "Smoker: ", "value": '-', "color": "text-dark" },
            { "label": "Alcoholic: ", "value": '-', "color": "text-dark" },
            { "label": "Last Menstrual: ", "value": '-', "color": "text-dark" }
          );
          this.vitalNotes = "";
        }
      }
    );
    this.peService.getPE(this.transactionID).subscribe(
      data => {
        if (data !== null) {
          this.pes.push(
            { "label": "Skin: ", "value": data.skin },
            { "label": "Head and Neck: ", "value": data.hn },
            { "label": "Chest/Breast/Lungs: ", "value": data.cbl },
            { "label": "Cardiac/Heart: ", "value": data.ch },
            { "label": "Abdomen: ", "value": data.abdo },
            { "label": "Extremities: ", "value": data.extre },
            { "label": "Others/Notes: ", "value": data.ot }
          );

          this.findings = data.find;
          this.phy = data.doctor;
          this.phyLic = data.license;

        } else {
          this.pes.push(
            { "label": "Skin: ", "value": '-' },
            { "label": "Head and Neck: ", "value": '-' },
            { "label": "Chest/Breast/Lungs: ", "value": '-' },
            { "label": "Cardiac/Heart: ", "value": '-' },
            { "label": "Abdomen: ", "value": '-' },
            { "label": "Extremities: ", "value": '-' },
            { "label": "Others/Notes: ", "value": '-' }
          );

          this.findings = "-";
          this.phy = "-";
          this.phyLic = "-";
        }
      }
    );

    this.transactionService.getOneTrans('getTransaction/' + this.transactionID).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientID = data[0]['patientId'];
        this.patientService.getOnePatient('getPatient/' + this.patientID).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );

    Promise.all(this.dataDetails).then(() => {
      this.math.onPeFormReady();
    });
  }

  getPeFormReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

}
