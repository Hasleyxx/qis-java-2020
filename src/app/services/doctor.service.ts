import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Global } from '../global.variable';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { docItems, doctorCovid } from './service.interface';

@Injectable({
  providedIn: "root"
})
export class DoctorService {
  constructor(
    private httpClient: HttpClient,
    private ehs: ErrorService,
    private global: Global
  ) {}

  // Clearances
  addDocClearance(clearance: any): Observable<any> {
    return this.httpClient
      .post<any>(this.global.url + "/addDocClearance", clearance)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  getDocClearance(id: any): Observable<any> {
    return this.httpClient
      .get<any>(this.global.url + "/getOneDocClearance/" + id)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  getOneDocClearanceRef(ref: any): Observable<any> {
    return this.httpClient
      .get<any>(this.global.url + "/getOneDocClearanceRef/" + ref)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  getPidDocClearance(pid: any): Observable<any> {
    return this.httpClient
      .get<any>(this.global.url + "/getPidDocClearance/" + pid)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  updateDocClearance(clearance): Observable<any> {
    return this.httpClient
      .post<any>(this.global.url + "/updateClearance", clearance)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  // Medical Certificate
  addDocMedcert(medcert: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addDocMedcert', medcert)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }


  getDocMedcert(id: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getDocMedcert/' + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getPidDocMedcert(pid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getPidDocMedcert/' + pid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }


  updateDocMedcert(medcert): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updateDocMedcert', medcert)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  // Physical Examination
  getDocPes(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocPe")
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  };

  addDocPe(pe: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addDocPe', pe)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocPe(dataRef: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getDocPe/' + dataRef)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }


  updateDocPe(pe): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updateDocPe', pe)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  // Doctor Items
  getDocItems(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocItems")
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addDocItems(datas): Observable<docItems> {
    return this.httpClient.post<docItems>(this.global.url + "/addDocItems", datas)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateDocItems(datas): Observable<docItems> {
    return this.httpClient.post<docItems>(this.global.url + "/updateDocItems", datas)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  // Doctor Items
  getDocMed(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocMed")
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocMedId(pid): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocMedId/" + pid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocMedUid(uid): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocMedUid/" + uid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  addDocMed(datas): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addDocMed", datas)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  updateDocMed(datas): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/updateDocMed", datas)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }
  deleteDocMed(id): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/deleteDocMed/" + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  // Doctor Requests
  addDocRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addDocRequest", data).
    pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getDocRequestId(pid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocRequestId/" + pid).
    pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getDocRequestRef(ref: any): Observable<any[]> {
    return this.httpClient.get<any[]>(this.global.url + "/getDocRequestRef/" + ref).
      pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocRequestCategory(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocRequestCategory").
    pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getDocRequestCategoryId(id): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocRequestCategory/" + id).
    pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addDocRequestCategory(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addDocRequestCategory", data).
    pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateDocRequestCategory(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/updateDocRequestCategory", data).
    pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addDocRequestOption(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addDocRequestOption", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  updateDocRequestOption(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/updateDocRequestOption", data)
    .pipe(
      retry(1), 
      catchError(this.ehs.handleError)
    )
  }

  removeDocRequestOption(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/removeDocRequestOption", data)
    .pipe(
      retry(1), 
      catchError(this.ehs.handleError)
    )
  }

  getDocRequestOptionId(id): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocRequestOptionId/" + id)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getDocRequestOptionIdAll(id): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocRequestOptionIdAll/" + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocPatient(pid: number): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getDocPatient/" + pid)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addDocPatient(data: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addDocPatient", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateDocPatient(data: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/updateDocPatient", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getFindDiag(pid: number): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getFindDiag/" + pid)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getFindDiagByDate(pid: number, date: string): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/getFindDiagByDate/" + pid + "/" + date)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addFindDiag(data: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addFindDiag", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getCovidHistory(pid: number): Observable<doctorCovid[]>{
    return this.httpClient.get<doctorCovid[]>(this.global.url + "/getCovidHistory/" + pid)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getCovidHistoryOne(transactionDocId: number): Observable<doctorCovid> {
    return this.httpClient.get<doctorCovid>(this.global.url + "/getCovidHistoryOne/" + transactionDocId)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError
      )
    );
  }

  addCovid(data: doctorCovid): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addCovid", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateCovid(data: doctorCovid): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/updateCovid", data)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getLabPid(url: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + "/" + url)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }
}
