import { Component, OnInit } from '@angular/core';
import { DoctorService } from 'src/app/services/doctor.service';
import { doctorCovid } from 'src/app/services/service.interface';

@Component({
  selector: 'print-covid',
  templateUrl: './print-covid.component.html',
  styleUrls: ['./print-covid.component.scss']
})
export class PrintCovidComponent implements OnInit {

  public docCovidId: any;
  public covidData: doctorCovid;

  constructor(
    private DS: DoctorService
    ) { }

  ngOnInit() {
    this.docCovidId = sessionStorage.getItem("docCovidId");

    this.getData();
  }

  getData() {
    this.DS.getCovidHistoryOne(this.docCovidId).subscribe(data => {
      if (data !== null) {
        this.covidData = data;
      }
    });
  }

}
