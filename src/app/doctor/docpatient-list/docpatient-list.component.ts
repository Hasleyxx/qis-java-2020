import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-docpatient-list',
  templateUrl: './docpatient-list.component.html',
  styleUrls: ['./docpatient-list.component.scss']
})
export class DocpatientListComponent implements OnInit {
  public docPatient = "docPatient";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("doctor");
  }

  ngOnInit() {
  }
}
