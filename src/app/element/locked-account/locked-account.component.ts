import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MathService } from 'src/app/services/math.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-locked-account',
  templateUrl: './locked-account.component.html',
  styleUrls: ['./locked-account.component.scss']
})
export class LockedAccountComponent implements OnInit {

  constructor(
    private user: UserService,
    private math: MathService,
    public route: Router
  ) {
    this.math.navVisibility(false);
  }

  ngOnInit() {
    let userID = parseInt(sessionStorage.getItem('token'));
    if (userID) {
      this.user.getUser(userID).subscribe(
        user => {
          if (user[0]) {
            this.user.checkUser(user[0].userName, '', true).subscribe(
              data => {

              }
            )
          }
        }
      )
    }
  }

  redirect() {
    this.route.navigate(['authentication/signin']);
  }

}
