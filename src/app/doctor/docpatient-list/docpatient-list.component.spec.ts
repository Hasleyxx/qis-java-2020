import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocpatientListComponent } from './docpatient-list.component';

describe('DocpatientListComponent', () => {
  let component: DocpatientListComponent;
  let fixture: ComponentFixture<DocpatientListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocpatientListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocpatientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
