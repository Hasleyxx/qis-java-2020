import { Component, OnInit, Inject } from '@angular/core';
import { PeService } from 'src/app/services/pe.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-pe-data',
  templateUrl: './pe-data.component.html',
  styleUrls: ['./pe-data.component.scss']
})
export class PeDataComponent implements OnInit {

  public patientId: any;
  public transactionId: any;
  public patientDatas: any;
  public transactionDatas: any;
  public vitalDatas: any;
  public medHisDatas = [];
  public vitals = [];
  public pes = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private peService: PeService,
    private patientService: PatientService,
    private transactionService: TransactionService,
  ) { }

  ngOnInit() {
    this.getPatientdata();
  }

  getPatientdata() {
    this.patientId = this.data.pid;
    this.transactionId = this.data.tid;

    this.peService.getMedhis(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.medHisDatas.push(
            { "label": "Astma: ", "value": data.asth },
            { "label": "Tuberculosis: ", "value": data.tb },
            { "label": "Diabetes Mellitus: ", "value": data.dia },
            { "label": "High Blood Pressure: ", "value": data.hp },
            { "label": "Heart Problem: ", "value": data.hp },
            { "label": "Kidney Problem: ", "value": data.kp },
            { "label": "Abdominal/Hernia: ", "value": data.ab },
            { "label": "Joint/Back/Scoliosis: ", "value": data.jbs },
            { "label": "Psychiatric Problem: ", "value": data.pp },
            { "label": "Migraine/Headache: ", "value": data.mh },
            { "label": "Fainting/Seizures: ", "value": data.fs },
            { "label": "Allergies: ", "value": data.alle },
            { "label": "Cancer/Tumor: ", "value": data.ct },
            { "label": "Hepatitis: ", "value": data.hep },
            { "label": "STD: ", "value": data.std }
          );

        }else {
          this.medHisDatas.push(
            { "label": "Astma: ", "value": '-' },
            { "label": "Tuberculosis: ", "value": '-' },
            { "label": "Diabetes Mellitus: ", "value": '-' },
            { "label": "High Blood Pressure: ", "value": '-' },
            { "label": "Heart Problem: ", "value": '-' },
            { "label": "Kidney Problem: ", "value": '-' },
            { "label": "Abdominal/Hernia: ", "value": '-' },
            { "label": "Joint/Back/Scoliosis: ", "value": '-' },
            { "label": "Psychiatric Problem: ", "value": '-' },
            { "label": "Migraine/Headache: ", "value": '-' },
            { "label": "Fainting/Seizures: ", "value": '-' },
            { "label": "Allergies: ", "value": '-' },
            { "label": "Cancer/Tumor: ", "value": '-' },
            { "label": "Hepatitis: ", "value": '-' },
            { "label": "STD: ", "value": '-' }
          );
        }
      }
    );
    this.peService.getVital(this.transactionId).subscribe(
      data => {
        this.vitalDatas = data;
        if (data !== null) {
          this.vitals.push(
            { "label": "Ishihara Test: ", "value": data.cv },
            { "label": "Hearing: ", "value": data.hearing },
            { "label": "Hospitalization: ", "value": data.hosp },
            { "label": "Operations: ", "value": data.opr },
            { "label": "Medications: ", "value": data.pm },
            { "label": "Smoker: ", "value": data.smoker },
            { "label": "Alcoholic: ", "value": data.ad },
            { "label": "Last Menstrual: ", "value": data.lmp },
            { "label": "Others/Notes: ", "value": data.notes }
          );

        }else {
          this.vitals.push(
            { "label": "Ishihara Test: ", "value": '-' },
            { "label": "Hearing: ", "value": '-' },
            { "label": "Hospitalization: ", "value": '-' },
            { "label": "Operations: ", "value": '-' },
            { "label": "Medications: ", "value": '-' },
            { "label": "Smoker: ", "value": '-' },
            { "label": "Alcoholic: ", "value": '-' },
            { "label": "Last Menstrual: ", "value": '-' },
            { "label": "Others/Notes: ", "value": '-' }
          );
        }
      }
    );
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.pes.push(
            { "label": "Skin: ", "value": data.skin },
            { "label": "Head and Neck: ", "value": data.hn },
            { "label": "Chest/Breast/Lungs: ", "value": data.cbl },
            { "label": "Cardiac/Heart: ", "value": data.ch },
            { "label": "Abdomen: ", "value": data.abdo },
            { "label": "Extremities: ", "value": data.extre },
            { "label": "Others/Notes: ", "value": data.ot },
            { "label": "Findings: ", "value": data.find },
            { "label": "Physican: ", "value": data.doctor },
            { "label": "License: ", "value": data.license }
          );

        }else {
          this.pes.push(
            { "label": "Skin: ", "value": '-' },
            { "label": "Head and Neck: ", "value": '-' },
            { "label": "Chest/Breast/Lungs: ", "value": '-' },
            { "label": "Cardiac/Heart: ", "value": '-' },
            { "label": "Abdomen: ", "value": '-' },
            { "label": "Extremities: ", "value": '-' },
            { "label": "Others/Notes: ", "value": '-' },
            { "label": "Findings: ", "value": '-' },
            { "label": "Physican: ", "value": '-' },
            { "label": "License: ", "value": '-' }
          );
        }
      }
    );
    this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => this.patientDatas = data[0]
    );
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => this.transactionDatas = data[0]
    );
  }

}
