import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  btnClick= function () {
    this.router.navigateByUrl('laboratory/basic-two');
};


}
