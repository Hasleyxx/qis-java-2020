import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialog, MatDialogRef } from '@angular/material';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { CovidHistoryComponent } from '../covid-history/covid-history.component';
import * as moment from 'moment';

@Component({
  selector: 'app-covid-dialog',
  templateUrl: './covid-dialog.component.html',
  styleUrls: ['./covid-dialog.component.scss']
})
export class CovidDialogComponent implements OnInit {

  public patientId: any;
  public docCovidId: any;
  public patientDatas: any = [];

  public form: FormGroup;
  public options = ["NONREACTIVE", "REACTIVE"];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private math: MathService,
    private PS: PatientService,
    private DS: DoctorService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CovidDialogComponent>,
    private formBuilder: FormBuilder
  ) {
    this.patientId = this.data.pid;
    this.docCovidId = this.data.docCovidId;
  }

  ngOnInit() {
    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => this.patientDatas = data[0]
    );

    this.form = this.formBuilder.group({
      docCovidId: [''],
      patientId: [this.patientId],
      result: ['', Validators.required],
      userID: [+parseInt(sessionStorage.getItem('token'))],
      CreationDate: [''],
      DateUpdate: ['']
    });

    if (this.docCovidId !== undefined) {
      this.DS.getCovidHistoryOne(this.data.docCovidId).subscribe(data => {
        this.form.patchValue({
          docCovidId: data.docCovidId,
          patientId: this.patientId,
          result: data.result,
          CreationDate: data.CreationDate,
          DateUpdate: data.DateUpdate
        });
      });

    }
  }

  submit() {
    if (this.docCovidId == undefined) {
      const dialogRef = this.dialog.open(ConfirmComponent, {
        width: '20%',
        data: { Title: "Are you sure?", Content: "Covid test result will be saved!" }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          this.DS.addCovid(this.form.value).subscribe(data => {
            if (data == 1) {
              this.math.openSnackBar("Covid test result has been saved!", "close");
              this.dialogRef.close();

              const covidHistoryRef = this.dialog.open(CovidHistoryComponent, {
                data: {
                  pid: this.patientId
                },
                width: "60%",
                disableClose: true
              });
            }
          });
        }
      });

    } else {
      this.form.get('DateUpdate').setValue(moment().format("YYYY-MM-DD h:mm:ss"));

      const dialogRef = this.dialog.open(ConfirmComponent, {
        width: '20%',
        data: { Title: "Are you sure?", Content: "Covid test result will be updated!" }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          this.DS.updateCovid(this.form.value).subscribe(data => {
            if (data == 1) {
              this.math.openSnackBar("Covid test result has been updated!", "close");
              this.dialogRef.close();

              const covidHistoryRef = this.dialog.open(CovidHistoryComponent, {
                data: {
                  pid: this.patientId
                },
                width: "60%",
                disableClose: true
              });
            }
          });
        }
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.data.pid
      }
    });
  }
}
