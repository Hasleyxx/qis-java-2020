import { Component, OnInit } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-imaging-print-sticker',
  templateUrl: './imaging-print-sticker.component.html',
  styleUrls: ['./imaging-print-sticker.component.scss']
})
export class ImagingPrintStickerComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];
  public dateForm: any;
  public dateTo: any;
  public stickers = [];
  public patientData: any;
  public transactionData: any;
  public xrayData: any;
  public trans: any;

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private XS: XrayService
  ) {
    this.data = route.snapshot.params['stickers'].split(',');
  }

  ngOnInit() {
    this.dataDetails = this.data.map(data => this.getStickerReady(data));
    let url = '';
    if (this.data[0] == 'date') {
      url = "getTransactionDate/" + this.data[1] + "/" + this.data[2];
    } else {
      url = "getTransactionId/" + this.data[1] + "/" + this.data[2];
    }

    this.TS.getTransactions(url).subscribe(
      data => {
        data.forEach(element => {
          const patientId = element['patientId'];
          const transactionId = element['transactionId'];
          let tempData = {
            patient: [],
            transaction: [],
            xray: []
          };

          this.XS.getMarker(transactionId).subscribe(
            result => {
              if (result.length == 1) {
                tempData.xray = result[0];

                this.PS.getOnePatient('getPatient/' + patientId).subscribe(
                  data => {
                    tempData.patient = data[0];
                  }
                );

                this.TS.getOneTrans("getTransaction/" + transactionId).subscribe(
                  data => {
                    tempData.transaction = data[0];
                  }
                );

                this.stickers.push(tempData);
              }
            }
          );
        });
      }
    );

    Promise.all(this.dataDetails).then(() => {
      this.math.onStickerReady();
    });
  }

  getStickerReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

}
