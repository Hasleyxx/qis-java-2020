import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedcertHistoryComponent } from './medcert-history.component';

describe('MedcertHistoryComponent', () => {
  let component: MedcertHistoryComponent;
  let fixture: ComponentFixture<MedcertHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedcertHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedcertHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
