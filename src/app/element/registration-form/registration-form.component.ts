import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: 'registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {
  public id; 

  public procedures = ["OB EARLY PELVIC HEMMORAGE", "OB EARLY PREVIA PELVIC ", "PELVIC ULTRASOUND001", "PELVIC ULTRASOUND002", "PELVIC ULTRASOUND003", "PELVIC ULTRASOUND004"];
  public procedureValue = 'OB EARLY PELVIC HEMMORAGE';

  public personels = ["Arby Kaevilette G. Bolibol, RRT", "Judy R. Desaliza "];
  public personelValue = 'Arby Kaevilette G. Bolibol, RRT';

  public radiologist = ["RICARDO MA. O. PACHECO,MD.DPBR"];
  public radiologistValue = 'RICARDO MA. O. PACHECO,MD.DPBR';



  constructor(
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private XS: XrayService,
    private formBuilder: FormBuilder
  ) {
    this.math.navSubs("imaging")
  }

  ngOnInit() {
  
  }

  procedure(value: any) {
    this.procedureValue = value;
  }

  personel(value: any) {
    this.personelValue = value;
  }

  radiology(value: any) {
    this.radiologistValue = value;
  }
  update(){
    
  }

}
