import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Global } from '../global.variable';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class DocmedcertService {


  constructor(
    private httpClient: HttpClient,
    private ehs: ErrorService,
    private global: Global
  ) { }

  addDocMedcert(medcert: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addDocMedcert', medcert)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }


  getDocMedcert(tid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getDocMedcert/' + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getPidDocMedcert(pid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getPidDocMedcert/' + pid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }


  updateDocMedcert(medcert): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updateDocMedcert', medcert)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

}
