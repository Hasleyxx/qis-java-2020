import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfSummaryReportComponent } from './pdf-summary-report.component';

describe('PdfSummaryReportComponent', () => {
  let component: PdfSummaryReportComponent;
  let fixture: ComponentFixture<PdfSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
