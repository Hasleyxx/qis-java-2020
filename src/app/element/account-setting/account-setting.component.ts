import { Component, OnInit, Inject } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmComponent } from '../confirm/confirm.component';
import { Md5 } from 'md5-typescript';
import * as moment from 'moment';

@Component({
  selector: 'app-account-setting',
  templateUrl: './account-setting.component.html',
  styleUrls: ['./account-setting.component.scss']
})
export class AccountSettingComponent implements OnInit {

  public accountForm: FormGroup;
  public userID: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData,
    private math: MathService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AccountSettingComponent>
  ) {
    this.userID = this.dialogData.userID;
  }

  ngOnInit() {
    this.accountForm = this.formBuilder.group({
      userID: ['', Validators.required],
      userName: ['', Validators.required],
      userPassTemp: ['', [Validators.required, Validators.minLength(5)]],
      userPass: [''],
      userPassUpdate: ['']
    });

    this.getData();
  }

  getData() {
    setTimeout(() => {
      this.userService.getUser(parseInt(sessionStorage.getItem('token'))).subscribe(user => {
        if (user) {
          this.accountForm.patchValue({
            userID: user[0].userID,
            userName: user[0].userName
          });
        }
      });
    }, 1000);
  }

  accountSubmit() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Your password will be updated!"
      },
      width: '25%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        let pass = Md5.init(this.accountForm.controls.userPassTemp.value);
        this.accountForm.get('userPass').setValue(pass);
        this.accountForm.get('userPassUpdate').setValue(moment().format("YYYY-MM-DD"));

        this.userService.updateUserPassword(this.accountForm.value).subscribe(data => {
          if (data == 1) {
            this.math.openSnackBar("Your account has been updated!", "close");
            this.dialogRef.close("Updated");
          } else {
            this.math.openSnackBar("Failed to update account! Please seek help to IT.", "close");
          }
        });
      }
    });
  }

}
