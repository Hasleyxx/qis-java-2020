import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustrialSummaryComponent } from './industrial-summary.component';

describe('IndustrialSummaryComponent', () => {
  let component: IndustrialSummaryComponent;
  let fixture: ComponentFixture<IndustrialSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndustrialSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustrialSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
