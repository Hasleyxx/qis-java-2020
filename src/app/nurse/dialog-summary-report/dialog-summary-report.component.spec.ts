import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSummaryReportComponent } from './dialog-summary-report.component';

describe('DialogSummaryReportComponent', () => {
  let component: DialogSummaryReportComponent;
  let fixture: ComponentFixture<DialogSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
