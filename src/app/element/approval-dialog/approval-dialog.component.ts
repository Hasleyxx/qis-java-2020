import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-approval-dialog",
  templateUrl: "./approval-dialog.component.html",
  styleUrls: ["./approval-dialog.component.scss"]
})
export class ApprovalDialogComponent implements OnInit {
  public hint: any = "";
  public total: any = 0;
  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ApprovalDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public config: any,
    private formBuilder: FormBuilder
  ) {
    this.total = this.config.total;
  }

  ngOnInit() {
    if (this.total >= 1000) {
      this.hint = "This is required!";
      this.form = this.formBuilder.group({
        approvalCode: ["", Validators.required]
      });
    } else {
      this.hint = "This is optional!";
      this.form = this.formBuilder.group({
        approvalCode: [""]
      });
    }
  }

  formSubmit() {
    let approvalCode = this.form.controls["approvalCode"].value;
    this.dialogRef.close(approvalCode);
  }

  close() {
    this.dialogRef.close('close');
  }

}
