import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import * as moment from 'moment';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: 'app-xray-result',
  templateUrl: './xray-result.component.html',
  styleUrls: ['./xray-result.component.scss']
})
export class XrayResultComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];
  public patientID: any;
  public transactionID: any;
  public patientDatas: any;
  public transactionDatas: any;
  public rad: any;
  public currentTime = moment().format("YYYY-MM-DD h:mm:ss");

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private XS: XrayService,
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.transactionID = this.data[0];
  }

  ngOnInit() {
    this.getXray();
  }

  getXray() {
    this.dataDetails = this.data.map(data => this.getXrayResultReady(data));
    this.transactionService.getOneTrans('getTransaction/' + this.transactionID).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientID = data[0]['patientId'];
        this.patientService.getOnePatient('getPatient/' + this.patientID).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );

    this.XS.getOneXray(this.transactionID).subscribe(
      data => {
        if (data == null) {
          this.rad.push({
            'qa': '-',
            'radiologist': '-'
          });

        } else {
          this.rad = data;
        }

      }
    );

    Promise.all(this.dataDetails).then(() => {
      this.math.onXrayResultReady();
    });
  }

  getXrayResultReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

}
