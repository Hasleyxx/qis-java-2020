import { Injectable } from '@angular/core';
import { Global } from '../global.variable';
import { HttpClient } from '@angular/common/http';
import { colorFlow } from './service.interface';
import { ErrorService } from './error.service';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ColorFlowService {

  constructor(
    private global: Global,
    private http: HttpClient,
    private ehs: ErrorService
  ) { }

  getColorFlow(tid: number): Observable<colorFlow> {
    return this.http.get<colorFlow>(this.global.url + "/getColorFlow/" + tid)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  addColorFlow(colorFlow: colorFlow): Observable<colorFlow> {
    return this.http.post<colorFlow>(this.global.url + "/addColorFlow", colorFlow)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  updateColorFlow(colorFlow: colorFlow): Observable<colorFlow> {
    return this.http.post<colorFlow>(this.global.url + "/updateColorFlow", colorFlow)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }
}
