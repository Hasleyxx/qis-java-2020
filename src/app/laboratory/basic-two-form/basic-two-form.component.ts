import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { medtech, transaction } from 'src/app/services/service.interface';
import * as moment from 'moment';
import { SnackSuccessComponent } from 'src/app/element/snack-success/snack-success.component';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogConfirmComponent } from 'src/app/element/dialog-confirm/dialog-confirm.component';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-basic-two-form',
  templateUrl: './basic-two-form.component.html',
  styleUrls: ['./basic-two-form.component.scss']
})

export class BasicTwoFormComponent implements OnInit {

  public transactionId: any;
  public patientId: any;
  public medID: any;
  public pathID: any;
  public qualityID: any;
  public creationDate: any;
  public dateUpdate: any;
  public hemaID: any;
  public microID: any;
  public seroID: any;
  public toxicID: any;
  public update: boolean = false;
  medtech: medtech[];
  transaction: transaction;

  public basictwo: FormGroup;
  public hemaForm = [];
  public microForm = [];
  public seroForm = [];
  public toxiForm = [];

  public hemaData:any ;
  public microData:any ;
  public seroData:any ;
  public toxiData:any ;

  public colors = ["-", "STRAW", "LIGHT YELLOW", "YELLOW", "DARK YELLOW", "RED", "ORANGE", "AMBER"];
  public transparencys = ["-", "CLEAR", "HAZY", "SL. TURBID", "TURBID"];
  public pHs = ["-", "5.0", "6.0", "6.5", "7.0", "7.5", "8.0", "8.5", "9.0", "9.5"];
  public counts = ["NONE", "Rare", "Few", "Moderate", "Many"];
  public spGravs = ["-", "1.000", "1.005", "1.010", "1.015", "1.020", "1.025", "1.030"];
  public countNums = ["-", "Negative", "Trace", "1+", "2+", "3+", "4+"];
  public negaPosis = ["N/A", "NEGATIVE", "POSITIVE"];
  public reactives = ["N/A", "NON-REACTIVE", "REACTIVE"];
  public fecaColors = ["N/A", "BROWN", "LIGHT BROWN", "DARK BROWN", "GREEN"];
  public fecaConsiss = ["N/A", "FORMED", "SEMI-FORMED", "SOFT"];
  public fecaFindings = ["N/A", "NO OVA OR PARASITE SEEN", "Presence of: "];

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private router: Router,
    private route: ActivatedRoute,
    private math: MathService,
    private lab: LaboratoryService,
    private TS: TransactionService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<BasicTwoFormComponent>
  ) {
    this.math.navSubs("lab");

    // this.transactionId = this.route.snapshot.paramMap.get('tid');
    this.transactionId = this.dialogData.tid;

    if (isNaN(this.transactionId)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {
    this.basictwo = this.formBuilder.group({
      whiteBlood: [''],
      hemoglobin: [''],
      hemoNR: ['N/A'],
      hematocrit: [''],
      hemaNR: ['N/A'],
      cbcot: [''],
      uriColor: ['-'],
      rbc: [''],
      uriTrans: ['-'],
      wbc: [''],
      uriPh: ['-'],
      ecells: ['NONE'],
      uriSp: ['-'],
      mthreads: ['NONE'],
      uriPro: ['-'],
      bac: ['NONE'],
      uriGlu: ['-'],
      amorph: ['NONE'],
      uriOt: [''],
      coAx: ['NONE'],
      meth: ['N/A'],
      tetra: ['N/A'],
      drugtest: ['N/A'],
      hbsAG: ['N/A'],
      pregTest: ['N/A'],
      seroOt: [''],
      fecColor: ['N/A'],
      fecCon: ['N/A'],
      fecMicro: ['N/A'],
      fecOt: [''],
      medID: ['1'],
      qualityID: ['1'],
      pathID: ['5'],
      creationDate: [''],
      dateUpdate: ['']
    });

    this.TS.getOneTrans("getTransaction/" + this.transactionId).subscribe(
      data => {
        if (data[0].length == 0) {
          this.router.navigate(['error/404']);
        } else {
          this.transaction = data[0];
          this.patientId = data[0]['patientId'];
        }
      }
    );

    this.lab.getMedtech().subscribe(
      medtech => {
        this.medtech = medtech;
      }
    );



    this.lab.getMicroscopy(this.transactionId).subscribe(
      data => {
        if (data.length !== 0) {
          this.update = true;
          const result = data[0];
          this.basictwo.patchValue({
            "uriColor": result['uriColor'],
            "uriTrans": result['uriTrans'],
            "uriPh": result['uriPh'],
            "uriSp": result['uriSp'],
            "uriPro": result['uriPro'],
            "uriGlu": result['uriGlu'],
            "rbc": result['rbc'],
            "wbc": result['wbc'],
            "ecells": result['ecells'],
            "mthreads": result['mthreads'],
            "bac": result['bac'],
            "amorph": result['amorph'],
            "coAx": result['coAx'],
            "fecColor": result['fecColor'],
            "fecCon": result['fecCon'],
            "fecMicro": result['fecMicro'],
            "fecOt": result['fecOt'],
            "pregTest": result['pregTest']
          });
          this.pathID = result['pathID'];
          this.medID = result['medID'];
          this.qualityID = result['qualityID'];
          this.creationDate = result['creationDate'];
          this.dateUpdate = result['dateUpdate'];
          this.microID = result['microID'];
        }
      }
    );


  }

  addbasictwo() {
    Object.keys(this.basictwo.controls).forEach(key => {
      const value = this.basictwo.get(key).value;

      if (value == "" || value == null) {
        this.basictwo.get(key).setValue("N/A");
      }
    });


    this.microForm.push({
      "transactionID": this.transactionId,
      "patientID": this.patientId,
      "pathID": this.basictwo.get('pathID').value,
      "medID": this.basictwo.get('medID').value,
      "qualityID": this.basictwo.get('qualityID').value,
      "creationDate": moment().format("YYYY-MM-DD h:mm:ss"),
      "dateUpdate": moment().format("YYYY-MM-DD h:mm:ss"),
      "uriColor": this.basictwo.get('uriColor').value,
      "uriTrans": this.basictwo.get('uriTrans').value,
      "uriOt": this.basictwo.get('uriOt').value,
      "uriPh": this.basictwo.get('uriPh').value,
      "uriSp": this.basictwo.get('uriSp').value,
      "uriPro": this.basictwo.get('uriPro').value,
      "uriGlu": this.basictwo.get('uriGlu').value,
      "le": "N/A",
      "nit": "N/A",
      "uro": "N/A",
      "bld": "N/A",
      "ket": "N/A",
      "bil": "N/A",
      "rbc": this.basictwo.get('rbc').value,
      "wbc": this.basictwo.get('wbc').value,
      "ecells": this.basictwo.get('ecells').value,
      "mthreads": this.basictwo.get('mthreads').value,
      "bac": this.basictwo.get('bac').value,
      "amorph": this.basictwo.get('amorph').value,
      "coAx": this.basictwo.get('coAx').value,
      "fecColor": this.basictwo.get('fecColor').value,
      "fecCon": this.basictwo.get('fecCon').value,
      "fecMicro": this.basictwo.get('fecMicro').value,
      "fecOt": this.basictwo.get('fecOt').value,
      "pregTest": this.basictwo.get('pregTest').value,
      "occultBLD": "N/A",
      "afbva1": "N/A",
      "afbva2": "N/A",
      "afbr1": "N/A",
      "afbr2": "N/A",
      "afbd": "N/A"
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be saved to database!" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.lab.addMicroscopy(this.microForm[0]).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['laboratory/basictwo']);
            this.dialogRef.close();
          }
        );
      }
    });
  }

  updatebasictwo() {
    this.microForm = [];
    Object.keys(this.basictwo.controls).forEach(key => {
      const value = this.basictwo.get(key).value;

      if (value == "" || value == null) {
        this.basictwo.get(key).setValue("N/A");
      }
    });

    this.microForm.push({
      "microID": this.microID,
      "transactionID": this.transactionId,
      "patientID": this.patientId,
      "pathID": this.basictwo.get('pathID').value,
      "medID": this.basictwo.get('medID').value,
      "qualityID": this.basictwo.get('qualityID').value,
      "creationDate": this.creationDate,
      "dateUpdate": moment().format("YYYY-MM-DD h:mm:ss"),
      "uriColor": this.basictwo.get('uriColor').value,
      "uriTrans": this.basictwo.get('uriTrans').value,
      "uriOt": this.basictwo.get('uriOt').value,
      "uriPh": this.basictwo.get('uriPh').value,
      "uriSp": this.basictwo.get('uriSp').value,
      "uriPro": this.basictwo.get('uriPro').value,
      "uriGlu": this.basictwo.get('uriGlu').value,
      "le": "N/A",
      "nit": "N/A",
      "uro": "N/A",
      "bld": "N/A",
      "ket": "N/A",
      "bil": "N/A",
      "rbc": this.basictwo.get('rbc').value,
      "wbc": this.basictwo.get('wbc').value,
      "ecells": this.basictwo.get('ecells').value,
      "mthreads": this.basictwo.get('mthreads').value,
      "bac": this.basictwo.get('bac').value,
      "amorph": this.basictwo.get('amorph').value,
      "coAx": this.basictwo.get('coAx').value,
      "fecColor": this.basictwo.get('fecColor').value,
      "fecCon": this.basictwo.get('fecCon').value,
      "fecMicro": this.basictwo.get('fecMicro').value,
      "fecOt": this.basictwo.get('fecOt').value,
      "pregTest": this.basictwo.get('pregTest').value,
      "occultBLD": "N/A",
      "afbva1": "N/A",
      "afbva2": "N/A",
      "afbr1": "N/A",
      "afbr2": "N/A",
      "afbd": "N/A"
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be updated!" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.lab.addMicroscopy(this.microForm[0]).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly updated", "ok");
            } else {
              this.math.openSnackBar("Data not updated!!!", "ok");
            }
            // this.router.navigate(['laboratory/basictwo']);
            this.dialogRef.close();
          }
        );
      }
    });
  }

}

