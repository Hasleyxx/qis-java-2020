import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PeService } from 'src/app/services/pe.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: "app-report-pdf",
  templateUrl: "./report-pdf.component.html",
  styleUrls: ["./report-pdf.component.scss"]
})
export class ReportPdfComponent implements OnInit {

  @ViewChild("content") content: ElementRef;
  data: string[];
  dataDetails: Promise<any>[];
  public patientID: any;
  public transactionID: any;
  public patientDatas: any;
  public transactionDatas: any;
  public vitalDatas: any;
  public medHisDatas = [];
  public vitals = [];
  public pes = [];
  public findings: any;
  public phy: any;
  public phyLic: any;

  public date = moment().format("YYYY-MM-DD h:mm:ss");
  public hemaDatas: any = [];
  public seroDatas: any = [];
  public microDatas: any = [];
  public toxiDatas: any = [];
  public classification: any = [];

  public class: any;
  public classColor: any = "text-dark";
  public notes: any;
  public hbsag: any;
  public cbcot: any = "text-dark";
  public pt: any;
  public meth: any;
  public tetra: any;
  public urinotes: any;

  public medical: any = [];
  public quality: any = [];
  public path: any = [];
  public rad: any = [];

  constructor(
    // @Inject(MAT_DIALOG_DATA) public data,
    route: ActivatedRoute,
    private math: MathService,
    private peService: PeService,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private labService: LaboratoryService,
    private XS: XrayService
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.transactionID = this.data[0];
  }

  ngOnInit() {
    this.getPatientdata();
  }

  getPatientdata() {
    this.dataDetails = this.data.map(data => this.getReportPdfReady(data));
    this.peService.getMedhis(this.transactionID).subscribe(data => {
      if (data !== null) {
        this.medHisDatas.push(
          { label: "Astma: ", value: data.asth },
          { label: "Tuberculosis: ", value: data.tb },
          { label: "Diabetes Mellitus: ", value: data.dia },
          { label: "High Blood Pressure: ", value: data.hp },
          { label: "Heart Problem: ", value: data.hp },
          { label: "Kidney Problem: ", value: data.kp },
          { label: "Abdominal/Hernia: ", value: data.ab },
          { label: "Joint/Back/Scoliosis: ", value: data.jbs },
          { label: "Psychiatric Problem: ", value: data.pp },
          { label: "Migraine/Headache: ", value: data.mh },
          { label: "Fainting/Seizures: ", value: data.fs },
          { label: "Allergies: ", value: data.alle },
          { label: "Cancer/Tumor: ", value: data.ct },
          { label: "Hepatitis: ", value: data.hep },
          { label: "STD: ", value: data.std }
        );
      } else {
        this.medHisDatas.push(
          { label: "Astma: ", value: "-" },
          { label: "Tuberculosis: ", value: "-" },
          { label: "Diabetes Mellitus: ", value: "-" },
          { label: "High Blood Pressure: ", value: "-" },
          { label: "Heart Problem: ", value: "-" },
          { label: "Kidney Problem: ", value: "-" },
          { label: "Abdominal/Hernia: ", value: "-" },
          { label: "Joint/Back/Scoliosis: ", value: "-" },
          { label: "Psychiatric Problem: ", value: "-" },
          { label: "Migraine/Headache: ", value: "-" },
          { label: "Fainting/Seizures: ", value: "-" },
          { label: "Allergies: ", value: "-" },
          { label: "Cancer/Tumor: ", value: "-" },
          { label: "Hepatitis: ", value: "-" },
          { label: "STD: ", value: "-" }
        );
      }
    });
    this.peService.getVital(this.transactionID).subscribe(data => {
      this.vitalDatas = data;
      if (data !== null) {
        this.vitals.push(
          { label: "Ishihara Test: ", value: data.cv },
          { label: "Hearing: ", value: data.hearing },
          { label: "Hospitalization: ", value: data.hosp },
          { label: "Operations: ", value: data.opr },
          { label: "Medications: ", value: data.pm },
          { label: "Smoker: ", value: data.smoker },
          { label: "Alcoholic: ", value: data.ad },
          { label: "Last Menstrual: ", value: data.lmp },
          { label: "Others/Notes: ", value: data.notes }
        );
      } else {
        this.vitals.push(
          { label: "Ishihara Test: ", value: "-" },
          { label: "Hearing: ", value: "-" },
          { label: "Hospitalization: ", value: "-" },
          { label: "Operations: ", value: "-" },
          { label: "Medications: ", value: "-" },
          { label: "Smoker: ", value: "-" },
          { label: "Alcoholic: ", value: "-" },
          { label: "Last Menstrual: ", value: "-" },
          { label: "Others/Notes: ", value: "-" }
        );
      }
    });
    this.peService.getPE(this.transactionID).subscribe(data => {
      if (data !== null) {
        this.pes.push(
          { label: "Skin: ", value: data.skin },
          { label: "Head and Neck: ", value: data.hn },
          { label: "Chest/Breast/Lungs: ", value: data.cbl },
          { label: "Cardiac/Heart: ", value: data.ch },
          { label: "Abdomen: ", value: data.abdo },
          { label: "Extremities: ", value: data.extre },
          { label: "Others/Notes: ", value: data.ot }
        );

        this.findings = data.find;
        this.phy = data.doctor;
        this.phyLic = data.license;
      } else {
        this.pes.push(
          { label: "Skin: ", value: "-" },
          { label: "Head and Neck: ", value: "-" },
          { label: "Chest/Breast/Lungs: ", value: "-" },
          { label: "Cardiac/Heart: ", value: "-" },
          { label: "Abdomen: ", value: "-" },
          { label: "Extremities: ", value: "-" },
          { label: "Others/Notes: ", value: "-" }
        );

        this.findings = "-";
        this.phy = "FROILAN A. CANLAS, M.D.";
        this.phyLic = "82498";
      }
    });

    this.transactionService
      .getOneTrans("getTransaction/" + this.transactionID)
      .subscribe(data => {
        this.transactionDatas = data[0];
        this.patientID = data[0]["patientId"];
        this.patientService
          .getOnePatient("getPatient/" + this.patientID)
          .subscribe(data => (this.patientDatas = data[0]));
      });

    // Transaction
    this.transactionService
      .getOneTrans("getTransaction/" + this.transactionID)
      .subscribe(data => {
        this.patientID = data[0]["patientId"];
        // Patient
        this.patientService
          .getOnePatient("getPatient/" + this.patientID)
          .subscribe(data => (this.patientDatas = data[0]));
      });

    // Hematology
    this.labService.getHematology(this.transactionID).subscribe(data => {
      if (data.length !== 0) {
        this.hemaDatas = data[0];
        if (data[0]["cbcot"] == "REACTIVE") {
          this.cbcot = "text-danger";
        }
      }
    });

    // Microscopy
    this.labService.getMicroscopy(this.transactionID).subscribe(data => {
      this.microDatas = data[0];

      if (data[0] !== undefined) {
        if (this.microDatas.pregTest == "POSITIVE") {
          this.pt = "text-danger";
        }

        if (
          this.microDatas.uriOt !== "NORMAL" &&
          this.microDatas.uriOt !== ""
        ) {
          this.urinotes = "text-danger";
        }

        this.labService.getPersonnel(this.microDatas.medID).subscribe(data => {
          this.medical.push({
            name:
              data[0].firstName +
              " " +
              data[0].middleName +
              " " +
              data[0].lastName +
              ", " +
              data[0].positionEXT,
            license: data[0].licenseNO
          });
          this.medical = this.medical[0];
        });
        this.labService
          .getPersonnel(this.microDatas.qualityID)
          .subscribe(data => {
            if (data.length !== 0) {
              this.quality.push({
                name:
                  data[0].firstName +
                  " " +
                  data[0].middleName +
                  " " +
                  data[0].lastName +
                  ", " +
                  data[0].positionEXT,
                license: data[0].licenseNO
              });
              this.quality = this.quality[0];
            }
          });
        this.labService.getPersonnel(this.microDatas.pathID).subscribe(data => {
          this.path.push({
            name:
              data[0].firstName +
              " " +
              data[0].middleName +
              " " +
              data[0].lastName +
              ", " +
              data[0].positionEXT,
            license: data[0].licenseNO
          });
          this.path = this.path[0];
        });
      } else {
        this.pt = "text-dark";
        this.urinotes = "text-dark";
        this.medical.push({
          name: "-",
          license: "-"
        });

        this.quality.push({
          name: "-",
          license: "-"
        });

        this.path.push({
          name: "-",
          license: "-"
        });
      }
    });

    // Serology
    this.labService.getSerology(this.transactionID).subscribe(data => {
      this.seroDatas = data[0];

      if (data[0] !== undefined) {
        if (this.seroDatas.hbsAG == "REACTIVE") {
          this.hbsag = "text-danger";
        }
      } else {
        this.hbsag = "text-dark";
      }
    });

    // Toxicology
    this.labService.getToxicology(this.transactionID).subscribe(data => {
      this.toxiDatas = data[0];

      if (data[0] !== undefined) {
        if (this.toxiDatas.meth == "POSITIVE") {
          this.meth = "text-danger";
        }

        if (this.toxiDatas.tetra == "POSITIVE") {
          this.tetra = "text-danger";
        }
      } else {
        this.meth = "text-dark";
        this.tetra = "text-dark";
      }
    });

    // Class
    this.labService.getOneClass(this.transactionID).subscribe(data => {
      this.classification = data;

      if (data !== null) {
        if (this.classification.medicalClass == "CLASS A - Physically Fit") {
          this.class = "Class A - FIT TO WORK";
        } else if (
          this.classification.medicalClass ==
          "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency"
        ) {
          this.class = "Class B - FIT TO WORK";
        } else if (
          this.classification.medicalClass ==
          "CLASS C - With abnormal findings generally not accepted for employment."
        ) {
          this.class =
            "CLASS C - With abnormal findings generally not accepted for employment.";
        } else if (
          this.classification.medicalClass == "CLASS D - Unemployable"
        ) {
          this.class = "Class D - UNEMPLOYABLE";
        } else {
          this.class = "PENDING";
        }

        if (
          this.classification.notes != "NORMAL" &&
          this.classification.notes != ""
        ) {
          this.notes = "text-danger";
        }

        if (
          this.class == "Class D - UNEMPLOYABLE" ||
          this.class ==
            "CLASS C - With abnormal findings generally not accepted for employment."
        ) {
          this.classColor = "text-danger";
        }
      } else {
        this.class = "PENDING";
        this.notes = "text-dark";
        this.classColor = "text-dark";
      }
    });

    // Radiology
    this.XS.getOneXray(this.transactionID).subscribe(data => {
      if (data == null) {
        this.rad.push({
          qa: "-",
          radiologist: "-"
        });
      } else {
        this.rad = data;
      }
    });

    setTimeout(() => {
      let div = this.content.nativeElement;
      const options = {
        background: "white",
        scale: 1
      };
      html2canvas(div, options).then(canvas => {
        var img = canvas.toDataURL("image/PNG");
        var doc = new jsPDF("p", "mm", "legal", 1);


        // Add image Canvas to PDF
        const bufferX = 1;
        const bufferY = 1;
        const imgProps = (<any>doc).getImageProperties(img);
        let pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
        let pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        doc.addImage(
          img,
          "PNG",
          bufferX,
          bufferY,
          pdfWidth,
          pdfHeight - 63,
          undefined,
          "FAST"
        );

        return doc;
      })
      .then(doc => {
        let name = this.patientDatas.lastName + ', ' + this.patientDatas.firstName + ' ' + this.patientDatas.middleName;
        doc.save(name + " MEDICAL RESULT.pdf");
        // this.dialogRef.close();
      });

      Promise.all(this.dataDetails).then(() => {
        this.math.onReportPdfReady();
      });
    }, 1000);
  }

  getReportPdfReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }
}
