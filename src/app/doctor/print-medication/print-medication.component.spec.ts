import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintMedicationComponent } from './print-medication.component';

describe('PrintMedicationComponent', () => {
  let component: PrintMedicationComponent;
  let fixture: ComponentFixture<PrintMedicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintMedicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintMedicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
