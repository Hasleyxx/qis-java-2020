import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-nurse-patient-summary',
  templateUrl: './nurse-patient-summary.component.html',
  styleUrls: ['./nurse-patient-summary.component.scss']
})
export class NursePatientSummaryComponent implements OnInit {

  public peSummary = "peSummary";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("nurse");
  }
  ngOnInit() {
  }

}
