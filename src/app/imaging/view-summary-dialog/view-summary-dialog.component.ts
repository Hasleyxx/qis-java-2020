import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ImagingSummaryUpdateComponent } from '../imaging-summary-update/imaging-summary-update.component';
import { SnackSuccessComponent } from 'src/app/element/snack-success/snack-success.component';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: 'app-view-summary-dialog',
  templateUrl: './view-summary-dialog.component.html',
  styleUrls: ['./view-summary-dialog.component.scss']
})
export class ViewSummaryDialogComponent implements OnInit {


  public patientId: any;
  public transactionId: any;
  public patientDatas: any;
  public fullName: any;
  public transactionDatas: any;
  public vitalDatas: any;
  public medHisDatas = [];
  public vitals = [];
  public pes = [];
  public rad: any = [];;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private dialog: MatDialog,
    private XS: XrayService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getPatientdata();

  }

  xrayUpdate(tid) {
    const dialogRef = this.dialog.open(ImagingSummaryUpdateComponent, {
      data: {
        tid: tid,
        title: "UPDATE"
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.rad = [];
        this.rad['comment'] = result['comment'];
        this.rad['impression'] = result['impression'];
        this.rad['qa'] = result['qa'];
        this.rad['radiologist'] = result['radiologist'];
        this.openSnackBar("Radiology report has been updated!", "close");
      }
    });
  }

  getPatientdata() {
    this.transactionId = this.data.tid;
    this.fullName = this.data.fn;
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientId = data[0]['patientId'];
        this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => {
            this.patientDatas = data[0];
            this.fullName = data[0].fullName;
            this.patientId = data[0]['patientId'];
          }
        );
      }
    );

    // Radiology
    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        if (data == null) {
          this.rad.push({
            'qa': '-',
            'radiologist': '-'
          });

        } else {
          this.rad = data;
        }

      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
