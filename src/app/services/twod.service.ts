import { Injectable } from '@angular/core';
import { Global } from '../global.variable';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { echo } from './service.interface';
import { retry, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TwodService {

  constructor(
    private global: Global,
    private http: HttpClient,
    private ehs: ErrorService
  ) { }

  getTwod(tid: number): Observable<echo> {
    return this.http.get<echo>(this.global.url + "/getTwod/" + tid)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  addTwod(echo: echo): Observable<echo> {
    return this.http.post<echo>(this.global.url + "/addTwod", echo)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  updateTwod(echo: echo): Observable<echo> {
    return this.http.post<echo>(this.global.url + "/updateTwod", echo)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }
}
