import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { docTrans } from 'src/app/services/service.interface';
import { PatientRecComponent } from 'src/app/doctor/patient-rec/patient-rec.component';
import { SalesDialogComponent } from 'src/app/doctor/sales-dialog/sales-dialog.component';
import { DocTransactionService } from 'src/app/services/doc-transaction.service';
import { Generate } from 'src/app/page/report-list/report-list.component';

export interface consultationTemp {
  id: number,
  patientId: number,
  date: string,
  patient: string,
  paid: boolean
}

@Component({
  selector: "consultation",
  templateUrl: "./consultation.component.html",
  styleUrls: ["./consultation.component.scss"]
})
export class ConsultationComponent implements OnInit {
  @Input() type: string;

  d = new DatePipe("en-US");
  from: FormControl = new FormControl(
    this.d.transform(new Date(), "yyyy-MM-dd 06:00:00")
  );
  to: FormControl = new FormControl(
    this.d.transform(new Date(), "yyyy-MM-dd 22:00:00")
  );

  consultationTemp: consultationTemp[] = [];

  dataSource: MatTableDataSource<consultationTemp>;
  displayedColumns: string[] = ["id", "date", "patient", "action"];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  gen: Generate[] = [
    {value: "sr", viewValue: 'Sales Report'},
    {value: 'csv', viewValue: 'Generate CSV'},
    {value: 'pf', viewValue: 'Generate PF'}
  ];

  generateFile: FormControl = new FormControl("", [Validators.required]);

  constructor(
    private DTS: DocTransactionService,
    private PS: PatientService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData() {
    this.consultationTemp = [];

    let url: any;
    let from = new Date(this.from.value);
    let fromData = this.d.transform(from, "yyyy-MM-dd HH:mm:ss");
    let to = new Date(this.to.value);
    let toData = this.d.transform(to, "yyyy-MM-dd HH:mm:ss");

    this.DTS.getDocTransactions(fromData, toData).subscribe(data => {
      if (data.length > 0) {
        data.forEach((element: docTrans) => {
          let transactionId = element.transactionDocId;
          let patientId = element.patientId;
          let transactionDate = element.transactionDocDate;
          let paid: boolean;
          if (element.itemId == null) {
            paid = false;
          } else {
            paid = true;
          }

          this.PS.getOnePatient("/getPatient/" + patientId).subscribe(
            patient => {
              let patientName = patient[0].fullName;
              let consultationTemp = {
                id: transactionId,
                patientId: patientId,
                date: transactionDate,
                patient: patientName,
                paid: paid
              };

              this.consultationTemp.push(consultationTemp);
              this.dataSource = new MatTableDataSource(this.consultationTemp);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            }
          );
        });
      } else {
        this.dataSource = new MatTableDataSource([]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  patientRec(pid) {
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: pid
      }
    });
  }

  patientPay(tid, pid, type: string) {
    const dialogRef = this.dialog.open(SalesDialogComponent, {
      data: {
        tid: tid,
        pid: pid,
        type: type
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "paid") {
        this.getData();
      }
    });
  }
}
