import { Component, OnInit, Inject } from '@angular/core';
import { PeService } from 'src/app/services/pe.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { PeUpdateMedhisComponent } from 'src/app/pe/pe-update-medhis/pe-update-medhis.component';
import { PeUpdateVitalComponent } from 'src/app/pe/pe-update-vital/pe-update-vital.component';
import { PeUpdatePeComponent } from 'src/app/pe/pe-update-pe/pe-update-pe.component';

@Component({
  selector: 'app-dialog-pe-data',
  templateUrl: './dialog-pe-data.component.html',
  styleUrls: ['./dialog-pe-data.component.scss']
})
export class DialogPeDataComponent implements OnInit {

  public patientId: any;
  public transactionId: any;
  public patientDatas: any;
  public transactionDatas: any;
  public vitalDatas: any;
  public medHisDatas = [];
  public vitals = [];
  public pes = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private peService: PeService,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getPatientdata();
  }

  getPatientdata() {
    this.transactionId = this.data.tid;

    this.peService.getMedhis(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.medHisDatas.push(
            { "label": "Astma: ", "value": data.asth },
            { "label": "Tuberculosis: ", "value": data.tb },
            { "label": "Diabetes Mellitus: ", "value": data.dia },
            { "label": "High Blood Pressure: ", "value": data.hp },
            { "label": "Heart Problem: ", "value": data.hp },
            { "label": "Kidney Problem: ", "value": data.kp },
            { "label": "Abdominal/Hernia: ", "value": data.ab },
            { "label": "Joint/Back/Scoliosis: ", "value": data.jbs },
            { "label": "Psychiatric Problem: ", "value": data.pp },
            { "label": "Migraine/Headache: ", "value": data.mh },
            { "label": "Fainting/Seizures: ", "value": data.fs },
            { "label": "Allergies: ", "value": data.alle },
            { "label": "Cancer/Tumor: ", "value": data.ct },
            { "label": "Hepatitis: ", "value": data.hep },
            { "label": "STD/PLHIV: ", "value": data.std }
          );

        } else {
          this.medHisDatas.push(
            { "label": "Astma: ", "value": '-' },
            { "label": "Tuberculosis: ", "value": '-' },
            { "label": "Diabetes Mellitus: ", "value": '-' },
            { "label": "High Blood Pressure: ", "value": '-' },
            { "label": "Heart Problem: ", "value": '-' },
            { "label": "Kidney Problem: ", "value": '-' },
            { "label": "Abdominal/Hernia: ", "value": '-' },
            { "label": "Joint/Back/Scoliosis: ", "value": '-' },
            { "label": "Psychiatric Problem: ", "value": '-' },
            { "label": "Migraine/Headache: ", "value": '-' },
            { "label": "Fainting/Seizures: ", "value": '-' },
            { "label": "Allergies: ", "value": '-' },
            { "label": "Cancer/Tumor: ", "value": '-' },
            { "label": "Hepatitis: ", "value": '-' },
            { "label": "STD: ", "value": '-' }
          );
        }
      }
    );
    this.peService.getVital(this.transactionId).subscribe(
      data => {
        this.vitalDatas = data;
        if (data !== null) {
          this.vitals.push(
            { "label": "Ishihara Test: ", "value": data.cv },
            { "label": "Hearing: ", "value": data.hearing },
            { "label": "Hospitalization: ", "value": data.hosp },
            { "label": "Operations: ", "value": data.opr },
            { "label": "Medications: ", "value": data.pm },
            { "label": "Smoker: ", "value": data.smoker },
            { "label": "Alcoholic: ", "value": data.ad },
            { "label": "Last Menstrual: ", "value": data.lmp },
            { "label": "Others/Notes: ", "value": data.notes }
          );

        } else {
          this.vitals.push(
            { "label": "Ishihara Test: ", "value": '-' },
            { "label": "Hearing: ", "value": '-' },
            { "label": "Hospitalization: ", "value": '-' },
            { "label": "Operations: ", "value": '-' },
            { "label": "Medications: ", "value": '-' },
            { "label": "Smoker: ", "value": '-' },
            { "label": "Alcoholic: ", "value": '-' },
            { "label": "Last Menstrual: ", "value": '-' },
            { "label": "Others/Notes: ", "value": '-' }
          );
        }
      }
    );
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.pes.push(
            { "label": "Skin: ", "value": data.skin },
            { "label": "Head and Neck: ", "value": data.hn },
            { "label": "Chest/Breast/Lungs: ", "value": data.cbl },
            { "label": "Cardiac/Heart: ", "value": data.ch },
            { "label": "Abdomen: ", "value": data.abdo },
            { "label": "Extremities: ", "value": data.extre },
            { "label": "Others/Notes: ", "value": data.ot },
            { "label": "Findings: ", "value": data.find },
            { "label": "Physican: ", "value": data.doctor },
            { "label": "License: ", "value": data.license }
          );

        } else {
          this.pes.push(
            { "label": "Skin: ", "value": '-' },
            { "label": "Head and Neck: ", "value": '-' },
            { "label": "Chest/Breast/Lungs: ", "value": '-' },
            { "label": "Cardiac/Heart: ", "value": '-' },
            { "label": "Abdomen: ", "value": '-' },
            { "label": "Extremities: ", "value": '-' },
            { "label": "Others/Notes: ", "value": '-' },
            { "label": "Findings: ", "value": '-' },
            { "label": "Physican: ", "value": '-' },
            { "label": "License: ", "value": '-' }
          );
        }
      }
    );
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientId = data[0]['patientId'];

        this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );
  }

  updateMedhis(tid) {
    const dialogRef = this.dialog.open(PeUpdateMedhisComponent, {
      data: {
        tid: tid
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.medHisDatas = [];
        this.medHisDatas.push(
          { "label": "Astma: ", "value": result.asth },
          { "label": "Tuberculosis: ", "value": result.tb },
          { "label": "Diabetes Mellitus: ", "value": result.dia },
          { "label": "High Blood Pressure: ", "value": result.hp },
          { "label": "Heart Problem: ", "value": result.hp },
          { "label": "Kidney Problem: ", "value": result.kp },
          { "label": "Abdominal/Hernia: ", "value": result.ab },
          { "label": "Joint/Back/Scoliosis: ", "value": result.jbs },
          { "label": "Psychiatric Problem: ", "value": result.pp },
          { "label": "Migraine/Headache: ", "value": result.mh },
          { "label": "Fainting/Seizures: ", "value": result.fs },
          { "label": "Allergies: ", "value": result.alle },
          { "label": "Cancer/Tumor: ", "value": result.ct },
          { "label": "Hepatitis: ", "value": result.hep },
          { "label": "STD: ", "value": result.std }
        );
      }
    });
  }

  updateVital(tid) {
    const dialogRef = this.dialog.open(PeUpdateVitalComponent, {
      data: {
        tid: tid
      },
      width: '60vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.vitalDatas = result;
        this.vitals = [];
        this.vitals.push(
          { "label": "Ishihara Test: ", "value": result.cv },
          { "label": "Hearing: ", "value": result.hearing },
          { "label": "Hospitalization: ", "value": result.hosp },
          { "label": "Operations: ", "value": result.opr },
          { "label": "Medications: ", "value": result.pm },
          { "label": "Smoker: ", "value": result.smoker },
          { "label": "Alcoholic: ", "value": result.ad },
          { "label": "Last Menstrual: ", "value": result.lmp },
          { "label": "Others/Notes: ", "value": result.notes }
        );
      }
    });
  }

  updatePe(tid) {
    const dialogRef = this.dialog.open(PeUpdatePeComponent, {
      data: {
        tid: tid
      },
      width: '60vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.pes = [];
        this.pes.push(
          { "label": "Skin: ", "value": result.skin },
          { "label": "Head and Neck: ", "value": result.hn },
          { "label": "Chest/Breast/Lungs: ", "value": result.cbl },
          { "label": "Cardiac/Heart: ", "value": result.ch },
          { "label": "Abdomen: ", "value": result.abdo },
          { "label": "Extremities: ", "value": result.extre },
          { "label": "Others/Notes: ", "value": result.ot },
          { "label": "Findings: ", "value": result.find },
          { "label": "Physican: ", "value": result.doctor },
          { "label": "License: ", "value": result.license }
        );
      }
    });
  }

}
