import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerotoxicResultComponent } from './serotoxic-result.component';

describe('SerotoxicResultComponent', () => {
  let component: SerotoxicResultComponent;
  let fixture: ComponentFixture<SerotoxicResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerotoxicResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerotoxicResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
