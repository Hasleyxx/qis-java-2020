import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearanceHistoryComponent } from './clearance-history.component';

describe('ClearanceHistoryComponent', () => {
  let component: ClearanceHistoryComponent;
  let fixture: ComponentFixture<ClearanceHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearanceHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearanceHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
