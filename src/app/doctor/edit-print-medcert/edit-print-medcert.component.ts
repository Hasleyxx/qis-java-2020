import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { MathService } from 'src/app/services/math.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MedcertHistoryComponent } from '../medcert-history/medcert-history.component';

@Component({
  selector: 'app-edit-print-medcert',
  templateUrl: './edit-print-medcert.component.html',
  styleUrls: ['./edit-print-medcert.component.scss']
})
export class EditPrintMedcertComponent implements OnInit {

  public medcertId: any;
  public patientId: any;
  public patientData: any;

  public form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private PS: PatientService,
    public math: MathService,
    private DS: DoctorService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EditPrintMedcertComponent>,
    private formBuilder: FormBuilder
  ) {
    this.patientId = this.dialogData.pid;
    this.medcertId = this.dialogData.medcertId;
  }

  ngOnInit() {
    this.getData();

    this.form = this.formBuilder.group({
      value1: [''],
      value2: [''],
      value3: [''],
      value4: ['']
    });
  }

  getData() {
    this.PS.getOnePatient('/getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });

    this.DS.getDocMedcert(this.medcertId).subscribe(data => {
      if (data !== null) {
        let tempValue1 = "This is to certify that " + this.patientData.fullName + ", years old from " + this.patientData.address + " was personally seen/ examined/ managed by undersigned " + this.math.dateNow() + " due to " + data.due;

        let tempValue2 = "Diagnosis: " + data.diagnosis;
        let tempValue3 = "Patient is advised bedrest for " + data.days + " day(s).";
        let tempValue4 = "Remarks: " + data.remarks;

        this.form.patchValue({
          value1: tempValue1,
          value2: tempValue2,
          value3: tempValue3,
          value4: tempValue4
        });
      }
    });
  }

  print() {
    let value1: string, value2: string, value3: string, value4: string;
    value1 = this.form.get("value1").value;
    value2 = this.form.get("value2").value;
    value3 = this.form.get("value3").value;
    value4 = this.form.get("value4").value;

    sessionStorage.setItem("value1", JSON.stringify(value1));
    sessionStorage.setItem("value2", JSON.stringify(value2));
    sessionStorage.setItem("value3", JSON.stringify(value3));
    sessionStorage.setItem("value4", JSON.stringify(value4));

    this.dialogRef.close();
    const suffix = [this.patientId, 'medcert'];
    this.math.printDoc('', suffix);
  }

  cancel() {
    this.dialogRef.close();

    this.dialog.open(MedcertHistoryComponent, {
      data: {
        pid: this.patientId
      },
      disableClose: true
    });
  }

}
