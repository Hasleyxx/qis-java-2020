import { Component, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { navList } from './services/service.interface';
import { MathService } from './services/math.service';
import { UserService } from './services/user.service';
import { MatDialog } from '@angular/material';
import { AccountSettingComponent } from './element/account-setting/account-setting.component';
import { ConfirmComponent } from './element/confirm/confirm.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit{
  title = 'QIS JAVA';
  fillerNav: navList[];
  navLink: navList[];
  isNav: boolean = true;

  private _mobileQueryListener: () => void;
  mobileQuery: any;

  constructor(
    changeDetectorRef     : ChangeDetectorRef,
    public media          : MediaMatcher,
    private math          : MathService,
    private user          : UserService,
    private accountDialog : MatDialog) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.fillerNav = [
      {name: "Cashier", route: "cashier/transact", icon: "attach_money"},
      {name: "Laboratory", route: "laboratory/microscopy", icon: "local_hospital"},
      {name: "Imaging", route: "imaging/markers", icon: "flip"},
      {name: "Quality Control", route: "qc/email-results", icon: "assignment_ind"},
      {name: "Physical Examination", route: "pe/list", icon: "assessment"},
      {name: "Nurse", route: "nurse/classification", icon: "healing"},
      {name: "Inventory", route: "elements/released-results", icon: "storage" },
      {name: "Secretary", route: "doctor/registration", icon: "person_pin"},
      {name: "Doctor", route: "doctor/docpatient-list", icon: "sentiment_very_satisfied"},
      {name: "Queue", route: "queue", icon: "queue" },
      {name: "Chat", route: "chat/chat-log", icon: "message"},

    ];
  }

  ngOnInit(){
    this.math.changeEmitted$.subscribe(
      data => {
        this.navLink = data;
      }
    )
    this.math.changeVis.subscribe(
      data => {
        this.isNav = data;
      }
    )

    this.math.checkDefaultValues();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logout():void {
    this.user.logout();
  }

  accountSetting() {
    const dialogRef = this.accountDialog.open(AccountSettingComponent, {
      data: {
        userID: this.math.userID
      },
      width: '40%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "Updated") {
        const infoRef = this.accountDialog.open(ConfirmComponent, {
          data: {
            Title: "INFORMATION!", Content: "Update your password after 1 month!"
          },
          width: '25%'
        });
      }
    });
  }
}
