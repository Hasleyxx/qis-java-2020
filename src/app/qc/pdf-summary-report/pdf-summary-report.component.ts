import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TransactionService } from 'src/app/services/transaction.service';
import { PatientService } from 'src/app/services/patient.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { ExportAsService, ExportAsConfig } from "ngx-export-as";
import { MatDialog } from '@angular/material';
import { LoadingCssComponent } from 'src/app/element/loading-css/loading-css.component';
import { PeService } from 'src/app/services/pe.service';
import * as moment from 'moment';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: "app-pdf-summary-report",
  templateUrl: "./pdf-summary-report.component.html",
  styleUrls: ["./pdf-summary-report.component.scss"]
})
export class PdfSummaryReportComponent implements OnInit {

  exportAsConfig: ExportAsConfig = {
    type: "pdf", // the type you want to download
    elementId: "myTableElementId" // the id of html/table element
  };

  @ViewChild("content") content: ElementRef;
  data: string[];
  dataDetails: Promise<any>[];
  d = new DatePipe("en-US");
  heldData = [];
  displayedColumns = [
    "date",
    "name",
    "age",
    "chestxray",
    "urinalysis",
    "fecalysis",
    "cbc",
    "pregnancytest",
    "ishihara",
    "drugtest",
    "classification",
    "remarks"
  ];
  public datas = [];
  public url: any;
  public companyName: any;
  public loads: Promise<any>;
  public dialogRef: any;

  constructor(
    private TS: TransactionService,
    private PS: PatientService,
    private LS: LaboratoryService,
    private peService: PeService,
    private XS: XrayService,
    route: ActivatedRoute,
    private math: MathService,
    private exportAsService: ExportAsService,
    private dialog: MatDialog
  ) {
    this.data = route.snapshot.params["data"].split(",");
    this.url = this.data[0];
    this.companyName = this.data[1];
  }

  ngOnInit() {
    this.dialogRef = this.dialog.open(LoadingCssComponent, {
      data: {
        title: 'Generating PDF Summary Report'
      }
    });
    this.getData();
  }

  export() {
    setTimeout(() => {
      let dateUpdate = moment().format("MM-DD-YYYY");
      // download the file using old school javascript method
      this.exportAsService.save(this.exportAsConfig, this.companyName + " " + dateUpdate).subscribe(() => {
        // save started
      });
      this.math.onPdfSummaryReportReady();
      this.dialogRef.close();
    }, 2000);
  }

  getData() {
    this.dataDetails = this.data.map(data =>
      this.getPdfSummaryReportReady(data)
    );

    this.TS.getTransactions(this.url).subscribe(data => {
      this.heldData = [];

      if (data.length > 0) {
        let loads = new Promise((resolve, reject) => {
          data.forEach((trans, index, array) => {
            let transactionId = trans.transactionId;
            let patientId = trans.patientId;

            this.PS.getOnePatient("getPatient/" + patientId).subscribe(
              result => {
                let patInfo = result[0];

                let transData = {
                  date: null,
                  name: null,
                  age: null,
                  chestxray: null,
                  urinalysis: null,
                  fecalysis: null,
                  cbc: null,
                  pregnancytest: null,
                  ishihara: null,
                  drugtest: null,
                  classification: null,
                  remarks: null
                };

                transData['date'] = trans.transactionDate;
                transData['name'] = patInfo["fullName"];
                transData['age'] = patInfo["age"];
                // Hematology
                this.LS.getHematology(transactionId).subscribe(hema => {
                  if (hema.length > 0) {
                    transData['cbc'] = hema[0]["cbcot"];
                  }
                });

                // Microsocopy
                this.LS.getMicroscopy(transactionId).subscribe(micro => {
                  if (micro.length > 0) {
                    if (
                      micro[0]["fecMicro"] == "NO OVA OR PARASITE SEEN" ||
                      micro[0]["fecMicro"] == "NO INTESTINAL PARASITE SEEN"
                    ) {
                      transData['fecalysis'] = "NIPS";
                    } else if (
                      micro[0]["fecMicro"] == "Presence of:" ||
                      micro[0]["fecMicro"] == "Presence of: "
                    ) {
                      transData['fecalysis'] = micro[0]["fecOt"];
                    } else {
                      transData['fecalysis'] = "NO SPECIMEN";
                    }

                    transData['urinalysis'] = micro[0]["uriOt"];
                    transData['pregnancytest'] = micro[0]["pregTest"];
                  }
                });

                // Xray
                this.XS.getOneXray(transactionId).subscribe(xray => {
                  if (xray) {
                    if (
                      xray["impression"] == "NORMAL CHEST FINDINGS" ||
                      xray["impression"] == "CONSIDERED NORMAL CHEST PA"
                    ) {
                      transData['chestxray'] = "NORMAL";
                    } else if (
                      xray["impression"] == "NORMAL CHEST FINDINGS"
                    ) {
                      transData['chestxray'] = "NORMAL";
                    } else if (
                      xray["impression"] == "" ||
                      xray["impression"] == null
                    ) {
                      transData['chestxray'] = "NO XRAY";
                    } else {
                      transData['chestxray'] = xray["impression"];
                    }
                  }
                });

                // Classification
                this.LS.getOneClass(transactionId).subscribe(medClass => {
                  if (medClass) {
                    if (medClass["medicalClass"] == "CLASS A - Physically Fit") {
                      transData['classification'] = "Class A";
                      transData['remarks'] = "FIT TO WORK";
                    } else if (
                      medClass["medicalClass"] ==
                      "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency"
                    ) {
                      transData['classification'] = "Class B";
                      transData['remarks'] = "FIT TO WORK";
                    } else if (
                      medClass["medicalClass"] ==
                      "CLASS C - With abnormal findings generally not accepted for employment."
                    ) {
                      transData['classification'] = "Class C";
                      transData['remarks'] = "Unemployable";
                    } else if (
                      medClass["medicalClass"] == "CLASS D - Unemployable"
                    ) {
                      transData['classification'] = "Class D";
                      transData['remarks'] = "Unemployable";
                    } else {
                      transData['classification'] = "PENDING";
                      transData['remarks'] = medClass["notes"];
                    }
                  }
                });

                // Toxicology
                this.LS.getToxicology(transactionId).subscribe(data => {
                  if (data.length != 0) {
                    let toxic = data[0];
                    transData['drugtest'] = toxic.drugtest;
                  }
                });

                if (this.companyName == "DNATA" ||this.companyName == "DNATA TRAVEL INC") {
                  transData['ishihara'] = "15/15";
                } else {
                  this.peService.getVital(transactionId).subscribe(
                    vitals => {
                      if (vitals !== null) {
                        transData['ishihara'] = vitals.cv;
                      } else {
                        transData['ishihara'] = "-";
                      }
                    }
                  );
                }

                this.heldData.push(transData);
                this.datas = this.heldData;
                if (index === array.length - 1) resolve();
                // PE
                /* this.PES.getPE(transactionId).subscribe(
                  pe => {
                    if (pe) {
                      this.heldData.push(transData);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  }
                ); */
              }
            );
          });
        });

        loads.then(() => {
          this.export();
        });
      } else {
        this.datas = [];

        this.loads.then(() => {
          this.export();
        });
      }
    });
  }

  getPdfSummaryReportReady(data) {
    const amount = Math.floor(Math.random() * 100);
    return new Promise(resolve => setTimeout(() => resolve({ amount }), 10000));
  }
}
