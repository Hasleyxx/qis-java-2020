import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-xray-summary',
  templateUrl: './xray-summary.component.html',
  styleUrls: ['./xray-summary.component.scss']
})
export class XraySummaryComponent implements OnInit {

  public summary = "imagingSummary";

  constructor(
    private math: MathService,
  ) {
    this.math.navSubs("imaging")
  }

  ngOnInit() {

  }

}
