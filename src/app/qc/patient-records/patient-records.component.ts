import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-patient-records',
  templateUrl: './patient-records.component.html',
  styleUrls: ['./patient-records.component.scss']
})
export class PatientRecordsComponent implements OnInit {

  public peSummary = "peSummary";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("qc");
  }
  ngOnInit() {
  }


}
