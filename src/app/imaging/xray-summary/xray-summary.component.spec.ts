import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XraySummaryComponent } from './xray-summary.component';

describe('XraySummaryComponent', () => {
  let component: XraySummaryComponent;
  let fixture: ComponentFixture<XraySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XraySummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XraySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
