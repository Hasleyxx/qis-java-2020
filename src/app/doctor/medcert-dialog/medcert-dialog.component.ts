
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { ItemService } from 'src/app/services/item.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { DoctorService } from 'src/app/services/doctor.service';
import { MedcertHistoryComponent } from '../medcert-history/medcert-history.component';


@Component({
  selector: 'app-medcert-dialog',
  templateUrl: './medcert-dialog.component.html',
  styleUrls: ['./medcert-dialog.component.scss']
})
export class MedcertDialogComponent implements OnInit {

  public patientId: any;
  public type: any;

  public patientDatas: any = [];
  public transDatas: any = [];
  public availed: any;
  public description: any;
  public transactionType: any;
  public addMedcert: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private itemService: ItemService,
    private doctor: DoctorService,
    private formBuilder: FormBuilder,
    private TS: TransactionService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<MedcertDialogComponent>
  ) {
    this.patientId = this.data.pid;
  }

  ngOnInit() {
    this.getData();

    this.addMedcert = this.formBuilder.group({
      patientID: [''],
      due: ['', Validators.required],
      diagnosis: ['', Validators.required],
      remarks: ['', Validators.required],
      days: ['', Validators.required],
      dateCreated: [moment().format("YYYY-MM-DD h:mm:ss")],

    });
  }

  saveMedcert() {
    this.addMedcert.patchValue({
      patientID: +this.patientId
    });
    const dialogRefConfirm = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Patient medcert will be saved!"
      },
      width: '20%'
    });

    dialogRefConfirm.afterClosed().subscribe(
      result => {
        if (result == "ok") {

          this.doctor.addDocMedcert(this.addMedcert.value).subscribe(
            response => {
              if (response == 1) {
                this.openSnackBar("Patient medcert has been created.", "close");
                this.dialogRef.close("added");

                const dialogRef = this.dialog.open(MedcertHistoryComponent, {
                  data: {
                    pid: this.patientId
                  },
                  disableClose: true
                });
              }
            }
          );
        }
      }
    );
  }

  getData() {
    // Patient
    // console.log(this.patientId)
    this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => {
        this.patientDatas = data[0];
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(MedcertHistoryComponent, {
      data: {
        pid: this.patientId
      }
    });
  }

}
