import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-toxicology',
  templateUrl: './toxicology.component.html',
  styleUrls: ['./toxicology.component.scss']
})
export class ToxicologyComponent implements OnInit {

  public toxicology = "toxicology";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("lab");
  }

  ngOnInit() {
  }

}
