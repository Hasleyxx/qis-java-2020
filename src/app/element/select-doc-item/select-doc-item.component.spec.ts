import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectDocItemComponent } from './select-doc-item.component';

describe('SelectDocItemComponent', () => {
  let component: SelectDocItemComponent;
  let fixture: ComponentFixture<SelectDocItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectDocItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectDocItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
