import { Component, OnInit, Inject } from '@angular/core';
import { medtech, transaction } from 'src/app/services/service.interface';
import { MathService } from 'src/app/services/math.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-serology-form',
  templateUrl: './serology-form.component.html',
  styleUrls: ['./serology-form.component.scss']
})
export class SerologyFormComponent implements OnInit {

  id: any;
  medtech: medtech[];
  transaction: transaction;
  update: boolean = false;

  foo = ["NEGATIVE", "POSITIVE"];
  react = ["REACTIVE", "NONREACTIVE"];

  sero = new FormGroup({
    seroID: new FormControl(undefined),
    transactionID: new FormControl(""),
    patientID: new FormControl(""),

    pathID: new FormControl('5', [Validators.required]),
    medID: new FormControl('1', [Validators.required]),
    qualityID: new FormControl('11', [Validators.required]),

    patientIdRef: new FormControl(''),
    userID: new FormControl(''),

    creationDate: new FormControl("0000-00-00 00:00:00"),
    dateUpdate: new FormControl("0000-00-00 00:00:00"),

    hbsAG: new FormControl(""),
    antiHav: new FormControl(""),
    vdrl: new FormControl(""),
    antiHBS: new FormControl(""),
    hbeAG: new FormControl(""),
    antiHBE: new FormControl(""),
    antiHBC: new FormControl(""),
    psanti: new FormControl(""),
    tppa: new FormControl(""),

    tydotigM: new FormControl(""),
    tydotigG: new FormControl(""),

    tsh: new FormControl(""),
    ft3: new FormControl(""),
    t3: new FormControl(""),
    ft4: new FormControl(""),
    t4: new FormControl(""),

    cea: new FormControl(""),
    afp: new FormControl(""),
    ca125: new FormControl(""),
    ca19: new FormControl(""),
    ca15: new FormControl(""),

    crpdil: new FormControl(""),
    crpres: new FormControl(""),

    hiv1: new FormControl(""),
    hiv2: new FormControl(""),

    sarsg: new FormControl(""),
    sarsm: new FormControl(""),

    seroOt: new FormControl(""),


  })

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private math: MathService,
    private lab: LaboratoryService,
    private TS: TransactionService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SerologyFormComponent>
  ) {
    this.math.navSubs("lab");

    //this.id = this.route.snapshot.paramMap.get('id');
    this.id = this.dialogData.tid;

    if (isNaN(this.id)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {
    this.lab.getMedtech().subscribe(
      medtech => {
        this.medtech = medtech;
      }
    )

    this.TS.getOneTrans("getTransaction/" + this.id).subscribe(
      data => {
        if (data[0].length == 0) {
          this.router.navigate(['error/404']);
        } else {
          this.transaction = data[0];

          this.sero.controls.transactionID.setValue(data[0].transactionId);
          this.sero.controls.patientID.setValue(data[0].patientId);
          this.sero.controls.patientIdRef.setValue(data[0].patientIdRef);

          this.lab.getSerology(data[0].transactionId).subscribe(
            sero => {
              if (sero.length != 0) {
                this.update = true;
                console.log(sero);
                for (var i in sero[0]) {
                  this.sero.get(i).setValue(sero[0][i].toString());
                }
              }
            }
          )
        }
      }
    )
  }

  addSero() {
    this.sero.controls.creationDate.setValue(this.math.dateNow());
    this.sero.controls.dateUpdate.setValue(this.math.dateNow());
    this.sero.controls.userID.setValue(parseInt(sessionStorage.getItem('token')));

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be saved to database!" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.lab.addSerology(this.sero.value).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['laboratory/serology']);
            this.dialogRef.close();
          }
        )
      }

    })
  }

  updateSero() {
    this.sero.controls.dateUpdate.setValue(this.math.dateNow());
    this.sero.controls.userID.setValue(parseInt(sessionStorage.getItem('token')));

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be saved to database!" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.lab.addSerology(this.sero.value, "/updateSerology").subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['laboratory/serology']);
            this.dialogRef.close();
          }
        )
      }

    })
  }

}
