import { Component, OnInit, Inject, ViewChildren, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { MathService } from 'src/app/services/math.service';
import { DoctorService } from 'src/app/services/doctor.service';
import * as moment from 'moment';
import { MedicineDialogComponent } from '../medicine-dialog/medicine-dialog.component';

@Component({
  selector: 'app-edit-print-medication',
  templateUrl: './edit-print-medication.component.html',
  styleUrls: ['./edit-print-medication.component.scss']
})
export class EditPrintMedicationComponent implements OnInit {

  public patientId: any;
  public patientData: any;

  public tempIds: any[];
  public values: string[] = [];
  public dateCreated: any;

  @ViewChildren('value1') value1: ElementRef[];
  @ViewChildren('value2') value2: ElementRef[];
  @ViewChildren('value3') value3: ElementRef[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private PS: PatientService,
    public math: MathService,
    private DS: DoctorService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EditPrintMedicationComponent>
  ) {
    this.patientId = this.dialogData.patientId;
    this.tempIds = this.dialogData.ids;
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.PS.getOnePatient('/getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });

    for (let index = 0; index < this.tempIds.length; index++) {
      const element = this.tempIds[index];

      this.DS.getDocMedUid(element).subscribe(
        response => {
          this.values.push(response[0]);
          this.dateCreated = moment().format("YYYY-MM-DD");
        }
      );
    }
  }

  print() {
    let value1: string[] = [];
    let value2: string[] = [];
    let value3: string[] = [];

    this.value1.forEach(element => {
      let tempValue = element.nativeElement.value;
      if (tempValue == "" || tempValue == null) {
        value1.push("n/a");

      } else {
        value1.push(element.nativeElement.value);
      }
    });
    this.value2.forEach(element => {
      let tempValue = element.nativeElement.value;
      if (tempValue == "" || tempValue == null) {
        value2.push("n/a");

      } else {
        value2.push(element.nativeElement.value);
      }
    });
    this.value3.forEach(element => {
      let tempValue = element.nativeElement.value;
      if (tempValue == "" || tempValue == null) {
        value3.push("n/a");

      } else {
        value3.push(element.nativeElement.value);
      }
    });

    sessionStorage.setItem("value1", JSON.stringify(value1));
    sessionStorage.setItem("value2", JSON.stringify(value2));
    sessionStorage.setItem("value3", JSON.stringify(value3));

    this.dialogRef.close();
    const suffix = [this.patientId, 'medication'];
    this.math.printDoc('', suffix);
  }

  cancel() {
    this.dialogRef.close();

    this.dialog.open(MedicineDialogComponent, {
      data: {
        pid: this.patientId
      },
      disableClose: true
    });
  }
}
