import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import * as moment from 'moment';
import { MathService } from 'src/app/services/math.service';
import { RequestHistoryComponent } from '../request-history/request-history.component';

@Component({
  selector: 'app-request-dialog',
  templateUrl: './request-dialog.component.html',
  styleUrls: ['./request-dialog.component.scss']
})
export class RequestDialogComponent implements OnInit {

  public requestRef: any;
  public patientId: any;
  public patientDatas: any = [];
  public categories: any[] = [];
  public subCategories: any[] = [];
  public selects: any[] = [];
  public ids: any[] = [];

  @ViewChild('otherTests') otherTests: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private math: MathService,
    private PS: PatientService,
    private DS: DoctorService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<RequestDialogComponent>
  ) {
    this.patientId = this.data.pid;
    this.requestRef = this.patientId + ":" + this.math.randomNumber();
  }

  ngOnInit() {
    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => this.patientDatas = data[0]
    );

    this.getCategory();
  }

  getCategory() {
    this.DS.getDocRequestCategory().subscribe(
      datas => {
        if (datas.length < 1) {
          let temp = {"category": "No available category", "docReqCatId": "0"};
          this.categories.push(temp);
        } else {
          this.categories = datas;
        }
      }
    );
  }
  selectCategory(id) {
    this.DS.getDocRequestOptionId(id).subscribe(
      datas => {
        if (id !== "0") {
          if (datas.length < 1) {
            let temp = {
              "docReqOptionId": "0",
              "option": "No available test",
              "status": "0",
              "category": id
            };
            this.subCategories.push(temp);
          } else {
            this.subCategories = datas;
          }
        }
      }
    );
  }

  selectSub(id) {
    this.ids = [];
    this.selects.forEach(element => {
      this.ids.push(element['id']);
    });

    let check = this.ids.includes(id);
    if (check == false) {
      for (let index = 0; index < this.subCategories.length; index++) {
        const element = this.subCategories[index];
        if (element['docReqOptionId'] == id) {
          let temp = {
            "id": id,
            "label": element['option'],
            "category": element['category']
          };
          this.selects.push(temp);
        }
      }
    }
  }

  removeSelected(id) {
    for (let index = 0; index < this.selects.length; index++) {
      const element = this.selects[index];

      if (element['id'] == id) {
        this.selects.splice(index, 1);
      }
    }
  }

  submit() {
    if (this.selects.length < 1 && this.otherTests.nativeElement.value == "") {
      this.openSnackBar("You have not selected any test(s).", "close");
    } else {
      const dialogRef = this.dialog.open(ConfirmComponent, {
        data: {
          Title: "ARE YOU SURE?", Content: "Patient request(s) will be added!"
        },
        width: '25%'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == "ok") {
          this.selects.forEach((element, index) => {
            if (index + 1 == this.selects.length) {
              let data = {
                patientID: this.data.pid,
                category: element.category,
                test: element.label,
                otherTest: this.otherTests.nativeElement.value,
                requestRef: this.requestRef,
                dateCreated: moment().format("YYYY-MM-DD h:mm:ss")
              };
              this.DS.addDocRequest(data).subscribe();

            } else {
              let data = {
                patientID: this.data.pid,
                category: element.category,
                test: element.label,
                otherTest: "",
                requestRef: this.requestRef,
                dateCreated: moment().format("YYYY-MM-DD h:mm:ss")
              };
              this.DS.addDocRequest(data).subscribe();
            }

            if (index + 1 == this.selects.length) {
              setTimeout(() => {
                // this.math.openSnackBar("Patient request(s) has been added!", "close");
                this.dialogRef.close();
                const suffix = [this.patientId, 'request'];
                sessionStorage.setItem("requestRef", this.requestRef);
                this.math.printDoc('', suffix);
              }, 500);
            }
          });
        }
      });
    }
  }

  reset() {
    this.subCategories = [];
    this.selects = [];
    this.otherTests.nativeElement.value = "";
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(RequestHistoryComponent, {
      data: {
        pid: this.data.pid
      },
      disableClose: true
    });
  }

}
