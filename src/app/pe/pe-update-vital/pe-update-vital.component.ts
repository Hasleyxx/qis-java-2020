import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { PeService } from 'src/app/services/pe.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-pe-update-vital',
  templateUrl: './pe-update-vital.component.html',
  styleUrls: ['./pe-update-vital.component.scss']
})
export class PeUpdateVitalComponent implements OnInit {

  public transactionId: any;

  public updateVitalForm: FormGroup;
  public vitals = [
    { "label": "Ishihara Test: ", "value": 'cv' },
    { "label": "Hearing: ", "value": 'hearing' },
    { "label": "Hospitalization: ", "value": 'hosp' },
    { "label": "Operations: ", "value": 'opr' },
    { "label": "Medications: ", "value": 'pm' },
    { "label": "Smoker: ", "value": 'smoker' },
    { "label": "Alcoholic: ", "value": 'ad' },
    { "label": "Last Menstrual: ", "value": 'lmp' },
    { "label": "Others/Notes: ", "value": 'notes' }
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private math: MathService,
    private peService: PeService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<PeUpdateVitalComponent>
  ) { }

  ngOnInit() {
    this.transactionId = this.data.tid;

    this.updateVitalForm = this.formBuilder.group({
      height: [''],
      weight: [''],
      bmi: [''],
      bp: [''],
      pr: [''],
      rr: [''],
      uod: [''],
      uos: [''],
      cod: [''],
      cos: [''],
      cv: [''],
      hearing: [''],
      hosp: [''],
      opr: [''],
      pm: [''],
      smoker: [''],
      ad: [''],
      lmp: [''],
      notes: [''],
      userID: [''],
      creationDate: [''],
      dateUpdate: [''],
      vitalsID: ['']
    });

    this.getVital();

    this.updateVitalForm.get('weight')!.valueChanges.subscribe(data => {
      let tempHeight = this.updateVitalForm.get('height').value;
      if (tempHeight.includes(".")) {
        let f_height: any;
        let i_height: any;
        let weight: any;
        let bmi: any;

        tempHeight = tempHeight.split(".");
        f_height = tempHeight[0];
        i_height = tempHeight[1];

        weight = this.updateVitalForm.get('weight').value * 2.205;
        let i = f_height * 12 + i_height * 1.0;

        bmi = this.cal_bmi(weight, i);
        this.addBmiExt(bmi);
      }
    });

    this.updateVitalForm.get('height')!.valueChanges.subscribe(data => {
      let tempHeight = this.updateVitalForm.get('height').value;
      if (tempHeight.includes(".")) {
        let f_height: any;
        let i_height: any;
        let weight: any;
        let bmi: any;

        tempHeight = tempHeight.split(".");
        f_height = tempHeight[0];
        i_height = tempHeight[1];

        weight = this.updateVitalForm.get('weight').value * 2.205;
        let i = f_height * 12 + i_height * 1.0;

        bmi = this.cal_bmi(weight, i);
        this.addBmiExt(bmi);
      }
    });
  }

  addBmiExt(bmi) {
    if (bmi <= 18.5) {
      this.updateVitalForm.get('bmi').setValue(bmi + " / Underweight");
    } else if (bmi >= 18.6 && bmi <= 24.9) {
      this.updateVitalForm.get('bmi').setValue(bmi + " / Normal");
    } else if (bmi >= 25 && bmi <= 29.9) {
      this.updateVitalForm.get('bmi').setValue(bmi + " / Overweight");
    } else {
      this.updateVitalForm.get('bmi').setValue(bmi + " / Obese");
    }
  }

  cal_bmi(lbs, ins) {
    let h2 = ins * ins;
    let bmi: any = lbs / h2 * 703;
    let f_bmi = Math.floor(bmi);
    let diff = bmi - f_bmi;
    diff = diff * 10;
    diff = Math.round(diff);
    if (diff == 10) {
      f_bmi += 1;
      diff = 0;
    }
    bmi = f_bmi + "." + diff;
    return bmi;
  }

  getVital() {
    this.peService.getVital(this.transactionId).subscribe(
      data => {
        this.updateVitalForm.patchValue({
          height: data.height,
          weight: data.weight,
          bmi: data.bmi,
          bp: data.bp,
          pr: data.pr,
          rr: data.rr,
          uod: data.uod,
          uos: data.uos,
          cod: data.cod,
          cos: data.cos,
          cv: data.cv,
          hearing: data.hearing,
          hosp: data.hosp,
          opr: data.opr,
          pm: data.pm,
          smoker: data.smoker,
          ad: data.ad,
          lmp: data.lmp,
          notes: data.notes,
          userID: parseInt(sessionStorage.getItem('token')),
          creationDate: data.creationDate,
          dateUpdate: '',
          vitalsID: data.vitalsID
        });
      }
    );
  }

  updateVital() {
    let r_height = this.updateVitalForm.get('height').value;
    let r_weight = this.updateVitalForm.get('weight').value;
    if (r_height.includes(".")) {
      r_height = r_height.split(".");
      r_height = r_height[0] + "ft" + r_height[1] + "inch";
    }
    r_weight = r_weight + "kg";
    this.updateVitalForm.patchValue({
      dateUpdate: moment().format("YYYY-MM-DD h:mm:ss"),
      height: r_height,
      weight: r_weight,
    });

    this.peService.updateVital(this.updateVitalForm.value).subscribe(
      data => {
        if (data == 1) {
          this.dialogRef.close(this.updateVitalForm.value);
          this.openSnackBar("Vital signs has been updated!", "close");
        }
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
