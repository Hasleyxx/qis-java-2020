import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { ItemService } from 'src/app/services/item.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { DoctorService } from 'src/app/services/doctor.service';
import { MathService } from 'src/app/services/math.service';
import { ClearanceHistoryComponent } from '../clearance-history/clearance-history.component';

@Component({
  selector: 'app-clearance-dialog',
  templateUrl: './clearance-dialog.component.html',
  styleUrls: ['./clearance-dialog.component.scss']
})

export class ClearanceDialogComponent implements OnInit {

  public patientId: any;
  public type: any;

  public patientDatas: any = [];
  public availed: any;
  public description: any;
  public transactionType: any;
  public addClearance: FormGroup;
  public addPe: FormGroup;
  public dataRef: any;

  public medhisString = "";
  public medhis = [
    { "label": "Hypertension", "control": "hyper"},
    { "label": "Diabetes", "control": "dia"},
    { "label": "Bronchial Asthma", "control": "bron"},
    { "label": "CAD/IHD", "control": "cid"},
    { "label": "Smoking Hx", "control": "smoke"},
    { "label": "Alcohol Hx", "control": "alcohol"},
    { "label": "Allergies", "control": "alle"},
    { "label": "Others", "control": "others"}
  ];

  public rosString = "";
  public ros = [
    { "label": "Easy Fatigability", "control": "fatig" },
    { "label": "PND", "control": "pnd" },
    { "label": "Syncope", "control": "syn" },
    { "label": "Anginal Pain", "control": "anginal" },
    { "label": "Dizziness", "control": "diz" },
    { "label": "Palpitation", "control": "palp" },
    { "label": "Dyspnea on Exertion", "control": "dysp" },
    { "label": "Bipedal Edema", "control": "bipedal" },
    { "label": "Orthopnea", "control": "ortho" },
    { "label": "Polyuria", "control": "poly" },
    { "label": "Tremors", "control": "trem" },
    { "label": "Cough", "control": "cough" }
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private doctor : DoctorService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<ClearanceDialogComponent>,
    private math: MathService
  ) {
    this.patientId = this.data.pid;
   }

  ngOnInit() {
    this.doctor.getDocPes().subscribe(
      pes => {
        this.dataRef = this.math.dataRef(pes);
      }
    );

    this.getData();
    this.addClearance = this.formBuilder.group({
      patientID: [''],
      dataRef: [''],
      hyper: [''],
      dia: [''],
      bron: [''],
      cid: [''],
      smoke: [''],
      alcohol: [''],
      alle: [''],
      others: [''],
      fatig: [''],
      pnd: [''],
      syn: [''],
      anginal: [''],
      diz: [''],
      palp: [''],
      dysp: [''],
      bipedal: [''],
      ortho: [''],
      poly: [''],
      trem: [''],
      cough: [''],
      pastmedHis: [''],
      socialHis: [''],
      allergies: [''],
      prevSurgery: [''],
      rOs: [''],
      workUp: [''],
      assesmentPlan: [''],
      dateCreated: [moment().format("YYYY-MM-DD h:mm:ss")],
      addPe: this.formBuilder.group({
        patientID: [''],
        dataRef: [''],
        bp: ['120/80'],
        hr: ['60'],
        hn: ['Pink Conjunctivae, Anicteric Sclerae, (-) NVE'],
        cl: ['(-) Rales, (-) Wheezes'],
        cardiac: ['NRRR, (-) Murmur, (-) S3'],
        abd: ['(-) Organomegaly, (-) Tenderness'],
        ext: ['(-) Edema, (-) Cyanosis'],
        dateCreated: [moment().format("YYYY-MM-DD h:mm:ss")]
      })
    });
  }

  saveClearance(){
    this.medhisString = "";
    this.rosString = "";

    this.addClearance.patchValue({
      patientID: +this.patientId,
      dataRef: this.dataRef
    });
    const dialogRefConfirm = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Patient clearance will be saved!"
      },
      width: '20%'
    });

    this.medhis.forEach(element => {
      const control = element['control'];
      const label = element['label'];

      if (this.addClearance.get(control).value == true) {
        if (this.medhisString == "") {
          this.medhisString = label;
        } else {
          this.medhisString = this.medhisString + " | " + label;
        }
      }

      this.addClearance.get('pastmedHis').setValue(this.medhisString)
    });


    this.ros.forEach(element => {
      const control1 = element['control'];
      const label1 = element['label'];

      if (this.addClearance.get(control1).value == true) {
        if (this.rosString == "") {
          this.rosString = label1;
        } else {
          this.rosString = this.rosString + " | " + label1;
        }
      }
      this.addClearance.get('rOs').setValue(this.rosString)
    });

    dialogRefConfirm.afterClosed().subscribe(
      result => {
        if (result == "ok") {
          this.addClearance.get('addPe').patchValue({
            patientID: +this.patientId
          });
          this.addClearance.get('addPe').patchValue({
            dataRef: this.dataRef
          });
          this.doctor.addDocPe(this.addClearance.get('addPe').value).subscribe(
            response => {
              console.log(response);
            }
          );
          this.addClearance.removeControl('addPe');
          this.doctor.addDocClearance(this.addClearance.value).subscribe(
            response => {
              if (response == 1) {
                // this.openSnackBar("Patient clearance has been created.", "close");
                this.dialogRef.close({
                  status: "print",
                  dataRef: this.dataRef
                });
              }
            }
          );
        }
      }
    );
  }

  getData() {
    // Patient
    // console.log(this.patientId)
    this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => {
        this.patientDatas = data[0];
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  med(value: any) {
    this.medhis = value;
  }

  ross(value: any) {
    this.ros = value;
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(ClearanceHistoryComponent, {
      data: {
        pid: this.data.pid
      },
      disableClose: true
    });
  }
}
