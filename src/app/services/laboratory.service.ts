import { Injectable } from '@angular/core';
import { ErrorService } from './error.service';
import { Global } from '../global.variable';
import { Observable } from 'rxjs';
import { microscopy, medtech, hematology, chemistry, serology, toxicology } from './service.interface';
import { HttpClient } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LaboratoryService {

  constructor(
    private http: HttpClient,
    public ehs: ErrorService,
    private global: Global
  ) { }

  getMedtech():Observable<medtech[]>{
    return this.http.get<medtech[]>( this.global.url+"/getPersonnelDep/LAB" )
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  getPersonnel( id: number):Observable<medtech[]>{
    return this.http.get<medtech[]>( this.global.url+"/getPersonnel/" + id )
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  addMedtech(form: medtech): Observable<any>{
    return this.http.post<any>(
      this.global.url + "/addPersonnel",
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  getMicroscopy(id:any = ""):Observable<microscopy[]>{
    return this.http.get<microscopy[]>(this.global.url + "/getmicroscopy/" + id)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  addMicroscopy(form: any, url = "/addMicroscopy"): Observable<any>{
    return this.http.post<any>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  updateMicroscopy(form: any, url = "/updateMicroscopy"): Observable<any> {
    return this.http.post<any>(
      this.global.url + url,
      form, this.global.httpOptions)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  getHematology(id:any = ""):Observable<hematology[]>{
    return this.http.get<hematology[]>(this.global.url + "/getHema/" + id)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  addHematology(form: any, url = "/addHematology"): Observable<any>{
    return this.http.post<any>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  updateHematology(form: any, url = "/updateHematology"): Observable<any>{
    return this.http.post<any>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  getChemistry(id: any = ""): Observable<chemistry[]>{
    return this.http.get<chemistry[]>(this.global.url + "/getChemistry/" + id)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  addChemistry(form : chemistry, url = "/addChemistry"): Observable<any>{
    return this.http.post<any>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  getSerology(id: any = ""): Observable<any[]> {
    return this.http.get<any[]>(this.global.url + "/getSerology/" + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  addSerology(form: any, url = "/addSerology"): Observable<any>{
    return this.http.post<any>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  updateSerology(form: any, url = "/updateSerology"): Observable<any>{
    return this.http.post<serology>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  getToxicology(id: any = ""): Observable<toxicology[]> {
    return this.http.get<toxicology[]>(this.global.url + "/gettoxicology/" + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  addToxicology(form : any, url = "/addToxicology"): Observable<any>{
    return this.http.post<any>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  updateToxicology(form : any, url = "/updateToxicology"): Observable<any>{
    return this.http.post<toxicology>(
      this.global.url + url ,
      form, this.global.httpOptions)
    .pipe(
        retry(1),
        catchError(this.ehs.handleError)
    )
  }

  getOneClass(id: any = ""): Observable<any[]> {
    return this.http.get<any[]>(this.global.url + "/getOneClass/" + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  addClass(data): Observable<any> {
    return this.http.post<any>(this.global.url + "/addClass", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  updateClass(data): Observable<any> {
    return this.http.post<any>(this.global.url + "/updateClass", data)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }
}
