import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { SnackSuccessComponent } from 'src/app/element/snack-success/snack-success.component';
import { DialogFilmInventoryComponent } from '../dialog-film-inventory/dialog-film-inventory.component';
import * as moment from 'moment';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-imaging-xray-inventory',
  templateUrl: './imaging-xray-inventory.component.html',
  styleUrls: ['./imaging-xray-inventory.component.scss']
})
export class ImagingXrayInventoryComponent implements OnInit {

  maxDate = new Date();
  public filmForm: FormGroup;
  public stickerDateForm: FormGroup;
  public stickerIdForm: FormGroup;

  constructor(
    private math: MathService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private TS: TransactionService
  ) {
    this.math.navSubs("imaging");
  }

  ngOnInit() {
    this.filmForm = this.formBuilder.group({
      dateFromFilm: ['', Validators.required],
      dateToFilm: ['', Validators.required]
    });

    this.stickerDateForm = this.formBuilder.group({
      dateForm: ['', Validators.required],
      dateTo: ['', Validators.required]
    });

    this.stickerIdForm = this.formBuilder.group({
      idForm: ['', Validators.required],
      idTo: ['', Validators.required]
    });
  }

  table() {
    if (!this.filmForm.valid) {
      this.openSnackBar("You must select date from / date to!", "close");
    }else {
      const dateFromFilm = moment(this.filmForm.get('dateFromFilm').value).format("YYYY-MM-DD");
      const dateToFilm = moment(this.filmForm.get('dateToFilm').value).format("YYYY-MM-DD");

      const dialogRef = this.dialog.open(DialogFilmInventoryComponent, {
        data: {
          filmType: 'filmTable',
          dateFromFilm: dateFromFilm,
          dateToFilm: dateToFilm
        },
        width: '95vw'
      });
    }
  }

  stickerDate() {
    let from = moment(this.stickerDateForm.get('dateForm').value).format("YYYY-MM-DD");
    let to =  moment(this.stickerDateForm.get('dateTo').value).format("YYYY-MM-DD");

    if (from > to) {
      this.openSnackBar("Date from must be greater than date to.", "close");
    }else {
      from = from + " 05:00:00";
      to = to + " 20:00:00";
      let url = "getTransactionDate/" + from + "/" + to;
      this.TS.getTransactions(url).subscribe(
        data => {
          if (data.length == 0) {
            this.openSnackBar("No stickers found on selected dates.", "close");
          } else {
            const data = ['date', from, to];
            this.math.printSticker('', data);
          }
        }
      );
    }
  }

  stickerId() {
    let from = this.stickerIdForm.get('idForm').value;
    let to = this.stickerIdForm.get('idTo').value;

    if (from > to) {
      this.openSnackBar("Id from must be less than Id to.", "close");
    }else {
      const data = ['id', from, to];
      this.math.printSticker('', data);
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000
    });
  }

}
