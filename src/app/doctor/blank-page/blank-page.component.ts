import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';

export interface dialogData {
  tid: number,
  pid: number
}

@Component({
  selector: 'app-blank-page',
  templateUrl: './blank-page.component.html',
  styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {

  public patientData: any;
  public patientId: any;
  public transactionId: number;

  @ViewChild('text') text: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: dialogData,
    private PS: PatientService,
    public math: MathService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<BlankPageComponent>
  ) {
    this.transactionId = this.dialogData.tid;
    this.patientId = this.dialogData.pid;
  }

  ngOnInit() {
    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });
  }

  print() {
    let value = this.text.nativeElement.value;
    sessionStorage.setItem("value", value);

    this.dialogRef.close();
    const suffix = [this.patientId, 'blank'];
    this.math.printDoc('', suffix);
  }

  cancel() {
    this.dialogRef.close();

    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId
      }
    });
  }

}
