import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pe-certificate',
  templateUrl: './pe-certificate.component.html',
  styleUrls: ['./pe-certificate.component.scss']
})
export class PeCertificateComponent implements OnInit {

  public peCertificate = "peCertificate";

  constructor(
    private router: Router,
    private math: MathService
  ) {
    if (this.router.url == '/nurse/certificate') {
      this.math.navSubs("nurse");
      this.peCertificate = "nurseCertificate";

    } else if (this.router.url == '/imaging/certificate') {
      this.math.navSubs("imaging");
      this.peCertificate = "imagingCertificate";

    } else {
      this.math.navSubs("pe");
      this.peCertificate = "peCertificate";

    }
  }

  ngOnInit() {
  }

}
