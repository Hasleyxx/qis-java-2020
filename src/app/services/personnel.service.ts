import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Global } from '../global.variable';
import { Observable } from 'rxjs';
import { medtech } from './service.interface';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonnelService {

  constructor(
    private http: HttpClient,
    public ehs: ErrorService,
    private global: Global
  ) { }

  getMedtech(): Observable<medtech[]> {
    return this.http.get<medtech[]>(this.global.url + "/getPersonnelDep/LAB")
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  getRadtech(): Observable<medtech[]> {
    return this.http.get<medtech[]>(this.global.url + "/getPersonnelDep/IMAGING")
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  getPersonnelUrl(type: string): Observable<medtech[]> {
    return this.http.get<medtech[]>(this.global.url + "/" + type)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  getPersonnel(id: number): Observable<medtech[]> {
    return this.http.get<medtech[]>(this.global.url + "/getPersonnel/" + id)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  getPersonnelAll(): Observable<medtech[]> {
    return this.http.get<medtech[]>(this.global.url + "/getPersonnel")
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  addPersonnel(form: medtech): Observable<any> {
    return this.http.post<any>(this.global.url + "/addPersonnel", form)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }

  updatePersonnel(form: medtech): Observable<any> {
    return this.http.post<any>(this.global.url + "/updatePersonnel", form)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    )
  }
}
