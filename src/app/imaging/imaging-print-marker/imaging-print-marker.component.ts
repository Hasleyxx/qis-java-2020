import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: 'app-imaging-print-marker',
  templateUrl: './imaging-print-marker.component.html',
  styleUrls: ['./imaging-print-marker.component.scss']
})
export class ImagingPrintMarkerComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];
  public patientData = [];
  public transData = [];
  public addMarkerData = [];
  public transactionId: any;
  public patientId: any;

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private XS: XrayService
  ) {
    this.data = route.snapshot.params['datas'].split(',');
    this.transactionId = this.data[3];
  }

  ngOnInit() {
    this.dataDetails = this.data.map(data => this.getMarkerReady(data));
    this.TS.getOneTrans('/getTransaction/' + this.transactionId).subscribe(
      data => {
        this.transData = data[0];
        this.patientId = data[0]['patientId'];
        this.PS.getOnePatient('/getPatient/' + this.patientId).subscribe(
          data => {
            this.patientData = data[0];
            this.addMarkerData = [{
              "patientID": +this.patientId,
              "transactionID": +this.transactionId,
              "xrayFilm": this.data[0],
              "xrayType": this.data[2],
              "radTech": this.data[1],
              "totalCount": +0
            }];

            this.XS.getMarker(this.transactionId).subscribe(
              data => {
                if (data.length == 0) {
                  this.XS.addMarker(this.addMarkerData[0]).subscribe(
                    data => {
                      if (data == 1) {
                        console.log('Patient Marker has been added!');
                      } else {
                        console.log('Failed to add patient marker!');
                      }
                    }
                  );
                }
              }
            );
          }
        );
      }
    );
    Promise.all(this.dataDetails).then(() => {
      this.math.onMarkerReady();
    });
  }

  getMarkerReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

}
