import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-twod-echo-summary',
  templateUrl: './twod-echo-summary.component.html',
  styleUrls: ['./twod-echo-summary.component.scss']
})
export class TwodEchoSummaryComponent implements OnInit {

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("imaging");
  }
  ngOnInit() {
  }

}
