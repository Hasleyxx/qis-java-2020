import { Component, OnInit, Input } from '@angular/core';
import { serology, toxicology } from 'src/app/services/service.interface';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'serotoxic-result',
  templateUrl: './serotoxic-result.component.html',
  styleUrls: ['./serotoxic-result.component.scss']
})
export class SerotoxicResultComponent implements OnInit {

  @Input() serology: serology;
  @Input() toxicology: toxicology;

  constructor(
    public math: MathService
  ) { }

  ngOnInit() {
  }

}
