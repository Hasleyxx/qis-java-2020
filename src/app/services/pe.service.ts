import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Global } from '../global.variable';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeService {

  constructor(
    private httpClient: HttpClient,
    private ehs: ErrorService,
    private global: Global
  ) { }

  addMedhis(medhis: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addMedhis', medhis)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  addVital(vital: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addVital', vital)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  addPE(pe: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addPE', pe)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getMedhis(tid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getOneMedicalHistory/' + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getVital(tid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getOneVital/' + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getPE(tid: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getOnePe/' + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  checkPe(type: any): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/' + type)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateMedhis(medhis): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updateMedhis', medhis)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateVital(vitals): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updateVital', vitals)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updatePe(pes): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/updatePE', pes)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }
}
