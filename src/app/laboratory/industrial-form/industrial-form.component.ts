import { Component, OnInit, Input, Inject } from '@angular/core';
import { medtech, transaction, patient } from 'src/app/services/service.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MathService } from 'src/app/services/math.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: 'app-industrial-form',
  templateUrl: './industrial-form.component.html',
  styleUrls: ['./industrial-form.component.scss']
})
export class IndustrialFormComponent implements OnInit {

  id: any;
  medtech: medtech[];
  transaction: transaction;
  patient: patient;
  update: boolean = false;
  @Input() isIndustrial: boolean = true;

  urineColor = ["STRAW", "LIGHT YELLOW", "YELLOW", "DARK YELLOW", "RED", "ORANGE", "AMBER"];
  urineTransparency = ["CLEAR", "HAZY", "SL. TURBID", "TURBID"];
  ucValue = ["NEGATIVE", "TRACE", "1+", "2+", "3+", "4+"];
  ucpH = ["5.0", "6.0", "6.5", "7.0", "7.5", "8.0", "8.5", "9.0", "9.5"];
  ucSPG = ["1.000", "1.005", "1.010", "1.015", "1.020", "1.025", "1.030",];
  microscopicVal = ["RARE", "FEW", "MODERATE", "MANY"];

  fecColor = ["GREEN", "YELLOW", "LIGHT BROWN", "BROWN", "DARK BROWN"];
  fecCons = ["FORMED", "SEMI-FORMED", "SOFT", "WATERY", "SLIGHTLY MUCOID", "MUCOID"];

  foo = ["NEGATIVE", "POSITIVE"];
  react = ["REACTIVE", "NON-REACTIVE"];

  values = [
    { "one": "0.28", "two": "92", "three": "3.08" },
    { "one": "0.29", "two": "96", "three": "3.19" },
    { "one": "0.30", "two": "99", "three": "3.3" },
    { "one": "0.31", "two": "102", "three": "3.41" },
    { "one": "0.32", "two": "106", "three": "3.52" },
    { "one": "0.33", "two": "109", "three": "3.63" },
    { "one": "0.34", "two": "112", "three": "3.74" },
    { "one": "0.35", "two": "116", "three": "3.85" },
    { "one": "0.36", "two": "119", "three": "3.96" },
    { "one": "0.37", "two": "122", "three": "4.07" },
    { "one": "0.38", "two": "125", "three": "4.18" },
    { "one": "0.39", "two": "129", "three": "4.29" },
    { "one": "0.40", "two": "132", "three": "4.4" },
    { "one": "0.41", "two": "135", "three": "4.51" },
    { "one": "0.42", "two": "139", "three": "4.62" },
    { "one": "0.43", "two": "142", "three": "4.73" },
    { "one": "0.44", "two": "145", "three": "4.84" },
    { "one": "0.45", "two": "149", "three": "4.95" },
    { "one": "0.46", "two": "152", "three": "5.06" },
    { "one": "0.47", "two": "155", "three": "5.17" },
    { "one": "0.48", "two": "158", "three": "5.28" },
    { "one": "0.49", "two": "162", "three": "5.39" },
    { "one": "0.50", "two": "165", "three": "5.5" },
    { "one": "0.51", "two": "168", "three": "5.61" },
    { "one": "0.52", "two": "172", "three": "5.72" },
    { "one": "0.53", "two": "175", "three": "5.83" },
    { "one": "0.54", "two": "178", "three": "5.94" },
    { "one": "0.55", "two": "182", "three": "6.05" }
  ];

  laboratory = new FormGroup({

    microID: new FormControl(""),
    hemaID: new FormControl(""),
    seroID: new FormControl(""),
    toxicID: new FormControl(""),
    transactionID: new FormControl(""),
    patientID: new FormControl(""),

    pathID: new FormControl('5', [Validators.required]),
    medID: new FormControl('1', [Validators.required]),
    qualityID: new FormControl('11', [Validators.required]),

    patientIdRef: new FormControl(''),
    userID: new FormControl(''),

    creationDate: new FormControl("0000-00-00 00:00:00"),
    dateUpdate: new FormControl("0000-00-00 00:00:00"),

    uriColor: new FormControl(""),
    uriTrans: new FormControl(""),
    uriOt: new FormControl(""),

    uriPh: new FormControl(""),
    uriSp: new FormControl(""),
    uriPro: new FormControl(""),
    uriGlu: new FormControl(""),

    rbc: new FormControl(""),
    wbc: new FormControl(""),
    ecells: new FormControl("NONE"),
    mthreads: new FormControl("NONE"),
    bac: new FormControl("NONE"),
    amorph: new FormControl("NONE"),
    coAx: new FormControl("NONE"),

    fecColor: new FormControl("N/A"),
    fecCon: new FormControl("N/A"),
    fecMicro: new FormControl("N/A"),
    fecOt: new FormControl(""),

    pregTest: new FormControl("N/A"),

    cbcrbc: new FormControl(""),
    hemoglobin: new FormControl(""),
    hematocrit: new FormControl(""),
    hemaNR: new FormControl("N/A"),
    hemoNR: new FormControl("N/A"),
    cbcot: new FormControl(""),

    seroOt: new FormControl(""),

    meth: new FormControl("N/A"),
    tetra: new FormControl("N/A"),
    drugtest: new FormControl("N/A"),
    hbsAG: new FormControl("N/A"),

  })


  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private math: MathService,
    private lab: LaboratoryService,
    private TS: TransactionService,
    private router: Router,
    private dialog: MatDialog,
    private PS: PatientService,
    private dailogRef: MatDialogRef<IndustrialFormComponent>
  ) {
    this.math.navSubs("lab");

    //this.id = this.route.snapshot.paramMap.get('id');
    this.id = this.dialogData.tid;

    if (isNaN(this.id)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {

    this.TS.getOneTrans("getTransaction/" + this.id).subscribe(
      data => {
        if (data[0].length == 0) {
          this.router.navigate(['error/404']);
        } else {
          this.transaction = data[0];

          this.laboratory.controls.transactionID.setValue(data[0].transactionId);
          this.laboratory.controls.patientID.setValue(data[0].patientId);
          this.laboratory.controls.patientIdRef.setValue(data[0].patientIdRef);
          this.laboratory.controls.userID.setValue(+parseInt(sessionStorage.getItem('token')));

          this.PS.getOnePatient("getPatient/" + data[0].patientId).subscribe(data => {
            this.patient = data[0];

            // if (data[0].gender == "MALE") {
            //   this.laboratory.patchValue({
            //     hemoNR: "M: 137-175",
            //     hemaNR: "M:0.40-0.51"
            //   });
            // } else {
            //   this.laboratory.patchValue({
            //     hemoNR: "F: 112-175",
            //     hemaNR: "F:0.34-0.45"
            //   });
            // }
          });

          this.lab.getMicroscopy(data[0].transactionId).subscribe(
            micro => {
              if (micro.length != 0) {
                this.update = true;
                for (var i in micro[0]) {
                  if (this.math.hideRes(micro[0][i])) {
                    if (this.laboratory.get(i) !== null) {
                      this.laboratory.get(i).setValue(micro[0][i]);
                    }
                    // console.log(i + " : " + micro[0][i]);
                  }
                }
              }
            }
          )
          this.lab.getHematology(data[0].transactionId).subscribe(
            hema => {
              if (hema.length != 0) {
                this.update = true;
                for (var i in hema[0]) {
                  if (hema[0][i]) {
                    if (this.math.hideRes(hema[0][i])) {
                      if (this.laboratory.get(i) !== null) {
                        this.laboratory.get(i).setValue(hema[0][i]);
                      }
                      // console.log(i + " : " + hema[0][i]);
                    }
                  }
                }
              }
            }
          )
          this.lab.getSerology(data[0].transactionId).subscribe(
            sero => {
              if (sero.length != 0) {
                this.update = true;
                for (var i in sero[0]) {
                  if (this.math.hideRes(sero[0][i])) {
                    if (this.laboratory.get(i) !== null) {
                      this.laboratory.get(i).setValue(sero[0][i].toString());
                    }
                    // console.log(i + " : " + sero[0][i]);
                  }
                }
              }
            }
          )
          this.lab.getToxicology(data[0].transactionId).subscribe(
            toxic => {
              if (toxic.length != 0) {
                this.update = true;
                for (var i in toxic[0]) {
                  if (this.math.hideRes(toxic[0][i])) {
                    if (this.laboratory.get(i) !== null) {
                      this.laboratory.get(i).setValue(toxic[0][i].toString());
                    }
                    // console.log(i + " : " + toxic[0][i]);
                  }
                }
              }
            }
          )
        }
      }
    )

    this.lab.getMedtech().subscribe(
      medtech => {
        this.medtech = medtech;
      }
    )

    this.laboratory.get("hematocrit").valueChanges.subscribe(data => {
      this.values.forEach(element => {
        if (data == element.one) {
          this.laboratory.patchValue({
            hemoglobin: element.two,
            cbcrbc: element.three
          })
        }
      });
    })

    this.laboratory.get("rbc").valueChanges.subscribe(data => {
      if (data.includes("-")) {
        let temp = data.split("-");
        if (temp[1] !== "") {
          if (temp[1] > 3) {
            let tempData = this.laboratory.controls['uriOt'].value;
            let concatData = "";
            if (tempData == "") {
              concatData = tempData.concat("HEMATURIA");
            } else {
              if (tempData == "NORMAL") {
                concatData = "HEMATURIA";
              } else {
                concatData = tempData.concat(",HEMATURIA");
              }
            }
            this.laboratory.get("uriOt").setValue(concatData);
          }

          if (temp[1] < 3) {
            this.laboratory.get("uriOt").setValue("NORMAL");
          }
        }
      }
    })

    this.laboratory.get("wbc").valueChanges.subscribe(data => {
      if (data.includes("-")) {
        let temp = data.split("-");
        if (temp[1] !== "") {
          if (temp[1] > 10) {
            if (this.patient.gender == "FEMALE") {
              let tempData = this.laboratory.controls['uriOt'].value;
              let concatData = "";
              if (tempData == "") {
                concatData = tempData.concat("UTI");
              } else {
                if (tempData == "NORMAL") {
                  concatData = "UTI";
                } else {
                  concatData = tempData.concat(",UTI");
                }
              }
              this.laboratory.get("uriOt").setValue(concatData);
            }
          }

          if (temp[1] > 5) {
            if (this.patient.gender == "MALE") {
              let tempData = this.laboratory.controls['uriOt'].value;
              let concatData = "";
              if (tempData == "") {
                concatData = tempData.concat("UTI");
              } else {
                if (tempData == "NORMAL") {
                  concatData = "UTI";
                } else {
                  concatData = tempData.concat(",UTI");
                }
              }
              this.laboratory.get("uriOt").setValue(concatData);
            }
          }

          if (temp[1] < 11 || temp[1] < 6) {
            this.laboratory.get("uriOt").setValue("NORMAL");
          }
        }
      }
    });

    this.laboratory.get("cbcot").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.laboratory.get("cbcot").setValue("NORMAL");
      }
      if (data == "WA") {
        this.laboratory.get("cbcot").setValue("WAIVED");
      }
      if (data == "NP") {
        this.laboratory.get("cbcot").setValue("NO SPECIMEN");
      }
    });

    this.laboratory.get("uriOt").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.laboratory.get("uriOt").setValue("NORMAL");
      }
      if (data == "PR") {
        this.laboratory.get("uriOt").setValue("PROTEINURIA");
      }
      if (data == "UT") {
        this.laboratory.get("uriOt").setValue("UTI");
      }
      if (data == "GL") {
        this.laboratory.get("uriOt").setValue("GLUCOSURIA");
      }
    });

    this.laboratory.get("seroOt").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.laboratory.get("seroOt").setValue("NORMAL");
      }
    });

    this.laboratory.get("fecOt").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.laboratory.get("fecOt").setValue("NORMAL");
      }
    });
  }

  addInd() {
    this.laboratory.controls.creationDate.setValue(this.math.dateNow());

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be saved to database!" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        let hema = {
          hemaID: undefined,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          patientIdRef: this.laboratory.get("patientIdRef").value,
          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.math.dateNow(),

          whiteBlood: "N/A",
          neutrophils: "N/A",
          lymphocytes: "N/A",
          monocytes: "N/A",
          eos: "N/A",
          bas: "N/A",
          cbcrbc: this.laboratory.get("cbcrbc").value,
          hemoglobin: this.laboratory.get("hemoglobin").value,
          hematocrit: this.laboratory.get("hematocrit").value,
          hemaNR: this.laboratory.get("hemaNR").value,
          hemoNR: this.laboratory.get("hemoNR").value,
          cbcot: this.laboratory.get("cbcot").value,
          plt: "N/A",

          esr: "N/A",
          esrmethod: "N/A",

          apttime: "N/A",
          apttimeNV: "N/A",
          aptcontrol: "N/A",
          aptcontrolNV: "N/A",

          bloodType: "N/A",
          rh: "N/A",

          clottingTime: "N/A",

          bleedingTime: "N/A",

          ms: "N/A",


          ptime: "N/A",
          ptimeNV: "N/A",
          ptcontrol: "N/A",
          ptcontrolNV: "N/A",
          actPercent: "N/A",
          actPercentNV: "N/A",
          inr: "N/A",
          inrnv: "N/A",

          pr131: "N/A",

        }

        let micro = {
          microID: undefined,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          patientIdRef: this.laboratory.get("patientIdRef").value,
          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.math.dateNow(),

          uriColor: this.laboratory.get("uriColor").value,
          uriTrans: this.laboratory.get("uriTrans").value,
          uriOt: this.laboratory.get("uriOt").value,

          uriPh: this.laboratory.get("uriPh").value,
          uriSp: this.laboratory.get("uriSp").value,
          uriPro: this.laboratory.get("uriPro").value,
          uriGlu: this.laboratory.get("uriGlu").value,

          le: "N/A",
          nit: "N/A",
          uro: "N/A",
          bld: "N/A",
          ket: "N/A",
          bil: "N/A",

          rbc: this.laboratory.get("rbc").value,
          wbc: this.laboratory.get("wbc").value,
          ecells: this.laboratory.get("ecells").value,
          mthreads: this.laboratory.get("mthreads").value,
          bac: this.laboratory.get("bac").value,
          amorph: this.laboratory.get("amorph").value,
          coAx: this.laboratory.get("coAx").value,

          fecColor: this.laboratory.get("fecColor").value,
          fecCon: this.laboratory.get("fecCon").value,
          fecOt: this.laboratory.get("fecOt").value,
          fecMicro: this.laboratory.get("fecMicro").value,

          pregTest: this.laboratory.get("pregTest").value,

          occultBLD: "N/A",

          afbva1: "N/A",
          afbva2: "N/A",
          afbr1: "N/A",
          afbr2: "N/A",
          afbd: "N/A"
        }

        let sero = {
          seroID: undefined,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          patientIdRef: this.laboratory.get("patientIdRef").value,
          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.math.dateNow(),

          hbsAG: this.laboratory.get("hbsAG").value,
          antiHav: "N/A",
          seroOt: this.laboratory.get("seroOt").value,
          vdrl: "N/A",
          psanti: "N/A",
          antiHBS: "N/A",
          hbeAG: "N/A",
          antiHBE: "N/A",
          antiHBC: "N/A",
          tydotigM: "N/A",
          tydotigG: "N/A",
          cea: "N/A",
          afp: "N/A",
          ca125: "N/A",
          ca19: "N/A",
          ca15: "N/A",
          tsh: "N/A",
          ft3: "N/A",
          ft4: "N/A",
          crpdil: "N/A",
          crpres: "N/A",
          hiv1: "N/A",
          hiv2: "N/A"
        }

        let toxic = {
          toxicID: undefined,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          patientIdRef: this.laboratory.get("patientIdRef").value,
          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.math.dateNow(),

          meth: this.laboratory.get("meth").value,
          tetra: this.laboratory.get("tetra").value,
          drugtest: this.laboratory.get("drugtest").value
        }

        for (const key in micro) {
          if (key !== "microID") {
            if (key == "uriColor" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriTrans" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriOt" && micro[key] == "") {
              micro[key] = "N/A";
            }

            if (key == "uriPh" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriSp" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriPro" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriGlu" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "rbc" && micro[key] == "") {
              micro[key] = "N/A";
            }

            if (key == "wbc" && micro[key] == "") {
              micro[key] = "N/A";
            }
          }
        }

        for (const key in hema) {
          if (key !== "hemaID") {
            if (key == "cbcrbc" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hemoglobin" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hematocrit" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hemaNR" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hemoNR" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "cbcot" && hema[key] == "") {
              hema[key] = "N/A";
            }
          }
        }

        for (const key in sero) {
          if (key !== "seroID") {
            if (key == "seroOt" && sero[key] == "") {
              sero[key] = "N/A"
            }
          }
        }

        for (const key in toxic) {
          if (key !== "toxicID") {
            if (key == "meth" && toxic[key] == "") {
              toxic[key] = "N/A";
            }

            if (key == "tetra" && toxic[key] == "") {
              toxic[key] = "N/A";
            }

            if (key == "drugtest" && toxic[key] == "") {
              toxic[key] = "N/A";
            }
          }
        }

        if (this.laboratory.controls['meth'].value == "POSITIVE" || this.laboratory.controls['tetra'].value == "POSITIVE") {
          this.laboratory.get('drugtest').setValue("POSITIVE");
        }

        this.lab.addHematology(hema).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
              this.router.navigate(['/laboratory/industrial']);
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
          }
        )
        this.lab.addMicroscopy(micro).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['/laboratory/industrial']);
            this.dailogRef.close();
          }
        )
        this.lab.addSerology(sero).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['/laboratory/industrial']);
            this.dailogRef.close();
          }
        )
        this.lab.addToxicology(toxic).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['/laboratory/industrial']);
            this.dailogRef.close();
          }
        )

      }

    })
  }

  updateInd() {
    this.laboratory.controls.dateUpdate.setValue(this.math.dateNow());

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be updated!" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        let micro = {
          microID: this.laboratory.get("microID").value,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.laboratory.get("dateUpdate").value,

          uriColor: this.laboratory.get("uriColor").value,
          uriTrans: this.laboratory.get("uriTrans").value,
          uriOt: this.laboratory.get("uriOt").value,

          uriPh: this.laboratory.get("uriPh").value,
          uriSp: this.laboratory.get("uriSp").value,
          uriPro: this.laboratory.get("uriPro").value,
          uriGlu: this.laboratory.get("uriGlu").value,

          le: "N/A",
          nit: "N/A",
          uro: "N/A",
          bld: "N/A",
          ket: "N/A",
          bil: "N/A",

          rbc: this.laboratory.get("rbc").value,
          wbc: this.laboratory.get("wbc").value,
          ecells: this.laboratory.get("ecells").value,
          mthreads: this.laboratory.get("mthreads").value,
          bac: this.laboratory.get("bac").value,
          amorph: this.laboratory.get("amorph").value,
          coAx: this.laboratory.get("coAx").value,

          fecColor: this.laboratory.get("fecColor").value,
          fecCon: this.laboratory.get("fecCon").value,
          fecOt: this.laboratory.get("fecOt").value,
          fecMicro: this.laboratory.get("fecMicro").value,

          pregTest: this.laboratory.get("pregTest").value,

          occultBLD: "N/A",

          afbva1: "N/A",
          afbva2: "N/A",
          afbr1: "N/A",
          afbr2: "N/A",
          afbd: "N/A"
        }
        let hema = {
          hemaID: this.laboratory.get("hemaID").value,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.laboratory.get("dateUpdate").value,

          whiteBlood: "N/A",
          neutrophils: "N/A",
          lymphocytes: "N/A",
          monocytes: "N/A",
          eos: "N/A",
          bas: "N/A",
          cbcrbc: this.laboratory.get("cbcrbc").value,
          hemoglobin: this.laboratory.get("hemoglobin").value,
          hematocrit: this.laboratory.get("hematocrit").value,
          hemaNR: this.laboratory.get("hemaNR").value,
          hemoNR: this.laboratory.get("hemoNR").value,
          cbcot: this.laboratory.get("cbcot").value,
          plt: "N/A",

          esr: "N/A",
          esrmethod: "N/A",

          apttime: "N/A",
          apttimeNV: "N/A",
          aptcontrol: "N/A",
          aptcontrolNV: "N/A",

          bloodType: "N/A",
          rh: "N/A",

          clottingTime: "N/A",

          bleedingTime: "N/A",

          ms: "N/A",


          ptime: "N/A",
          ptimeNV: "N/A",
          ptcontrol: "N/A",
          ptcontrolNV: "N/A",
          actPercent: "",
          actPercentNV: "N/A",
          inr: "N/A",
          inrnv: "N/A",

          pr131: "N/A",

        }

        let sero = {
          seroID: this.laboratory.get("seroID").value,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.math.dateNow(),

          hbsAG: this.laboratory.get("hbsAG").value,
          antiHav: "N/A",
          seroOt: this.laboratory.get("seroOt").value,
          vdrl: "N/A",
          psanti: "N/A",
          antiHBS: "N/A",
          hbeAG: "N/A",
          antiHBE: "N/A",
          antiHBC: "N/A",
          tydotigM: "N/A",
          tydotigG: "N/A",
          cea: "N/A",
          afp: "N/A",
          ca125: "N/A",
          ca19: "N/A",
          ca15: "N/A",
          tsh: "N/A",
          ft3: "N/A",
          ft4: "N/A",
          crpdil: "N/A",
          crpres: "N/A",
          hiv1: "N/A",
          hiv2: "N/A"
        }

        let toxic = {
          toxicID: this.laboratory.get("toxicID").value,
          transactionID: this.laboratory.get("transactionID").value,
          patientID: this.laboratory.get("patientID").value,

          pathID: this.laboratory.get("pathID").value,
          medID: this.laboratory.get("medID").value,
          qualityID: this.laboratory.get("qualityID").value,

          userID: +parseInt(sessionStorage.getItem('token')),

          creationDate: this.laboratory.get("creationDate").value,
          dateUpdate: this.math.dateNow(),

          meth: this.laboratory.get("meth").value,
          tetra: this.laboratory.get("tetra").value,
          drugtest: this.laboratory.get("drugtest").value
        }

        for (const key in micro) {
          if (key !== "microID") {
            if (key == "uriColor" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriTrans" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriOt" && micro[key] == "") {
              micro[key] = "N/A";
            }

            if (key == "uriPh" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriSp" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriPro" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "uriGlu" && micro[key] == "") {
              micro[key] = "-";
            }

            if (key == "rbc" && micro[key] == "") {
              micro[key] = "N/A";
            }

            if (key == "wbc" && micro[key] == "") {
              micro[key] = "N/A";
            }
          }
        }

        for (const key in hema) {
          if (key !== "hemaID") {
            if (key == "cbcrbc" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hemoglobin" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hematocrit" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hemaNR" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "hemoNR" && hema[key] == "") {
              hema[key] = "N/A";
            }

            if (key == "cbcot" && hema[key] == "") {
              hema[key] = "N/A";
            }
          }
        }

        for (const key in sero) {
          if (key !== "seroID") {
            if (key == "seroOt" && sero[key] == "") {
              sero[key] = "N/A"
            }
          }
        }

        for (const key in toxic) {
          if (key !== "toxicID") {
            if (key == "meth" && toxic[key] == "") {
              toxic[key] = "N/A";
            }

            if (key == "tetra" && toxic[key] == "") {
              toxic[key] = "N/A";
            }

            if (key == "drugtest" && toxic[key] == "") {
              toxic[key] = "N/A";
            }
          }
        }

        if (micro['microID'] == "") {
          this.lab.addMicroscopy(micro).subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly saved", "ok");
              } else {
                this.math.openSnackBar("Error Saving data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )

        } else {
          this.lab.addMicroscopy(micro, "/updateMicroscopy").subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly update", "ok");
              } else {
                this.math.openSnackBar("Error Updating data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )

        }

        if (hema['hemaID'] == "") {
          this.lab.addHematology(hema).subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly saved", "ok");
              } else {
                this.math.openSnackBar("Error Saving data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )
        } else {
          this.lab.addHematology(hema, "/updateHematology").subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly update", "ok");
              } else {
                this.math.openSnackBar("Error Updating data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )

        }

        if (sero['seroID'] == "") {
          this.lab.addSerology(sero).subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly saved", "ok");
              } else {
                this.math.openSnackBar("Error Saving data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )
        } else {
          this.lab.addSerology(sero, "/updateSerology").subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly update", "ok");
              } else {
                this.math.openSnackBar("Error Updating data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )

        }

        if (toxic['toxicID'] == "") {
          this.lab.addToxicology(toxic).subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly saved", "ok");
              } else {
                this.math.openSnackBar("Error Saving data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )
        } else {
          this.lab.addToxicology(toxic, "/updateToxicology").subscribe(
            data => {
              if (data == 1) {
                this.math.openSnackBar("Data successfuly update", "ok");
              } else {
                this.math.openSnackBar("Error Updating data!!!", "ok");
              }
              // this.router.navigate(['/laboratory/industrial']);
              this.dailogRef.close();
            }
          )

        }
      }

    })

  }
}
