import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-pe-list',
  templateUrl: './pe-list.component.html',
  styleUrls: ['./pe-list.component.scss']
})
export class PeListComponent implements OnInit {

  public peList = "peList";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("pe");
  }

  ngOnInit() {
  }

}
