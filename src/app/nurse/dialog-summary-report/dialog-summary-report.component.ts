import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { TransactionService } from 'src/app/services/transaction.service';
import { PatientService } from 'src/app/services/patient.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { DatePipe } from '@angular/common';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import * as moment from 'moment';
import { MathService } from 'src/app/services/math.service';
import { PeService } from 'src/app/services/pe.service';
import { XrayService } from 'src/app/services/xray.service';
import { ItemService } from 'src/app/services/item.service';
import { itemList } from 'src/app/services/service.interface';

export interface trans_items {
  itemName: string,
  itemPrice: number,
  itemDescription: string,
  qty: number,
  subtotal: any,
  disc: number
}

@Component({
  selector: 'app-dialog-summary-report',
  templateUrl: './dialog-summary-report.component.html',
  styleUrls: ['./dialog-summary-report.component.scss']
})
export class DialogSummaryReportComponent implements OnInit {

  d = new DatePipe('en-US');
  heldData = [];
  displayedColumns = ["transactionId", "date", "company", "name", "agesex", "chestxray", "urinalysis", "fecalysis", "cbc", "medicalclassification", "remarks", "contactNo"];
  dataSource: MatTableDataSource<any>;

  public headers = [
    "TRANSACTION ID", "DATE", "COMPANY", "NAME",
    "AGE/SEX", "CHEST X-RAY", "URINALYSIS", "FECALYSIS", "CBC", "MEDICAL CLASSIFICATION", "REMARKS",
    "CONTACT NO"
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('type') type;
  public urlData: any;

  public types = ["Basic 5", "With DT", "With DT & HBSAG"];
  public typeDefault = this.types[0];
  public companyName: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private TS: TransactionService,
    private PS: PatientService,
    private LS: LaboratoryService,
    private XS: XrayService,
    private peService: PeService,
    private math: MathService,
    private IS: ItemService,
  ) {
    this.companyName = this.data.companyName;
  }

  ngOnInit() {
    this.getData();
  }

  change(value) {
    this.typeDefault = value;
    if (value == "With DT") {
      this.displayedColumns = ["transactionId", "date", "company", "name", "agesex", "chestxray", "urinalysis", "fecalysis", "cbc", "drugtest", "medicalclassification", "remarks", "contactNo"];

    } else if (value == "With DT & HBSAG") {
      this.displayedColumns = ["transactionId", "date", "company", "name", "agesex", "chestxray", "urinalysis", "fecalysis", "cbc", "drugtest", "hbsag", "medicalclassification", "remarks", "contactNo"];

    } else {
      this.displayedColumns = ["transactionId", "date", "company", "name", "agesex", "chestxray", "urinalysis", "fecalysis", "cbc", "medicalclassification", "remarks", "contactNo"];
    }
  }

  getData() {
    let from = this.d.transform(new Date(this.data.from), "yyyy-MM-dd HH:mm:ss");
    let to = this.d.transform(new Date(this.data.to), "yyyy-MM-dd HH:mm:ss");
    let companyName = this.companyName;

    const url = "getTransactionDateBillerSummary/" + from + "/" + to + "/" + companyName;
    this.urlData = url;
    console.log(url);
    this.TS.getTransactions(url).subscribe(
      data => {

        this.heldData = [];
        if (data.length > 0) {
          data.forEach(trans => {
            let transactionId = trans.transactionId;
            let patientId= trans.patientId;

            this.PS.getOnePatient("getPatient/" + patientId).subscribe(
              result => {
                let patInfo = result[0];

                if (companyName == "VXI") {
                  this.displayedColumns = ["date", "dateemailed", "datereceivedchardcopy", "dtsoftcopy", "lob", "remarks", "class", "lastname", "firstname", "middlename", "age", "sex", "ht(ft)", "wt(kg)", "bp(mmHg)", "visualacuity", "urinalysis", "cbc", "chestxray", "drugtest", "alv/spotview", "medicalhistory", "pastsurgicalhistory", "physicalexaminationfindings", "recommendations"];

                  let transData = {
                    date: "",
                    dateemailed: "",
                    datereceivedchardcopy: "",
                    dtsoftcopy: "",
                    lob: "",
                    remarks: "",
                    class: "",
                    lastname: "",
                    firstname: "",
                    middlename: "",
                    age: "",
                    sex: "",
                    "ht(ft)": "",
                    "wt(kg)": "",
                    "bp(mmHg)": "",
                    visualacuity: "",
                    urinalysis: "",
                    cbc: "",
                    chestxray: "",
                    drugtest: "",
                    "alv/spotview": "NONE",
                    medicalhistory: "",
                    pastsurgicalhistory: "",
                    physicalexaminationfindings: "NORMAL",
                    recommendations: ""
                  };

                  if (patInfo['companyName'] == companyName) {
                    transData.date = trans.transactionDate;
                    transData.lastname = patInfo['lastName'];
                    transData.firstname = patInfo['firstName'];
                    transData.middlename = patInfo['middleName'];
                    transData.age = patInfo['age'];
                    transData.sex = patInfo['gender'];

                    // Classification
                    this.LS.getOneClass(transactionId).subscribe(
                      medClass => {
                        if (medClass) {
                          if (medClass['medicalClass'] == "CLASS A - Physically Fit") {
                            transData.class = "Class A";
                            transData.remarks = "FIT TO WORK";

                          } else if (medClass['medicalClass'] == "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency") {
                            transData.class = "Class B";
                            transData.remarks = "FIT TO WORK";

                          } else if (medClass['medicalClass'] == "CLASS C - With abnormal findings generally not accepted for employment.") {
                            transData.class = "Class C";
                            transData.remarks = "Unemployable";

                          } else if (medClass['medicalClass'] == "CLASS D - Unemployable") {
                            transData.class = "Class D";
                            transData.remarks = "Unemployable";

                          } else {
                            transData.class = "PENDING";
                            transData.remarks = medClass['notes'];

                          }
                        }
                      }
                    );

                    // PE Vital
                    this.peService.getVital(transactionId).subscribe(
                      vital => {
                        if (vital !== null) {
                          transData['ht(ft)'] = vital.height;
                          transData['wt(kg)'] = vital.weight;
                          transData['bp(mmHg)'] = vital.bp;

                          if (vital.uod && vital.uos !== "") {
                            transData.visualacuity = vital.uod + "," + vital.uos + " -- ( UNCORRECTED )";
                          } else {
                            transData.visualacuity = vital.cod + "," + vital.cos + " -- ( CORRECTED )";
                          }

                          transData.pastsurgicalhistory = vital.opr;
                        }
                      }
                    );

                    // PE Medhis
                    this.peService.getMedhis(transactionId).subscribe(
                      medhis => {
                        if (medhis !== null) {
                          let histories = [
                            { "label": "Astma", "value": "asth" },
                            { "label": "Tuberculosis", "value": "tb" },
                            { "label": "Diabetes Mellitus", "value": "dia" },
                            { "label": "High Blood Pressure", "value": "hp" },
                            { "label": "Heart Problem", "value": "hp" },
                            { "label": "Kidney Problem", "value": "kp" },
                            { "label": "Abdominal/Hernia", "value": "ab" },
                            { "label": "Joint/Back/Scoliosis", "value": "jbs" },
                            { "label": "Psychiatric Problem", "value": "pp" },
                            { "label": "Migraine/Headache", "value": "mh" },
                            { "label": "Fainting/Seizures", "value": "fs" },
                            { "label": "Allergies", "value": "alle" },
                            { "label": "Cancer/Tumor", "value": "ct" },
                            { "label": "Hepatitis", "value": "hep" },
                            { "label": "STD", "value": "std" }
                          ];

                          let medHistory = [];
                          histories.forEach(element => {
                            let value = element.value;
                            if (medhis[value] == "YES") {
                              medHistory.push(element.label);
                            }
                          });

                          if (medHistory.length < 1) {
                            transData.medicalhistory = "-";
                          } else {
                            transData.medicalhistory = medHistory.toString();
                          }

                        } else {
                          transData.medicalhistory = "-";
                        }
                      }
                    );

                    // PE PE
                    this.peService.getPE(transactionId).subscribe(
                      pe => {
                        if (pe !== null) {
                          transData.recommendations = pe.ot;
                        }
                      }
                    );

                    // Hematology
                    this.LS.getHematology(transactionId).subscribe(
                      hema => {
                        if (hema.length > 0) {
                          transData.cbc = hema[0]['cbcot'];
                        }
                      }
                    );

                    // Microsocopy
                    this.LS.getMicroscopy(transactionId).subscribe(
                      micro => {
                        if (micro.length > 0) {
                          transData.urinalysis = micro[0]['uriOt'];
                        }
                      }
                    );

                    // Xray
                    this.XS.getOneXray(transactionId).subscribe(
                      xray => {
                        if (xray) {
                          if (xray['impression'] == "NORMAL CHEST FINDINGS" || xray['impression'] == "CONSIDERED NORMAL CHEST PA") {
                            transData.chestxray = "NORMAL";

                          } else if (xray['impression'] == "-NORMAL CHEST FINDINGS") {
                            transData.chestxray = "NORMAL";

                          } else if (xray['impression'] == "" || xray['impression'] == null) {
                            transData.chestxray = "NO XRAY";

                          } else {
                            transData.chestxray = xray['impression'];

                          }
                        }
                      }
                    );

                    // Toxicology
                    this.LS.getToxicology(transactionId).subscribe(
                      toxic => {
                        if (toxic.length > 0) {
                          transData.drugtest = "Meth: " + toxic[0]['meth'] + " Tetra: " + toxic[0]['tetra'];
                        }
                      }
                    );

                    this.heldData.push(transData);
                    this.dataSource = new MatTableDataSource(this.heldData);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                    // PE
                    /* this.PES.getPE(transactionId).subscribe(
                      pe => {
                        if (pe) {
                          this.heldData.push(transData);
                          this.dataSource = new MatTableDataSource(this.heldData);
                          this.dataSource.paginator = this.paginator;
                          this.dataSource.sort = this.sort;
                        }
                      }
                    ); */
                  }

                } else if (companyName == "HYGIENE") {
                  this.displayedColumns = ["date", "company", "lastname", "firstname", "middlename", "age", "sex", "urinalysis", "chestxray"];

                  let transData = {
                    transactionId: transactionId,
                    date: "",
                    company: "",
                    lastname: "",
                    firstname: "",
                    middlename: "",
                    age: "",
                    sex: "",
                    chestxray: "",
                    urinalysis: "",
                    fecalysis: "",
                    items: []
                  };

                  transData.date = trans.transactionDate;
                  transData.company = patInfo['companyName'];
                  transData.lastname = patInfo['lastName'];
                  transData.firstname = patInfo['firstName'];
                  transData.middlename = patInfo['middleName'];
                  transData.age = patInfo['age'];
                  transData.sex = patInfo['gender'];

                  this.TS.getTransExt(trans.transactionId).subscribe(transExt => {
                    if (transExt.length > 0) {
                      transExt.forEach((ext, index) => {
                        if (ext.packageName != null) {
                          this.IS.getPack("getPackageName/" + ext.packageName)
                            .subscribe(
                              pack => {
                                let packItem: itemList = {
                                  itemId: pack[0].packageName,
                                  itemName: pack[0].packageName,
                                  itemPrice: pack[0].packagePrice,
                                  itemDescription: pack[0].packageDescription,
                                  itemType: pack[0].packageType,
                                  deletedItem: pack[0].deletedPackage,
                                  neededTest: undefined,
                                  creationDate: pack[0].creationDate,
                                  dateUpdate: pack[0].dateUpdate,
                                }
                                transData.items.push(packItem);
                              }
                            )
                        } else if (ext.itemID) {
                          this.IS.getItemByID(ext.itemID)
                            .subscribe(item => {
                              transData.items.push(item[0]);
                            });
                        }
                      });
                    }
                  });

                  // Microsocopy
                  this.LS.getMicroscopy(transactionId).subscribe(
                    micro => {
                      if (micro.length > 0) {
                        if (micro[0]['fecMicro'] == "NO OVA OR PARASITE SEEN" || micro[0]['fecMicro'] == "NO INTESTINAL PARASITE SEEN") {
                          transData.fecalysis = "NIPS";

                        } else if (micro[0]['fecMicro'] == "Presence of:" || micro[0]['fecMicro'] == "Presence of: ") {
                          transData.fecalysis = micro[0]['fecOt'];

                        } else {
                          transData.fecalysis = "NO SPECIMEN";

                        }

                        transData.urinalysis = micro[0]['uriOt'];
                      }
                    }
                  );

                  // Xray
                  this.XS.getOneXray(transactionId).subscribe(
                    xray => {
                      if (xray) {
                        if (xray['impression'] == "NORMAL CHEST FINDINGS" || xray['impression'] == "CONSIDERED NORMAL CHEST PA") {
                          transData.chestxray = "NORMAL";

                        } else if (xray['impression'] == "-NORMAL CHEST FINDINGS") {
                          transData.chestxray = "NORMAL";

                        } else if (xray['impression'] == "" || xray['impression'] == null) {
                          transData.chestxray = "NO XRAY";

                        } else {
                          transData.chestxray = xray['impression'];

                        }
                      }
                    }
                  );

                  this.heldData.push(transData);
                  this.dataSource = new MatTableDataSource(this.heldData);
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  // PE
                /* this.PES.getPE(transactionId).subscribe(
                  pe => {
                    if (pe) {
                      this.heldData.push(transData);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  }
                );*/
                } else {
                  let transData = {
                    transactionId: transactionId,
                    date: "",
                    company: "",
                    name: "",
                    agesex: "",
                    chestxray: "",
                    urinalysis: "",
                    fecalysis: "",
                    cbc: "",
                    drugtest: "",
                    hbsag: "",
                    medicalclassification: "",
                    remarks: "",
                    contactNo: ""
                  };

                  transData.date = trans.transactionDate;
                  transData.company = patInfo['companyName'];
                  transData.name = patInfo['fullName'];
                  transData.agesex = patInfo['age'] + "/" + patInfo['gender'];

                  // Hematology
                  this.LS.getHematology(transactionId).subscribe(
                    hema => {
                      if (hema.length > 0) {
                        transData.cbc = hema[0]['cbcot'];
                      }
                    }
                  );

                  // Microsocopy
                  this.LS.getMicroscopy(transactionId).subscribe(
                    micro => {
                      if (micro.length > 0) {
                        if (micro[0]['fecMicro'] == "NO OVA OR PARASITE SEEN" || micro[0]['fecMicro'] == "NO INTESTINAL PARASITE SEEN") {
                          transData.fecalysis = "NIPS";

                        } else if (micro[0]['fecMicro'] == "Presence of:" || micro[0]['fecMicro'] == "Presence of: ") {
                          transData.fecalysis = micro[0]['fecOt'];

                        } else {
                          transData.fecalysis = "NO SPECIMEN";

                        }

                        transData.urinalysis = micro[0]['uriOt'];
                      }
                    }
                  );

                  // Xray
                  this.XS.getOneXray(transactionId).subscribe(
                    xray => {
                      if (xray) {
                        if (xray['impression'] == "NORMAL CHEST FINDINGS" || xray['impression'] == "CONSIDERED NORMAL CHEST PA") {
                          transData.chestxray = "NORMAL";

                        } else if (xray['impression'] == "-NORMAL CHEST FINDINGS") {
                          transData.chestxray = "NORMAL";

                        } else if (xray['impression'] == "" || xray['impression'] == null) {
                          transData.chestxray = "NO XRAY";

                        } else {
                          transData.chestxray = xray['impression'];

                        }
                      }
                    }
                  );

                  // Classification
                  this.LS.getOneClass(transactionId).subscribe(
                    medClass => {
                      if (medClass) {
                        if (medClass['medicalClass'] == "CLASS A - Physically Fit") {
                          transData.medicalclassification = "Class A";
                          transData.remarks = "FIT TO WORK";

                        } else if (medClass['medicalClass'] == "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency") {
                          transData.medicalclassification = "Class B";
                          transData.remarks = "FIT TO WORK";

                        } else if (medClass['medicalClass'] == "CLASS C - With abnormal findings generally not accepted for employment.") {
                          transData.medicalclassification = "Class C";
                          transData.remarks = "Unemployable";

                        } else if (medClass['medicalClass'] == "CLASS D - Unemployable") {
                          transData.medicalclassification = "Class D";
                          transData.remarks = "Unemployable";

                        } else {
                          transData.medicalclassification = "PENDING";
                          transData.remarks = medClass['notes'];

                        }
                      }
                    }
                  );

                  // Serology
                  this.LS.getSerology(transactionId).subscribe(
                    data => {
                      if (data.length > 0) {
                        transData.hbsag = data[0]['hbsAG'];
                      }
                    }
                  );

                  // Toxicology
                  this.LS.getToxicology(transactionId).subscribe(
                    toxic => {
                      if (toxic.length > 0) {
                        transData.drugtest = toxic[0]['drugtest'];
                      }
                    }
                  );

                  this.heldData.push(transData);
                  this.dataSource = new MatTableDataSource(this.heldData);
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  // PE
                  /* this.PES.getPE(transactionId).subscribe(
                    pe => {
                      if (pe) {
                        this.heldData.push(transData);
                        this.dataSource = new MatTableDataSource(this.heldData);
                        this.dataSource.paginator = this.paginator;
                        this.dataSource.sort = this.sort;
                      }
                    }
                  );*/
                }
              }
            );
          });
        }else {
          this.dataSource = new MatTableDataSource([]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  excel() {
    let csvName = this.companyName + " " + moment().format("YYYY-MM-DD");
    let data = [];

    if (this.companyName == "VXI") {
      this.headers = ["DATE", "DATE EMAILED", "DATE RECEIVED HARD COPY", "DT SOFT COPY", "LOB", "REMARKS", "CLASS", "LAST NAME", "FIRST NAME", "MIDDLE NAME", "AGE", "SEX", "HT(FT)", "WT(KG)", "BP(mmHg)", "VISUAL ACUITY", "URINALYSIS", "CBC", "CHEST XRAY", "DRUG TEST", "ALV/SPOT VIEW", "MEDICAL HISTORY", "PAST SURGICAL HISTORY", "PHYSICAL EXAMINATION HISTORY", "RECOMMENDATIONS"];

      this.heldData.forEach(trans => {
        let transArr = [{
          date: trans.date,
          dateemailed: trans.dateemailed,
          datereceivedchardcopy: trans.datereceivedchardcopy,
          dtsoftcopy: trans.dtsoftcopy,
          lob: trans.lob,
          remarks: trans.remarks,
          class: trans.class,
          lastname: trans.lastname,
          firstname: trans.firstname,
          middlename: trans.middlename,
          age: trans.age,
          sex: trans.sex,
          ht: trans.ht,
          wt: trans.wt,
          bp: trans.bp,
          visualacuity: trans.visualacuity,
          urinalysis: trans.urinalysis,
          cbc: trans.cbc,
          chestxray: trans.chestxray,
          drugtest: trans.drugtest,
          alv: trans.alv,
          medicalhistory: trans.medicalhistory,
          pastsurgicalhistory: trans.pastsurgicalhistory,
          physicalexaminationfindings: trans.physicalexaminationfindings,
          recommendations: trans.recommendations,
        }];

        data.push(transArr[0]);
      });
    } else if (this.companyName == "HYGIENE") {
      this.headers = ["DATE", "COMPANY", "LASTNAME", "FIRSTNAME", "MIDDLENAME", "AGE", "SEX", "PACKAGE", "URINALYSIS", "CHESTXRAY"];

      this.heldData.forEach(trans => {
        let packageName = "";
        trans.items.forEach(element => {
          if (packageName == "") {
            packageName = element.itemName;
          } else {
            packageName = packageName + " | " + element.itemName;
          }
        });
        let transArr = [{
          date: trans.date,
          company: trans.company,
          lastname: trans.lastname,
          firstname: trans.firstname,
          middlename: trans.middlename,
          age: trans.age,
          sex: trans.sex,
          package: packageName,
          urinalysis: trans.urinalysis,
          chestxray: trans.chestxray,
        }];

        data.push(transArr[0]);
      });
    } else {
      if (this.typeDefault == "With DT") {
        this.headers = [
          "TRANSACTION ID", "DATE", "COMPANY", "NAME",
          "AGE/SEX", "CHEST X-RAY", "URINALYSIS", "FECALYSIS", "CBC", "DRUGTEST", "MEDICAL CLASSIFICATION", "REMARKS",
          "CONTACT NO"
        ];

        this.heldData.forEach(trans => {
          let transArr = [{
            transactionId: trans.transactionId,
            date: trans.date,
            company: trans.company,
            name: trans.name,
            agesex: trans.agesex,
            chestxray: trans.chestxray,
            urinalysis: trans.urinalysis,
            fecalysis: trans.fecalysis,
            cbc: trans.cbc,
            drugtest: trans.drugtest,
            medicalclassification: trans.medicalclassification,
            remarks: trans.remarks,
            contactNo: trans.contactNo
          }];

          data.push(transArr[0]);
        });
      } else if (this.typeDefault == "With DT & HBSAG") {
        this.headers = [
          "TRANSACTION ID", "DATE", "COMPANY", "NAME",
          "AGE/SEX", "CHEST X-RAY", "URINALYSIS", "FECALYSIS", "CBC", "DRUGTEST", "HBSAG", "MEDICAL CLASSIFICATION", "REMARKS",
          "CONTACT NO"
        ];

        this.heldData.forEach(trans => {
          let transArr = [{
            transactionId: trans.transactionId,
            date: trans.date,
            company: trans.company,
            name: trans.name,
            agesex: trans.agesex,
            chestxray: trans.chestxray,
            urinalysis: trans.urinalysis,
            fecalysis: trans.fecalysis,
            cbc: trans.cbc,
            drugtest: trans.drugtest,
            hbsag: trans.hbsag,
            medicalclassification: trans.medicalclassification,
            remarks: trans.remarks,
            contactNo: trans.contactNo
          }];

          data.push(transArr[0]);
        });

      } else {
        this.heldData.forEach(trans => {
          let transArr = [{
            transactionId: trans.transactionId,
            date: trans.date,
            company: trans.company,
            name: trans.name,
            agesex: trans.agesex,
            chestxray: trans.chestxray,
            urinalysis: trans.urinalysis,
            fecalysis: trans.fecalysis,
            cbc: trans.cbc,
            medicalclassification: trans.medicalclassification,
            remarks: trans.remarks,
            contactNo: trans.contactNo
          }];

          data.push(transArr[0]);
        });
      }
    }

    new Angular5Csv(data, csvName, {
      headers: this.headers
    });
  }

  pdf() {
    const suffix = [this.urlData, this.companyName];
    this.math.printPdfSummaryReport('', suffix);
  }
}
