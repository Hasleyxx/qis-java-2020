import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PatientService } from 'src/app/services/patient.service';
import { XrayService } from 'src/app/services/xray.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-image-dialog-pdf-print',
  templateUrl: './image-dialog-pdf-print.component.html',
  styleUrls: ['./image-dialog-pdf-print.component.scss']
})
export class ImageDialogPdfPrintComponent implements OnInit {

  data: string[];
  tid: any = "";
  dataDetails: Promise<any>[];

  pid: number = 0;
  transData: any;
  patientData: any;
  xrayData: any;
  xrayImg = "";

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private TS: TransactionService,
    private PS: PatientService,
    private XS: XrayService
  ) {
    this.data = route.snapshot.params["data"].split(",");
    this.tid = this.data[0];
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.dataDetails = this.data.map(data => this.getXrayScanReady(data));

    this.TS.getOneTrans("getTransaction/" + this.tid).subscribe((data: any) => {
      if (data.length > 0) {
        this.transData = data[0];
        this.pid = data[0].patientId;

        this.PS.getOnePatient("getPatient/" + this.pid).subscribe((data: any) => {
          if (data.length > 0) {
            this.patientData = data[0];
          }
        });

        this.XS.getOneXray(this.tid).subscribe((data: any) => {
          if (data !== null) {
            this.xrayImg = "assets/xray/" + data.imgXray;
          }
        });

        this.XS.getMarker(this.tid).subscribe((data: any) => {
          if (data.length > 0) {
            this.xrayData = data[0];
          }
        });
      }
    });

    Promise.all(this.dataDetails).then(() => {
      setTimeout(() => {
        this.math.onXrayScanPrintReady();
      });
    });
  }

  getXrayScanReady(data) {
    const amount = Math.floor(Math.random() * 100);
    return new Promise(resolve => setTimeout(() => resolve({ amount }), 1000));
  }

}
