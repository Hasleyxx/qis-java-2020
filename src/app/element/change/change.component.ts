import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.scss']
})
export class ChangeComponent implements OnInit {

  Title = "";
  Content = "";
  cancelBTN = "Cancel";
  okBTN = "Continue";
  constructor(
    public dialogRef: MatDialogRef<ChangeComponent>,
    @Inject(MAT_DIALOG_DATA) public config: any) {
    if (config.Title) {
      this.Title = this.Title + config.Title;
      this.Content = this.Content + config.Content;
    }
    if (config.cancel) {
      this.cancelBTN = config.cancel;
    }
    if (config.ok) {
      this.okBTN = config.ok;
    }
  }

  ngOnInit() {
    // this.Title = this.config.Title;
    // this.Content = this.config.Content;
    // this.cancelBTN = this.config
  }
  ok() {
    this.dialogRef.close("ok");
  }
  cancel(rel: number = 0) {
    this.dialogRef.close("cancel");
    if (rel == 1) {
      location.reload();
    }
  }

}
