import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { XrayService } from 'src/app/services/xray.service';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-imaging-summary-update',
  templateUrl: './imaging-summary-update.component.html',
  styleUrls: ['./imaging-summary-update.component.scss']
})
export class ImagingSummaryUpdateComponent implements OnInit {

  public title: any;
  public personels = ["Febie Dane C. Buada, RRT", "George M. Sapnu", "Chris Noel Cornejo"];
  public personelValue = this.personels[0]['rad'];

  public radiologist = [
    {
      "rad": "Ricardo MA. O. Pacheco,MD.DPBR", "details": "Lung fields are clear. Heart is not enlarged. Diaphragm, its sulci visualized bone are intact."
    },
    {
      "rad": "Salvador Ramirez, MD.DPBR", "details": "No abnormal densities seen in both lung parenchyma. The heart is normal in size and configuration. Aorta is unremarkable. The diaphragms, costrophrenic sulci and bony throrax are intact."
    }
  ];
  public radiologistValue = this.radiologist[0]['rad'];

  public patientId: any;
  public transactionId: any;
  public patientDatas: any;
  public fullName: any;
  public transactionDatas: any;
  public xrayDatas: any;
  public rad: any = [];

  public xrayUpdateForm: FormGroup;
  route: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private XS: XrayService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ImagingSummaryUpdateComponent>
  ) {
    this.transactionId = this.data.tid;
    this.patientId = this.data.pid;
    this.title = this.data.title;
  }

  ngOnInit() {
    this.getData();
    this.xrayUpdateForm = this.formBuilder.group({
      xrayID: [''],
      transactionId: [''],
      patientId: [''],
      comment: ['', Validators.required],
      impression: ['', Validators.required],
      qa: ['', Validators.required],
      userID: [''],
      radiologist: ['', Validators.required],
      creationDate: [''],
      dateUpdate: ['']
    });
  }

  getData() {
    this.TS.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientId = data[0]['patientId'];
        this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => {
            this.patientDatas = data[0];
          }
        );
      }
    );

    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        this.xrayDatas = data;
        this.xrayUpdateForm.patchValue({
          xrayID: +data['xrayID'],
          comment: data['comment'],
          impression: data['impression'],
          qa: data['qa'],
          radiologist: data['radiologist'],
          creationDate: data['creationDate'],
        });
      }
    );

  }

  updateXray() {
    this.xrayUpdateForm.patchValue({
      transactionId: this.transactionId,
      patientId: this.patientId,
      userID: parseInt(sessionStorage.getItem('token'))
    });
    this.xrayUpdateForm.patchValue({
      dateUpdate: moment().format("YYYY-MM-DD h:mm:ss"),
    });
    this.XS.updateXray(this.xrayUpdateForm.value).subscribe(
      result => {
        if (result == "1") {
          this.dialogRef.close(this.xrayUpdateForm.value);
        }
      }
    );
  }

  personel(value: any) {
    this.personelValue = value;
  }

  radiology(value: any) {
    for (let index = 0; index < this.radiologist.length; index++) {
      const element = this.radiologist[index];
      if (element.rad == value) {
        this.xrayUpdateForm.patchValue({
          comment: element.details,
          impression: 'NORMAL CHEST FINDING'
        });
      }
    }
  }
}
