import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PeService } from 'src/app/services/pe.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-pe-update',
  templateUrl: './pe-update.component.html',
  styleUrls: ['./pe-update.component.scss']
})
export class PeUpdateComponent implements OnInit {

  public patientId: any;
  public transactionId: any;

  public patientDatas: any = [];
  public transactionDatas: any = [];
  public medHisDatas: any = [];
  public vitalDatas: any = [];
  public peDatas: any = [];

  public peUpdateForm: FormGroup;

  public medicals = [
    { "category": "Asthma", "control": "asth" },
    { "category": "Tuberculosis", "control": "tb" },
    { "category": "Diabetes Mellitus", "control": "dia" },
    { "category": "High Blood Pressure", "control": "hb" },
    { "category": "Heart Problem", "control": "hp" },
    { "category": "Kidney Problem", "control": "kp" },
    { "category": "Abdominal/Hernia", "control": "ab" },
    { "category": "Joint/Back/Scoliosis", "control": "jbs" },
    { "category": "Psychiatric Problem", "control": "pp" },
    { "category": "Migraine/Headache", "control": "mh" },
    { "category": "Fainting/Seizures", "control": "fs" },
    { "category": "Allergies", "control": "alle" },
    { "category": "Cancer/Tumor", "control": "ct" },
    { "category": "Hepatitis", "control": "hep" },
    { "category": "STD/PLHIV", "control": "std" }
  ];

  public areas = [
    { "area": "Skin", "control": "skin" },
    { "area": "Head and Neck", "control": "hn" },
    { "area": "Chest/Breast/Lungs", "control": "cbl" },
    { "area": "Cardiac/Heart", "control": "ch" },
    { "area": "Abdomen", "control": "abdo" },
    { "area": "Extremities", "control": "extre" }
  ]

  public doctors = [
    { "doctor": "FROILAN A. CANLAS, M.D.", "license": "82498" },
    { "doctor": "JOHN TANGLAO, M.D.", "license": "79549" },
    { "doctor": "MARIGOLD A. SIBAL, M.D.", "license": "104200" }
  ];

  constructor(
    private math: MathService,
    private route: ActivatedRoute,
    private PS: PatientService,
    private TS: TransactionService,
    private peService: PeService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.math.navSubs('pe');
  }

  ngOnInit() {
    this.patientId = this.route.snapshot.paramMap.get('pid');
    this.transactionId = this.route.snapshot.paramMap.get('tid');

    this.getData();

    this.peUpdateForm = this.formBuilder.group({
      medhisForm: this.formBuilder.group({
        transactionId: [''],
        patientId: [''],
        asth: [''],
        tb: [''],
        dia: [''],
        hb: [''],
        hp: [''],
        kp: [''],
        ab: [''],
        jbs: [''],
        pp: [''],
        mh: [''],
        fs: [''],
        alle: [''],
        ct: [''],
        hep: [''],
        std: [''],
        creationDate: [''],
        dateUpdate: ['']
      }),
      vitalForm: this.formBuilder.group({
        transactionId: [''],
        patientId: [''],
        height: ['', Validators.required],
        weight: ['', Validators.required],
        bmi: [''],
        bp: ['', Validators.required],
        pr: ['', Validators.required],
        rr: ['', Validators.required],
        uod: [''],
        uos: [''],
        cod: [''],
        cos: [''],
        cv: [''],
        hearing: [''],
        hosp: [''],
        opr: [''],
        pm: [''],
        smoker: [''],
        ad: [''],
        lmp: [''],
        notes: [''],
        creationDate: [''],
        dateUpdate: ['']
      }),
      peForm: this.formBuilder.group({
        transactionId: [''],
        patientId: [''],
        skin: [''],
        hn: [''],
        cbl: [''],
        ch: [''],
        abdo: [''],
        extre: [''],
        ot: [''],
        find: [''],
        doctor: [''],
        license: [''],
        creationDate: [''],
        dateUpdate: ['']
      })
    });
  }

  getData() {
    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => {
        this.patientDatas = data[0];
      }
    );

    this.TS.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.transactionDatas = data[0];
      }
    );

    this.peService.getMedhis(this.transactionId).subscribe(
      data => {
        this.medHisDatas = data;
        this.peUpdateForm.patchValue({
          medhisForm: {
            transactionId: [+this.transactionId],
            patientId: [+this.patientId],
            asth: [],
            tb: [data.tb],
            dia: [data.dia],
            hb: [data.hb],
            hp: [data.hp],
            kp: [data.kp],
            ab: [data.ap],
            jbs: [data.jbs],
            pp: [data.pp],
            mh: [data.mh],
            fs: [data.fs],
            alle: [data.alle],
            ct: [data.ct],
            hep: [data.hep],
            std: [data.std],
            creationDate: [data.creationDate],
            dateUpdate: [data.dateUpdate]
          }
        });
      }
    );

    this.peService.getVital(this.transactionId).subscribe(
      data => {
        this.vitalDatas = data;
        this.peUpdateForm.patchValue({
          vitalForm: {
            transactionID: [+this.transactionId],
            patientID: [+this.patientId],
            height: [data.height],
            weight: [data.weight],
            bmi: [data.bmi],
            bp: [data.bp],
            pr: [data.pr],
            rr: [data.rr],
            uod: [data.uod],
            uos: [data.uos],
            cod: [data.cod],
            cos: [data.cos],
            cv: [data.cv],
            hearing: [data.hearing],
            hosp: [data.hosp],
            opr: [data.opr],
            pm: [data.pm],
            smoker: [data.smoker],
            ad: [data.ad],
            lmp: [data.lmp],
            notes: [data.notes],
            creationDate: [data.creationDate],
            dateUpdate: [data.dateUpdate]
          }
        });
      }
    );

    this.peService.getPE(this.transactionId).subscribe(
      data => {
        this.peDatas = data;
        this.peUpdateForm.patchValue({
          peForm: {
            transactionID: [+this.transactionId],
            patientID: [+this.patientId],
            skin: [data.skin],
            hn: [data.hn],
            cbl: [data.cbl],
            ch: [data.ch],
            abdo: [data.abdo],
            extre: [data.extre],
            ot: [data.ot],
            find: [data.find],
            doctor: [data.doctor],
            license: [data.license],
            creationDate: [data.creationDate],
            dateUpdate: [data.dateUpdate]
          }
        });
      }
    );
  }

  changeDoctor(event: any) {
    let doctor = event;
    for (let index = 0; index < this.doctors.length; index++) {
      if (this.doctors[index]['doctor'] === doctor) {
        this.peUpdateForm.patchValue({ peForm: { license: this.doctors[index]['license'] } });
      }
    }
  }

  back() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Operation will be canceled!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result === 'ok') {
          this.router.navigate(['/pe/list']);
        }
      }
    );
  }

  updatePe() {
    console.log(this.peUpdateForm.value);
  }

}
