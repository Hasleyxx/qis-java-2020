import { Component, OnInit, Inject } from '@angular/core';
import { XrayService } from 'src/app/services/xray.service';
import { MathService } from 'src/app/services/math.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss']
})
export class ImageDialogComponent implements OnInit {

  tid: any = "";
  content: boolean = false;
  xrayContent: boolean = false;
  xrayDatas = [];
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  uploadStatus: boolean = true;

  form: FormGroup;
  imgXray: any = "";
  patientData: any;
  transRef: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private XS: XrayService,
    private math: MathService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ImageDialogComponent>,
    private PS: PatientService,
    private TS: TransactionService
  ) {
    this.tid = this.data.tid;
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      tid: [''],
      image: ['', Validators.required]
    });
    this.getData();
  }

  getData() {
    this.form.get('tid').setValue(this.tid);
    this.XS.getOneXray(this.tid).subscribe((data: any) => {
      if (data !== null) {
        this.content = true;
        this.xrayDatas = data;
        //this.form.get('tid').setValue(data.transactionID);

        if (data.imgXray !== null) {
          this.imgXray = "assets/xray/" + data.imgXray;
          this.xrayContent = true;
        }
      }
    });

    this.TS.getOneTrans("getTransaction/" + this.tid).subscribe((data: any) => {
      if (data.length > 0) {
        let patientId = data[0].patientId;
        this.transRef = data[0].transactionRef;
        this.PS.getOnePatient("getPatient/" + patientId).subscribe((data: any) => {
          if (data.length > 0) {
            this.patientData = data[0];
          }
        });
      }
    });
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    if (this.fileData.size > 999999) {
      this.uploadStatus = false;
    } else {
      this.uploadStatus = true;
    }
    this.preview();
  }

  preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }

  formSubmit() {
    if (!this.uploadStatus) {
      this.math.openSnackBar("Image size exceeded the maximum size.", "close");
      return;
    }

    const formData = new FormData();
    const fileName = this.transRef + "_" + "_" + this.math.randomNumber() + this.fileData.name;
    formData.append('image', this.fileData, fileName);
    formData.append('tid', this.form.get('tid').value);

    this.XS.addXrayScan(formData).subscribe(data => {
      if (data == 1) {
        this.math.openSnackBar("Scanned xray has been uploaded.", "close");

        this.XS.getOneXray(this.tid).subscribe((data: any) => {
          if (data !== null) {
            this.content = true;

            if (data.imgXray !== "") {
              this.imgXray = "assets/xray/" + data.imgXray;
              this.xrayContent = true;
              this.previewUrl = null;
            }
          }
        });
      } else {
        this.math.openSnackBar("Failed to uploaded image.", "close");
      }
    });
  }

  generatePdf(tid: any) {
    this.dialogRef.close();

    let copy = tid + "_" + this.patientData.fullName;
    // Set TID + name to Clipboard
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = copy;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    const suffix = [tid];
    this.math.printXrayScan('', suffix);
  }

  generatePdfPrint(tid: any) {
    this.dialogRef.close();

    let copy = tid + "_" + this.patientData.fullName;
    // Set TID + name to Clipboard
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = copy;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    const suffix = [tid];
    this.math.printXrayScanPrint('', suffix);
  }
}
