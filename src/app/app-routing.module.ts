import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiptComponent } from './element/receipt/receipt.component';
import { TransactionComponent } from './page/transaction/transaction.component';
import { ErrorPageComponent } from './page/error-page/error-page.component';
import { TransactionListComponent } from './element/transaction-list/transaction-list.component';
import { HMOListComponent } from './page/hmo-list/hmo-list.component';
import { ReportListComponent } from './page/report-list/report-list.component';
import { ManagePackageComponent } from './page/manage-package/manage-package.component';
import { SalesPdfComponent } from './element/sales-pdf/sales-pdf.component';
import { AuthenticationComponent } from './page/authentication/authentication.component';
import { SigninComponent } from './element/signin/signin.component';
import { SignupComponent } from './element/signup/signup.component';
import { AuthGuard } from './services/auth.guard';
import { PrivErrorComponent } from './page/priv-error/priv-error.component';
import { RefundComponent } from './page/refund/refund.component';
import { PendingAccountComponent } from './element/pending-account/pending-account.component';
import { ManageUserComponent } from './admin/manage-user/manage-user.component';
import { BillingComponent } from './admin/billing/billing.component';
import { BillingPdfComponent } from './admin/element/billing-pdf/billing-pdf.component';
import { SoaListComponent } from './admin/soa-list/soa-list.component';
import { AccountPaymentComponent } from './admin/account-payment/account-payment.component';
import { MicroscopyComponent } from './laboratory/microscopy/microscopy.component';
import { MicroscopyFormComponent } from './laboratory/microscopy-form/microscopy-form.component';
import { LabResultComponent } from './laboratory/lab-result/lab-result.component';
import { HematologyComponent } from './laboratory/hematology/hematology.component';
import { HematologyFormComponent } from './laboratory/hematology-form/hematology-form.component';
import { ChemistryComponent } from './laboratory/chemistry/chemistry.component';
import { ChemistryFormComponent } from './laboratory/chemistry-form/chemistry-form.component';
// import { MarkersComponent } from './imaging/markers/markers.component';
import { RegistrationComponent } from './doctor/registration/registration.component';
import { PeAddComponent } from './pe/pe-add/pe-add.component';
import { PeDataComponent } from './element/pe-data/pe-data.component';
import { PeUpdateComponent } from './pe/pe-update/pe-update.component';
import { ImagingMarkersComponent } from './imaging/imaging-markers/imaging-markers.component';
import { ImagingPrintMarkerComponent } from './imaging/imaging-print-marker/imaging-print-marker.component';
import { ImagingSummaryComponent } from './imaging/imaging-summary/imaging-summary.component';
import { ImagingUltrasoundReportComponent } from './imaging/imaging-ultrasound-report/imaging-ultrasound-report.component';
import { ImagingXrayInventoryComponent } from './imaging/imaging-xray-inventory/imaging-xray-inventory.component';
import { PeSummaryComponent } from './element/pe-summary/pe-summary.component';
import { PeListComponent } from './pe/pe-list/pe-list.component';
import { PeRecordComponent } from './pe/pe-record/pe-record.component';
import { PeCertificateComponent } from './pe/pe-certificate/pe-certificate.component';
import { SalesComponent } from './doctor/sales/sales.component';
import { IndustrialComponent } from './laboratory/industrial/industrial.component';
import { IndustrialFormComponent } from './laboratory/industrial-form/industrial-form.component';
import { ImagingSummaryFormComponent } from './imaging/imaging-summary-form/imaging-summary-form.component';
import { DisplayPeFormComponent } from './pe/display-pe-form/display-pe-form.component';
import { DisplayPeMedicalComponent } from './pe/display-pe-medical/display-pe-medical.component';
import { NurseClassificationComponent } from './nurse/nurse-classification/nurse-classification.component';
import { NursePatientSummaryComponent } from './nurse/nurse-patient-summary/nurse-patient-summary.component';
import { ImagingPrintStickerComponent } from './imaging/imaging-print-sticker/imaging-print-sticker.component';
import { PackagesComponent } from './laboratory/packages/packages.component';
import { SummaryReportComponent } from './nurse/summary-report/summary-report.component';
import { IndustrialSummaryComponent } from './laboratory/industrial-summary/industrial-summary.component';
import { BasicTwoComponent } from './laboratory/basic-two/basic-two.component';
import { BasicTwoFormComponent } from './laboratory/basic-two-form/basic-two-form.component';
import { BasicTwoSummaryComponent } from './laboratory/basic-two-summary/basic-two-summary.component';
import { EmailResultsComponent } from './qc/email-results/email-results.component';
import { SerologyComponent } from './laboratory/serology/serology.component';
import { SerologyFormComponent } from './laboratory/serology-form/serology-form.component';
import { ToxicologyComponent } from './laboratory/toxicology/toxicology.component';
import { ToxicologyFormComponent } from './laboratory/toxicology-form/toxicology-form.component';
import { DocpatientListComponent } from './doctor/docpatient-list/docpatient-list.component';
import { ReportMainComponent } from './admin/reports/report-main/report-main.component';
import { ChatLogComponent } from './chat/chat-log/chat-log.component';
import { PatientRecordsComponent } from './qc/patient-records/patient-records.component';
import { EcgComponent } from './qc/ecg/ecg.component';
import { EcgFormComponent } from './qc/ecg-form/ecg-form.component';
import { CertificatesComponent } from './qc/certificates/certificates.component';
import { ImportComponent } from './admin/import/import.component';
import { ReportPdfComponent } from './qc/report-pdf/report-pdf.component';
import { PrintClearanceComponent } from "./doctor/print-clearance/print-clearance.component";
import { ClearanceDialogComponent } from './doctor/clearance-dialog/clearance-dialog.component';
import { QcComponent } from './qc/qc/qc.component';
import { PdfSummaryReportComponent } from './qc/pdf-summary-report/pdf-summary-report.component';
import { PrintMedcertComponent } from "./doctor/print-medcert/print-medcert.component";
import { ClearanceHistoryComponent } from "./doctor/clearance-history/clearance-history.component";
import { MedcertHistoryComponent } from "./doctor/medcert-history/medcert-history.component";
import { PrintMedicationComponent } from './doctor/print-medication/print-medication.component';
import { ReleasedResultsComponent } from './element/released-results/released-results.component';
import { XraySummaryComponent } from './imaging/xray-summary/xray-summary.component';
import { XrayResultComponent } from './imaging/xray-result/xray-result.component';
import { QueueComponent } from './page/queue/queue.component';
import { BarcodeComponent } from './element/barcode/barcode.component';
import { PasswordComponent } from './page/password/password.component';
import { ImageDialogPdfComponent } from './imaging/image-dialog-pdf/image-dialog-pdf.component';
import { LockedAccountComponent } from './element/locked-account/locked-account.component';
import { BlankPageComponent } from './doctor/blank-page/blank-page.component';
import { ImageDialogPdfPrintComponent } from './imaging/image-dialog-pdf-print/image-dialog-pdf-print.component';
import { TwodEchoSummaryComponent } from './imaging/twod-echo-summary/twod-echo-summary.component';
import { DocBodyComponent } from './doctor/element/doc-body/doc-body.component';
import { TwodEchoPrintComponent } from './imaging/twod-echo-print/twod-echo-print.component';
import { PersonnelComponent } from './admin/personnel/personnel.component';
import { RequestValueComponent } from './doctor/request-value/request-value.component';

const routes: Routes = [
  {
    path: "print/:ids",
    outlet: "print",
    component: ReceiptComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:dates",
    outlet: "salesReport",
    component: SalesPdfComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:ids",
    outlet: "billing",
    component: BillingPdfComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:ids",
    outlet: "lab",
    component: LabResultComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:datas",
    outlet: "imaging",
    component: ImagingPrintMarkerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:stickers",
    outlet: "imagingSticker",
    component: ImagingPrintStickerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "peForm",
    component: DisplayPeFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "reportPdf",
    component: ReportPdfComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "peMedical",
    component: DisplayPeMedicalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:ids",
    outlet: "basicTwo",
    component: BasicTwoSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "ecgForm",
    component: EcgFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "xrayResult",
    component: XrayResultComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "pdfSummaryReport",
    component: PdfSummaryReportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "barCode",
    component: BarcodeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "xrayScan",
    component: ImageDialogPdfComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "xrayScanPrint",
    component: ImageDialogPdfPrintComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:data",
    outlet: "doc",
    component: DocBodyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "print/:datas",
    outlet: "echo",
    component: TwodEchoPrintComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "",
    redirectTo: "/cashier/transact",
    pathMatch: "full"
  },
  {
    path: "cashier/transact",
    component: TransactionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "cashier/transactions",
    component: TransactionListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "cashier/hmo",
    component: HMOListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "cashier/sales",
    component: ReportListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "cashier/manage-items",
    component: ManagePackageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "cashier/refund-exchange",
    component: RefundComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "authentication",
    component: AuthenticationComponent,
    children: [
      { path: "", redirectTo: "signin", pathMatch: "full" },
      { path: "signin", component: SigninComponent },
      { path: "signup", component: SignupComponent }
    ]
  },
  {
    path: "authentication/pending",
    component: PendingAccountComponent
  },
  {
    path: "authentication/locked",
    component: LockedAccountComponent
  },
  {
    path: "admin/manage-user",
    component: ManageUserComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/account-payment",
    component: AccountPaymentComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/import",
    component: ImportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/billing",
    component: BillingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/billing/pdf",
    component: BillingPdfComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/reports",
    component: ReportMainComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "elements/released-results",
    component: ReleasedResultsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/microscopy",
    component: MicroscopyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/microscopy/form/:id",
    component: MicroscopyFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/hematology",
    component: HematologyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/hematology/form/:id",
    component: HematologyFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/chemistry",
    component: ChemistryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/chemistry/form/:id",
    component: ChemistryFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/serology",
    component: SerologyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/serology/form/:id",
    component: SerologyFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/toxicology",
    component: ToxicologyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/toxicology/form/:id",
    component: ToxicologyFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/industrial",
    component: IndustrialComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/industrial/form/:id",
    component: IndustrialFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/industrial-summary/:tid/:pid",
    component: IndustrialSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/basic-two",
    component: BasicTwoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/basic-two/form/:tid",
    component: BasicTwoFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/basic-two-summary/:tid/:pid",
    component: BasicTwoSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "laboratory/packages",
    component: PackagesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/summary",
    component: PeSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/list",
    component: PeListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/list/form/:tid",
    component: PeAddComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "img/xray/:pid/:tid",
    component: ImagingSummaryFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/data/:pid/:tid",
    component: PeDataComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/update/:pid/:tid",
    component: PeUpdateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/record",
    component: PeRecordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/certificate",
    component: PeCertificateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/form/:pid/:tid",
    component: DisplayPeFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pe/medical/:pid/:tid",
    component: DisplayPeMedicalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/markers",
    component: ImagingMarkersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/inventory",
    component: ImagingXrayInventoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/summary",
    component: ImagingSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/xray-summary",
    component: XraySummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/ultrasound-report",
    component: ImagingUltrasoundReportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/2decho-summary",
    component: TwodEchoSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/echo-print",
    component: TwodEchoPrintComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/certificate",
    component: PeCertificateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/form/:pid/:tid",
    component: DisplayPeFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "imaging/medical/:pid/:tid",
    component: DisplayPeMedicalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nurse/classification",
    component: NurseClassificationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nurse/patient-summary",
    component: NursePatientSummaryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nurse/certificate",
    component: PeCertificateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nurse/form/:pid/:tid",
    component: DisplayPeFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nurse/medical/:pid/:tid",
    component: DisplayPeMedicalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/email-results",
    component: EmailResultsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/summary-report",
    component: SummaryReportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/patient-records",
    component: PatientRecordsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/ecg",
    component: EcgComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/ecg-form",
    component: EcgFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/certificates",
    component: CertificatesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "qc/qc",
    component: QcComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/registration",
    component: RegistrationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/print-clearance",
    component: PrintClearanceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/clearance-history",
    component: ClearanceHistoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/print-medcert",
    component: PrintMedcertComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/medcert-history",
    component: MedcertHistoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/clearance-dialog",
    component: ClearanceDialogComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/docpatient-list",
    component: DocpatientListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/blank-page",
    component: BlankPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/sales",
    component: SalesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "doctor/request-value",
    component: RequestValueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "chat/chat-log",
    component: ChatLogComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "queue",
    component: QueueComponent
  },
  {
    path: "admin/passwords",
    component: PasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin/personel",
    component: PersonnelComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "error/privilege",
    component: PrivErrorComponent
  },
  {
    path: "**",
    component: ErrorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
