import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-serology',
  templateUrl: './serology.component.html',
  styleUrls: ['./serology.component.scss']
})
export class SerologyComponent implements OnInit {

  constructor(
    private math: MathService
  ) { 
    this.math.navSubs("lab");
  }

  ngOnInit() {
  }

}
