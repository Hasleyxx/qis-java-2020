import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MathService } from 'src/app/services/math.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';
import { transaction } from 'src/app/services/service.interface';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import * as moment from 'moment';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'imaging-summary-form',
  templateUrl: './imaging-summary-form.component.html',
  styleUrls: ['./imaging-summary-form.component.scss']
})
export class ImagingSummaryFormComponent implements OnInit {

  public title: any;
  public personels = ["Febie Dane C. Buada, RRT", "George M. Sapnu", "Chris Noel Cornejo"];
  public personelValue = this.personels[0];


  public templates = [
    {
    "temp": "DONE OUTSIDE", "details": "N/A"
    },
    {
    "temp": "PREGNANT", "details": "N/A"
    }
  ];
  public templateValue = this.templates[0];

  public radiologist = [
    {
      "rad": "Ricardo MA. O. Pacheco,MD.DPBR", "details": "Lung fields are clear. Heart is not enlarged. Diaphragm, its sulci visualized bone are intact."
    },
    {
      "rad": "Salvador Ramirez, MD.DPBR", "details": "No abnormal densities seen in both lung parenchyma. The heart is normal in size and configuration. Aorta is unremarkable. The diaphragms, costrophrenic sulci and bony throrax are intact."
    },
    {
      "rad": "N/A", "details": "N/A"
    }

  ];
  public radiologistValue = this.radiologist[0];

  public addSummaryForm: FormGroup;
  public transactionId: any;
  public patientId: any;
  transaction: transaction;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private math: MathService,
    private TS: TransactionService,
    private XS: XrayService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<ImagingSummaryFormComponent>,
    private dialog: MatDialog
  ) {
    this.math.navSubs("imaging")
    this.transactionId = this.data.tid;
    this.title = this.data.title;

    if (isNaN(this.transactionId)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {
    this.addSummaryForm = this.formBuilder.group({
      transactionID: [''],
      patientID: [''],
      comment: ['', Validators.required],
      impression: ['', Validators.required],
      radiologist: [this.radiologistValue, Validators.required],
      qa: this.personelValue,
      patientIdRef: '',
      userID: parseInt(sessionStorage.getItem('token')),
      creationDate: [moment().format("YYYY-MM-DD h:mm:ss")],
      dateUpdate: [moment().format("YYYY-MM-DD h:mm:ss")]
    });

    this.TS.getOneTrans("getTransaction/" + this.transactionId).subscribe(
      data => {
        if(data[0].length == 0){
          this.router.navigate(['error/404']);
        }else{
          this.transaction = data[0];
          this.patientId = data[0]['patientId'];
          this.addSummaryForm.patchValue({
            patientIdRef: data[0]['patientIdRef']
          });
        }
      }
    )

  }

  addSummary() {
    this.addSummaryForm.patchValue({
      transactionID: +this.transactionId,
      patientID: +this.patientId,
      userID: parseInt(sessionStorage.getItem('token'))
    });
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Patient summary will be saved!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "ok") {
          this.XS.addXray(this.addSummaryForm.value).subscribe(
            response => {
              if (response == 1) {
                this.openSnackBar("Patient summary has been added.", "close");
                this.dialogRef.close("added");
              }
            }
          );
        }
      }
    );
  }


  personel(value: any) {
    this.personelValue = value;
  }

  template(value: any) {
    for (let index = 0; index < this.templates.length; index++) {
      const element = this.templates[index];
      console.log(value)
    /* if (element.temp == value) {
        this.addSummaryForm.patchValue({
          comment: element.details,
          impression: 'NORMAL CHEST FINDINGS'
        });
      }*/
      if (value=="DONE OUTSIDE") {
        this.addSummaryForm.patchValue({
          comment: element.details,
          impression: 'XRAY WAS DONE OUTSIDE (NORMAL)',
          radiologist: 'N/A'
        });
      }
      else {
        this.addSummaryForm.patchValue({
          comment: element.details,
          impression: 'PATIENT IS PREGNANT (N/A)',
          radiologist: 'N/A'
        });
      }

    }
  }

  radiology(value: any) {
    for (let index = 0; index < this.radiologist.length; index++) {
      const element = this.radiologist[index];
      if (element.rad == value) {
        this.addSummaryForm.patchValue({
          comment: element.details,
          impression: 'NORMAL CHEST FINDINGS'
        });
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
