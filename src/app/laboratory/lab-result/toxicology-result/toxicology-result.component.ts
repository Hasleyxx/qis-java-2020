import { Component, OnInit, Input } from '@angular/core';
import { toxicology } from 'src/app/services/service.interface';

@Component({
  selector: 'toxicology-result',
  templateUrl: './toxicology-result.component.html',
  styleUrls: ['./toxicology-result.component.scss']
})
export class ToxicologyResultComponent implements OnInit {

  @Input() toxicology: toxicology;

  constructor() { }

  ngOnInit() {
  }

}
