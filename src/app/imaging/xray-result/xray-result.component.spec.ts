import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XrayResultComponent } from './xray-result.component';

describe('XrayResultComponent', () => {
  let component: XrayResultComponent;
  let fixture: ComponentFixture<XrayResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XrayResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XrayResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
