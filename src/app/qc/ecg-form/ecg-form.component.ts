import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-ecg-form',
  templateUrl: './ecg-form.component.html',
  styleUrls: ['./ecg-form.component.scss']
})
export class EcgFormComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];
  public patientID: any;
  public transactionID: any;
  public patientDatas: any;
  public transactionDatas: any;

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private patientService: PatientService,
    private transactionService: TransactionService,
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.transactionID = this.data[0];
  }
  ngOnInit() {
    this.getPatientdata();
  }

  getPatientdata() {
    this.dataDetails = this.data.map(data => this.getEcgFormReady(data));
    this.transactionService.getOneTrans('getTransaction/' + this.transactionID).subscribe(
      data => {
        this.transactionDatas = data[0];
        this.patientID = data[0]['patientId'];
        this.patientService.getOnePatient('getPatient/' + this.patientID).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );

    Promise.all(this.dataDetails).then(() => {
      this.math.onEcgFormReady();
    });
  }

  getEcgFormReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }
}
