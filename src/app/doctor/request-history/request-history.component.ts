import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import { PatientService } from "src/app/services/patient.service";
import { MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatPaginator, MatSort, MatDialogRef } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { DoctorService } from 'src/app/services/doctor.service';
import * as moment from 'moment';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';
import { RequestDialogComponent } from '../request-dialog/request-dialog.component';
import { EditPrintRequestComponent } from '../edit-print-request/edit-print-request.component';

@Component({
  selector: 'app-request-history',
  templateUrl: './request-history.component.html',
  styleUrls: ['./request-history.component.scss']
})
export class RequestHistoryComponent implements OnInit {

  displayedColumns: string[] = ['date', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  heldData: any[] = [];

  public patientId: any;
  public type: any;

  public patientDatas: any = [];
  public availed: any;
  public description: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private PS: PatientService,
    private doctor: DoctorService,
    public state: ActivatedRoute,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<RequestHistoryComponent>
  ) {
    this.patientId = this.data.pid;
    if (this.data.status) {
      this.type = this.data.status;
    }
  }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData() {
    // Patient
    this.PS.getOnePatient("getPatient/" + this.patientId).subscribe(data => {
      this.patientDatas = data[0];
    });

    // Request History
    this.doctor.getDocRequestId(this.patientId).subscribe(
      data => {
        if (data.length > 0) {
          this.heldData = [];
          data.forEach(element => {
            let transData: any = {
              date: moment(element['dateCreated']).format("LLL"),
              requestRef: element['requestRef']
            }
            this.heldData.push(transData);
          });
          this.dataSource = new MatTableDataSource(this.heldData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    )
  }

  addRequest(pid) {
    this.dialog.open(RequestDialogComponent, {
      data: {
        pid: pid
      },
      disableClose: true
    });
  }

  printRequest(ref) {
    this.dialogRef.close();
    this.dialog.open(EditPrintRequestComponent, {
      data: {
        pid: this.patientId,
        requestRef: ref
      },
      width: '70%'
    });
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId,
      }
    });
  }

}
