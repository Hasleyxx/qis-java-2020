import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { chemistry } from 'src/app/services/service.interface';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'chemistry-result',
  templateUrl: './chemistry-result.component.html',
  styleUrls: ['./chemistry-result.component.scss']
})
export class ChemistryResultComponent implements OnInit {

  @Input() chemistry: chemistry;

  constructor(public math: MathService) { }

  ngOnInit() {
    // console.log(this.con.nativeElement.offsetHeight);

  }

}
