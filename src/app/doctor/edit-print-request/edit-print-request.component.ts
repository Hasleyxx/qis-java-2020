import { Component, OnInit, ViewChildren, ElementRef, Inject } from '@angular/core';
import { RequestHistoryComponent } from '../request-history/request-history.component';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { MathService } from 'src/app/services/math.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { EditPrintClearanceComponent } from '../edit-print-clearance/edit-print-clearance.component';

export interface requestData {
  docRequestID: number,
  patientID: number,
  category: number,
  test: string,
  otherTest: string
}

@Component({
  selector: 'app-edit-print-request',
  templateUrl: './edit-print-request.component.html',
  styleUrls: ['./edit-print-request.component.scss']
})
export class EditPrintRequestComponent implements OnInit {

  public patientId: any;
  public requestRef: any;
  public patientData: any;

  public hemas: string[] = [];
  public micros: string[] = [];
  public chems: string[] = [];
  public xrays: string[] = [];
  public others: string = "";

  public dateNow: any;

  @ViewChildren('hemasValue') hemasValue: ElementRef[];
  @ViewChildren('microsValue') microsValue: ElementRef[];
  @ViewChildren('chemsValue') chemsValue: ElementRef[];
  @ViewChildren('xraysValue') xraysValue: ElementRef[];
  @ViewChildren('othersValue') othersValue: ElementRef[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private PS: PatientService,
    public math: MathService,
    private DS: DoctorService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EditPrintClearanceComponent>
  ) {
    this.patientId = this.dialogData.pid;
    this.requestRef = this.dialogData.requestRef;

    this.dateNow = this.math.dateNow();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.PS.getOnePatient('/getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });

    this.DS.getDocRequestRef(this.requestRef).subscribe(data => {
      if (data.length > 0) {
        data.forEach((element: requestData, index) => {
          let category = element.category;
          let count = index + 1;
          if (category == 1) {
            this.hemas.push(element.test);

          } else if (category == 2) {
            this.micros.push(element.test);

          } else if (category == 3) {
            this.chems.push(element.test);

          } else {
            this.xrays.push(element.test);
          }

          if (element.otherTest !== "" || element.otherTest !== null) {
            this.others = element.otherTest;
          }

          if (count == data.length) {
            if (this.hemas.length == 0) {
              this.hemas.push("-");
            }
            if (this.micros.length == 0) {
              this.micros.push("-");
            }
            if (this.chems.length == 0) {
              this.chems.push("-");
            }
            if (this.xrays.length == 0) {
              this.xrays.push("-");
            }
            if (this.others == "" || this.others == null) {
              this.others = "-";
            }
          }
        });
      }
    });
  }

  print() {
    let tempHemas: any[] = [];
    let tempMicros: any[] = [];
    let tempChems: any[] = [];
    let tempXrays: any[] = [];
    let tempOthers: any[] = [];

    this.hemasValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempHemas.push(value);
      }
    });
    this.microsValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempMicros.push(value);
      }
    });
    this.chemsValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempChems.push(value);
      }
    });
    this.xraysValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempXrays.push(value);
      }
    });
    this.othersValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempOthers.push(value);
      }
    });

    sessionStorage.setItem("hemas", JSON.stringify(tempHemas));
    sessionStorage.setItem("micros", JSON.stringify(tempMicros));
    sessionStorage.setItem("chems", JSON.stringify(tempChems));
    sessionStorage.setItem("xrays", JSON.stringify(tempXrays));
    sessionStorage.setItem("others", JSON.stringify(tempOthers));

    this.dialogRef.close();
    const suffix = [this.patientId, 'request'];
    this.math.printDoc('', suffix);
  }

  cancel() {
    this.dialogRef.close();

    this.dialog.open(RequestHistoryComponent, {
      data: {
        pid: this.patientId
      },
      disableClose: true
    });
  }

}
