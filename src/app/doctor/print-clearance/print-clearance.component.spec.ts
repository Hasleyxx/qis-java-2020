import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintClearanceComponent } from './print-clearance.component';

describe('PrintClearanceComponent', () => {
  let component: PrintClearanceComponent;
  let fixture: ComponentFixture<PrintClearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintClearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintClearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
