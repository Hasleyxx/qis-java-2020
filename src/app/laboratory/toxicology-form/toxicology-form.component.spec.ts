import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToxicologyFormComponent } from './toxicology-form.component';

describe('ToxicologyFormComponent', () => {
  let component: ToxicologyFormComponent;
  let fixture: ComponentFixture<ToxicologyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToxicologyFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToxicologyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
