import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { SendDialogComponent } from '../send-dialog/send-dialog.component';

@Component({
  selector: 'app-email-results',
  templateUrl: './email-results.component.html',
  styleUrls: ['./email-results.component.scss']
})
export class EmailResultsComponent implements OnInit {

  constructor(
    private math: MathService,
    private dialog : MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.math.navSubs("qc")
  }

  ngOnInit() {
  }

  openDialog() {
    //const dialogConfig = new MatDialogConfig();
    //dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(SendDialogComponent, {
      width: "60vw"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "close") {
        this.openSnackBar("File has been sent!", "close");
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}

