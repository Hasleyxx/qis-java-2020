import { Component, OnInit, Inject } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonnelService } from 'src/app/services/personnel.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-personnel-form',
  templateUrl: './personnel-form.component.html',
  styleUrls: ['./personnel-form.component.scss']
})
export class PersonnelFormComponent implements OnInit {

  public personnelID: number = undefined;
  public update: boolean = false;
  public form: FormGroup;
  public depts = ["LAB", "IMAGING"];
  public departmentDefault: string = "";
  public deptFinal: string = "";

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private formBuilder: FormBuilder,
    private math: MathService,
    private PS: PersonnelService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<PersonnelFormComponent>
  ) {
    if (this.dialogData.personnelID !== 0) {
      this.personnelID = this.dialogData.personnelID;
      this.update = true;
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      personnelID: [''],
      firstName: ['', Validators.required],
      middleName: ['', Validators.required],
      lastName: ['', Validators.required],
      licenseNO: ['', Validators.required],
      position: ['', Validators.required],
      positionEXT: ['', Validators.required],
      department: ['', Validators.required]
    });

    this.getData();
  }

  getData() {
    if (this.update) {
      this.PS.getPersonnel(this.personnelID).subscribe(data => {
        let personnel = data[0];

        this.form.patchValue({
          personnelID: this.personnelID,
          firstName: personnel.firstName,
          middleName: personnel.middleName,
          lastName: personnel.lastName,
          licenseNO: personnel.licenseNO,
          position: personnel.position,
          positionEXT: personnel.positionEXT,
          department: personnel.department
        });

        this.departmentDefault = personnel.department;
      });
    }
  }

  departmentSelect(value) {
    if (value == "LAB") {
      this.deptFinal = "lab";
    } else {
      this.deptFinal = "imaging";
    }
  }

  submit() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?", Content: "New Personnel will be added!"
      },
      width: '25%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        if (this.update) {
          this.updatePersonnel();
        } else {
          this.addPersonnel();
        }
      }
    });
  }

  addPersonnel() {
    this.PS.addPersonnel(this.form.value).subscribe((data: any) => {
      if (data == 1) {
        this.dialogRef.close({
          status: "added",
          dept: this.deptFinal
        });
      } else {
        this.math.openSnackBar("Failed to add new Personnel.", "close");
      }
    });
  }

  updatePersonnel() {
    this.PS.updatePersonnel(this.form.value).subscribe((data: any) => {
      if (data == 1) {
        this.dialogRef.close({
          status: "updated",
          dept: this.deptFinal
        });
      } else {
        this.math.openSnackBar("Failed to update Personnel.", "close");
      }
    });
  }

}
