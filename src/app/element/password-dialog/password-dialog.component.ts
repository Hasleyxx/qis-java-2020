import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordService } from 'src/app/password.service';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.scss']
})
export class PasswordDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<PasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public config: any,
    private formBuilder: FormBuilder,
    private PS: PasswordService,
    private math: MathService) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      password: ['', Validators.required]
    });
  }

  formSubmit() {
    let password = this.form.controls['password'].value;
    this.PS.getPassword().subscribe(data => {
      if (data !== null) {
        if (password == data.password) {
          this.dialogRef.close("ok");
        } else {
          this.math.openSnackBar("Password doesn't match.", "close");
        }
      } else {
        this.math.openSnackBar("Password doesn't match.", "close");
      }
    });
  }

}
