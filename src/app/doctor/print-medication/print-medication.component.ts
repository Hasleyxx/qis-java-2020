import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'print-medication',
  templateUrl: './print-medication.component.html',
  styleUrls: ['./print-medication.component.scss']
})
export class PrintMedicationComponent implements OnInit {

  public values: string[] = [];
  public medcerts: any;
  public patientData: any;
  public dateCreated: any;

  public value1: string[] = [];
  public value2: string[] = [];
  public value3: string[] = [];

  constructor() {
    this.value1 = JSON.parse(sessionStorage.getItem("value1"));
    this.value2 = JSON.parse(sessionStorage.getItem("value2"));
    this.value3 = JSON.parse(sessionStorage.getItem("value3"));

    sessionStorage.removeItem("value1");
    sessionStorage.removeItem("value2");
    sessionStorage.removeItem("value3");
  }

  ngOnInit() {
  }
}
