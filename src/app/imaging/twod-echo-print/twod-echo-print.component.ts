import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: 'app-twod-echo-print',
  templateUrl: './twod-echo-print.component.html',
  styleUrls: ['./twod-echo-print.component.scss']
})
export class TwodEchoPrintComponent implements OnInit {

  data: string[];
  public patientData: any;
  dataDetails: Promise<any>[];

  //a
  lvedd = "";
  lvedv = "";
  laap =  "";

  //b
  lvesd = ""
  lvesv = "";
  larl = "";

  //c
  lveddbsa = "";
  sv = "";
  labsa = "";

  //d
  lvesdbsa = "";
  co = "";
  a1 = "";

  //e
  ivsd = "";
  efm = "";
  a2 = "";

  //f
  ivss = "";
  efs = "";
  lavi = "";

  //g
   pwd = "";
   fs = "";
   aorta = "";

   //h
   pws = "";
   epss = "";
   lvot = "";

   //i
   lvmi = "";  
   lvet = ""; 
   mva = "";

   //j
   rwt = "";
   rvm = "";
   tva = "";

   //k
   rarl = "";
   rvfac = "";
   rvot = "";

   //l
   rabsa = "";
   tapse = "";
   mpa = "";

   //m
   rvwt = "";

   //n 
   lvelocity =""; 
   lpeakgrad ="";
   lvti ="";
   lvalve ="";
   lratio ="";
   ljetarea ="";
   lvc ="";

   //o 
   avvelocity =""; 
   avpeakgrad ="";
   avvti ="";
   avvalve ="";
   avratio ="";
   avjetarea ="";
   avvc ="";

  //p
   mvvelocity =""; 
   mvpeakgrad ="";
   mvvti ="";
   mvvalve ="";
   mvratio ="";
   mvjetarea ="";
   mvvc ="";

  //q
   tvvelocity =""; 
   tvpeakgrad ="";
   tvvti ="";
   tvvalve ="";
   tvratio ="";
   tvjetarea ="";
   tvvc ="";

   //r
   pvvelocity =""; 
   pvpeakgrad ="";
   pvvti ="";
   pvvalve ="";
   pvratio ="";
   pvjetarea ="";
   pvvc ="";

   
   //s
   pat =""; 
   rvat ="";
   trjet ="";



  public a = [
    { "label": "LVEDD"},
    { "label": this.lvedd} ,
    { "label": "  "},
    { "label": "LVEDV"},
    { "label": this.lvedv} ,
    { "label": "56-104/67-155"},
    { "label": "LA(A-P)"},
    { "label": this.laap} ,
    { "label": "2.7-3.8/3.0-4.0"},
  ];
  public b = [
    { "label": "LVESD"},
    { "label": this.lvesd} ,
    { "label": " "},
    { "label": "LVESV"},
    { "label": this.lvesv} ,
    { "label": "19-49/22-58"},
    { "label": "LA(R-L)"},
    { "label": this.larl} ,
    { "label": " "},
  ];
  public c = [
    { "label": "LVEDD/BSA"},
    { "label": this.lveddbsa} ,
    { "label": "2.4-3.2/2.2-3.1"},
    { "label": "SV"},
    { "label": this.sv} ,
    { "label": ">65"},
    { "label": "LA/BSA"},
    { "label": this.labsa} ,
    { "label": "1.5-2.3 cm/m2"},
  ];
  public d = [
    { "label": "LVESD/BSA"},
    { "label": this.lvesdbsa} ,
    { "label": "1.4-2.1"},
    { "label": "C.O."},
    { "label": this.co} ,
    { "label": ">4.5"},
    { "label": "A1"},
    { "label": this.a1} ,
    { "label": "L1"},
  ];
  public e = [
    { "label": "IVSD"},
    { "label": this.ivsd} ,
    { "label": "0.6-0.9/0.6-1.0"},
    { "label": "EF(M-Mode)"},
    { "label": this.efm} ,
    { "label": ">55%"},
    { "label": "A2"},
    { "label": this.a2} ,
    { "label": "L2"},
  ];
  public f = [
    { "label": "IVSS"},
    { "label": this.ivss} ,
    { "label": " "},
    { "label": "EF(Simpson's)"},
    { "label": this.efs} ,
    { "label": ">55%"},
    { "label": "LAVI"},
    { "label": this.lavi} ,
    { "label": "<28 cc/m2"},
  ];
  public g = [
    { "label": "PWD"},
    { "label": this.pwd} ,
    { "label": "0.6-0.9/0.6-1.0"},
    { "label": "FS"},
    { "label": this.fs} ,
    { "label": "27-45%-24-43%"},
    { "label": "AORTA"},
    { "label": this.aorta} ,
    { "label": " "},
  ];
  public h = [
    { "label": "PWS"},
    { "label": this.pws} ,
    { "label": " "},
    { "label": "EPSS"},
    { "label": this.epss} ,
    { "label": "<0.7"},
    { "label": "LVOT"},
    { "label": this.lvot} ,
    { "label": "1.8-2.4"},
  ];
  public i = [
    { "label": "LVMI"},
    { "label": this.lvmi} ,
    { "label": "43-95/49-115"},
    { "label": "LVET"},
    { "label": this.lvet} ,
    { "label": " "},
    { "label": "MV Annulus"},
    { "label": this.mva} ,
    { "label": "1.9-3.4"},
  ];
  public j = [
    { "label": "RWT"},
    { "label": this.rwt} ,
    { "label": "0.24-0.42"},
    { "label": "RV(Mid)"},
    { "label": this.rvm} ,
    { "label": "2.7-3.5"},
    { "label": "TV Annulus"},
    { "label": this.tva} ,
    { "label": "1.3-2.8"},
  ];
  public k = [
    { "label": "RA(R-L)"},
    { "label": this.rarl} ,
    { "label": "2.9-4.5"},
    { "label": "RVFAC"},
    { "label": this.rvfac} ,
    { "label": " "},
    { "label": "RVOT"},
    { "label": this.rvot} ,
    { "label": "2.5-2.9"},
  ];
  public l = [
    { "label": "RA/BSA)"},
    { "label": this.rabsa} ,
    { "label": " "},
    { "label": "TAPSE"},
    { "label": this.tapse} ,
    { "label": "≥16mm"},
    { "label": "MPA"},
    { "label": this.mpa} ,
    { "label": "1.5-3.5"},
  ];
  public m = [
    { "label": " "},
    { "label": " "} ,
    { "label": " "},
    { "label": "RWVT"},
    { "label": this.rvwt} ,
    { "label": "<0.5"},
    { "label": ""},
    { "label": " "} ,
    { "label": " "},
  ];

  public n = [
    { "label": "LVOT"},
    { "label": this.lvelocity},
    { "label": this.lpeakgrad},
    { "label": this.lvti},
    { "label": this.lvalve},
    { "label": this.lratio},
    { "label": this.ljetarea},
    { "label": this.lvc},
  ];

  public o = [
    { "label": "AORTIC VALVE"},
    { "label": this.avvelocity},
    { "label": this.avpeakgrad},
    { "label": this.avvti},
    { "label": this.avvalve},
    { "label": this.avratio},
    { "label": this.avjetarea},
    { "label": this.avvc},
  ];

  public p = [
    { "label": "MITRAL VALVE"},
    { "label": this.mvvelocity},
    { "label": this.mvpeakgrad},
    { "label": this.mvvti},
    { "label": this.mvvalve},
    { "label": this.mvratio},
    { "label": this.mvjetarea},
    { "label": this.mvvc},
  ];

  public q = [
    { "label": "TRICUSPID VALVE"},
    { "label": this.tvvelocity},
    { "label": this.tvpeakgrad},
    { "label": this.tvvti},
    { "label": this.tvvalve},
    { "label": this.tvratio},
    { "label": this.tvjetarea},
    { "label": this.tvvc},
  ];

  public r = [
    { "label": "PULMONIC VALVE"},
    { "label": this.pvvelocity},
    { "label": this.pvpeakgrad},
    { "label": this.pvvti},
    { "label": this.pvvalve},
    { "label": this.pvratio},
    { "label": this.pvjetarea},
    { "label": this.pvvc},
  ];

  public s = [
    { "label": "PULMONIC ARTERY PRESSURE"},
    { "label": "PAT: " + this.pat},
    { "label": "RVAT: " + this.rvat},
    { "label": "TR Jet: " + this.trjet},
    { "label": ""},
    { "label": ""},
    { "label": ""},
    { "label": ""},
  ];


  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private patientService: PatientService
  ) {
    this.data = route.snapshot.params['data'].split(',');
  }


  ngOnInit() {
    this.dataDetails = this.data.map(data => this.getEchoReady(data));
    this.patientService.getOnePatient('getPatient/' + this.data[0]).subscribe(
      data => this.patientData = data[0]
    );
    Promise.all(this.dataDetails).then(() => {
      setTimeout(() => {
        this.math.onBarcodeReady();
      });
    });
  }

  line1(value: any) {
    this.a = value;
  }

  line2(value: any) {
    this.b = value;
  }

  line3(value: any) {
    this.c = value;
  }

  line4(value: any) {
    this.d = value;
  }

  line5(value: any) {
    this.e = value;
  }
  
  line6(value: any) {
    this.f = value;
  }

  line7(value: any) {
    this.g = value;
  }

  line8(value: any) {
    this.h = value;
  }

  line9(value: any) {
    this.i = value;
  }

  line10(value: any) {
    this.j = value;
  }

  line11(value: any) {
    this.k = value;
  }

  line12(value: any) {
    this.l = value;
  }

  line13(value: any) {
    this.m = value;
  }

  line14(value: any) {
    this.n = value;
  }

  line15(value: any) {
    this.o = value;
  }

  line16(value: any) {
    this.p = value;
  }

  line17(value: any) {
    this.q = value;
  }

  line18(value: any) {
    this.r = value;
  }

  line19(value: any) {
    this.s = value;
  }

  getEchoReady(data) {
    const amount = Math.floor(Math.random() * 100);
    return new Promise(resolve => setTimeout(() => resolve({ amount }), 1000));
  }

}
