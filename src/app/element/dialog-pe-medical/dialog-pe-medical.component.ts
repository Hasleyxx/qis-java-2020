import { Component, OnInit, Inject } from '@angular/core';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { PatientService } from 'src/app/services/patient.service';
import { PeService } from 'src/app/services/pe.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-dialog-pe-medical',
  templateUrl: './dialog-pe-medical.component.html',
  styleUrls: ['./dialog-pe-medical.component.scss']
})
export class DialogPeMedicalComponent implements OnInit {

  public patientId: any;
  public transactionId: any;
  public patientDatas: any = [];
  public hemaDatas: any = [];
  public seroDatas: any = [];
  public microDatas: any = [];
  public toxiDatas: any = [];
  public classification: any = [];
  public pes: any = [];

  public class: any;
  public classColor: any = 'text-dark';
  public notes: any;
  public cbcot: any = 'text-dark';
  public hbsag: any;
  public antiHav: any;
  public pt: any;
  public meth: any;
  public tetra: any;
  public urinotes: any;
  public fecanotes: any = 'text-dark';

  public medical: any = [];
  public quality: any = [];
  public path: any = [];
  public rad: any = [];

  public date = moment().format("YYYY-MM-DD");

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private peService: PeService,
    private labService: LaboratoryService,
    private XS: XrayService,
    public math: MathService
  ) {
    this.transactionId = this.data.tid;
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    // Transaction
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      data => {
        this.patientId = data[0]['patientId'];
        // Patient
        this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => this.patientDatas = data[0]
        );
      }
    );

    // PE
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        this.pes = data;
      }
    );

    // Hematology
    this.labService.getHematology(this.transactionId).subscribe(
      data => {
        if (data.length > 0) {
          this.hemaDatas = data[0];
          if (data[0].cbcot !== "NORMAL") {
            this.cbcot = "text-danger";
          }
        }
      }
    );

    // Microscopy
    this.labService.getMicroscopy(this.transactionId).subscribe(
      data => {
        this.microDatas = data[0];

        if (data[0] !== undefined) {
          if (this.microDatas.pregTest == 'POSITIVE') {
            this.pt = 'text-danger';
          }

          if (this.microDatas.uriOt != 'NORMAL') {
            this.urinotes = 'text-danger';
          }

          this.labService.getPersonnel(this.microDatas.medID).subscribe(
            data => {
              this.medical.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
            }
          );
          this.labService.getPersonnel(this.microDatas.qualityID).subscribe(
            data => {
              this.quality.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
            }
          );
          this.labService.getPersonnel(this.microDatas.pathID).subscribe(
            data => {
              this.path.push({
                'name': data[0].firstName + ' ' + data[0].middleName + ' ' + data[0].lastName + ', ' + data[0].positionEXT,
                'license': data[0].licenseNO
              });
            }
          );

        } else {
          this.pt = 'text-dark';
          this.urinotes = 'text-dark';
          this.medical.push({
            'name': '-',
            'license': '-'
          });

          this.quality.push({
            'name': '-',
            'license': '-'
          });

          this.path.push({
            'name': '-',
            'license': '-'
          });
        }
      }
    );

    // Serology
    this.labService.getSerology(this.transactionId).subscribe(
      data => {
        this.seroDatas = data[0];

        if (data[0] !== undefined) {
          if (this.seroDatas.hbsAG == 'REACTIVE') {
            this.hbsag = 'text-danger';
          }

          if (this.seroDatas.antiHav == 'REACTIVE') {
            this.antiHav = 'text-danger';
          }

        } else {
          this.hbsag = 'text-dark';
          this.antiHav = 'text-dark';
        }
      }
    );

    // Toxicology
    this.labService.getToxicology(this.transactionId).subscribe(
      data => {
        this.toxiDatas = data[0];

        if (data[0] !== undefined) {
          if (this.toxiDatas.meth == 'POSITIVE') {
            this.meth = 'text-danger';
          }

          if (this.toxiDatas.tetra == 'POSITIVE') {
            this.tetra = 'text-danger';
          }

        } else {
          this.meth = 'text-dark';
          this.tetra = 'text-dark';
        }
      }
    );

    // Class
    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        this.classification = data;

        if (data !== null) {
          if (this.classification.medicalClass == 'CLASS A - Physically Fit') {
            this.class = 'Class A - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency') {
            this.class = 'Class B - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.class = 'CLASS C - With abnormal findings generally not accepted for employment.';
          }
          else if (this.classification.medicalClass == 'CLASS D - Unemployable') {
            this.class = 'Class D - UNEMPLOYABLE';
          }
          else {
            this.class = 'PENDING';
          }

          if (this.classification.notes != 'NORMAL' && this.classification.notes != '') {
            this.notes = 'text-danger';
          }

          if (this.class == 'Class D - UNEMPLOYABLE' || this.class == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.classColor = 'text-danger';
          }

        } else {
          this.class = 'PENDING';
          this.notes = 'text-dark';
          this.classColor = 'text-dark';
        }
      }
    );

    // Radiology
    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        if (data == null) {
          this.rad.push({
            'qa': '-',
            'radiologist': '-'
          });

        } else {
          this.rad = data;
        }

      }
    );

  }

}
