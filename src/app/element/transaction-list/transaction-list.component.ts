import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { itemList, heldTable, transaction, patient } from 'src/app/services/service.interface';
import { TransactionService } from 'src/app/services/transaction.service';
import { PatientService } from 'src/app/services/patient.service';
import { ItemService } from 'src/app/services/item.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { MathService } from 'src/app/services/math.service';
import { EditHMOComponent } from '../edit-hmo/edit-hmo.component';
import { MatSnackBar } from '@angular/material';
import { PaymentComponent } from 'src/app/admin/element/payment/payment.component';
import { Router, ActivatedRoute } from '@angular/router';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ImagingSummaryFormComponent } from 'src/app/imaging/imaging-summary-form/imaging-summary-form.component';
import { ImagingUltrasoundFormComponent } from 'src/app/imaging/imaging-ultrasound-form/imaging-ultrasound-form.component';
import { XrayService } from 'src/app/services/xray.service';
import { DialogPeSummaryComponent } from '../dialog-pe-summary/dialog-pe-summary.component';
import { DialogPeDataComponent } from '../dialog-pe-data/dialog-pe-data.component';
import { PeService } from 'src/app/services/pe.service';
import { PatientFormComponent } from '../patient-form/patient-form.component';
import { DialogPeFormComponent } from '../dialog-pe-form/dialog-pe-form.component';
import { DialogPeMedicalComponent } from '../dialog-pe-medical/dialog-pe-medical.component';
import { MarkerOptionService } from 'src/app/services/marker-option.service';
import { SalesDialogComponent } from 'src/app/doctor/sales-dialog/sales-dialog.component';
import { ViewSummaryDialogComponent } from 'src/app/imaging/view-summary-dialog/view-summary-dialog.component';
import { IndustrialSummaryComponent } from 'src/app/laboratory/industrial-summary/industrial-summary.component';
import { PatientRecComponent } from 'src/app/doctor/patient-rec/patient-rec.component';
import { ChecklistDialogComponent } from '../checklist-dialog/checklist-dialog.component';
import { QcFormComponent } from 'src/app/qc/qc-form/qc-form.component';
import { MicroscopyFormComponent } from 'src/app/laboratory/microscopy-form/microscopy-form.component';
import { HematologyFormComponent } from 'src/app/laboratory/hematology-form/hematology-form.component';
import { ChemistryFormComponent } from 'src/app/laboratory/chemistry-form/chemistry-form.component';
import { SerologyFormComponent } from 'src/app/laboratory/serology-form/serology-form.component';
import { ToxicologyFormComponent } from 'src/app/laboratory/toxicology-form/toxicology-form.component';
import { IndustrialFormComponent } from 'src/app/laboratory/industrial-form/industrial-form.component';
import { BasicTwoFormComponent } from 'src/app/laboratory/basic-two-form/basic-two-form.component';
import { PeAddComponent } from 'src/app/pe/pe-add/pe-add.component';
import { ImageDialogComponent } from 'src/app/imaging/image-dialog/image-dialog.component';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import * as moment from 'moment';
import { TwodEchoFormComponent } from 'src/app/imaging/twod-echo-form/twod-echo-form.component';
import { TwodEchoPrintComponent } from 'src/app/imaging/twod-echo-print/twod-echo-print.component';

export interface trans_itemsTemp {
  itemName: string,
  itemPrice: number,
  itemDescription: string,
  qty: number,
  subtotal: any,
  disc: number
}

export interface heldTableTemp {
  id: number,
  trans: transaction,
  patInfo: patient,
  patient: string,
  items: trans_itemsTemp[],
  date: any,
  biller: any,
  type: string,
  action: any,
  cashier: any,
  color: string
}

/** Constants used to fill up our data base. */
@Component({
  selector: 'transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TransactionListComponent implements OnInit {

  public checkedBox: boolean = true;

  laboratory: any[] = [];
  isUpdate: boolean = false;
  micro: any[] = [];
  hema: any[] = [];
  ac: number = 0;

  displayedColumns: string[] = ['id', 'date', 'type', 'patient', 'biller', 'action'];
  dataSource: MatTableDataSource<heldTable>;
  heldData: heldTable[] = [];
  expandedElement: heldTable | null;

  @Input() listType: string;
  @Input() transType: string;
  @Input() markerOptions: string[];
  @Input() filmType: string;
  @Input() dateFromFilm: string;
  @Input() dateToFilm: string;
  @Output() addTrans = new EventEmitter();
  @Output() printMarker = new EventEmitter();
  @Output() filmInventory = new EventEmitter();
  @Output() datas = new EventEmitter();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  showLoading: boolean = true;
  d = new DatePipe('en-US');
  months: Array<any> = [
    { value: "01", name: "January" },
    { value: "02", name: "February" },
    { value: "03", name: "March" },
    { value: "04", name: "April" },
    { value: "05", name: "May" },
    { value: "06", name: "June" },
    { value: "07", name: "July" },
    { value: "08", name: "August" },
    { value: "09", name: "September" },
    { value: "10", name: "October" },
    { value: "11", name: "November" },
    { value: "12", name: "December" }
  ]

  years: Array<any> = ["2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025"];

  monthVal: FormControl = new FormControl(this.d.transform(new Date(), "MM"));
  yearVal: FormControl = new FormControl(this.d.transform(new Date(), "yyyy"));

  from: FormControl = new FormControl(this.d.transform(new Date(), "yyyy-MM-dd 06:00:00"));
  to: FormControl = new FormControl(this.d.transform(new Date(),"yyyy-MM-dd 20:00:00"));
  /* from: FormControl = new FormControl(this.d.transform(new Date(), "2020-02-29 06:00:00"));
  to: FormControl = new FormControl(this.d.transform(new Date(), "2020-02-29 20:00:00")); */
  // myFilter = (d: Date): boolean => {
  //   const day = d.getDay();
  //   return day !== 10;
  // }

  public filmArray = [];
  dataDetails: Promise<any>[];

  constructor(
    private TS: TransactionService,
    private PS: PatientService,
    private IS: ItemService,
    private math: MathService,
    private lab: LaboratoryService,
    public dialog: MatDialog,
    private router: Router,
    public state: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private XS: XrayService,
    private peService: PeService,
    private MOS: MarkerOptionService,
  ) {

  }

  ngOnInit() {
    this.setData();

    if (!this.listType) {
      this.math.navSubs("cashier");
      this.listType = "transactions";
    }

    if (!this.filmType) {
      //Laboratory industrial
      if (this.router.url === '/laboratory/industrial') {
        this.listType = "industrial";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }

      //Imaging markers
      if (this.router.url === '/imaging/markers') {
        this.listType = "imagingMarkers";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //Imaging Summary
      if (this.router.url === '/imaging/summary') {
        this.listType = "imagingSummary";
        // this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'radiologist', 'action'];
      }
      //2D
      if (this.router.url === '/imaging/2decho-summary') {
        this.listType = "imaging2DEchoSummary";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
        this.math.navSubs("imaging");
      }
      //Imaging Xray Summary
      if (this.router.url === '/imaging/xray-summary') {
        this.listType = "imagingXraySummary";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'radiologist', 'company', 'action'];
      }
      //Imaging Xray Summary
      if (this.router.url === '/imaging/xray-summary') {
        this.listType = "imagingXraySummary";
       // this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'biller', 'action', 'certificate'];
      }
      //Imaging Ultrasound
      if (this.router.url === '/imaging/ultrasound-report') {
        this.listType = "imagingUltrasound";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'reffered by', 'procedure', 'action'];
      }
      //Imaging Certificate
      if (this.router.url === '/imaging/certificate') {
        this.listType = "imagingCertificate";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //QC Email
      if (this.router.url === '/qc/email-results') {
        this.listType = "emailResults";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'classification', 'remarks', 'action'];
      }
      //PE Summary
      if (this.router.url === '/pe/summary') {
        this.listType = "peSummary";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //PE List
      if (this.router.url === '/pe/list') {
        this.listType = "peList";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //PE Record
      if (this.router.url === '/pe/record') {
        this.listType = "peRecord";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //PE Certificates
      if (this.router.url === '/pe/certificate') {
        this.listType = "peCertificate";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //Secretary
      if (this.router.url === '/doctor/registration') {
        this.listType = "docPatient";
        this.displayedColumns = ['patient', 'action'];
      }
      //Doctor
      if (this.router.url === '/doctor/docpatient-list') {
        this.listType = "docpatientList";
        this.displayedColumns = ['id', 'patientID', 'patient', 'action'];
      }
      //Nurse Classification
      if (this.router.url === '/nurse/classification') {
        this.listType = "nurseClassification";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //QC ECG
      if (this.router.url === '/qc/ecg') {
        this.listType = "ecg";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }
      //QC QC
      if (this.router.url === '/qc/qc') {
        this.listType = "qc";
        this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
      }

    } else {
      this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'xrayFilm'];
    }

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  //Filter Date
  setData() {

    this.showLoading = true;
    let url: any;
    if (this.transType == undefined) {

      let from = new Date(this.from.value);
      let fromData = this.d.transform(from,"yyyy-MM-dd HH:mm:ss");
      let to = new Date(this.to.value);
      let toData = this.d.transform(to,"yyyy-MM-dd HH:mm:ss");

      url = "getTransactionDate/" + fromData + "/" + toData;
        // this.yearVal.value + "-" + this.monthVal.value + "-01/" +
        // this.yearVal.value + "-" + this.monthVal.value + "-31";
    } else if (this.transType == "billing" || this.transType == "accounting") {
      url = "getTransBillingDate/" +
        this.yearVal.value + "-" + this.monthVal.value + "-01/" +
        this.yearVal.value + "-" + this.monthVal.value + "-31";
    } else if (this.transType == "xrayMarker") {
      url = "getTransactionDate/" + this.dateFromFilm + "/" + this.dateToFilm;
    } else {

      let from = new Date(this.from.value);
      let fromData = this.d.transform(from,"yyyy-MM-dd HH:mm:ss");
      let to = new Date(this.to.value);
      let toData = this.d.transform(to,"yyyy-MM-dd HH:mm:ss");

      url = "getTransTypeDate/" + this.transType + "/" +
        fromData + "/" + toData;
    }

    // Xray Marker Table
    this.TS.getTransactions(url).subscribe(
      data => {
        this.heldData = [];
        let totalFilm = 0;

        if (this.transType == "xrayMarker") {
          this.MOS.getFilmSize().subscribe(
            result => {
              result.forEach(element => {
                this.filmArray.push({ "name": element.name, "count": 0 });
              });
              this.filmArray.push({ "name": "Total film count", "count": 0 });
            }
          );
        }

        if (data.length > 0) {
          data.forEach(trans => {
            if (this.listType == "microscopy") {
              this.lab.getMicroscopy(trans.transactionId).subscribe(
                micro => {
                  if (micro[0]) {
                    this.laboratory.push(micro[0]);
                  }
                }
              )
            } else if (this.listType == "hematology") {
              this.lab.getHematology(trans.transactionId).subscribe(
                hema => {
                  if (hema[0]) {
                    this.laboratory.push(hema[0]);
                  }
                }
              )
            } else if (this.listType == "chemistry") {
              this.lab.getChemistry(trans.transactionId).subscribe(
                chem => {
                  if (chem[0]) {
                    this.laboratory.push(chem[0]);
                  }
                }
              )

            } else if (this.listType == "serology") {
              this.lab.getSerology(trans.transactionId).subscribe(
                sero => {
                  if (sero[0]) {
                    this.laboratory.push(sero[0]);
                  }
                }
              );
            } else if (this.listType == "toxicology") {
              this.lab.getToxicology(trans.transactionId).subscribe(
                toxic => {
                  if (toxic[0]) {
                    this.laboratory.push(toxic[0]);
                  }
                }
              );
            } else if (this.listType == "industrial") {
              this.lab.getMicroscopy(trans.transactionId).subscribe(
                micro => {
                  if (micro[0]) {
                    this.laboratory.push(micro[0]);
                  }
                }
              )
            } else if (this.listType == "basictwo") {
              this.lab.getMicroscopy(trans.transactionId).subscribe(
                micro => {
                  if (micro[0]) {
                    this.laboratory.push(micro[0]);
                  }
                }
              );

              this.lab.getHematology(trans.transactionId).subscribe(
                hema => {
                  if (hema[0]) {
                    this.laboratory.push(hema[0]);
                  }
                }
              );

              this.lab.getChemistry(trans.transactionId).subscribe(
                chem => {
                  if (chem[0]) {
                    this.laboratory.push(chem[0]);
                  }
                }
              );
            }
            else if (this.listType == "industrial") {
              this.lab.getMicroscopy(trans.transactionId).subscribe(
                micro => {
                  if (micro[0]) {
                    this.micro.push(micro[0]);
                    this.isUpdate = true;
                  }
                }
              )
              this.lab.getHematology(trans.transactionId).subscribe(
                hema => {
                  if (hema[0]) {
                    this.hema.push(hema[0]);
                    this.isUpdate = true;
                  }
                }
              )
            }

            let color = "black";
            if (trans.salesType == "refund") {
              color = "red";
            }

            let transData: heldTable = {
              id: trans.transactionId,
              patient: undefined,
              patInfo: undefined,
              items: [],
              type: trans.transactionType,
              date: trans.transactionDate,
              biller: trans.biller,
              action: "",
              color: color,
              marker: "success",
              patientID: "",
              peStatus: "without",
              nurseStatus: "without",
              xrayStatus: "without",
              xrayFilm: "",
              impression: "",
              radiologist: "",
              twod: "",
              qualityassurance: "",
              industrial: "false",
              classification: "",
              classificationStatus: "",
              remarks: "",
              released_status: trans.released_status,
              company: trans.biller,
              currency: trans.currency
            }
            this.PS.getOnePatient("getPatient/" + trans.patientId)
              .subscribe(pat => {
                transData.patient = pat[0].lastName + ", " + pat[0].firstName + " " + pat[0].middleName;
                transData.patInfo = pat[0];

                transData.patientID = pat[0].patientID;
                const patientID = pat[0].patientID;
                const transactionID = trans.transactionId;

                if (this.listType == 'imagingMarkers' || this.filmType) {
                  this.XS.getMarker(transactionID).subscribe(
                    data => {
                      if (data.length !== 0) {
                        transData.marker = "accent";
                        transData.xrayFilm = data[0].xrayFilm;
                        const xrayFilm = data[0].xrayFilm;

                        if (this.transType == "xrayMarker") {
                          for (let index = 0; index < this.filmArray.length; index++) {
                            const element = this.filmArray[index];
                            let count = element['count'];
                            if (element['name'] == xrayFilm) {
                              count++;
                              totalFilm++;
                              this.filmArray[index]['count'] = count;
                              this.filmArray[this.filmArray.length - 1]['count'] = totalFilm;
                            }
                          }
                        }
                      }
                    }
                  );
                }

                if (this.listType == 'peList') {
                  this.peService.checkPe('checkPe/' + patientID + '/' + transactionID).subscribe(
                    value => {
                      if (value.length !== 0) {
                        transData.peStatus = "with";
                      }
                    }
                  );
                }

                if (this.listType == 'imaging2DEchoSummary') {
                  
                }

                if (this.listType == 'nurseClassification' || this.listType == "emailResults") {
                  this.lab.getOneClass(transactionID).subscribe(
                    value => {
                      if (value !== null) {
                        transData.nurseStatus = "with";
                        transData.classification = value['medicalClass'];
                        if (value['medicalClass'] == "CLASS A - Physically Fit") {
                          transData.medicalclassification = "Class A";
                          transData.remarks = "FIT TO WORK";

                        } else if (value['medicalClass'] == "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency") {
                          transData.medicalclassification = "Class B";
                          transData.remarks = "FIT TO WORK";

                        } else if (value['medicalClass'] == "CLASS C - With abnormal findings generally not accepted for employment.") {
                          transData.medicalclassification = "Class C";
                          transData.remarks = "Unemployable";

                        } else if (value['medicalClass'] == "CLASS D - Unemployable") {
                          transData.medicalclassification = "Class D";
                          transData.remarks = "Unemployable";

                        } else {
                          transData.medicalclassification = "PENDING";
                          transData.remarks = value['notes'];

                        }
                      }
                    }
                  );
                }

                if (this.listType == 'imagingSummary' || this.listType == "imagingXraySummary") {
                  this.XS.getOneXray(transactionID).subscribe(
                    value => {
                      if (value !== null) {
                        if (value.length !== 0) {
                          transData.xrayStatus = "with";
                        }
                      }
                    }
                  );

                  this.XS.getOneXray(transactionID).subscribe(
                    data => {
                      if (data !== null) {
                        let result = {
                          impression: data['impression'],
                          radiologist: data['radiologist'],
                          qa: data['qa'],
                        };
                        // this.displayedColumns = ['id', 'patientID', 'date', 'patient', 'action'];
                        transData.impression = result.impression;
                        transData.radiologist = result.radiologist;
                        transData.qualityassurance = result.qa;

                      }
                    }
                  );
                }

                this.lab.getOneClass(transactionID).subscribe(
                  data => {
                    if (data == null) {
                      transData.classificationStatus = "false";
                    } else {
                      transData.classificationStatus = "true";
                    }
                  }
                );
              });
            this.TS.getTransExt(trans.transactionId).subscribe(transExt => {
              if (transExt.length > 0) {
                transExt.forEach((ext, index) => {
                  if (ext.packageName != null) {
                    this.IS.getPack("getPackageName/" + ext.packageName)
                      .subscribe(
                        pack => {
                          let packItem: itemList = {
                            itemId: pack[0].packageName,
                            itemName: pack[0].packageName,
                            itemPrice: pack[0].packagePrice,
                            itemDescription: pack[0].packageDescription,
                            itemType: pack[0].packageType,
                            deletedItem: pack[0].deletedPackage,
                            neededTest: undefined,
                            creationDate: pack[0].creationDate,
                            dateUpdate: pack[0].dateUpdate,
                          }
                          transData.items.push(packItem);
                        }
                      )
                  } else if (ext.itemID) {
                    this.IS.getItemByID(ext.itemID)
                      .subscribe(item => {
                        transData.items.push(item[0]);
                      });
                  }
                  if (transExt.length - 1 == index) {
                    this.showLoading = false;
                  }
                });
              }
              else {
                this.showLoading = false;
              }
            });
            /* this.TS.getTransRef(trans.transactionId).subscribe(
              transRefData => {
                if (transRefData.length > 0) {
                  if (this.router.url == "/laboratory/microscopy" || this.router.url == "/laboratory/hematology" || this.router.url == "/laboratory/chemistry" || this.router.url == "/laboratory/serology" || this.router.url == "/laboratory/toxicology" || this.router.url == "/laboratory/industrial" || this.router.url == "/laboratory/basic-two") {
                    if (transRefData[0].stool > 0 || transRefData[0].urine > 0 || transRefData[0].blood > 0 || transRefData[0].specimen > 0) {
                      this.heldData.push(transData);
                      this.filmInventory.emit(this.filmArray);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  } else if (this.router.url == "/imaging/markers" || this.router.url == "/imaging/summary" || this.router.url == "/imaging/ultrasound-report") {
                    if (transRefData[0].xray > 0) {
                      this.heldData.push(transData);
                      this.filmInventory.emit(this.filmArray);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  } else if (this.router.url == "/imaging/ultrasound-report") {
                    if (transRefData[0].ultrasound > 0) {
                      this.heldData.push(transData);
                      this.filmInventory.emit(this.filmArray);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  } else if (this.router.url == "/pe/summary" || this.router.url == "/pe/list" || this.router.url == "/pe/record") {
                    if (transRefData[0].physicalExam > 0) {
                      this.heldData.push(transData);
                      this.filmInventory.emit(this.filmArray);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  }
                }
              }
            ); */

            this.heldData.push(transData);
            this.filmInventory.emit(this.filmArray);
            this.dataSource = new MatTableDataSource(this.heldData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          });
          this.datas.emit(this.heldData);
        } else {
          this.showLoading = false;
          this.dataSource = new MatTableDataSource([]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    );

    if (this.listType == "transactions") {
      this.math.navSubs("cashier");
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  receipt(value) {
    const suffix = [value];
    this.math.printDocument('', suffix);

  }

  edit(value) {
    const dialogRef = this.dialog.open(EditHMOComponent, {
      data: value
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res.status) {
        this.openSnackBar(res.message, res.status);
      }
    })
  }

  getTrans(data: heldTable) {
    this.addTrans.emit(data);
  }

  payment(id) {
    let dial = this.dialog.open(PaymentComponent, {
      data: { id: id, type: 1 }
    })

    dial.afterClosed().subscribe(res => {
      if (res) {
        this.math.openSnackBar(res.message, res.status);
      }

    })
  }
  labNavigate(tid, type) {
    //this.snapshot = this.state.snapshot;
    //this.router.navigate([this.snapshot.routeConfig.path + "/form", id]);

    if (type == "microscopy") {
      this.dialog.open(MicroscopyFormComponent, {
        data: {
          tid: tid,
        },
      });
    } else if (type == "hematology") {
      this.dialog.open(HematologyFormComponent, {
        data: {
          tid: tid
        }
      });
    } else if (type == "chemistry") {
      this.dialog.open(ChemistryFormComponent, {
        data: {
          tid: tid
        }
      });
    } else if (type == "serology") {
      this.dialog.open(SerologyFormComponent, {
        data: {
          tid: tid
        }
      });
    } else if (type == "toxicology") {
      this.dialog.open(ToxicologyFormComponent, {
        data: {
          tid: tid
        }
      });
    }

  }


  peNavigate(tid) {
    /* this.snapshot = this.state.snapshot;
    this.router.navigate([this.snapshot.routeConfig.path + "/form", tid]); */
    this.dialog.open(PeAddComponent, {
      data: {
        tid: tid
      }
    });
  }

  checkLab(id): boolean {
    let found = this.laboratory.find(lab => lab.transactionID === id);
    if (found) {
      return true;
    } else {
      return false;
    }
  }

  labRes(id, section) {
    const suffix = [id, section];
    this.math.printLab('', suffix);
  }


  labRes1(tid, pid) {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(IndustrialSummaryComponent, {
      data: {
        tid: tid,
        pid: pid,
      },
    });
  }



  labRes2(pid, tid) {
    const suffix = [tid, pid];
    this.math.printBasic('', suffix);
  }


  //Radiology Dialog
  openDialog(tid) {
    const dialogRef = this.dialog.open(ImagingSummaryFormComponent, {
      data: {
        tid: tid,
        title: 'ADD'
      }
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "added") {
          //this.setData();
        }
      }
    );
  }

  
  //2D
  open2D(tid) {
    const dialogRef = this.dialog.open(TwodEchoFormComponent, {
      data: {
        tid: tid,
        title: 'ADD'
      },
      
      width: '90vw',
      
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "added") {
          //this.setData();
        }
      }
    );
  }


  //Ultrasound Dialog
  openUltra(id) {

    this.dialog.open(ImagingUltrasoundFormComponent, {
      data: {
        tid: id
      },

    });
  }

  //View Radiology Image
  uploadImage(tid: any) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      data: {
        tid: tid
      },
      width: '100vw'
    });
  }

  //View Radiology Report
  viewSummary(tid: any, fn: any) {
    const dialogRef = this.dialog.open(ViewSummaryDialogComponent, {
      data: {
        tid: tid,
        type: 'UPDATE'
      },
      width: '110vw'
    });
  }

  //2d Echo Result
  echoResult(tid: any) {
    const suffix = [tid];
    this.math.printEcho('', suffix);
  }

  //Print Marker
  printMarkerClick(tid: any) {
    this.printMarker.emit({
      tid: tid
    });
  }

  //PE Summary Dialog
  clickPeSummary(tid: any) {
    const dialogRef = this.dialog.open(DialogPeSummaryComponent, {
      data: {
        tid: tid
      },
      width: '110vw'
    });
  }

  //View PE Data
  clickPeData(tid: any) {
    const dialogRef = this.dialog.open(DialogPeDataComponent, {
      data: {
        tid: tid
      },
      width: '110vw'
    });
  }

  //Get Patient Info
  clickPatientInfo(pid: any) {
    this.PS.getOnePatient("getPatient/" + pid).subscribe(
      data => {
        const dialogRef = this.dialog.open(PatientFormComponent, {
          data: data[0],
          width: '60%'
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            if (result['status'] == "ok") {
              this.router.navigate(['/pe/record']);
            }
          }
        });
      }
    );
  }

  //doctor's
  clickPatientInfo1(pid: any) {
    this.PS.getOnePatient("getPatient/" + pid).subscribe(
      data => {
        const dialogRef = this.dialog.open(PatientFormComponent, {
          data: data[0],
          width: '60%'
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result['status'] == "ok") {
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate(['/doctor/registration']);
          }
        });
      }
    );
  }


  patientRec(tid, pid) {
    this.dialog.open(PatientRecComponent, {
      data: {
        tid: tid,
        pid: pid,
      },
    });
  }

  patientRec1(tid, pid) {
    this.dialog.open(SalesDialogComponent, {
      data: {
        tid: tid,
        pid: pid,
      },
    });
  }

  checklist(tid) {
    this.dialog.open(ChecklistDialogComponent, {
      data: {
        tid: tid
      },
      width: '50vw'
    });
  }


  //Doctor Sales Dialog
  openDoc(id) {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SalesDialogComponent, {
      data: {
        tid: id
      }
    });
  }
 //classify
  openClassify(tid) {
    const dialogRef = this.dialog.open(DialogPeSummaryComponent, {
      data: {
        tid: tid,
        type: 'nurse-classification'
      },
      width: '90vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "updated" || result == "added") {
        let message = '';
        if (result == 'updated') {
          message = 'Classification has been updated.';
        } else {
          message = 'Classification has been added.';
        }
        //this.setData();
        this.openSnackBar(message, "close");
      }
    });
  }

  changePE(value, tid) {
    if (value == "View PE") {
      const dialogRef = this.dialog.open(DialogPeFormComponent, {
        data: {
          tid: tid
        }
      });
    } else {
      const suffix = [tid];
      this.math.printPeForm('', suffix);
    }
  }

  changeMedical(value, tid) {
    if (value == "View Medical") {
      const dialogRef = this.dialog.open(DialogPeMedicalComponent, {
        data: {
          tid: tid
        }
      });
    } else {
      const suffix = [tid];
      this.math.printPeMedical('', suffix);
    }
  }

  addIndustrial(tid) {
    /* this.snapshot = this.state.snapshot;
    this.router.navigate([this.snapshot.routeConfig.path + "/form", tid]); */

    this.dialog.open(IndustrialFormComponent, {
      data: {
        tid: tid
      }
    });
  }

  addBasictwo(tid) {
    /* this.snapshot = this.state.snapshot;
    this.router.navigate([this.snapshot.routeConfig.path + "/form", tid]); */

    this.dialog.open(BasicTwoFormComponent, {
      data: {
        tid: tid
      }
    });
  }

  reportPdf(tid: any) {
    const suffix = [tid];
    this.math.printReportPdf('', suffix);
  }

  ecgForm(tid: any) {
    const suffix = [tid];
    this.math.printEcgForm('', suffix);
  }

  xrayResult(tid: any) {
    const suffix = [tid];
    this.math.printXrayResult('', suffix);
  }

  addQc(tid: any) {
    const dialogRef = this.dialog.open(QcFormComponent, {
      data: {
        tid: tid,
        type: "add"
      }
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "added") {
          this.openSnackBar("Quality Control data has been added!", "close");
        } else if (result == "error") {
          this.openSnackBar("Failed to add Quality Control data!", "close");
        }
      }
    );
  }

  updateQc(tid: any) {
    const dialogRef = this.dialog.open(QcFormComponent, {
      data: {
        tid: tid,
        type: "update"
      }
    });


    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "updated") {
          this.openSnackBar("Quality Control data has been updated!", "close");
        } else if (result == "error") {
          this.openSnackBar("Failed to update Quality Control data!", "close");
        }
      }
    );
  }

  releaseMe(tid) {
    let url = "getTransaction/" + tid;
    this.TS.getOneTrans(url).subscribe(
      result => {
        let trans = result[0];
        let status = undefined;
        if (trans.released_status == "0") {
          status = 1;
        } else {
          status = 0;
        }

        let data = {
          tid: tid,
          status: status
        };

        this.TS.releaseMe(data).subscribe(
          data => {
            // console.log(data);
          }
        );
      }
    );
  }

  printBarcode(tid: any) {
    const suffix = [tid];
    this.math.printBarcode('', suffix);
  }

  xrayRad() {
    let data: Array<any> = [];

    this.heldData.forEach((trans, index) => {
      let packageName = "";
      trans.items.forEach(data => {
        if (packageName.length == 0) {
          packageName = packageName + "" + data.itemName;
        } else {
          packageName = packageName + " | " + data.itemName;
        }
      });
      let transArr: Array<any> = [{
        id: trans.id, pid: trans.patInfo.patientID,
        date: trans.date, company: trans.patInfo.companyName, patName: trans.patient,
        radiologist: trans.radiologist, items: packageName,
      }];

      data.push(transArr[0]);
    });

    new Angular5Csv(data, 'My Report', {
      headers: ["ID", "Patient ID", "Transaction Date", "Company Name", "Patient Name", "Radiologist", "Package"]
    });
  }

  classExc() {
    /* let data: Array<any> = [];

    this.heldData.forEach((trans, index) => {
      let packageName = "";
      trans.items.forEach(data => {
        if (packageName.length == 0) {
          packageName = packageName + "" + data.itemName;
        } else {
          packageName = packageName + " | " + data.itemName;
        }
      });
      let transArr: Array<any> = [{
        id: trans.id, pid: trans.patInfo.patientID,
        date: trans.date, company: trans.patInfo.companyName, patName: trans.patient,
        items: packageName,
      }];

      data.push(transArr[0]);
    });

    new Angular5Csv(data, 'My Report', {
      headers: ["ID", "Patient ID", "Transaction Date", "Company Name", "Patient Name", "Package", "Remarks"]
    }); */
    let data: Array<any> = [];
    let cash: Array<any> = [
      {
        date: "CASH", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
        subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
      }];
    let account: Array<any> = [
      {
        date: "ACCOUNT", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
        subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
      }
    ];
    let hmo: Array<any> = [
      {
        date: "", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
        subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
      }
    ];
    let dollar: Array<any> = [
      {
        date: "DOLLAR", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
        subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
      }
    ];

    this.heldData.forEach((trans: any, index) => {
      let packageName = "";
      trans.items.forEach(data => {
        if (packageName.length == 0) {
          packageName = packageName + "" + data.itemName;
        } else {
          packageName = packageName + " | " + data.itemName;
        }
      });

      let transArr: Array<any> = [{
        date: trans.date, id: trans.id, transType: trans.type, patName: trans.patient,
        comName: trans.patInfo.companyName, items: packageName
      }];

      if (trans.type == "CASH" || trans.type == "HYGIENE") {
        // USD dollar transaction
        if (trans.currency == "USD") {
          dollar = dollar.concat(transArr);
        } else if (trans.currency == "PESO") {
          // CASH or walkin transaction
          cash = cash.concat(transArr);
        }

      } else if (trans.type == "ACCOUNT" || trans.type == "APE") {
        account = account.concat(transArr);
      } else if (trans.type == "HMO") {
        hmo = hmo.concat(transArr);
      } else if (trans.type == "CREDIT") {

      } else if (trans.type == "ONLINE") {

      }

      if (index + 1 == this.heldData.length) {
        hmo.shift();
        account = account.concat(hmo);
      }
    });

    cash.push({
      date: "", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
      subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
    });

    dollar.push({
      date: "", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
      subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
    });

    account.push({
      date: "", id: "", transType: "", patName: "", comName: "", items: "", qty: "",
      subTotal: "", total: "", biller: "", cashier: "", tendered: "", change: ""
    });

    data = cash.concat(dollar, account);

    new Angular5Csv(data, 'My Report', {
      headers: [
        "Date and Time", "Receipt No.", "Transaction Type", "Patient Name",
        "Company Name", "Items"
      ]
    });
  }

  //rad accounts
  radAcc() {
    let data: Array<any> = [];
    let account: Array<any> = [
      {
        date: "ACCOUNT"
      }
    ];
    let cash: Array<any> = [{}];
    let hmo: Array<any> = [{}];
    let dollar: Array<any> = [{}];
    this. ac = 0;

    this.heldData.forEach((trans: any, index) => {
      let packageName = "";
      trans.items.forEach(data => {
        if (packageName.length == 0) {
          packageName = packageName + "" + data.itemName;
        } else {
          packageName = packageName + " | " + data.itemName;
        }
      });

      let transArr: Array<any> = [{
        date: trans.date, id: trans.id, transType: trans.type, patName: trans.patient,
        comName: trans.patInfo.companyName, items: packageName, radiologist: trans.radiologist, remarks: trans.impression,
      }];

      if (trans.type == "ACCOUNT" || trans.type == "APE") {
        account = account.concat(transArr);
        this.ac +=1;
      } else if (trans.type == "HMO") {
        hmo = hmo.concat(transArr);
      } else if (trans.type == "CREDIT") {

      } else if (trans.type == "ONLINE") {

      }

      if (index + 1 == this.heldData.length) {
        hmo.shift();
        account = account.concat(hmo);
      }


    });


    account.push({
      date: "COUNT: " + this.ac, id: "", transType: "", patName: "", comName: "", items: "", radiologist: "",remarks: ""
    });


    data = cash.concat(dollar, account);
    let csv = "Account-report-" + moment().format("YYYY-MM-DD");
    new Angular5Csv(data, csv , {
      headers: [
        "Date and Time", "Receipt No.", "Transaction Type", "Patient Name",
        "Company Name", "Items", "Radiologist", "Remarks", "For Apico", "With Findings", "Pending"
      ]

    });

  }


 //rad walkin
 radCash() {
  let data: Array<any> = [];
  let cash: Array<any> = [
    {
      date: "WALK-IN"
    }];

  let account: Array<any> = [{ }];
  let hmo: Array<any> = [{}];
  let dollar: Array<any> = [{}];

  this.heldData.forEach((trans: any, index) => {
    let packageName = "";
    trans.items.forEach(data => {
      if (packageName.length == 0) {
        packageName = packageName + "" + data.itemName;
      } else {
        packageName = packageName + " | " + data.itemName;
      }
    });

    let transArr: Array<any> = [{
      date: trans.date, id: trans.id, transType: trans.type, patName: trans.patient,
      comName: trans.patInfo.companyName, items: packageName, radiologist: trans.radiologist, remarks: trans.impression
    }];


    if (trans.type == "CASH") {
      // USD dollar transaction
      if (trans.currency == "USD") {
        dollar = dollar.concat(transArr);
      } else if (trans.currency == "PESO") {
        // CASH or walkin transaction
        cash = cash.concat(transArr);
      }
    }
  });

  cash.push({
    date: "", id: "", transType: "", patName: "", comName: "", items: "", radiologist: "",remarks: ""
  });

  data = cash.concat(dollar, account);

 let csv = "Walk-in-report-" + moment().format("YYYY-MM-DD");
 new Angular5Csv(data, csv , {
    headers: [
      "Date and Time", "Receipt No.", "Transaction Type", "Patient Name",
      "Company Name", "Items", "Radiologist", "Remarks", "For Apico", "With Findings", "Pending"
    ]
  });
}


  //rad hygiene
  radHygiene() {
    let data: Array<any> = [];
    let cash: Array<any> = [
      {
        date: "HYGIENE"
      }];

    let account: Array<any> = [{ }];
    let hmo: Array<any> = [{}];
    let dollar: Array<any> = [{}];

    this.heldData.forEach((trans: any, index) => {
      let packageName = "";
      trans.items.forEach(data => {
        if (packageName.length == 0) {
          packageName = packageName + "" + data.itemName;
        } else {
          packageName = packageName + " | " + data.itemName;
        }
      });

      let transArr: Array<any> = [{
        date: trans.date, id: trans.id, transType: trans.type, patName: trans.patient,
        comName: trans.patInfo.companyName, items: packageName, radiologist: trans.radiologist, remarks: trans.impression
      }];


      if (trans.type == "HYGIENE") {
        // USD dollar transaction
        if (trans.currency == "USD") {
          dollar = dollar.concat(transArr);
        } else if (trans.currency == "PESO") {
          // CASH or walkin transaction
          cash = cash.concat(transArr);
        }
      }
    });

    cash.push({
      date: "", id: "", transType: "", patName: "", comName: "", items: "", radiologist: "",remarks: ""
    });

    data = cash.concat(dollar, account);
    let csv = "Hygiene-report-" + moment().format("YYYY-MM-DD");
    new Angular5Csv(data, csv , {
      headers: [
        "Date and Time", "Receipt No.", "Transaction Type", "Patient Name",
        "Company Name", "Items", "Radiologist", "Remarks", "For Apico", "With Findings", "Pending"
      ]
    });
  }



  export(type: any) {
    let data: Array<any> = [];
    let headerType: any = undefined;

    if(type == "microscopy") {
      headerType = ["patientIdRef", "userID", "creationDate", "dateUpdate", "patientID", "transactionID", "wbc", "le", "amorph", "nit", "fecColor", "fecCon", "fecOt", "coAx", "uro", "mthreads", "ecells", "uriTrans", "uriPro", "microID", "uriColor", "uriPh", "rbc", "uriGlu", "uriOt", "fecMicro", "bac", "uriSp", "afbr2", "bld", "qualityID", "afbva1", "occultBLD", "afbr1", "pathID", "pregTest", "afbva2", "medID", "ket", "afbd", "bil"];

    } else if (type == "hematology") {
      headerType = ["hemaID", "patientIdRef", "userID", "bloodType", "rh", "clottingTime", "bleedingTime", "creationDate", "dateUpdate", "patientID", "transactionID", "qualityID", "pathID", "medID", "whiteBlood", "monocytes", "cbcrbc", "hemaNR", "lymphocytes", "hemoNR", "plt", "eos", "neutrophils", "hemoglobin", "bas", "ptime", "hematocrit", "cbcot", "apttime", "ptimeNV", "esrmethod", "inrnv", "inr", "aptcontrolNV", "esr", "actPercent", "ptcontrol", "apttimeNV", "ms", "pr131", "aptcontrol", "ptcontrolNV", "actPercentNV"];

    } else if (type == "chemistry") {
      headerType = [ "chemID", "totalCPK", "chemNotes", "patientIdRef", "userID", "creationDate", "dateUpdate", "patientID", "transactionID", "cl", "qualityID", "pathID", "medID", "buacon", "crea", "fbscon", "bua", "fbs", "trig", "chol", "k", "hdlcon", "ldl", "ch", "trigcon", "ast", "psa", "rbs", "vldl", "alp", "rbscon", "hb", "hdl", "ggtp", "ldh", "cholcon", "na", "creacon", "ldlcon", "alt", "ogtt2con", "lipase", "ogtt1", "ogtt1con", "ogct", "bun", "ionCalcium", "albumin", "magnesium", "ogctcon", "amylase", "inPhos", "bildirect", "biltotal", "bilindirect", "globulin", "cpkmm", "calcium", "ogtt2", "protein", "cpkmb", "agratio", "buncon"];

    } else if (type == "serology") {
      headerType = [ "seroID", "patientIdRef", "userID", "creationDate", "dateUpdate", "patientID", "transactionID", "qualityID", "pathID", "medID", "seroOt", "hbsAG", "ca125", "crpres", "vdrl", "afp", "hiv1", "antiHav", "tydotigG", "psanti", "hbeAG", "tsh", "crpdil", "hiv2", "antiHBS", "antiHBE", "ca15", "antiHBC", "ft3", "ft4", "ca19", "tydotigM", "cea"];

    } else if (type == "toxicology") {
      headerType = [ "toxicID", "patientIdRef", "userID", "creationDate", "dateUpdate", "patientID", "transactionID", "qualityID", "pathID", "medID", "meth", "tetra", "drugtest"];

    } else {
      headerType = [ "imgXray", "patientIdRef", "userID", "comment", "creationDate", "dateUpdate", "patientID", "radiologist", "xrayID", "impression", "transactionID", "qa"];

    }

    this.dataDetails = this.heldData.map(data => this.getReady(data));

    this.heldData.forEach(trans => {
      if (type == "microscopy") {
        this.lab.getMicroscopy(trans.id).subscribe(
          micro => {
            if (micro[0]) {
              let rbc = micro[0]['rbc'].toString();
              let wbc = micro[0]['wbc'].toString();
              micro[0]['rbc'] = "foo||" + rbc;
              micro[0]['wbc'] = "foo||" + wbc;
              data.push(micro[0]);
            }
          }
        )
      } else if (type == "hematology") {
        this.lab.getHematology(trans.id).subscribe(
          hema => {
            if (hema[0]) {
              data.push(hema[0]);
            }
          }
        )
      } else if (type == "chemistry") {
        this.lab.getChemistry(trans.id).subscribe(
          chem => {
            if (chem[0]) {
              data.push(chem[0]);
            }
          }
        )

      } else if (type == "serology") {
        this.lab.getSerology(trans.id).subscribe(
          sero => {
            if (sero[0]) {
              data.push(sero[0]);
            }
          }
        );
      } else if (type == "toxicology") {
        this.lab.getToxicology(trans.id).subscribe(
          toxic => {
            if (toxic[0]) {
              data.push(toxic[0]);
            }
          }
        );
      } else {
        this.XS.getOneXray(trans.id).subscribe(xray => {
          if (xray !== null) {
            if (xray.length !== 0) {
              data.push(xray);
            }
          }
        });
      }
    });

    Promise.all(this.dataDetails).then(() => {
      let name = type.toUpperCase() + "_" + moment().format("YYYY-MM-DD h:mm:ss");
      this.math.getReady(name, data, headerType);
    });

  }

  getReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }

  generatePending() {
    let data: Array<any> = [];
    let headers = [
      "Transaction ID", "Patient ID", "Transaction Date", "Patient Name", "Company", "Package", "Remarks"
    ];
    this.heldData.forEach(element => {
      if (element.type == "ACCOUNT") {
        if (element.biller == "ALORICA TMO" || element.biller == "ALORICA UPS" || element.biller == "SHORE360" || element.biller == "TOA GLOBAL" || element.biller == "DNATA TRAVEL INC" || element.biller == "VXI" || element.biller == "PARAGON" || element.biller == "ORIGO" || element.biller == "HAWAII FAMILY CENTER BPO, INC." || element.biller == "IVIRTUALLY" || element.biller == "OWENS ASIA") {
          let peStatus: boolean = false;
          let xrayStatus: boolean = false;
          let microStatus: boolean = false;
          let hemaStatus: boolean = false;
          let classStatus: boolean = false;
          let classNotes: string;

          let packageName = "";

          element.items.forEach(data => {
            if (packageName == "") {
              packageName = packageName + "" + data.itemName;
            } else {
              packageName = packageName + " | " + data.itemName;
            }
          });

          this.peService.getPE(element.id).subscribe(pe => {
            if (pe !== null) {
              peStatus = true;
            } else {
              if (classNotes == "") {
                classNotes = " | NO PE";
              } else {
                classNotes = classNotes + " | NO PE";
              }
            }
          });

          this.XS.getOneXray(element.id).subscribe(xray => {
            if (xray !== null) {
              if (xray.length !== 0) {
                xrayStatus = true;
              }
            } else {
              if (classNotes == "") {
                classNotes = " | NO XRAY";
              } else {
                classNotes = classNotes + " | NO XRAY";
              }
            }
          });

          this.lab.getMicroscopy(element.id).subscribe(micro => {
            if (micro[0]) {
              microStatus = true;
            } else {
              if (classNotes == "") {
                classNotes = " | NO MICROSCOPY";
              } else {
                classNotes = classNotes + " | NO MICROSCOPY";
              }
            }
          });

          this.lab.getHematology(element.id).subscribe(hema => {
            if (hema[0]) {
              hemaStatus = true;
            } else {
              if (classNotes == "") {
                classNotes = " | NO HEMATOLOGY";
              } else {
                classNotes = classNotes + " | NO HEMATOLOGY";
              }
            }
          });

          this.lab.getOneClass(element.id).subscribe(classification => {
            if (classification !== null) {
              if (classification['medicalClass'] == "Pending - These are cases that are equivocal as to the classification and are being evaluated further. After a certain period of time these cases will be classified whether fit or unfit fot employment.") {
                if (classNotes == "") {
                  classNotes = " PENDING - " + classification['notes'];
                } else {
                  classNotes = classNotes + " PENDING - " + classification['notes'];
                }
              } else {
                classStatus = true;
              }
            } else {
              if (classNotes == "") {
                classNotes = " | NOT CLASSIFIED YET";
              } else {
                classNotes = classNotes + " | NOT CLASSIFIED YET";
              }
            }
          });

          setTimeout(() => {
            if (peStatus == false || xrayStatus == false || microStatus == false || hemaStatus == false || classStatus == false) {
              let classNotesTemp = classNotes.replace("undefined | ", "");
              let classNotesTempTemp = classNotesTemp.replace("undefined ", "");

              let tempData = {
                tid: element.id,
                pid: element.patientID,
                date: element.date,
                name: element.patient,
                company: element.patInfo.companyName,
                package: packageName,
                notes: classNotesTempTemp
              };

              data.push(tempData);
            }
          }, 2000);
        }
      }
    });

    this.math.openSnackBar("Generating Pending for Accounts", "close");
    setTimeout(() => {
      let dateTimeNow = "PENDING SUMMARY - " + moment().format("YYYY-MM-DD h:mm:ss");
      new Angular5Csv(data, dateTimeNow, {
        headers: headers
      });
    }, 10000);
  }

  generatePendingImaging() {
    let data: Array<any> = [];
    let headers = [
      "Transaction ID", "Patient ID", "Transaction Date", "Patient Name", "Company", "Package", "Remarks"
    ];
    this.heldData.forEach(element => {
      if (element.type == "ACCOUNT") {
        if (element.biller == "ALORICA TMO" || element.biller == "ALORICA UPS" || element.biller == "SHORE360" || element.biller == "TOA GLOBAL" || element.biller == "DNATA TRAVEL INC" || element.biller == "VXI" || element.biller == "PARAGON" || element.biller == "ORIGO" || element.biller == "HAWAII FAMILY CENTER BPO, INC." || element.biller == "IVIRTUALLY" || element.biller == "OWENS ASIA") {
          let xrayStatus: boolean = false;
          let classNotes: string;

          let packageName = "";

          element.items.forEach(data => {
            if (packageName == "") {
              packageName = packageName + "" + data.itemName;
            } else {
              packageName = packageName + " | " + data.itemName;
            }
          });

          this.XS.getOneXray(element.id).subscribe(xray => {
            if (xray !== null) {
              xrayStatus = true;
              if (xray['comment'] == "Lung fields are clear. Heart is not enlarged. Diaphragm, its sulci visualized bone are intact.") {
                classNotes = "NORMAL";

                let tempData = {
                  tid: element.id,
                  pid: element.patientID,
                  date: element.date,
                  name: element.patient,
                  company: element.patInfo.companyName,
                  package: packageName,
                  notes: classNotes
                };

                data.push(tempData);

              } else if (xray['comment'] == "No abnormal densities seen in both lung parenchyma. The heart is normal in size and configuration. Aorta is unremarkable. The diaphragms, costrophrenic sulci and bony throrax are intact.") {
                classNotes = "NORMAL";

                let tempData = {
                  tid: element.id,
                  pid: element.patientID,
                  date: element.date,
                  name: element.patient,
                  company: element.patInfo.companyName,
                  package: packageName,
                  notes: classNotes
                };

                data.push(tempData);

              } else {
                classNotes = xray['comment'];

                let tempData = {
                tid: element.id,
                pid: element.patientID,
                date: element.date,
                name: element.patient,
                company: element.patInfo.companyName,
                package: packageName,
                notes: classNotes
              };

              data.push(tempData);

              }
            } else {
              classNotes = "NO XRAY";

              let tempData = {
                tid: element.id,
                pid: element.patientID,
                date: element.date,
                name: element.patient,
                company: element.patInfo.companyName,
                package: packageName,
                notes: classNotes
              };
              data.push(tempData);
            }
          });

          /* let classNotesTemp = classNotes.replace("undefined | ", "");
          let classNotesTempTemp = classNotesTemp.replace("undefined ", ""); */
        }
      }
    });

    this.math.openSnackBar("Generating Pending for Accounts", "close");
    setTimeout(() => {
      let dateTimeNow = "PENDING SUMMARY - " + moment().format("YYYY-MM-DD h:mm:ss");
      new Angular5Csv(data, dateTimeNow, {
        headers: headers
      });
    }, 10000);
  }
}
