import { Component, OnInit, ViewChild } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { DoctorService } from 'src/app/services/doctor.service';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { CatOptionDialogComponent } from '../element/cat-option-dialog/cat-option-dialog.component';

@Component({
  selector: 'app-request-value',
  templateUrl: './request-value.component.html',
  styleUrls: ['./request-value.component.scss']
})
export class RequestValueComponent implements OnInit {

  public categories: any[] = [];
  public subs: any[] = [];

  public labelOptions: string = "Select category first";

  public tempDocReqId: any;

  public displayedColumns: string[] = ['id', 'option', 'action'];
  public dataSource: MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private math: MathService,
    private dialog: MatDialog,
    private DS: DoctorService
  ) {
    this.math.navSubs("secretary");
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.categories = [];
    this.subs = [];

    this.DS.getDocRequestCategory().subscribe(data => {
      if (data.length > 0) {
        data.forEach((element, index) => {
          this.categories.push(element);
        });
      }
    });
  }

  getCatOption(docReqCatId) {
    this.subs = [];
    this.tempDocReqId = docReqCatId;

    this.DS.getDocRequestCategoryId(docReqCatId).subscribe(dataLabel => {
      let templabel = dataLabel[0].category.toLowerCase();

      this.labelOptions = templabel.charAt(0).toUpperCase() + templabel.slice(1) + " Options";
    });

    this.DS.getDocRequestOptionIdAll(docReqCatId).subscribe(data => {
      if (data.length > 0) {
        data.forEach((element, index) => {
          let temp = {
            id: element.docReqOptionId,
            option: element.option,
            status: element.status
          };
          this.subs.push(temp);

          if (index + 1 == data.length) {
            this.setTableData();
          }
        });
      } else {
        this.subs = [];
        this.setTableData();
      }
    });
  }

  setTableData() {
    this.dataSource = new MatTableDataSource(this.subs);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  updateOption(id, category) {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?", Content: "Option's Category will be updated!",
      },
      width: "20%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        let tempData = {
          docReqOptionId: id,
          category: category
        };

        this.DS.updateDocRequestOption(tempData).subscribe(data => {
          if (data == 1) {
            this.getCatOption(this.tempDocReqId);
            this.math.openSnackBar("Option's category has been updated!", "close");
          } else {
            this.math.openSnackBar("Failed to update option's category", "close");           
          }
        });
      }
    });
  }

  removeOption(id, status) {
    let tempStatus: any;
    let textSuccess: any;
    let textFailed: any;
    let textLabel: any;

    if (status == 1) {
      tempStatus = 2;
      textSuccess = "Option has been removed from the list!";
      textLabel = "Option will be removed from the list!";
      textFailed = "Failed to remove option from the list!";
    } else {
      tempStatus = 1;
      textSuccess = "Option has been restored!";
      textLabel = "Option will be available from the list!";
      textFailed = "Failed to restore option!";
    }

    let tempData = {
      docReqOptionId: id,
      status: tempStatus
    };

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: textLabel
      },
      width: "20%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.DS.removeDocRequestOption(tempData).subscribe(data => {
          
          if (data == 1) {
            
            this.getCatOption(this.tempDocReqId);
            this.math.openSnackBar(textSuccess, "close");
          } else {
            this.math.openSnackBar(textFailed, "close");
          }
        });
      }
    });
  }

  new(type) {
    const dialogRef = this.dialog.open(CatOptionDialogComponent, {
      data: {
        type: type
      },
      width: "40%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.math.openSnackBar(result.text, "close");
        this.getData();

        if (result.type == "option") {
          this.getCatOption(result.id);
        }
      }
    });
  }
}
