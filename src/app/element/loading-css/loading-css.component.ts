import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-loading-css',
  templateUrl: './loading-css.component.html',
  styleUrls: ['./loading-css.component.scss']
})
export class LoadingCssComponent implements OnInit {

  public title: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.title = this.data.title;
  }

  ngOnInit() {
  }

}
