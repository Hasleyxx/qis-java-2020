import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';

@Component({
  selector: 'app-dialog-film-inventory',
  templateUrl: './dialog-film-inventory.component.html',
  styleUrls: ['./dialog-film-inventory.component.scss']
})
export class DialogFilmInventoryComponent implements OnInit {

  public listType = "xrayMarkerInventory";
  public filmType = "filmTable";
  public transType = "xrayMarker";
  public dateFromFilm = "";
  public dateToFilm = "";
  public dateFrom = "";
  public dateTo = "";
  public filmCounts: string[];
  public totalFilms = 0;
  public datas: any;
  public excelDatas: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dateFromFilm = this.data.dateFromFilm + " 01:00:00";
    this.dateToFilm = this.data.dateToFilm + " 23:00:00";

    this.dateFrom = this.data.dateFromFilm;
    this.dateTo = this.data.dateToFilm;
  }

  ngOnInit() {
    // this.genCsv();
  }

  onFilmInventory(event) {
    this.filmCounts = event;
    // console.log(event);
  }

  onDatas(event) {
    this.datas = event;
    // console.log(event);
  }

  genCsv() {
    let title = this.dateFrom + " - " + this.dateTo + " Film Inventory";
    let excel = [];
    let blank = {
      id: "",
      patientID: "",
      date: "",
      patient: "",
      xrayFilm: ""
    };

    // Patients
    this.datas.forEach(element => {
      let tempData = {
        id: element['id'],
        patientID: element['patientID'],
        date: element['date'],
        patient: element['patient'],
        xrayFilm: element['xrayFilm'],
      };
      excel.push(tempData);
    });

    excel.push(blank);

    // Film counts
    this.filmCounts.forEach(element => {
      let tempData = {
        id: "",
        patientID: "",
        date: "",
        patient: element['name'],
        xrayFilm: element['count']
      }
      excel.push(tempData);
    });

    new Angular5Csv(excel, title, {
      headers: [
        "ID", "PatientID", "Date", "Patient", "XrayFilm"
      ]
    });
  }

}
