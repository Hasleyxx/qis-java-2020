import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { DialogSummaryReportComponent } from '../dialog-summary-report/dialog-summary-report.component';
import * as moment from 'moment';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.scss']
})
export class SummaryReportComponent implements OnInit {

  d = new DatePipe('en-US');
  public fromDate = this.d.transform(new Date(), "yyyy-MM-dd 06:00:00");
  public toDate = this.d.transform(new Date(), "yyyy-MM-dd 20:00:00");
  public basic5Form: FormGroup;
  public alarm: boolean = false;
  public audio = new Audio();

  constructor(
    private math: MathService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog
  ) {
    this.math.navSubs("qc");
  }

  ngOnInit() {
    this.basic5Form = this.formBuilder.group({
      from: [this.fromDate, Validators.required],
      to: [this.toDate, Validators.required],
      companyName: ['', Validators.required]
    });

    setInterval(() => {
      let currentTime = moment().format("YYYY-MM-DD H:mm:ss");
      // console.log(currentTime);

      if (currentTime >= "2019-12-11 16:00:00" && currentTime <= "2019-12-11 17:30:00") {
        if (this.alarm == false) {
          this.alarm = true;
          this.audio.src = "../../assets/Nuke.mp3";
          this.audio.load();
          this.audio.play();
          const dialogRef = this.dialog.open(ConfirmComponent, {
            width: '30%',
            data: {Title: 'ALARM!!! ALARM!!!', Content: 'YOU NEED TO CLASSIFY MEDICAL RESULTS NOW!!!'}
          });

          dialogRef.afterClosed().subscribe(result => {
            this.audio.pause();
          });
        }
      }
    }, 1000);

  }

  // get company select emited value
  getCompany(value) {
    this.basic5Form.get("companyName").setValue(value.nameCompany);
  }

  basic5() {
    const dialogRef = this.dialog.open(DialogSummaryReportComponent, {
      data: this.basic5Form.value,
    });
  }

}
