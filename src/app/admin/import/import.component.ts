import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { itemList, transaction, total } from 'src/app/services/service.interface';
import { TransactionService } from 'src/app/services/transaction.service';
import { DatePipe } from '@angular/common';
import { PatientService } from 'src/app/services/patient.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { PeService } from 'src/app/services/pe.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { XrayService } from 'src/app/services/xray.service';

@Component({
  selector: "app-import",
  templateUrl: "./import.component.html",
  styleUrls: ["./import.component.scss"]
})
export class ImportComponent implements OnInit {
  public importForm: FormGroup;
  public types = ["APE", "CASH", "ACCOUNT", "HYGIENE", "MEDHIS", "PE", "VITAL", "HEMATOLOGY", "MICROSCOPY", "CHEMISTRY", "SEROLOGY", "TOXICOLOGY", "XRAY"];
  public typeDefault = this.types[0];
  items: itemList[] = [];
  itemPrices: any[] = [];
  itemIds: any[] = [];
  transactions: transaction;
  total: total[] = [];
  transID = undefined;
  transactionRef: number;
  userID: number = parseInt(sessionStorage.getItem("token"));
  d = new DatePipe('en-US');
  totalVal: number = 0;
  public loading: boolean = false;
  @ViewChild('fileLabel') public fileLabel: ElementRef;

  constructor(
    private math: MathService,
    private formBuilder: FormBuilder,
    private trans: TransactionService,
    private pat: PatientService,
    private pes: PeService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private lab: LaboratoryService,
    private xray: XrayService
    ) {
    this.math.navSubs("admin");
  }
  ngOnInit() {

    this.importForm = this.formBuilder.group({
      file: ["", Validators.required],
      type: ["APE", Validators.required],
      itemId: ['', Validators.required],
      itemPrice: ["", Validators.required],
      data: ['']
    });
  }

  importData() {
    this.loading = true;
    let datas = this.importForm.get('data').value;
    let type = this.importForm.get('type').value;
    // let arrays = [];
    if(datas.length > 0) {
      if (type == "MEDHIS") {
        datas.forEach(element => {
          let ref_asth = element['asth'];
          let ref_tb = element['tb'];
          let ref_dia = element['dia'];
          let ref_hb = element['hb'];
          let ref_hp = element['hp'];
          let ref_kp = element['kp'];
          let ref_ab = element['ab'];
          let ref_jbs = element['jbs'];
          let ref_pp = element['pp'];
          let ref_mh = element['mh'];
          let ref_fs = element['fs'];
          let ref_alle = element['alle'];
          let ref_ct = element['ct'];
          let ref_hep = element['hep'];
          let ref_std = element['std'];
          let ref_patientIdRef = element['patientIdRef'];

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if(data.length !== 0) {
              let temp = {
                transactionID: data[0].transactionId,
                patientID: data[0].patientId,
                userID: +parseInt(sessionStorage.getItem("token")),
                asth: ref_asth,
                tb: ref_tb,
                dia: ref_dia,
                hb: ref_hb,
                hp: ref_hp,
                kp: ref_kp,
                ab: ref_ab,
                jbs: ref_jbs,
                pp: ref_pp,
                mh: ref_mh,
                fs: ref_fs,
                alle: ref_alle,
                ct: ref_ct,
                hep: ref_hep,
                std: ref_std,
                patientIdRef: ref_patientIdRef,
                creationDate: moment().format('YYYY-MM-DD HH:mm:ss'),
                dateUpdate: moment().format('0000-00-00 00:00:00')
              }

              this.pes.addMedhis(temp).subscribe();
            }
          });
        });
      } else if (type == "PE") {
        datas.forEach(element => {

          let ref_skin = element['skin'];
          let ref_hn = element['hn'];
          let ref_cbl = element['cbl'];
          let ref_ch = element['ch'];
          let ref_abdo = element['abdo'];
          let ref_extre = element['extre'];
          let ref_ot = element['ot'];
          let ref_find = element['find'];
          let ref_Doctor = element['Doctor'];
          let ref_License = element['License'];
          let ref_patientIdRef = element['patientIdRef'];

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data.length !== 0) {
              let temp = {
                transactionID: data[0].transactionId,
                patientID: data[0].patientId,
                userID: +parseInt(sessionStorage.getItem("token")),
                skin: ref_skin,
                hn: ref_hn,
                cbl: ref_cbl,
                ch: ref_ch,
                abdo: ref_abdo,
                extre: ref_extre,
                ot: ref_ot,
                find: ref_find,
                doctor: ref_Doctor,
                license: +82498,
                patientIdRef: ref_patientIdRef,
                creationDate: moment().format('YYYY-MM-DD HH:mm:ss'),
                dateUpdate: moment().format('0000-00-00 00:00:00')
              }

              this.pes.addPE(temp).subscribe();
            }
          });
        });
      } else if (type == "VITAL") {
        datas.forEach(element => {
          let ref_height = (element['height'] == undefined ? "-" : element['height']);
          let ref_weight = (element['weight'] == undefined ? "-" : element['weight']);
          let ref_bmi = (element['bmi'] == undefined ? "-" : element['bmi']);
          let ref_bp = (element['bp'] == undefined ? "-" : element['bp']);
          let ref_pr = (element['pr'] == undefined ? "-" : element['pr']);
          let ref_rr = (element['rr'] == undefined ? "-" : element['rr']);
          let ref_uod = (element['uod'] == undefined ? "-" : element['uod']);
          let ref_uos = (element['uos'] == undefined ? "-" : element['uos']);
          let ref_cod = (element['cod'] == undefined ? "-" : element['cod']);
          let ref_cos = (element['cos'] == undefined ? "-" : element['cos']);
          let ref_cv = (element['cv'] == undefined ? "-" : element['cv']);
          let ref_hearing = (element['hearing'] == undefined ? "-" : element['hearing']);
          let ref_hosp = (element['hosp'] == undefined ? "-" : element['hosp']);
          let ref_opr = (element['opr'] == undefined ? "-" : element['opr']);
          let ref_pm = (element['pm'] == undefined ? "-" : element['pm']);
          let ref_smoker = (element['smoker'] == undefined ? "-" : element['smoker']);
          let ref_ad = (element['ad'] == undefined ? "-" : element['ad']);
          let ref_lmp = (element['lmp'] == undefined ? "-" : element['lmp']);
          let ref_Notes = (element['Notes'] == undefined ? "-" : element['Notes']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                transactionID: data[0].transactionId,
                patientID: data[0].patientId,
                userID: +parseInt(sessionStorage.getItem("token")),
                height: ref_height,
                weight: ref_weight,
                bmi: ref_bmi,
                bp: ref_bp,
                pr: ref_pr,
                rr: ref_rr,
                uod: ref_uod,
                uos: ref_uos,
                cod: ref_cod,
                cos: ref_cos,
                cv: ref_cv,
                hearing: ref_hearing,
                hosp: moment(ref_hosp).format('YYYY-MM-DD'),
                opr: moment(ref_opr).format('YYYY-MM-DD'),
                pm: ref_pm,
                smoker: ref_smoker,
                ad: ref_ad,
                lmp: moment(ref_lmp).format('YYYY-MM-DD'),
                notes: ref_Notes,
                patientIdRef: ref_patientIdRef,
                creationDate: moment().format('YYYY-MM-DD HH:mm:ss'),
                dateUpdate: moment().format('0000-00-00 00:00:00')
              }

              this.pes.addVital(temp).subscribe();
            }
          });
        });
      } else if (type == "HEMATOLOGY") {
        datas.forEach(element => {
          // let ref_hemaID = (element['hemaID'] == undefined ? "-" : element['hemaID']);
          let ref_patientID = (element['patientID'] == undefined ? "-" : element['patientID']);
          let ref_creationDate = moment().format('YYYY-MM-DD HH:mm:ss');
          let ref_dateUpdate = "0000-00-00 00:00:00";
          let ref_transactionID = (element['transactionID'] == undefined ? "-" : element['transactionID']);
          let ref_pathID = (element['pathID'] == undefined ? "-" : element['pathID']);
          let ref_medID = (element['medID'] == undefined ? "-" : element['medID']);
          let ref_qualityID = (element['qualityID'] == undefined ? "-" : element['qualityID']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);
          let ref_userID = (element['userID'] == undefined ? "-" : element['userID']);
          let ref_monocytes = (element['monocytes'] == undefined ? "-" : element['monocytes']);
          let ref_bas = (element['bas'] == undefined ? "-" : element['bas']);
          let ref_hemoglobin = (element['hemoglobin'] == undefined ? "-" : element['hemoglobin']);
          let ref_cbcrbc = (element['cbcrbc'] == undefined ? "-" : element['cbcrbc']);
          let ref_lymphocytes = (element['lymphocytes'] == undefined ? "-" : element['lymphocytes']);
          let ref_whiteBlood = (element['whiteBlood'] == undefined ? "-" : element['whiteBlood']);
          let ref_eos = (element['eos'] == undefined ? "-" : element['eos']);
          let ref_hematocrit = (element['hematocrit'] == undefined ? "-" : element['hematocrit']);
          let ref_ptime = (element['ptime'] == undefined ? "-" : element['ptime']);
          let ref_ptcontrol = (element['ptcontrol'] == undefined ? "-" : element['ptcontrol']);
          let ref_actPercent = (element['actPercent'] == undefined ? "-" : element['actPercent']);
          let ref_cbcot = (element['cbcot'] == undefined ? "-" : element['cbcot']);
          let ref_inr = (element['inr'] == undefined ? "-" : element['inr']);
          let ref_plt = (element['plt'] == undefined ? "-" : element['plt']);
          let ref_hemoNR = (element['hemoNR'] == undefined ? "-" : element['hemoNR']);
          let ref_neutrophils = (element['neutrophils'] == undefined ? "-" : element['neutrophils']);
          let ref_hemaNR = (element['hemaNR'] == undefined ? "-" : element['hemaNR']);
          let ref_actPercentNV = (element['actPercentNV'] == undefined ? "-" : element['actPercentNV']);
          let ref_aptcontrolNV = (element['aptcontrolNV'] == undefined ? "-" : element['aptcontrolNV']);
          let ref_ptcontrolNV = (element['ptcontrolNV'] == undefined ? "-" : element['ptcontrolNV']);
          let ref_ms = (element['ms'] == undefined ? "-" : element['ms']);
          let ref_apttime = (element['apttime'] == undefined ? "-" : element['apttime']);
          let ref_ptimeNV = (element['ptimeNV'] == undefined ? "-" : element['ptimeNV']);
          let ref_esr = (element['esr'] == undefined ? "-" : element['esr']);
          let ref_aptcontrol = (element['aptcontrol'] == undefined ? "-" : element['aptcontrol']);
          let ref_pr131 = (element['pr131'] == undefined ? "-" : element['pr131']);
          let ref_inrnv = (element['inrnv'] == undefined ? "-" : element['inrnv']);
          let ref_esrmethod = (element['esrmethod'] == undefined ? "-" : element['esrmethod']);
          let ref_apttimeNV = (element['apttimeNV'] == undefined ? "-" : element['apttimeNV']);
          let ref_bloodType = (element['bloodType'] == undefined ? "-" : element['bloodType']);
          let ref_rh = (element['rh'] == undefined ? "-" : element['rh']);
          let ref_clottingTime = (element['clottingTime'] == undefined ? "-" : element['clottingTime']);
          let ref_bleedingTime = (element['bleedingTime'] == undefined ? "-" : element['bleedingTime']);

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                patientID: data[0].patientId,
                creationDate: ref_creationDate,
                dateUpdate: ref_dateUpdate,
                transactionID: data[0].transactionId,
                pathID: ref_pathID,
                medID: ref_medID,
                qualityID: ref_qualityID,
                patientIdRef: ref_patientIdRef,
                userID: ref_userID,
                monocytes: ref_monocytes,
                bas: ref_bas,
                hemoglobin: ref_hemoglobin,
                cbcrbc: ref_cbcrbc,
                lymphocytes: ref_lymphocytes,
                whiteBlood: ref_whiteBlood,
                eos: ref_eos,
                hematocrit: ref_hematocrit,
                ptime: ref_ptime,
                ptcontrol: ref_ptcontrol,
                actPercent: ref_actPercent,
                cbcot: ref_cbcot,
                inr: ref_inr,
                plt: ref_plt,
                hemoNR: ref_hemoNR,
                neutrophils: ref_neutrophils,
                hemaNR: ref_hemaNR,
                actPercentNV: ref_actPercentNV,
                aptcontrolNV: ref_aptcontrolNV,
                ptcontrolNV: ref_ptcontrolNV,
                ms: ref_ms,
                apttime: ref_apttime,
                ptimeNV: ref_ptimeNV,
                esr: ref_esr,
                aptcontrol: ref_aptcontrol,
                pr131: ref_pr131,
                inrnv: ref_inrnv,
                esrmethod: ref_esrmethod,
                apttimeNV: ref_apttimeNV,
                bloodType: ref_bloodType,
                rh: ref_rh,
                clottingTime: ref_clottingTime,
                bleedingTime: ref_bleedingTime
              }

              this.lab.addHematology(temp).subscribe();
            }
          });
        });
      } else if (type == "MICROSCOPY") {
        datas.forEach(element => {
          let ref_transactionID = (element['transactionID'] == undefined ? "-" : element['transactionID']);
          let ref_patientID = (element['patientID'] == undefined ? "-" : element['patientID']);
          let ref_pathID = (element['pathID'] == undefined ? "-" : element['pathID']);
          let ref_medID = (element['medID'] == undefined ? "-" : element['medID']);
          let ref_qualityID = (element['qualityID'] == undefined ? "-" : element['qualityID']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);
          let ref_userID = (element['userID'] == undefined ? "-" : element['userID']);
          let ref_creationDate = moment().format('YYYY-MM-DD HH:mm:ss');
          let ref_dateUpdate = moment().format('0000-00-00 00:00:00');

          let ref_uriColor = (element['uriColor'] == undefined ? "-" : element['uriColor']);
          let ref_uriTrans = (element['uriTrans'] == undefined ? "-" : element['uriTrans']);
          let ref_uriOt = (element['uriOt'] == undefined ? "-" : element['uriOt']);

          let ref_uriPh = (element['uriPh'] == undefined ? "-" : element['uriPh']);
          let ref_uriSp = (element['uriSp'] == undefined ? "-" : element['uriSp']);
          let ref_uriPro = (element['uriPro'] == undefined ? "-" : element['uriPro']);
          let ref_uriGlu = (element['uriGlu'] == undefined ? "-" : element['uriGlu']);
          let ref_le = (element['le'] == undefined ? "-" : element['le']);
          let ref_nit = (element['nit'] == undefined ? "-" : element['nit']);
          let ref_uro = (element['uro'] == undefined ? "-" : element['uro']);
          let ref_bld = (element['bld'] == undefined ? "-" : element['bld']);
          let ref_ket = (element['ket'] == undefined ? "-" : element['ket']);
          let ref_bil = (element['bil'] == undefined ? "-" : element['bil']);

          let ref_rbc_temp = (element['rbc'] == undefined ? "-" : element['rbc']);
          let ref_wbc_temp = (element['wbc'] == undefined ? "-" : element['wbc']);
          let ref_rbc_temp_split = ref_rbc_temp.split("||");
          let ref_wbc_temp_split = ref_wbc_temp.split("||");

          let ref_rbc = ref_rbc_temp_split[1];
          let ref_wbc = ref_wbc_temp_split[1];

          let ref_ecells = (element['ecells'] == undefined ? "-" : element['ecells']);
          let ref_mthreads = (element['mthreads'] == undefined ? "-" : element['mthreads']);
          let ref_bac = (element['bac'] == undefined ? "-" : element['bac']);
          let ref_amorph = (element['amorph'] == undefined ? "-" : element['amorph']);
          let ref_coAx = (element['coAx'] == undefined ? "-" : element['coAx']);

          let ref_fecColor = (element['fecColor'] == undefined ? "-" : element['fecColor']);
          let ref_fecCon = (element['fecCon'] == undefined ? "-" : element['fecCon']);
          let ref_fecMicro = (element['fecMicro'] == undefined ? "-" : element['fecMicro']);
          let ref_fecOt = (element['fecOt'] == undefined ? "-" : element['fecOt']);

          let ref_pregTest = (element['pregTest'] == undefined ? "-" : element['pregTest']);

          let ref_occultBLD = (element['occultBLD'] == undefined ? "-" : element['occultBLD']);

          let ref_afbva1 = (element['afbva1'] == undefined ? "-" : element['afbva1']);
          let ref_afbva2 = (element['afbva2'] == undefined ? "-" : element['afbva2']);
          let ref_afbr1 = (element['afbr1'] == undefined ? "-" : element['afbr1']);
          let ref_afbr2 = (element['afbr2'] == undefined ? "-" : element['afbr2']);
          let ref_afbd = (element['afbd'] == undefined ? "-" : element['afbd']);

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                transactionID: data[0].transactionId,
                patientID: data[0].patientId,
                pathID: ref_pathID,
                medID: ref_medID,
                qualityID: ref_qualityID,
                patientIdRef: ref_patientIdRef,
                userID: ref_userID,
                creationDate: ref_creationDate,
                dateUpdate: ref_dateUpdate,
                uriColor: ref_uriColor,
                uriTrans: ref_uriTrans,
                uriOt: ref_uriOt,
                uriPh: ref_uriPh,
                uriSp: ref_uriSp,
                uriPro: ref_uriPro,
                uriGlu: ref_uriGlu,
                le: ref_le,
                nit: ref_nit,
                uro: ref_uro,
                bld: ref_bld,
                ket: ref_ket,
                bil: ref_bil,
                rbc: ref_rbc,
                wbc: ref_wbc,
                ecells: ref_ecells,
                mthreads: ref_mthreads,
                bac: ref_bac,
                amorph: ref_amorph,
                coAx: ref_coAx,
                fecColor: ref_fecColor,
                fecCon: ref_fecCon,
                fecMicro: ref_fecMicro,
                fecOt: ref_fecOt,
                pregTest: ref_pregTest,
                occultBLD: ref_occultBLD,
                afbva1: ref_afbva1,
                afbva2: ref_afbva2,
                afbr1: ref_afbr1,
                afbr2: ref_afbr2,
                afbd: ref_afbd
              }

              this.lab.addMicroscopy(temp).subscribe();
            }
          });
        });
      } else if (type == "CHEMISTRY") {
        datas.forEach(element => {
          let ref_patientID = (element['patientID'] == undefined ? "-" : element['patientID']);
          let ref_transactionID = (element['transactionID'] == undefined ? "-" : element['transactionID']);
          let ref_pathID = (element['pathID'] == undefined ? "-" : element['pathID']);
          let ref_medID = (element['medID'] == undefined ? "-" : element['medID']);
          let ref_qualityID = (element['qualityID'] == undefined ? "-" : element['qualityID']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);
          let ref_userID = (element['userID'] == undefined ? "-" : element['userID']);
          let ref_creationDate = moment().format('YYYY-MM-DD HH:mm:ss');
          let ref_dateUpdate = moment().format('0000-00-00 00:00:00');
          let ref_totalCPK = (element['totalCPK'] == undefined ? "-" : element['totalCPK']);
          let ref_agratio = (element['agratio'] == undefined ? "-" : element['agratio']);
          let ref_buncon = (element['buncon'] == undefined ? "-" : element['buncon']);
          let ref_bun = (element['bun'] == undefined ? "-" : element['bun']);
          let ref_fbs = (element['fbs'] == undefined ? "-" : element['fbs']);
          let ref_fbscon = (element['fbscon'] == undefined ? "-" : element['fbscon']);
          let ref_trig = (element['trig'] == undefined ? "-" : element['trig']);
          let ref_crea = (element['crea'] == undefined ? "-" : element['crea']);
          let ref_creacon = (element['creacon'] == undefined ? "-" : element['creacon']);
          let ref_hdlcon = (element['hdlcon'] == undefined ? "-" : element['hdlcon']);
          let ref_ldl = (element['ldl'] == undefined ? "-" : element['ldl']);
          let ref_trigcon = (element['trigcon'] == undefined ? "-" : element['trigcon']);
          let ref_alt = (element['alt'] == undefined ? "-" : element['alt']);
          let ref_chol = (element['chol'] == undefined ? "-" : element['chol']);
          let ref_ldlcon = (element['ldlcon'] == undefined ? "-" : element['ldlcon']);
          let ref_bua = (element['bua'] == undefined ? "-" : element['bua']);
          let ref_cholcon = (element['cholcon'] == undefined ? "-" : element['cholcon']);
          let ref_ch = (element['ch'] == undefined ? "-" : element['ch']);
          let ref_na = (element['na'] == undefined ? "-" : element['na']);
          let ref_buacon = (element['buacon'] == undefined ? "-" : element['buacon']);
          let ref_cl = (element['cl'] == undefined ? "-" : element['cl']);
          let ref_ast = (element['ast'] == undefined ? "-" : element['ast']);
          let ref_hb = (element['hb'] == undefined ? "-" : element['hb']);
          let ref_vldl = (element['vldl'] == undefined ? "-" : element['vldl']);
          let ref_k = (element['k'] == undefined ? "-" : element['k']);
          let ref_alp = (element['alp'] == undefined ? "-" : element['alp']);
          let ref_psa = (element['psa'] == undefined ? "-" : element['psa']);
          let ref_hdl = (element['hdl'] == undefined ? "-" : element['hdl']);
          let ref_lipase = (element['lipase'] == undefined ? "-" : element['lipase']);
          let ref_biltotal = (element['biltotal'] == undefined ? "-" : element['biltotal']);
          let ref_ogtt1 = (element['ogtt1'] == undefined ? "-" : element['ogtt1']);
          let ref_ogtt1con = (element['ogtt1con'] == undefined ? "-" : element['ogtt1con']);
          let ref_calcium = (element['calcium'] == undefined ? "-" : element['calcium']);
          let ref_albumin = (element['albumin'] == undefined ? "-" : element['albumin']);
          let ref_ldh = (element['ldh'] == undefined ? "-" : element['ldh']);
          let ref_amylase = (element['amylase'] == undefined ? "-" : element['amylase']);
          let ref_ogtt2 = (element['ogtt2'] == undefined ? "-" : element['ogtt2']);
          let ref_rbs = (element['rbs'] == undefined ? "-" : element['rbs']);
          let ref_globulin = (element['globulin'] == undefined ? "-" : element['globulin']);
          let ref_ggtp = (element['ggtp'] == undefined ? "-" : element['ggtp']);
          let ref_ogctcon = (element['ogctcon'] == undefined ? "-" : element['ogctcon']);
          let ref_cpkmm = (element['cpkmm'] == undefined ? "-" : element['cpkmm']);
          let ref_ionCalcium = (element['ionCalcium'] == undefined ? "-" : element['ionCalcium']);
          let ref_bildirect = (element['bildirect'] == undefined ? "-" : element['bildirect']);
          let ref_protein = (element['protein'] == undefined ? "-" : element['protein']);
          let ref_ogct = (element['ogct'] == undefined ? "-" : element['ogct']);
          let ref_bilindirect = (element['bilindirect'] == undefined ? "-" : element['bilindirect']);
          let ref_rbscon = (element['rbscon'] == undefined ? "-" : element['rbscon']);
          let ref_magnesium = (element['magnesium'] == undefined ? "-" : element['magnesium']);
          let ref_ogtt2con = (element['ogtt2con'] == undefined ? "-" : element['ogtt2con']);
          let ref_cpkmb = (element['cpkmb'] == undefined ? "-" : element['cpkmb']);
          let ref_inPhos = (element['inPhos'] == undefined ? "-" : element['inPhos']);
          let ref_chemNotes = (element['chemNotes'] == undefined ? "-" : element['chemNotes']);

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                patientID: data[0].patientId,
                transactionID: data[0].transactionId,
                pathID: ref_pathID,
                medID: ref_medID,
                qualityID: ref_qualityID,
                patientIdRef: ref_patientIdRef,
                userID: ref_userID,
                creationDate: ref_creationDate,
                dateUpdate: ref_dateUpdate,
                totalCPK: ref_totalCPK,
                agratio: ref_agratio,
                buncon: ref_buncon,
                bun: ref_bun,
                fbs: ref_fbs,
                fbscon: ref_fbscon,
                trig: ref_trig,
                crea: ref_crea,
                creacon: ref_creacon,
                hdlcon: ref_hdlcon,
                ldl: ref_ldl,
                trigcon: ref_trigcon,
                alt: ref_alt,
                chol: ref_chol,
                ldlcon: ref_ldlcon,
                bua: ref_bua,
                cholcon: ref_cholcon,
                ch: ref_ch,
                na: ref_na,
                buacon: ref_buacon,
                cl: ref_cl,
                ast: ref_ast,
                hb: ref_hb,
                vldl: ref_vldl,
                k: ref_k,
                alp: ref_alp,
                psa: ref_psa,
                hdl: ref_hdl,
                lipase: ref_lipase,
                biltotal: ref_biltotal,
                ogtt1: ref_ogtt1,
                ogtt1con: ref_ogtt1con,
                calcium: ref_calcium,
                albumin: ref_albumin,
                ldh: ref_ldh,
                amylase: ref_amylase,
                ogtt2: ref_ogtt2,
                rbs: ref_rbs,
                globulin: ref_globulin,
                ggtp: ref_ggtp,
                ogctcon: ref_ogctcon,
                cpkmm: ref_cpkmm,
                ionCalcium: ref_ionCalcium,
                bildirect: ref_bildirect,
                protein: ref_protein,
                ogct: ref_ogct,
                bilindirect: ref_bilindirect,
                rbscon: ref_rbscon,
                magnesium: ref_magnesium,
                ogtt2con: ref_ogtt2con,
                cpkmb: ref_cpkmb,
                inPhos: ref_inPhos,
                chemNotes: ref_chemNotes
              }

              this.lab.addChemistry(temp).subscribe();
            }
          });
        });
      } else if (type == "SEROLOGY") {
        datas.forEach(element => {
          let ref_transactionID = (element['transactionID'] == undefined ? "-" : element['transactionID']);
          let ref_patientID = (element['patientID'] == undefined ? "-" : element['patientID']);
          let ref_pathID = (element['pathID'] == undefined ? "-" : element['pathID']);
          let ref_medID = (element['medID'] == undefined ? "-" : element['medID']);
          let ref_qualityID = (element['qualityID'] == undefined ? "-" : element['qualityID']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);
          let ref_userID = (element['userID'] == undefined ? "-" : element['userID']);
          let ref_hbsAG = (element['hbsAG'] == undefined ? "-" : element['hbsAG']);
          let ref_antiHav = (element['antiHav'] == undefined ? "-" : element['antiHav']);
          let ref_seroOt = (element['seroOt'] == undefined ? "-" : element['seroOt']);
          let ref_vdrl = (element['vdrl'] == undefined ? "-" : element['vdrl']);
          let ref_psanti = (element['psanti'] == undefined ? "-" : element['psanti']);
          let ref_tppa = (element['tppa'] == undefined ? "-" : element['tppa']);
          let ref_antiHBS = (element['antiHBS'] == undefined ? "-" : element['antiHBS']);
          let ref_hbeAG = (element['hbeAG'] == undefined ? "-" : element['hbeAG']);
          let ref_antiHBE = (element['antiHBE'] == undefined ? "-" : element['antiHBE']);
          let ref_antiHBC = (element['antiHBC'] == undefined ? "-" : element['antiHBC']);
          let ref_tydotigM = (element['tydotigM'] == undefined ? "-" : element['tydotigM']);
          let ref_tydotigG = (element['tydotigG'] == undefined ? "-" : element['tydotigG']);
          let ref_cea = (element['cea'] == undefined ? "-" : element['cea']);
          let ref_afp = (element['afp'] == undefined ? "-" : element['afp']);
          let ref_ca125 = (element['ca125'] == undefined ? "-" : element['ca125']);
          let ref_ca19 = (element['ca19'] == undefined ? "-" : element['ca19']);
          let ref_ca15 = (element['ca15'] == undefined ? "-" : element['ca15']);
          let ref_tsh = (element['tsh'] == undefined ? "-" : element['tsh']);
          let ref_ft3 = (element['ft3'] == undefined ? "-" : element['ft3']);
          let ref_ft4 = (element['ft4'] == undefined ? "-" : element['ft4']);
          let ref_crpdil = (element['crpdil'] == undefined ? "-" : element['crpdil']);
          let ref_crpres = (element['crpres'] == undefined ? "-" : element['crpres']);
          let ref_hiv1 = (element['hiv1'] == undefined ? "-" : element['hiv1']);
          let ref_hiv2 = (element['hiv2'] == undefined ? "-" : element['hiv2']);
          let ref_creationDate = moment().format('YYYY-MM-DD HH:mm:ss');
          let ref_dateUpdate = moment().format('0000-00-00 00:00:00');

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                transactionID: data[0].transactionId,
                patientID: data[0].patientId,
                pathID: ref_pathID,
                medID: ref_medID,
                qualityID: ref_qualityID,
                patientIdRef: ref_patientIdRef,
                userID: ref_userID,
                hbsAG: ref_hbsAG,
                antiHav: ref_antiHav,
                seroOt: ref_seroOt,
                vdrl: ref_vdrl,
                psanti: ref_psanti,
                tppa: ref_tppa,
                antiHBS: ref_antiHBS,
                hbeAG: ref_hbeAG,
                antiHBE: ref_antiHBE,
                antiHBC: ref_antiHBC,
                tydotigM: ref_tydotigM,
                tydotigG: ref_tydotigG,
                cea: ref_cea,
                afp: ref_afp,
                ca125: ref_ca125,
                ca19: ref_ca19,
                ca15: ref_ca15,
                tsh: ref_tsh,
                ft3: ref_ft3,
                ft4: ref_ft4,
                crpdil: ref_crpdil,
                crpres: ref_crpres,
                hiv1: ref_hiv1,
                hiv2: ref_hiv2,
                creationDate: ref_creationDate,
                dateUpdate: ref_dateUpdate
              }

              this.lab.addSerology(temp).subscribe();
            }
          });
        });
      } else if (type == "TOXICOLOGY") {
        datas.forEach(element => {
          let ref_patientID = (element['patientID'] == undefined ? "-" : element['patientID']);
          let ref_transactionID = (element['transactionID'] == undefined ? "-" : element['transactionID']);
          let ref_pathID = (element['pathID'] == undefined ? "-" : element['pathID']);
          let ref_medID = (element['medID'] == undefined ? "-" : element['medID']);
          let ref_qualityID = (element['qualityID'] == undefined ? "-" : element['qualityID']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);
          let ref_userID = (element['userID'] == undefined ? "-" : element['userID']);
          let ref_meth = (element['meth'] == undefined ? "-" : element['meth']);
          let ref_tetra = (element['tetra'] == undefined ? "-" : element['tetra']);
          let ref_drugtest = (element['drugtest'] == undefined ? "-" : element['drugtest']);
          let ref_creationDate = moment().format('YYYY-MM-DD HH:mm:ss');
          let ref_dateUpdate = moment().format('0000-00-00 00:00:00');

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                patientID: data[0].patientId,
                transactionID: data[0].transactionId,
                pathID: ref_pathID,
                medID: ref_medID,
                qualityID: ref_qualityID,
                patientIdRef: ref_patientIdRef,
                userID: ref_userID,
                meth: ref_meth,
                tetra: ref_tetra,
                drugtest: ref_drugtest,
                creationDate: ref_creationDate,
                dateUpdate: ref_dateUpdate
              }

              this.lab.addToxicology(temp).subscribe();
            }
          });
        });
      } else if (type == "XRAY") {
        datas.forEach(element => {
          let ref_imgXray = (element['imgXray'] == undefined ? "-" : element['imgXray']);
          let ref_patientIdRef = (element['patientIdRef'] == undefined ? "-" : element['patientIdRef']);
          let ref_userID = (element['userID'] == undefined ? "-" : element['userID']);
          let ref_comment = (element['comment'] == undefined ? "-" : element['comment']);
          let ref_patientID = (element['patientID'] == undefined ? "-" : element['patientID']);
          let ref_creationDate = moment().format('YYYY-MM-DD HH:mm:ss');
          let ref_dateUpdate = moment().format('0000-00-00 00:00:00');
          let ref_transactionID = (element['transactionID'] == undefined ? "-" : element['transactionID']);
          let ref_radiologist = (element['radiologist'] == undefined ? "-" : element['radiologist']);
          let ref_impression = (element['impression'] == undefined ? "-" : element['impression']);
          let ref_xrayID = (element['xrayID'] == undefined ? "-" : element['xrayID']);
          let ref_qa = (element['qa'] == undefined ? "-" : element['qa']);

          this.trans.getpatientIdRef(ref_patientIdRef).subscribe(data => {
            if (data[0] !== null) {
              let temp = {
                imgXray: ref_imgXray,
                patientIdRef: ref_patientIdRef,
                userID: ref_userID,
                comment: ref_comment,
                patientID: ref_patientID,
                creationDate: ref_creationDate,
                dateUpdate: ref_dateUpdate,
                transactionID: ref_transactionID,
                radiologist: ref_radiologist,
                impression: ref_impression,
                xrayID: ref_xrayID,
                qa: ref_qa
              }

              this.xray.addXray(temp).subscribe();
            }
          });
        });
      } else {
        datas.forEach(element => {
          // arrays.push(element['TransactionID']);
          // return;
          let ref_address = (element['Address'] !== undefined) ? element['Address'] : "";
          let ref_age = (element['Age'] !== undefined) ? element['Age'] : 0 ;
          let ref_contact_no = (element['ContactNo'] !== undefined) ? element['ContactNo'] : "" ;
          let ref_gender = (element['Gender'] !== undefined) ? element['Gender'] : "" ;
          let ref_middle_name = (element['MiddleName'] !== undefined) ? element['MiddleName'] : "" ;
          let ref_bday = (element["Birthdate"] !== undefined) ? this.d.transform(new Date(element["Birthdate"]), "yyyy-MM-dd") : "";
          let ref_notes = (element["Notes"] !== undefined) ? element['Notes'] : "";
          let ref_position = (element["Position"] !== undefined) ? element['Position'] : "";
          let ref_patientIdRef = element['PatientRef'];

          if (ref_bday !== "") {
            ref_age = this.math.computeAge(ref_bday);
          }
          //Generate random numbers and check patient ref for duplicate
          this.pat.getPatient("getPatient").subscribe(
            data => {
              let ref_patient = this.math.patcheckRef(data);
              let patientData: any = {
                companyName: element["CompanyName"],
                position: ref_position,
                firstName: element["FirstName"],
                middleName: ref_middle_name,
                lastName: element["LastName"],
                address: ref_address,
                gender: ref_gender,
                birthdate: ref_bday,
                age: +ref_age,
                contactNo: ref_contact_no,
                email: "",
                sid: "",
                dateUpdate: "00-00-00",
                creationDate: this.d.transform(new Date(), "yyyy-MM-dd"),
                patientType: "",
                notes: ref_notes,
                patientRef: ref_patient
              };

              this.pat.addPatient(patientData).subscribe(
                (patData: any) => {
                  if (patData == 1) {
                    this.pat.getOnePatient("checkRef/" + patientData.patientRef).subscribe(data => {
                      // Generate transaction random numbers
                      this.trans.getTransactions("getTransaction").subscribe(transDatas => {
                        this.transactionRef = this.math.transcheckRef(transDatas);

                        let transaction: any = {
                          transactionId: this.transID,
                          transactionRef: this.transactionRef,
                          patientId: data[0].patientID,
                          userId: +this.userID,
                          transactionType: this.importForm.get('type').value,
                          biller: element["CompanyName"],
                          totalPrice: this.totalVal,
                          paidIn: 0,
                          paidOut: 0,
                          grandTotal: this.totalVal,
                          status: 1,
                          salesType: "sales",
                          loe: "",
                          an: "",
                          ac: "",
                          notes: "",
                          currency: "PESO",
                          transactionDate: this.math.getDateNow(),
                          patientIdRef: ref_patientIdRef
                        };

                        this.trans.saveTransaction(
                          transaction,
                          this.total,
                          this.items
                        ).subscribe(success => {
                          // this.openSnackBar("Data imported", "close");
                        });
                      });
                    });
                  }
                }
              );
            }
          );

        });
      }
      // console.log(arrays);
      // return;
      this.openSnackBar("Data imported", "close");
      this.router.navigateByUrl('element/select-item', { skipLocationChange: true }).then(() => {
        this.router.navigate(['admin/import']);
      });
    }
  }

  onLoadFile(e) {
    let files = e.target.files, f = files[0];
    let fileType = files[0].type;
    let fileName = files[0].name;
    this.fileLabel.nativeElement.innerHTML = fileName;

    if (fileType == "application/vnd.ms-excel") {
      let reader = new FileReader();
      let jsonSheet;
      reader.onload = (e: any) => {
        let data = new Uint8Array(e.target.result);
        let workbook = XLSX.read(data, { type: "array", cellDates: true, dateNF: 'yyyy/mm/dd;@' });
        let first_sheet_name = workbook.SheetNames[0];
        let worksheet = workbook.Sheets[first_sheet_name];
        jsonSheet = XLSX.utils.sheet_to_json(worksheet, {
          raw: true
        });

        this.importForm.patchValue({
          data: jsonSheet
        });
      };
      reader.readAsArrayBuffer(f);
    } else {
      this.openSnackBar("Invalid file format!", "close");
    }
  }

  //get item from dropdown list
  getItem(value) {
    let found = this.items.find(item => item.itemId === value.itemId);
    if (found === undefined) {
      this.items.push(value);

      this.itemIds.push(value.itemId);
      this.itemPrices.push(value.itemPrice);
      this.totalVal = this.totalVal + value.itemPrice;
      this.importForm.patchValue({
        itemId: this.itemIds,
        itemPrice: this.totalVal
      });
    }

    let totalTemp = {
      id: value.itemId,
      discount: 0,
      price: value.itemPrice,
      quantity: 1,
      subtotal: value.itemPrice
    };

    this.total.push(totalTemp);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000
    });
  }

  changeType(event) {
    if(event.value == "MEDHIS" || event.value == "PE" || event.value == "VITAL") {
      this.importForm.valid;
    } else {
      this.importForm.invalid;
    }
  }
}
