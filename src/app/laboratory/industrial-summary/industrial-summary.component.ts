import { Component, OnInit, Inject } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PeService } from 'src/app/services/pe.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ItemService } from 'src/app/services/item.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DialogPeSummaryComponent } from 'src/app/element/dialog-pe-summary/dialog-pe-summary.component';
import { XrayService } from 'src/app/services/xray.service';


@Component({
  selector: 'app-industrial-summary',
  templateUrl: './industrial-summary.component.html',
  styleUrls: ['./industrial-summary.component.scss']
})


export class IndustrialSummaryComponent implements OnInit {


  public patientId: any;
  public transactionId: any;
  public type: any;

  public patientDatas: any = [];
  public transDatas: any = [];
  public medicDatas: any = [];
  public vitalDatas: any = [];
  public peDatas: any = [];

  public vitals: any = [];
  public pes: any = [];

  public availed: any;
  public description: any;
  public transactionType: any;

  public hemaDatas: any = [];
  public seroDatas: any = [];
  public microDatas: any = [];
  public toxiDatas: any = [];
  public classification: any = [];

  public medical: any;
  public quality: any;
  public path: any;

  public rad: any;
  public class: any;
  public notes: any;
  public classColor: any;

  public classifyForm: FormGroup;

  public classifications = ["CLASS A - Physically Fit","CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency","CLASS C - With abnormal findings generally not accepted for employment.","CLASS D - Unemployable","Pending - These are cases that are equivocal as to the classification and are being evaluated further. After a certain period of time these cases will be classified whether fit or unfit fot employment."
  ];
  public classificationDefault = this.classifications[0];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private peService: PeService,
    private labService: LaboratoryService,
    private itemService: ItemService,
    private XS: XrayService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DialogPeSummaryComponent>
  ) { }

  ngOnInit() {
    this.patientId = this.data.pid;
    this.transactionId = this.data.tid;

    if (this.data.status) {
      this.type = this.data.status;
    }

    this.getData();

    this.classifyForm = this.formBuilder.group({
      transactionID: +this.transactionId,
      patientID: +this.patientId,
      medicalClass: ['', Validators.required],
      notes: ['', Validators.required],
      qc: [''],
      qclicense: +0,
      creationDate: moment().format("YYYY-MM-DD h:mm:ss"),
      classId: ''
    });

    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.classifyForm.patchValue({
            medicalClass: data['medicalClass'],
            notes: data['notes'],
            classId: data['classID']
          });
        }
      }
    );

  }

  getData() {
    // Patient
    this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
      data => {
        this.patientDatas = data[0];
      }
    );

    // Transaction
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      value => {
        this.transDatas = value[0];
        this.transactionService.getTransExt(this.transactionId).subscribe(
          data => {
            if (data[0].itemID !== null) {
              const itemID = data[0].itemID;
              this.itemService.getItemByID(itemID).subscribe(
                result => {
                  this.availed = result[0].itemName;
                  this.description = result[0].itemDescription;
                  this.transactionType = value[0].transactionType;
                }
              );
            }else {
              this.availed = '-';
              this.description = '-';
              this.transactionType = '-';
            }
          }
        );
      }
    );


    this.peService.getMedhis(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.medicDatas.push(
            { "label": "Astma", "value": data.asth },
            { "label": "Tuberculosis", "value": data.tb },
            { "label": "Diabetes Mellitus", "value": data.dia },
            { "label": "High Blood Pressure", "value": data.hp },
            { "label": "Heart Problem", "value": data.hp },
            { "label": "Kidney Problem", "value": data.kp },
            { "label": "Abdominal/Hernia", "value": data.ab },
            { "label": "Joint/Back/Scoliosis", "value": data.jbs },
            { "label": "Psychiatric Problem", "value": data.pp },
            { "label": "Migraine/Headache", "value": data.mh },
            { "label": "Fainting/Seizures", "value": data.fs },
            { "label": "Allergies", "value": data.alle },
            { "label": "Cancer/Tumor", "value": data.ct },
            { "label": "Hepatitis", "value": data.hep },
            { "label": "STD", "value": data.std }
          );

        }else {
          this.medicDatas.push(
            { "label": "Astma", "value": '-' },
            { "label": "Tuberculosis", "value": '-' },
            { "label": "Diabetes Mellitus", "value": '-' },
            { "label": "High Blood Pressure", "value": '-' },
            { "label": "Heart Problem", "value": '-' },
            { "label": "Kidney Problem", "value": '-' },
            { "label": "Abdominal/Hernia", "value": '-' },
            { "label": "Joint/Back/Scoliosis", "value": '-' },
            { "label": "Psychiatric Problem", "value": '-' },
            { "label": "Migraine/Headache", "value": '-' },
            { "label": "Fainting/Seizures", "value": '-' },
            { "label": "Allergies", "value": '-' },
            { "label": "Cancer/Tumor", "value": '-' },
            { "label": "Hepatitis", "value": '-' },
            { "label": "STD", "value": '-' }
          );
        }
      }
    );
    this.peService.getVital(this.transactionId).subscribe(
      data => {
        this.vitalDatas = data;
        if (data !== null) {
          this.vitals.push(
            { "label": "Ishihara Test:", "value": data.cv },
            { "label": "Hearing", "value": data.hearing },
            { "label": "Hospitalization:", "value": data.hosp },
            { "label": "Operations:", "value": data.opr },
            { "label": "Medications:", "value": data.pm },
            { "label": "Smoker:", "value": data.smoker },
            { "label": "Alcoholic:", "value": data.ad },
            { "label": "Last Menstrual:", "value": data.lmp },
            { "label": "Others/Notes:", "value": data.notes }
          );

        }else {
          this.vitals.push(
            { "label": "Ishihara Test:", "value": '-' },
            { "label": "Hearing", "value": '-' },
            { "label": "Hospitalization:", "value": '-' },
            { "label": "Operations:", "value": '-' },
            { "label": "Medications:", "value": '-' },
            { "label": "Smoker:", "value": '-' },
            { "label": "Alcoholic:", "value": '-' },
            { "label": "Last Menstrual:", "value": '-' },
            { "label": "Others/Notes:", "value": '-' }
          );
        }
      }
    );
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.pes.push(
            { "label": "Skin:", "value": data.skin },
            { "label": "Head and Neck:", "value": data.hn },
            { "label": "Chest/Breast/Lungs:", "value": data.cbl },
            { "label": "Cardiac/Heart:", "value": data.ch },
            { "label": "Abdomen:", "value": data.abdo },
            { "label": "Extremities:", "value": data.extre },
            { "label": "Others/Notes:", "value": data.ot },
            { "label": "Findings:", "value": data.find },
            { "label": "Physican:", "value": data.doctor },
            { "label": "License:", "value": data.license }
          );

        }else {
          this.pes.push(
            { "label": "Skin:", "value": '-' },
            { "label": "Head and Neck:", "value": '-' },
            { "label": "Chest/Breast/Lungs:", "value": '-' },
            { "label": "Cardiac/Heart:", "value": '-' },
            { "label": "Abdomen:", "value": '-' },
            { "label": "Extremities:", "value": '-' },
            { "label": "Others/Notes:", "value": '-' },
            { "label": "Findings:", "value": '-' },
            { "label": "Physican:", "value": '-' },
            { "label": "License:", "value": '-' }
          );
        }
      }
    );

    // Hematology
    this.labService.getHematology(this.transactionId).subscribe(
      data => {
        this.hemaDatas = data[0];
      }
    );

    // Microscopy
    this.labService.getMicroscopy(this.transactionId).subscribe(
      data => {
        if (data.length !== 0) {
          this.microDatas = data[0];

          this.labService.getPersonnel(this.microDatas.medID).subscribe(
            data => {
              this.medical = data[0];
            }
          );
          this.labService.getPersonnel(this.microDatas.qualityID).subscribe(
            data => {
              this.quality = data[0];
            }
          );
          this.labService.getPersonnel(this.microDatas.pathID).subscribe(
            data => {
              this.path = data[0];
            }
          );
        }
      }
    );

    // Serology
    this.labService.getSerology(this.transactionId).subscribe(
      data => {
        this.seroDatas = data[0];
      }
    );

    // Toxicology
    this.labService.getToxicology(this.transactionId).subscribe(
      data => {
        this.toxiDatas = data[0];
      }
    );

    // Radiology
    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.rad = data;
        }else {
          this.rad = '';
        }
      }
    );
  }

  classify() {
    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        if (data == null) {
          this.labService.addClass(this.classifyForm.value).subscribe(
            data => {
              if (data == 1) {
                this.dialogRef.close("added");
              }
            }
          );

        }else {
          this.labService.updateClass(this.classifyForm.value).subscribe(
            data => {
              if (data == 1) {
                this.dialogRef.close("updated");
              }
            }
          );
        }
      }
    );
  }

}
