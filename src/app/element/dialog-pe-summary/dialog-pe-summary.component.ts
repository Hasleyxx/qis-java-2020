import { Component, OnInit, Inject } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PeService } from 'src/app/services/pe.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ItemService } from 'src/app/services/item.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ConfirmComponent } from '../confirm/confirm.component';
import { XrayService } from 'src/app/services/xray.service';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-dialog-pe-summary',
  templateUrl: './dialog-pe-summary.component.html',
  styleUrls: ['./dialog-pe-summary.component.scss']
})
export class DialogPeSummaryComponent implements OnInit {

  public patientId: any;
  public transactionId: any;
  public type: any;

  public patientDatas: any = [];
  public transDatas: any = [];
  public medisDatas: any = [];
  public vitalDatas: any = [];
  public peDatas: any = [];

  public vitals: any = [];
  public pes: any = [];

  public findings: any;
  public phy: any;
  public phyLic: any;

  public availed: any;
  public description: any;
  public transactionType: any;

  public hemaDatas: any = [];
  public seroDatas: any = [];
  public microDatas: any = [];
  public toxiDatas: any = [];
  public classification: any = [];

  public medical: any;
  public quality: any;
  public path: any;

  public rad: any;
  public class: any;
  public notes: any;
  public classColor: any;

  public classifyForm: FormGroup;
  public classifyFormCheck = false;
  public classifyFormData = true;

  public classifications = [];
  public classificationDefault = this.classifications[0];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private peService: PeService,
    private labService: LaboratoryService,
    private XS: XrayService,
    private itemService: ItemService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DialogPeSummaryComponent>,
    private dialog: MatDialog,
    public math: MathService
  ) {
    this.transactionId = this.data.tid;
  }

  ngOnInit() {
    if (this.router.url == "/nurse/classification") {
      this.classifyFormCheck = true;
    }

    if (this.data.status) {
      this.type = this.data.status;
    }

    this.getData();

    this.classifyForm = this.formBuilder.group({
      transactionID: [this.transactionId],
      patientID: [''],
      medicalClass: ['', Validators.required],
      notes: ['', Validators.required],
      qc: [''],
      qclicense: +0,
      userID: [parseInt(sessionStorage.getItem('token'))],
      creationDate: moment().format("YYYY-MM-DD h:mm:ss"),
      classId: ''
    });

    this.classifyForm.controls['medicalClass'].valueChanges.subscribe(data => {
      if (data == "CLASS A - Physically Fit") {
        this.classifyForm.get('notes').setValue('NORMAL');
      }
    });

    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.classifyForm.patchValue({
            medicalClass: data['medicalClass'],
            notes: data['notes'],
            classId: data['classID']
          });
        }
      }
    );

    this.classifyForm.get('notes').valueChanges.subscribe(data => {
      let value = data.toUpperCase();
      if (value == "NO") {
        this.classifyForm.get('notes').setValue("NORMAL");
      }
    });

  }

  getData() {
    // Transaction
    this.transactionService.getOneTrans('getTransaction/' + this.transactionId).subscribe(
      value => {
        this.patientId = value[0]['patientId'];
        this.transDatas = value[0];

        this.classifyForm.get('patientID').setValue(value[0]['patientId']);

        // Patient
        this.patientService.getOnePatient('getPatient/' + this.patientId).subscribe(
          data => {
            this.patientDatas = data[0];
          }
        );

        this.transactionService.getTransExt(this.transactionId).subscribe(
          data => {
            if (data[0].itemID !== null) {
              const itemID = data[0].itemID;
              this.itemService.getItemByID(itemID).subscribe(
                result => {
                  this.availed = result[0].itemName;
                  this.description = result[0].itemDescription;
                  this.transactionType = value[0].transactionType;
                }
              );
            }else {
              this.availed = data[0].packageName;
              this.transactionType = value[0].transactionType;
              this.itemService.getPackage("getPackageName/" + data[0].packageName).subscribe(
                data => {
                  if (data.length > 0) {
                    this.description = data[0].packageDescription;
                  } else {
                    this.description = "-";
                  }
                }
              );
              /* this.availed = '-';
              this.description = '-';
              this.transactionType = '-'; */
            }
          }
        );
      }
    );


    this.peService.getMedhis(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.medisDatas.push(
            { "label": "Astma", "value": data.asth },
            { "label": "Tuberculosis", "value": data.tb },
            { "label": "Diabetes Mellitus", "value": data.dia },
            { "label": "High Blood Pressure", "value": data.hp },
            { "label": "Heart Problem", "value": data.hp },
            { "label": "Kidney Problem", "value": data.kp },
            { "label": "Abdominal/Hernia", "value": data.ab },
            { "label": "Joint/Back/Scoliosis", "value": data.jbs },
            { "label": "Psychiatric Problem", "value": data.pp },
            { "label": "Migraine/Headache", "value": data.mh },
            { "label": "Fainting/Seizures", "value": data.fs },
            { "label": "Allergies", "value": data.alle },
            { "label": "Cancer/Tumor", "value": data.ct },
            { "label": "Hepatitis", "value": data.hep },
            { "label": "STD", "value": data.std }
          );

        }else {
          this.medisDatas.push(
            { "label": "Astma", "value": '-' },
            { "label": "Tuberculosis", "value": '-' },
            { "label": "Diabetes Mellitus", "value": '-' },
            { "label": "High Blood Pressure", "value": '-' },
            { "label": "Heart Problem", "value": '-' },
            { "label": "Kidney Problem", "value": '-' },
            { "label": "Abdominal/Hernia", "value": '-' },
            { "label": "Joint/Back/Scoliosis", "value": '-' },
            { "label": "Psychiatric Problem", "value": '-' },
            { "label": "Migraine/Headache", "value": '-' },
            { "label": "Fainting/Seizures", "value": '-' },
            { "label": "Allergies", "value": '-' },
            { "label": "Cancer/Tumor", "value": '-' },
            { "label": "Hepatitis", "value": '-' },
            { "label": "STD", "value": '-' }
          );
          this.classifyFormData = false;
        }
      }
    );
    this.peService.getVital(this.transactionId).subscribe(
      data => {
        this.vitalDatas = data;
        if (data !== null) {
          this.vitals.push(
            { "label": "Ishihara Test:", "value": data.cv },
            { "label": "Hearing", "value": data.hearing },
            { "label": "Hospitalization:", "value": data.hosp },
            { "label": "Operations:", "value": data.opr },
            { "label": "Medications:", "value": data.pm },
            { "label": "Smoker:", "value": data.smoker },
            { "label": "Alcoholic:", "value": data.ad },
            { "label": "Last Menstrual:", "value": data.lmp },
            { "label": "Others/Notes:", "value": data.notes }
          );

        }else {
          this.vitals.push(
            { "label": "Ishihara Test:", "value": '-' },
            { "label": "Hearing", "value": '-' },
            { "label": "Hospitalization:", "value": '-' },
            { "label": "Operations:", "value": '-' },
            { "label": "Medications:", "value": '-' },
            { "label": "Smoker:", "value": '-' },
            { "label": "Alcoholic:", "value": '-' },
            { "label": "Last Menstrual:", "value": '-' },
            { "label": "Others/Notes:", "value": '-' }
          );
          this.classifyFormData = false;
        }
      }
    );
    this.peService.getPE(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.pes.push(
            { "label": "Skin:", "value": data.skin },
            { "label": "Head and Neck:", "value": data.hn },
            { "label": "Chest/Breast/Lungs:", "value": data.cbl },
            { "label": "Cardiac/Heart:", "value": data.ch },
            { "label": "Abdomen:", "value": data.abdo },
            { "label": "Extremities:", "value": data.extre },
            { "label": "Others/Notes:", "value": data.ot }
          );

          this.findings = data.find;
          this.phy = data.doctor;
          this.phyLic = data.license;

        }else {
          this.pes.push(
            { "label": "Skin:", "value": '-' },
            { "label": "Head and Neck:", "value": '-' },
            { "label": "Chest/Breast/Lungs:", "value": '-' },
            { "label": "Cardiac/Heart:", "value": '-' },
            { "label": "Abdomen:", "value": '-' },
            { "label": "Extremities:", "value": '-' },
            { "label": "Others/Notes:", "value": '-' },
          );

          this.findings = '-';
          this.phy = '-';
          this.phyLic = '-';
          this.classifyFormData = false;
        }
      }
    );

    // Hematology
    this.labService.getHematology(this.transactionId).subscribe(
      data => {
        if (data.length > 0) {
          this.hemaDatas = data[0];
        } else {
          this.classifyFormData = false;
        }
      }
    );

    // Microscopy
    this.labService.getMicroscopy(this.transactionId).subscribe(
      data => {
        if (data.length !== 0) {
          this.microDatas = data[0];

          this.labService.getPersonnel(this.microDatas.medID).subscribe(
            data => {
              this.medical = data[0];
            }
          );
          this.labService.getPersonnel(this.microDatas.qualityID).subscribe(
            data => {
              this.quality = data[0];
            }
          );
          this.labService.getPersonnel(this.microDatas.pathID).subscribe(
            data => {
              this.path = data[0];
            }
          );
        } else {
          this.classifyFormData = false;
        }
      }
    );

    // Serology
    this.labService.getSerology(this.transactionId).subscribe(
      data => {
        if (data.length > 0) {
          this.seroDatas = data[0];
        } else {
          this.classifyFormData = false;
        }
      }
    );

    // Toxicology
    this.labService.getToxicology(this.transactionId).subscribe(
      data => {
        if (data.length > 0) {
          this.toxiDatas = data[0];

          if (this.toxiDatas.meth == "POSITIVE" || this.toxiDatas.tetra == "POSITIVE" || this.toxiDatas.drugtest == "POSITIVE") {
            this.classifications.push("CLASS D - Unemployable", "Pending - These are cases that are equivocal as to the classification and are being evaluated further. After a certain period of time these cases will be classified whether fit or unfit fot employment.");
          } else {
            this.classifications.push("CLASS A - Physically Fit", "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency", "CLASS C - With abnormal findings generally not accepted for employment.", "CLASS D - Unemployable", "Pending - These are cases that are equivocal as to the classification and are being evaluated further. After a certain period of time these cases will be classified whether fit or unfit fot employment.");
          }
        } else {
          this.classifyFormData = false;
          this.classifications.push("CLASS A - Physically Fit", "CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency", "CLASS C - With abnormal findings generally not accepted for employment.", "CLASS D - Unemployable", "Pending - These are cases that are equivocal as to the classification and are being evaluated further. After a certain period of time these cases will be classified whether fit or unfit fot employment.");
        }
      }
    );

    // Class
    this.labService.getOneClass(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.classification = data;

          if (this.classification.medicalClass == 'CLASS A - Physically Fit') {
            this.class = 'Class A - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS B - Physically Fit but with minor condition curable within a short period of time, that will not adversely affect the workers efficiency') {
            this.class = 'Class B - FIT TO WORK';
          }
          else if (this.classification.medicalClass == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.class = 'CLASS C - With abnormal findings generally not accepted for employment.';
          }
          else if (this.classification.medicalClass == 'CLASS D - Unemployable') {
            this.class = 'Class D - UNEMPLOYABLE';
          }
          else {
            this.class = 'PENDING';
          }

          if (this.classification.notes != 'NORMAL' && this.classification.notes != '') {
            this.notes = 'text-danger';
          }

          if (this.class == 'Class D - UNEMPLOYABLE' || this.class == 'CLASS C - With abnormal findings generally not accepted for employment.') {
            this.classColor = 'text-danger';
          }

        }else {
          this.classification = '-';
          this.class = 'PENDING';
          this.notes = 'text-dark';
          this.classColor = 'text-dark';
        }
      }
    );

    // Radiology
    this.XS.getOneXray(this.transactionId).subscribe(
      data => {
        if (data !== null) {
          this.rad = data;
        }else {
          this.rad = '';
          this.classifyFormData = false;
        }
      }
    );
  }

  classify() {
    this.classifyForm.get('userID').setValue(parseInt(sessionStorage.getItem('token')));

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Patient will be classified!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "ok") {
          this.labService.getOneClass(this.transactionId).subscribe(
            data => {
              if (data == null) {
                this.labService.addClass(this.classifyForm.value).subscribe(
                  data => {
                    if (data == 1) {
                      this.dialogRef.close("added");
                    }
                  }
                );

              }else {
                this.labService.updateClass(this.classifyForm.value).subscribe(
                  data => {
                    if (data == 1) {
                      this.dialogRef.close("updated");
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }

  classificationChange(value) {
    if (value == this.classificationDefault) {
      this.classifyForm.get('notes').setValue('NORMAL');
    }
  }

}
