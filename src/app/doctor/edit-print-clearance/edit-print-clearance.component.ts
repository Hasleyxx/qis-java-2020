import { Component, OnInit, Inject, ViewChildren, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';
import { MathService } from 'src/app/services/math.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { ClearanceHistoryComponent } from '../clearance-history/clearance-history.component';

@Component({
  selector: 'app-edit-print-clearance',
  templateUrl: './edit-print-clearance.component.html',
  styleUrls: ['./edit-print-clearance.component.scss']
})
export class EditPrintClearanceComponent implements OnInit {

  public patientId: any;
  public clearanceId: any;
  public patientData: any;
  public clearanceData: any;
  public peData: any;

  public pastMedhis: any[] = [];
  public ros: any[] = [];

  public dateNow: any;

  @ViewChildren('pastMedicalHistoryValue') pastMedicalHistoryValue: ElementRef[];
  @ViewChildren('peValue') peValue: ElementRef[];
  @ViewChildren('rOsValue') rOsValue: ElementRef[];

  @ViewChildren('allergiesValue') allergiesValue: ElementRef[];
  @ViewChildren('workUpValue') workUpValue: ElementRef[];
  @ViewChildren('prevSurgeryValue') prevSurgeryValue: ElementRef[];
  @ViewChildren('assesmentPlanValue') assesmentPlanValue: ElementRef[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private PS: PatientService,
    public math: MathService,
    private DS: DoctorService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EditPrintClearanceComponent>
  ) {
    this.patientId = this.dialogData.pid;
    this.clearanceId = this.dialogData.clearanceId;

    this.dateNow = this.math.dateNow();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.PS.getOnePatient('/getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });

    this.DS.getDocClearance(this.clearanceId).subscribe(clearanceData => {
      if (clearanceData !== null) {
        let tempPastMedHis = clearanceData.pastMedHis;
        let tempRos = clearanceData.rOs;

        this.clearanceData = clearanceData;

        if (tempPastMedHis == null || tempPastMedHis == undefined || tempPastMedHis == "") {
          this.pastMedhis.push("-");
        } else {
          this.pastMedhis = tempPastMedHis.split("|");
        }

        if (tempRos == null || tempRos == undefined || tempRos == "") {
          this.ros.push("-");
        } else {
          this.pastMedhis = tempRos.split("|");
        }

        this.DS.getDocPe(clearanceData.dataRef).subscribe(peData => {
          if (peData !== null) {
            this.peData = peData;
          }
        });
      }
    });

  }

  print() {
    let tempPastMedHis: any[] = [];
    let tempPe: any[] = [];
    let tempRos: any[] = [];

    let tempAllergies: any;
    let tempWorkUp: any;
    let tempPrevSurgery: any;
    let tempAssessmentPlan: any;

    this.pastMedicalHistoryValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempPastMedHis.push(value);
      }
    });

    this.peValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempPe.push(value);
      }
    });

    this.rOsValue.forEach(element => {
      let value = element.nativeElement.innerText;

      if (value !== "") {
        tempRos.push(value);
      }
    });

    this.allergiesValue.forEach(element => {
      tempAllergies = element.nativeElement.innerText;
    });
    this.workUpValue.forEach(element => {
      tempWorkUp = element.nativeElement.innerText;
    });
    this.prevSurgeryValue.forEach(element => {
      tempPrevSurgery = element.nativeElement.innerText;
    });
    this.assesmentPlanValue.forEach(element => {
      tempAssessmentPlan = element.nativeElement.innerText;
    });

    sessionStorage.setItem("pastMedHis", JSON.stringify(tempPastMedHis));
    sessionStorage.setItem("pe", JSON.stringify(tempPe));
    sessionStorage.setItem("ros", JSON.stringify(tempRos));

    sessionStorage.setItem("allergies", tempAllergies);
    sessionStorage.setItem("workUp", tempWorkUp);
    sessionStorage.setItem("prevSurgery", tempPrevSurgery);
    sessionStorage.setItem("assessmentPlan", tempAssessmentPlan);
    
    this.dialogRef.close();
    const suffix = [this.patientId, 'clearance'];
    this.math.printDoc('', suffix);
  }

  cancel() {
    this.dialogRef.close();

    this.dialog.open(ClearanceHistoryComponent, {
      data: {
        pid: this.patientId
      },
      disableClose: true
    });
  }
}
