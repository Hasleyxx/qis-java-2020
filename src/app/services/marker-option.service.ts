import { Injectable } from '@angular/core';
import { Global } from '../global.variable';
import { ErrorService } from './error.service';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MarkerOptionService {

  constructor(
    private httpClient: HttpClient,
    private global: Global,
    private ehs: ErrorService,
  ) { }

  addMarker(data: any): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addMarkerOption', data)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getMarkers(type): Observable<any[]> {
    return this.httpClient.get<any[]>(this.global.url + '/getMarkerOptions/' + type)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getFilmSize(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getFilmSize')
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getRadtech(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getRadtech')
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getBodypart(): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/getBodypart')
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  activateMarkerOption(id): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/activateMarkerOption/' + id)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  deactivateMarkerOption(id): Observable<any> {
    return this.httpClient.get<any>(this.global.url + '/deactivateMarkerOption/' + id)
    .pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }
}
