import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'blank-page-print',
  templateUrl: './blank-page-print.component.html',
  styleUrls: ['./blank-page-print.component.scss']
})
export class BlankPagePrintComponent implements OnInit {

  data: string[];
  dataDetails: Promise<any>[];

  public patientData: any;
  public patientId: any;

  public value: string = "";

  constructor(
    private PS: PatientService,
    public math: MathService,
    route: ActivatedRoute,
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.patientId = this.data[0];
    this.value = sessionStorage.getItem("value");

    sessionStorage.removeItem("value");
  }

  ngOnInit() {
    this.dataDetails = this.data.map(data => this.getDocBlankReady(data));

    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });

    Promise.all(this.dataDetails).then(() => {
      // this.math.onDocBlankReady();
    });
  }

  getDocBlankReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }
}
