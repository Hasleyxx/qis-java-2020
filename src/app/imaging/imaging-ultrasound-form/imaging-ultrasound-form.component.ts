import { Component, OnInit, Inject } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';
import * as moment from 'moment';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { transaction } from 'src/app/services/service.interface';
import { ItemService } from 'src/app/services/item.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-imaging-ultrasound-form',
  templateUrl: './imaging-ultrasound-form.component.html',
  styleUrls: ['./imaging-ultrasound-form.component.scss']
})
export class ImagingUltrasoundFormComponent implements OnInit {

  public update: boolean = false;
  public procedures: any;
  public procedureValue = "-";

  public personels = ["Arby Kaevilette G. Bolibol, RRT", "Judy R. Desaliza "];
  public personelValue = 'Arby Kaevilette G. Bolibol, RRT';

  public sonologist = ["SALVADOR R. RAMIREZ, MD, DPBR"];
  public sonologistValue = 'SALVADOR R. RAMIREZ, MD, DPBR';

  public ultraForm: FormGroup;
  id: any;
  transaction: transaction;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private IS: ItemService,
    private XS: XrayService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.math.navSubs("imaging")
    this.id = this.data.tid;
    if (isNaN(this.id)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {
    this.ultraForm = this.formBuilder.group({
      procedure: ['', Validators.required],
      uterus: ['anteverted'],
      hr: [''],
      crl: [''],
      week: [''],
      day: [''],
      datee: [''],
      iweek: [''],
      iday: [''],
      fets: [''],
      pres: [''],
      bpd: [''],
      bpdwd: [''],
      hc: [''],
      hcwd: [''],
      ac: [''],
      acwd: [''],
      fl: [''],
      flwd: [''],
      est: [''],
      loc: [''],
      igen: ['MALE'],
      head: [''],
      body: [''],
      tail: [''],
      span: [''],
      rk: [''],
      lk: [''],
      prevv: [''],
      postvv: [''],
      vw: [''],
      intd: [''],
      cdt: [''],
      utem: [''],
      echog: [''],
      rovary: [''],
      lovary: [''],
      iuterus: ['RETROVERTED'],
      qaf: [''],
      bpwd: [''],
      qualityID: ['', Validators.required],
      sonoID: ['', Validators.required]
    });

    this.TS.getOneTrans("getTransaction/" + this.id).subscribe(
      data => {
        if(data[0].length == 0){
          this.router.navigate(['error/404']);
        }else{
          this.transaction = data[0];

          this.TS.getTransExt(this.id).subscribe(
            transExt => {
              if (transExt.length > 0) {
                transExt.forEach((ext) => {
                  if (ext.itemID) {
                    this.IS.getItemByID(ext.itemID).subscribe(item => {
                      const itemPack = item[0];
                      let procedureList;
                      if (itemPack.itemId == "1061") {
                        procedureList = [
                          { "value": "PELVIC ULTRASOUND", "label": "OB EARLY PELVIC HEMORRHAGE" },
                          { "value": "OB EARLY PREVIA PELVIC", "label": "OB EARLY PREVIA PELVIC" },
                          { "value": "PELVIC ULTRASOUND001", "label": "PELVIC ULTRASOUND001" },
                          { "value": "PELVIC ULTRASOUND002", "label": "PELVIC ULTRASOUND002" },
                          { "value": "PELVIC ULTRASOUND003", "label": "PELVIC ULTRASOUND003" },
                          { "value": "PELVIC ULTRASOUND004", "label": "PELVIC ULTRASOUND004" },
                        ];
                      } else if (itemPack.itemId == "1070") {
                        procedureList = [
                          { "value": "PELVIC ULTRASOUND WITH BIOPHYSICAL SCORE", "label": "PELVIC ULTRASOUND WITH BIOPHYSICAL SCORE" }
                        ];
                      } else if (itemPack.itemId == "") {
                        procedureList = [
                          { "value": "TVS", "label": "TVS" },
                          { "value": "TVS ANTE obscured", "label": "TVS ANTE obscured" },
                          { "value": "TVS COMPLETE ABORTION RETRO", "label": "TVS COMPLETE ABORTION RETRO" },
                          { "value": "TVS COMPLETE ABORTION", "label": "TVS COMPLETE ABORTION" },
                          { "value": "TVS cyst LEFT", "label": "TVS cyst LEFT" },
                          { "value": "TVS ES FLUID CYST  CERVIX obscured", "label": "TVS ES FLUID CYST  CERVIX obscured" },
                          { "value": "TVS THICKENED MYOMA cyst large mid", "label": "TVS THICKENED MYOMA cyst large mid" },
                          { "value": "TVS ES FLUID RETRO obscured", "label": "TVS ES FLUID RETRO obscured" },
                          { "value": "TVS MYOMA ovarian cyst", "label": "TVS MYOMA ovarian cyst" },
                          { "value": "TVS THICKENED MYOMA SUBMUCOUS", "label": "TVS THICKENED MYOMA SUBMUCOUS" },
                          { "value": "TVS CUL DE SAC FLUID CYST LEFT", "label": "TVS CUL DE SAC FLUID CYST LEFT" },
                          { "value": "TVS THICKENED fluid cul de sac obscured", "label": "TVS THICKENED fluid cul de sac obscured" },
                          { "value": "TRANSVAGINAL ULTRASOUND", "label": "TVS THICKENED ovarian cyst RT" }
                        ];
                      } else if (itemPack.itemId == "1057") {
                        procedureList = [
                          { "value": "WHOLE ABDOMEN ULTRASOUND", "label": "WHOLE ADBDOMEN absent GB fatty liver" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND001", "label": "WHOLE ADBDOMEN absent GB hydroneph RT" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND002", "label": "WHOLE ADBDOMEN absent GB" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND003", "label": "WHOLE ADBDOMEN CIRRHOSIS KIDNEY" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND004", "label": "WHOLE ADBDOMEN LIVER CIRRHOSIS" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND005", "label": "WHOLE ADBDOMEN LIVER CONGESTION" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND006", "label": "WHOLE ADBDOMEN MALE enlarged cirrhosis kidney stone" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND007", "label": "WHOLE ADBDOMEN MALE enlarged cirrhosis splenomegaly" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND008", "label": "WHOLE ADBDOMEN MALE  cirrhosis choli" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND009", "label": "WHOLE ADBDOMEN fatty liver" },
                          { "value": "WHOLE ABDOMEN ULTRASOUND010", "label": "WHOLE ADBDOMEN fatty liver GB polyp" },
                        ];
                      } else {
                        procedureList = [];
                      }
                      this.procedures = procedureList;
                    });
                  }
                  // For testing
                  // this.procedures = [{ "value": "WHOLE ABDOMEN ULTRASOUND006", "label": "WHOLE ABDOMEN ULTRASOUND006" }];
                });
              }
            }
          )
        }
      }
    )

  }

  procedure(value: any) {
    this.procedureValue = value;

    if (value == "PELVIC ULTRASOUND") {

    }
  }

  personel(value: any) {
    this.personelValue = value;
  }

  sonology(value: any) {
    this.sonologistValue = value;
  }

  ultraSubmit() {

  }
}
