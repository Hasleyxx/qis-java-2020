import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicTwoSummaryComponent } from './basic-two-summary.component';

describe('BasicTwoSummaryComponent', () => {
  let component: BasicTwoSummaryComponent;
  let fixture: ComponentFixture<BasicTwoSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicTwoSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicTwoSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
