import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './services/error.service';
import { Global } from './global.variable';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { password, validity } from './services/service.interface';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor(
    private httpClient: HttpClient,
    private ehs: ErrorService,
    private global: Global
  ) { }

  getPassword(): Observable<password> {
    return this.httpClient.get<password>(this.global.url + "/getPassword").pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addPassword(data: password): Observable<password>{
    return this.httpClient.post<password>(this.global.url + "/addPassword", data).pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  addValidity(data): Observable<validity> {
    return this.httpClient.post<validity>(this.global.url + "/addValidity", data).pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  getValidity(): Observable<validity> {
    return this.httpClient.get<validity>(this.global.url + "/getValidity").pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }

  updateValidity(data: validity): Observable<validity> {
    return this.httpClient.post<validity>(this.global.url + "/updateValidity", data).pipe(
      retry(1),
      catchError(this.ehs.handleError)
    );
  }
}
