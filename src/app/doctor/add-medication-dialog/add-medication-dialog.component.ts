import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MathService } from 'src/app/services/math.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DoctorService } from 'src/app/services/doctor.service';

@Component({
  selector: 'app-add-medication-dialog',
  templateUrl: './add-medication-dialog.component.html',
  styleUrls: ['./add-medication-dialog.component.scss']
})
export class AddMedicationDialogComponent implements OnInit {

  public datas: any;
  public patientId: any;
  public addMedicationForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialogRef: MatDialogRef<AddMedicationDialogComponent>,
    private math: MathService,
    private formBuilder: FormBuilder,
    private doctor: DoctorService
  ) {
    this.datas = this.data.data;
    this.patientId = this.data.pid;
  }

  ngOnInit() {
    this.addMedicationForm = this.formBuilder.group({
      patientID: [this.patientId],
      genericName: [this.datas.generic],
      brandName: [this.datas.medicine],
      quantity: ['', Validators.required],
      dosage: [this.datas.dosage],
      sched: ['', Validators.required],
      duration: ['', Validators.required],
      notes: [''],
      dateCreated: ['']
    });
  }

  addMedication() {
    this.addMedicationForm.get('dateCreated').setValue(
      moment().format("YYYY-MM-DD h:mm:ss")
    );

    this.doctor.addDocMed(this.addMedicationForm.value).subscribe(
      result => {
        if (result == 1) {
          this.dialogRef.close("added");
        } else {
          this.dialogRef.close("error");
        }
      }
    );
  }

}
