import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicTwoComponent } from './basic-two.component';

describe('BasicTwoComponent', () => {
  let component: BasicTwoComponent;
  let fixture: ComponentFixture<BasicTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
