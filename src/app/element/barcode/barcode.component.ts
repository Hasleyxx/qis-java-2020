import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MathService } from "src/app/services/math.service";
import { TransactionService } from 'src/app/services/transaction.service';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: "app-barcode",
  templateUrl: "./barcode.component.html",
  styleUrls: ["./barcode.component.scss"]
})
export class BarcodeComponent implements OnInit {

  data: string[];
  tid: any = "";
  pid: any = "";
  dataDetails: Promise<any>[];
  patientName: any;

  constructor(
    route: ActivatedRoute,
    private math: MathService,
    private TS: TransactionService,
    private PS: PatientService
  ) {
    this.data = route.snapshot.params["data"].split(",");
    this.tid = this.data[0];
  }

  ngOnInit() {
    this.dataDetails = this.data.map(data => this.getBarcodeReady(data));
    this.TS.getOneTrans("getTransaction/" + this.tid).subscribe(trans => {

      this.PS.getOnePatient("getPatient/" + trans[0]['patientId']).subscribe(patient => {
        this.patientName = patient[0]['fullName'];
      });
    });

    Promise.all(this.dataDetails).then(() => {
      setTimeout(() => {
        this.math.onBarcodeReady();
      });
    });
  }

  getBarcodeReady(data) {
    const amount = Math.floor(Math.random() * 100);
    return new Promise(resolve => setTimeout(() => resolve({ amount }), 1000));
  }
}
