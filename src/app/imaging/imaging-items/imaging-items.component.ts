import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar, MatDialog } from '@angular/material';
import { MarkerOptionService } from 'src/app/services/marker-option.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-imaging-items',
  templateUrl: './imaging-items.component.html',
  styleUrls: ['./imaging-items.component.scss']
})
export class ImagingItemsComponent implements OnInit {

  public tables = ["Film Size", "Bodypart", "RadTech"];
  public filterTable = this.tables[0];

  public dataSource: any;
  public displayedColumns = ['name', 'type', 'action'];

  public addMarkerForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private MOS: MarkerOptionService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getMarkers(this.filterTable);
    this.addMarkerForm = this.formBuilder.group({
      type: ['Film Size', Validators.required],
      name: ['', Validators.required]
    });
  }

  table(value: any) {
    this.filterTable = value;
  }

  getMarkers(type) {
    this.MOS.getMarkers(type).subscribe(
      data => {
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  filterItems(value) {
    this.getMarkers(value);
    this.filterTable = value
  }

  addMarker() {
    this.MOS.addMarker(this.addMarkerForm.value).subscribe(
      response => {
        if (response == 1) {
          this.openSnackBar("Item has been added!", "close");
        }
      }
    );
  }

  refreshItems() {
    this.filterItems(this.filterTable);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  activate(id) {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Marker will be activated!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'ok') {
        this.MOS.activateMarkerOption(id).subscribe(
          response => {
            if (response == 1) {
              this.openSnackBar("Marker option has been activated!", "close");
            }
            this.refreshItems();
          }
        );
      }
    });
  }

  deactivate(id) {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Marker will be deactivated!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'ok') {
        this.MOS.deactivateMarkerOption(id).subscribe(
          response => {
            if (response == 1) {
              this.openSnackBar("Marker option has been deactivated!", "close");
            }
            this.refreshItems();
          }
        );
      }
    });
  }

}
