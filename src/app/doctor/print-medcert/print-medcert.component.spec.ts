import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintMedcertComponent } from './print-medcert.component';

describe('PrintMedcertComponent', () => {
  let component: PrintMedcertComponent;
  let fixture: ComponentFixture<PrintMedcertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintMedcertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintMedcertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
