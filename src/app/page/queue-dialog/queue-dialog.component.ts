import { Component, OnInit, Inject, ViewChild, ElementRef, QueryList } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef } from '@angular/material';
import { MathService } from 'src/app/services/math.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { PatientService } from 'src/app/services/patient.service';
import { PeService } from 'src/app/services/pe.service';
import { XrayService } from 'src/app/services/xray.service';
import { transRef, heldTable, itemList } from 'src/app/services/service.interface';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { ItemService } from 'src/app/services/item.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { PeAddComponent } from 'src/app/pe/pe-add/pe-add.component';
import { MarkerOptionService } from 'src/app/services/marker-option.service';
import { ImagingItemsComponent } from 'src/app/imaging/imaging-items/imaging-items.component';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { Router } from '@angular/router';

@Component({
  selector: "app-queue-dialog",
  templateUrl: "./queue-dialog.component.html",
  styleUrls: ["./queue-dialog.component.scss"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class QueueDialogComponent implements OnInit {
  showLoading: boolean = true;
  public title: string = "";
  public url: string = "";
  public type: string = "";
  pes: any[] = [];
  xrays: any[] = [];
  phlebs: any[] = [];

  displayedColumns: string[] = [
    "id",
    "date",
    "type",
    "patient",
    "biller",
    "action"
  ];
  dataSource: MatTableDataSource<heldTable>;
  heldData: heldTable[] = [];
  expandedElement: heldTable | null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public markers = "markers";
  public markerOptions = [];

  public filmSizes: Array<any>[];
  public radTechs: Array<any>[];
  public bodyParts: Array<any>[];

  public filmSizeValue: any[];
  public radTechValue: any[];
  public bodyPartValue: any[];

  @ViewChild("filmSizeSelect") filmSizeSelect: ElementRef;
  @ViewChild("radTechSelect") radTechSelect: ElementRef;
  @ViewChild("bodyPartSelect") bodyPartSelect: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private math: MathService,
    private TS: TransactionService,
    private PS: PatientService,
    private PES: PeService,
    private XS: XrayService,
    private LS: LaboratoryService,
    private IS: ItemService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<QueueDialogComponent>,
    private MOS: MarkerOptionService,
    private router: Router
  ) {
    this.url = this.dialogData.url;
    this.type = this.dialogData.type;
    if (this.dialogData.type == "pe") {
      this.title = "List of Patient for PE";
    } else if (this.dialogData.type == "xray") {
      this.title = "List of Patient for X-RAY";
    } else {
      this.title = "List of Patient for Extraction";
    }
  }

  ngOnInit() {
    this.getData();
    if (this.type == "xray") {
      this.getMarkerOptions();
    }

    setInterval(() => {
      if (this.type == "all") {
        this.getData();
      }
    }, 10000);
  }

  getData() {
    if (this.type !== "all") {
      this.showLoading = true;
    }
    this.TS.getTransactions(this.url).subscribe(data => {
      this.heldData = [];
      data.forEach(trans => {
        let color = "black";
        let transData: heldTable = {
          id: trans.transactionId,
          patient: undefined,
          patInfo: undefined,
          items: [],
          type: trans.transactionType,
          date: trans.transactionDate,
          biller: trans.biller,
          action: "",
          color: color,
          marker: "success",
          patientID: "",
          peStatus: "without",
          nurseStatus: "without",
          xrayStatus: "without",
          xrayFilm: "",
          impression: "",
          radiologist: "",
          qualityassurance: "",
          industrial: "false",
          classification: "",
          classificationStatus: "",
          remarks: "",
          released_status: trans.released_status
        };

        this.PS.getOnePatient("getPatient/" + trans.patientId).subscribe(
          pat => {
            transData.patient =
              pat[0].lastName +
              ", " +
              pat[0].firstName +
              " " +
              pat[0].middleName;
            transData.patInfo = pat[0];
            transData.patientID = pat[0].patientID;
          }
        );

        this.TS.getTransExt(trans.transactionId).subscribe(transExt => {
          if (transExt.length > 0) {
            transExt.forEach((ext, index) => {
              if (ext.packageName != null) {
                this.IS.getPack("getPackageName/" + ext.packageName).subscribe(
                  pack => {
                    let packItem: itemList = {
                      itemId: pack[0].packageName,
                      itemName: pack[0].packageName,
                      itemPrice: pack[0].packagePrice,
                      itemDescription: pack[0].packageDescription,
                      itemType: pack[0].packageType,
                      deletedItem: pack[0].deletedPackage,
                      neededTest: undefined,
                      creationDate: pack[0].creationDate,
                      dateUpdate: pack[0].dateUpdate
                    };
                    transData.items.push(packItem);
                  }
                );
              } else if (ext.itemID) {
                this.IS.getItemByID(ext.itemID).subscribe(item => {
                  transData.items.push(item[0]);
                });
              }
              if (transExt.length - 1 == index) {
                this.showLoading = false;
              }
            });
          } else {
            this.showLoading = false;
          }
        });

        if (trans.transactionType !== "APE") {
          this.TS.getTransRef(trans.transactionId).subscribe(ref => {
            let refData: transRef = ref[0];

            if (this.type == "pe") {
              if (refData.physicalExam == 1) {
                this.PES.getMedhis(trans.transactionId).subscribe(pes => {
                  if (pes == null) {
                    this.heldData.push(transData);
                    this.dataSource = new MatTableDataSource(this.heldData);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                  }
                });
              }
            } else if (this.type == "xray") {
              if (refData.xray == 1) {
                this.XS.getMarker(trans.transactionId).subscribe(xray => {
                  if (xray == "") {
                    this.heldData.push(transData);
                    this.dataSource = new MatTableDataSource(this.heldData);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                  }
                });
              }
            } else if (this.type == "phleb") {
              if (refData.blood == 1) {
                if (trans["have_blood"] == 0) {
                  this.LS.getHematology(trans.transactionId).subscribe(hema => {
                    if (hema.length == 0) {
                      this.LS.getChemistry(trans.transactionId).subscribe(
                        chem => {
                          if (chem.length == 0) {
                            this.heldData.push(transData);
                            this.dataSource = new MatTableDataSource(
                              this.heldData
                            );
                          }
                        }
                      );
                    } else {
                      this.heldData.push(transData);
                      this.dataSource = new MatTableDataSource(this.heldData);
                      this.dataSource.paginator = this.paginator;
                      this.dataSource.sort = this.sort;
                    }
                  });
                }
              }
            } else {
              this.pes = [];
              this.xrays = [];
              this.phlebs = [];
              // List of Queues
              if (refData.physicalExam == 1) {
                this.PES.getMedhis(trans.transactionId).subscribe(pes => {
                  if (pes == null) {
                    let tempData = {
                      name: transData.patient
                    };

                    this.pes.push(tempData);
                  }
                });
              }

              if (refData.xray == 1) {
                this.XS.getMarker(trans.transactionId).subscribe(xray => {
                  if (xray == "") {
                    let tempData = {
                      name: transData.patient
                    };

                    this.xrays.push(tempData);
                  }
                });
              }

              if (refData.blood == 1) {
                if (trans["have_blood"] == 0) {
                  this.LS.getHematology(trans.transactionId).subscribe(hema => {
                    if (hema.length == 0) {
                      this.LS.getChemistry(trans.transactionId).subscribe(
                        chem => {
                          if (chem.length == 0) {
                            let tempData = {
                              name: transData.patient
                            };

                            this.phlebs.push(tempData);
                          }
                        }
                      );
                    } else {
                      let tempData = {
                        name: transData.patient
                      };

                      this.phlebs.push(tempData);
                    }
                  });
                }
              }
            }
          });
        }
      });

      this.showLoading = false;
    });
  }

  refresh() {
    this.getData();
  }

  addPe(tid) {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(PeAddComponent, {
      data: {
        tid: tid,
        type: this.type
      }
    });

    window.addEventListener("afterprint", event => {
      if (this.router.url == "/queue") {
        dialogRef.afterClosed().subscribe(data => {
          this.dialog.open(QueueDialogComponent, {
            data: {
              url: this.url,
              type: data
            },
            width: '95%'
          });
        });
      }
    });
  }

  manageMarkers() {
    const dialogRef = this.dialog.open(ImagingItemsComponent, {
      width: "120vh"
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getMarkerOptions();
    });
  }

  getMarkerOptions() {
    // Film sizes
    this.MOS.getFilmSize().subscribe(data => {
      this.filmSizes = data;
      this.filmSizeValue = data[0].name;
    });

    // Radtech
    this.MOS.getRadtech().subscribe(data => {
      this.radTechs = data;
      this.radTechValue = data[0].name;
    });

    // Bodyparts
    this.MOS.getBodypart().subscribe(data => {
      this.bodyParts = data;
      this.bodyPartValue = data[0].name;
    });
  }

  printMarker(tid: any) {
    const confirmDialog = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Marker will be saved!"
      },
      width: "20%"
    });

    confirmDialog.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.dialogRef.close();

        const filmSize = this.filmSizeSelect["value"];
        const radTech = this.radTechSelect["value"];
        const bodyPart = this.bodyPartSelect["value"];
        const data = [filmSize, radTech, bodyPart, tid];

        this.math.printMarker("", data);
        window.addEventListener("afterprint", event => {
          if (this.router.url == "/queue") {
            const dialog = this.dialog.open(QueueDialogComponent, {
              data: {
                type: "xray",
                url: this.url
              },
              width: "95%"
            });
          }
        });
      }
    });
  }

  printBarcode(tid: any) {
    const confirmDialog = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content:
          "Barcode will be printed and this patient will be marked as 'Extracted'"
      },
      width: "20%"
    });

    confirmDialog.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.dialogRef.close();
        const suffix = [tid];

        this.math.printBarcode("", suffix);
        window.addEventListener("afterprint", event => {
          let data = {
            transactionId: tid,
            type: "have_blood",
            status: 1
          };
          this.TS.updateChecklist(data).subscribe(result => {
            if (this.router.url == "/queue") {
              const dialog = this.dialog.open(QueueDialogComponent, {
                data: {
                  type: "phleb",
                  url: this.url
                },
                width: "95%"
              });
            }
          });
        });
      }
    });
  }
}
