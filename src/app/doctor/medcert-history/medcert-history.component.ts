import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import { PatientService } from "src/app/services/patient.service";
import { MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatPaginator, MatSort, MatDialogRef } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { DoctorService } from 'src/app/services/doctor.service';
import * as moment from 'moment';
import { MedcertDialogComponent } from '../medcert-dialog/medcert-dialog.component';
import { MathService } from 'src/app/services/math.service';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';
import { EditPrintMedcertComponent } from '../edit-print-medcert/edit-print-medcert.component';

@Component({
  selector: 'app-medcert-history',
  templateUrl: './medcert-history.component.html',
  styleUrls: ['./medcert-history.component.scss']
})
export class MedcertHistoryComponent implements OnInit {

  displayedColumns: string[] = ['date', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  heldData: any[] = [];

  public patientId: any;
  public type: any;

  public patientDatas: any = [];
  public availed: any;
  public description: any;
  public transactionType: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private math: MathService,
    private PS: PatientService,
    private doctor: DoctorService,
    public state: ActivatedRoute,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<MedcertHistoryComponent>
  ) {
    this.patientId = this.data.pid;
    if (this.data.status) {
      this.type = this.data.status;
    }
  }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData() {
    // Patient
    this.PS.getOnePatient("getPatient/" + this.patientId).subscribe(data => {
      this.patientDatas = data[0];
    });

    // Medcert
    this.doctor.getPidDocMedcert(this.patientId).subscribe(
      data => {
        if (data.length > 0) {
          this.heldData = [];
          data.forEach(element => {
            let transData: any = {
              date: moment(element['dateCreated']).format("LLL"),
              id: element['docMedcertID']
            }
            this.heldData.push(transData);
          });
          this.dataSource = new MatTableDataSource(this.heldData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    )
  }

  addMedcert(pid) {
    const dialogRef = this.dialog.open(MedcertDialogComponent, {
      data: {
        pid: pid
      }
    });
  }

  printMedcert(id) {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(EditPrintMedcertComponent, {
      data: {
        pid: this.patientId,
        medcertId: id
      },
      width: '70%'
    })
    /* sessionStorage.setItem("medcertId", id);
    const suffix = [this.patientId, 'medcert'];
    this.math.printDoc('', suffix); */
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId,
      }
    });
  }

}
