import { Component, OnInit, ViewChild, VERSION } from '@angular/core';
import { patient, itemList, transaction } from 'src/app/services/service.interface';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { DatePipe } from '@angular/common';
import { FormControl, Validators } from '@angular/forms';
import { TransactionService } from 'src/app/services/transaction.service';
import { ItemService } from 'src/app/services/item.service';
import { PatientService } from 'src/app/services/patient.service';
import { MathService } from 'src/app/services/math.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { UserService } from 'src/app/services/user.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import { MarkersComponent } from 'src/app/imaging/markers/markers.component';



export interface trans_items{
  itemName        : string,
  itemPrice       : number,
  itemDescription : string,
  qty             : number,
  subtotal        : any,
  disc            : number
}
export interface heldTable{
  id      : number,
  trans   : transaction,
  patInfo : patient,
  patient : string,
  items   : trans_items[],
  date    : any,
  type    : string,
  biller  : string,
  action  : any,
  cashier : any,
  color   : string
}
export interface Generate{
  value: string;
  viewValue: string;
}


@Component({
  selector: 'marker-list',
  templateUrl: './marker-list.component.html',
  styleUrls: ['./marker-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class MarkerListComponent implements OnInit {
 
  displayedColumns: string[] = ['id', 'date', 'patient', 'action'];
  dataSource: MatTableDataSource<heldTable>;
  heldData: heldTable[] = [];
  expandedElement: heldTable | null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort; 

  showLoading: boolean = true;
  d = new DatePipe('en-US');
  months: Array<any> = [
    { value: "01", name: "January" },
    { value: "02", name: "February" },
    { value: "03", name: "March" },
    { value: "04", name: "April" },
    { value: "05", name: "May" },
    { value: "06", name: "June" },
    { value: "07", name: "July" },
    { value: "08", name: "August" },
    { value: "09", name: "September" },
    { value: "10", name: "October" },
    { value: "11", name: "November" },
    { value: "12", name: "December" }
  ]

  years: Array<any> = ["2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025"];

  monthVal: FormControl = new FormControl(this.d.transform(new Date(), "MM"));
  yearVal: FormControl = new FormControl(this.d.transform(new Date(), "yyyy"));
  // myFilter = (d: Date): boolean => {
  //   const day = d.getDay();
  //   return day !== 10;
  // }

  

  from : FormControl = new FormControl(this.d.transform(new Date(),"yyyy-MM-dd 06:00:00"));
  to : FormControl = new FormControl(this.d.transform(new Date(),"yyyy-MM-dd 20:00:00"));
  generateFile: FormControl = new FormControl("", [Validators.required]);
  transType: any;
  listType: any;
  // myFilter = (d: Date): boolean => {
  //   const day = d.getDay();
  //   return day !== 10;
  // }

 
  constructor (private TS    : TransactionService,
    private PS    : PatientService,
    private IS    : ItemService,
    private math  : MathService,  
    public dialog : MatDialog,
    private _snackBar : MatSnackBar,
    private user : UserService) { this.math.navSubs("imaging"); }


 openDialog() {

        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        this.dialog.open(MarkersComponent, dialogConfig);
    }


    ngOnInit(){
      this.setData();
      if (!this.listType) {;
        this.listType = "transactions";
      }
    }

    applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
  
    setData() {
      this.showLoading = true;
      let url: any;
      if (this.transType == undefined) {
        url = "getTransactionDate/" +
          this.yearVal.value + "-" + this.monthVal.value + "-01/" +
          this.yearVal.value + "-" + this.monthVal.value + "-31";
      } else if (this.transType == "billing" || this.transType == "accounting") {
        url = "getTransBillingDate/" +
          this.yearVal.value + "-" + this.monthVal.value + "-01/" +
          this.yearVal.value + "-" + this.monthVal.value + "-31";
      }
      else {
        url = "getTransTypeDate/" + this.transType + "/" +
          this.yearVal.value + "-" + this.monthVal.value + "-01/" +
          this.yearVal.value + "-" + this.monthVal.value + "-31";
      }
      
      this.TS.getTransactions(url)
     .subscribe(
       data => {
        
        this.heldData = [];
  
        if(data.length > 0){
        data.forEach(trans => {
          let color = "black";
          if(trans.salesType == "refund"){
            color = "red";
          }
          let transData : heldTable = {
            id      : trans.transactionId,
            trans   : trans,
            patient : undefined,
            patInfo : undefined,
            items   : [],
            type    : trans.transactionType,
            date    : trans.transactionDate,
            biller  : trans.biller,
            action  : "",
            cashier : "",
            color   : color
          }
          this.user.getUser(trans.userId)
          .subscribe(user => {
            transData.cashier = user[0].userName;
          })
          this.PS.getOnePatient("getPatient/" + trans.patientId)
          .subscribe( pat => {
            transData.patient = pat[0].fullName;
            transData.patInfo = pat[0];
          });
          this.TS.getTransExt(trans.transactionId)
          .subscribe(
            transExt => {
              if(transExt.length > 0){
                transExt.forEach((ext, index) => {
                if(ext.packageName != null){
                  this.IS.getPack("getPackageName/" + ext.packageName)
                  .subscribe(
                    pack => {
                      let packItem : trans_items = {
                        itemName        : pack[0].packageName,
                        itemPrice       : pack[0].packagePrice,
                        itemDescription : pack[0].packageDescription,
                        qty             : ext.itemQTY,
                        disc            : ext.itemDisc,
                        subtotal        : ""
                      }
                      packItem.subtotal = this.math.computeDisc(
                          pack[0]. packagePrice, ext.itemDisc, ext.itemQTY
                        );
                      transData.items.push(packItem); 
                    }
                  )
                }else if(ext.itemID){
                    this.IS.getItemByID(ext.itemID)
                    .subscribe( item => {
                      let Item : trans_items = {
                        itemName        : item[0].itemName,
                        itemPrice       : item[0].itemPrice,
                        itemDescription : item[0].itemDescription,
                        qty             : ext.itemQTY,
                        disc            : ext.itemDisc,
                        subtotal        : ""
                      }
                      Item.subtotal = this.math.computeDisc(
                          item[0].itemPrice, ext.itemDisc, ext.itemQTY
                        );
                      transData.items.push(Item);                    
                    });
                }
                if(transExt.length - 1 == index ){
                   this.showLoading = false;
                }
              });
              }
              else{
                this.showLoading = false;
              }
            }
          )
          this.heldData.push(transData);
        });
        this.dataSource = new MatTableDataSource(this.heldData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        }else{
          this.showLoading = false;
          this.dataSource = new MatTableDataSource([]);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        }
     ) 
     
    }
  
    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 3000,
      });
    }
  
    
    generate(){
     
      (this.generateFile.value == "sr")

        const suffix = [this.from.value, this.to.value];
  
        this.math.printLab('', suffix);
      }
    }