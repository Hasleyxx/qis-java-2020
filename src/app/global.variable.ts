import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class Global {
  // url: string = "http://DESKTOP-JFA506B:8088";
  // url: string = "http://questsite:8088";
  // url: string = "http://JAVADEV1:8088";
  url: string = "http://MichaelMamaclay:8088";
  // url: string = "http://desktop-i3pua41:8088";
  userID: number = parseInt(sessionStorage.getItem("token"));
  public httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "qwelqwjhelkqwjhe9qio9821iuidfksajh87dasj"
    })
  };
  constructor() {}
}
