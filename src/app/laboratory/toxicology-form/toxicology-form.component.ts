import { Component, OnInit, Inject } from '@angular/core';
import { medtech, transaction } from 'src/app/services/service.interface';
import { MathService } from 'src/app/services/math.service';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-toxicology-form',
  templateUrl: './toxicology-form.component.html',
  styleUrls: ['./toxicology-form.component.scss']
})
export class ToxicologyFormComponent implements OnInit {

  id: any;
  medtech: medtech[];
  transaction: transaction;
  update: boolean = false;

  foo = ["NEGATIVE", "POSITIVE"];

  toxic = new FormGroup({
    toxicID: new FormControl(undefined),
    transactionID: new FormControl(""),
    patientID: new FormControl(""),

    pathID: new FormControl('5', [Validators.required]),
    medID: new FormControl('1', [Validators.required]),
    qualityID: new FormControl('11', [Validators.required]),

    patientIdRef: new FormControl(''),
    userID: new FormControl(''),

    creationDate: new FormControl("0000-00-00 00:00:00"),
    dateUpdate: new FormControl("0000-00-00 00:00:00"),

    meth: new FormControl(""),
    tetra: new FormControl(""),
    drugtest: new FormControl("")
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private math: MathService,
    private lab: LaboratoryService,
    private TS: TransactionService,
    private router: Router,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ToxicologyFormComponent>
  ) {
    this.math.navSubs("lab");

    this.id = this.dialogData.tid;

    if (isNaN(this.id)) {
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {
    this.lab.getMedtech().subscribe(
      medtech => {
        this.medtech = medtech;
      }
    )

    this.TS.getOneTrans("getTransaction/" + this.id).subscribe(
      data => {
        if (data[0].length == 0) {
          this.router.navigate(['error/404']);
        } else {
          this.transaction = data[0];

          this.toxic.controls.transactionID.setValue(data[0].transactionId);
          this.toxic.controls.patientID.setValue(data[0].patientId);
          this.toxic.controls.patientIdRef.setValue(data[0].patientIdRef);

          this.lab.getToxicology(data[0].transactionId).subscribe(
            toxic => {
              if (toxic.length != 0) {
                this.update = true;
                for (var i in toxic[0]) {
                  this.toxic.get(i).setValue(toxic[0][i].toString());
                }
              }
            }
          )
        }
      }
    )
  }

  addToxicology() {
    this.toxic.controls.creationDate.setValue(this.math.dateNow());
    this.toxic.controls.dateUpdate.setValue(this.math.dateNow());
    this.toxic.controls.userID.setValue(parseInt(sessionStorage.getItem('token')));

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be saved to database!" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.lab.addToxicology(this.toxic.value).subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['laboratory/toxicology']);
            this.dialogRef.close();
          }
        )
      }

    })
  }

  updateToxicology() {
    this.toxic.controls.dateUpdate.setValue(this.math.dateNow());
    this.toxic.controls.userID.setValue(parseInt(sessionStorage.getItem('token')));

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: { Title: "Are you sure?", Content: "Data will be saved to database!" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.lab.addToxicology(this.toxic.value, "/updateToxicology").subscribe(
          data => {
            if (data == 1) {
              this.math.openSnackBar("Data successfuly saved", "ok");
            } else {
              this.math.openSnackBar("Data not saved!!!", "ok");
            }
            // this.router.navigate(['laboratory/toxicology']);
            this.dialogRef.close();
          }
        )
      }

    })
  }

}
