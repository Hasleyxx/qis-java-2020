import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { TransactionService } from 'src/app/services/transaction.service';
import { MathService } from 'src/app/services/math.service';
import { Router } from '@angular/router';
import { PatientFormComponent } from 'src/app/element/patient-form/patient-form.component';
import { PatientService } from 'src/app/services/patient.service';
import { PeSummaryComponent } from 'src/app/element/pe-summary/pe-summary.component';
import { PeDataComponent } from 'src/app/element/pe-data/pe-data.component';
import { PeService } from 'src/app/services/pe.service';
import { PeAddComponent } from 'src/app/pe/pe-add/pe-add.component';
import { DialogPeFormComponent } from 'src/app/element/dialog-pe-form/dialog-pe-form.component';
import { DialogPeMedicalComponent } from 'src/app/element/dialog-pe-medical/dialog-pe-medical.component';


@Component({
  selector: 'app-imaging-pe',
  templateUrl: './imaging-pe.component.html',
  styleUrls: ['./imaging-pe.component.scss']
})


export class ImagingPeComponent implements OnInit {

  public dataSource: any;
  public temp: any;
  public displayedColumns = [];
  public currentUrl: any;
  public patients: any = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private peService: PeService,
    private patientService: PatientService,
    private transactionService: TransactionService,
    private math: MathService,
    private router: Router,
    private dialog: MatDialog
  ) {
    this.math.navSubs("imaging");
  }

  ngOnInit() {
    this.getPatient();
    if (this.router.url === '/pe/summary') {
      this.displayedColumns = ['transactionId', 'patientId', 'transactionDate', 'companyName', 'patientName', 'action'];
      this.currentUrl = 'summary';

    } else if (this.router.url === '/pe/list') {
      this.displayedColumns = ['transactionId', 'patientId', 'transactionDate', 'companyName', 'patientName', 'action'];
      this.currentUrl = 'list';

    } else if (this.router.url === '/pe/record') {
      this.displayedColumns = ['patientId', 'dateUpdate', 'companyName', 'patientName', 'action'];
      this.currentUrl = 'record';

    } else {
      this.displayedColumns = ['transactionId', 'patientId', 'transactionDate', 'companyName', 'patientName', 'action'];
      this.currentUrl = 'certificate';
    }
  }

  searchPatient(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.dialog.open(PeAddComponent, dialogConfig);
}

  getPatient() {
    if (this.router.url !== '/pe/record') {
      this.transactionService.getTransactions('getTransaction').subscribe(
        (data: any) => {
          data.forEach((element: any) => {
            const transactionId = element['transactionId'];
            const patientId = element['patientId'];
            const transactionDate = element['transactionDate'];
            this.patientService.getOnePatient('getPatient/' + patientId).subscribe(
              result => {
                const companyName = result[0].companyName;
                const firstName = result[0].firstName;
                const middleName = result[0].middleName;
                const lastName = result[0].lastName;

                this.peService.checkPe('checkPe/' + patientId + '/' + transactionId).subscribe(
                  value => {
                    let dataStatus = 'with';
                    if (value.length == 0) {
                      dataStatus = 'without';
                    }
                    this.patients.push(
                      {
                        "transactionId": transactionId,
                        "patientId": patientId,
                        "transactionDate": transactionDate,
                        "patientName": lastName + ', ' + firstName + ' ' + middleName,
                        "companyName": companyName,
                        "dataStatus": dataStatus
                      }
                    );
                    this.dataSource = new MatTableDataSource(this.patients);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                  }
                );
              }
            );
          });
        },
      );
    } else {
      this.patientService.getPatient('getPatient').subscribe(
        data => {
          this.dataSource = new MatTableDataSource<any>(data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      );
    }
  }

  editInfo(pid: any) {
    this.patientService.getOnePatient("getPatient/" + pid).subscribe(
      data => {
        const dialogRef = this.dialog.open(PatientFormComponent, {
          data: data[0],
          width: '90%'
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.status == "ok") {
            this.getPatient();
          }
        });
      }
    );
  }

  peSummary(pid: any, tid: any) {
    const dialogRef = this.dialog.open(PeSummaryComponent, {
      data: {
        pid: pid,
        tid: tid
      }
    });
  }

  peData(pid: any, tid: any) {
    const dialogRef = this.dialog.open(PeDataComponent, {
      data: {
        pid: pid,
        tid: tid
      }
    });

    dialogRef.afterClosed().subscribe(
      response => {
        if (response == 'update') {
          this.router.navigate(['pe/update/' + pid + '/' + tid]);
        }
      }
    );
  }

  peForm(pid: any, tid: any) {
    const dialogRef = this.dialog.open(DialogPeFormComponent, {
      data: {
        pid: pid,
        tid: tid
      }
    });
  }

  peMedical(pid: any, tid: any) {
    const dialogRef = this.dialog.open(DialogPeMedicalComponent, {
      data: {
        pid: pid,
        tid: tid
      }
    });
  }

}
