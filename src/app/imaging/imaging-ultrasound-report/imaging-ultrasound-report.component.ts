import { Component, OnInit, ViewChild } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { XrayService } from 'src/app/services/xray.service';
import * as moment from 'moment';
import { MatPaginator, MatTableDataSource, MatSort, MatDialogConfig, MatDialog } from '@angular/material';
import { ImagingUltrasoundFormComponent } from '../imaging-ultrasound-form/imaging-ultrasound-form.component';


@Component({
  selector: 'app-imaging-ultrasound-report',
  templateUrl: './imaging-ultrasound-report.component.html',
  styleUrls: ['./imaging-ultrasound-report.component.scss']
})


export class ImagingUltrasoundReportComponent implements OnInit {
  [x: string]: any;

  public ultrasound = "ultrasound";
  public patients = [];
  public currentYear = moment().year();
  public years = ["2018", "2019", "2020", "2021", "2022"];

  public displayedColumns: any = ["transactionId", "patientId", "transactionDate", "companyName", "patientName", "packageName", "action"];
  public dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private math: MathService,
    private PS: PatientService,
    private TS: TransactionService,
    private XS: XrayService,
    public dialog : MatDialog
  ) {
    this.math.navSubs("imaging")
  }

  ngOnInit() {
    this.getData(this.currentYear);
  }


  openDialog() {

    const dialogConfig = new MatDialogConfig();

    //dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    // this.dialog.open(ImagingSummaryFormComponent, dialogConfig);
    this.dialog.open(ImagingUltrasoundFormComponent, dialogConfig);
}

  getData(year: any) {
    this.TS.getTransactionsYear('getTransactionYear/' + year).subscribe(
      data => {
        if (data.length !== 0) {
          data.forEach(element => {
            const patientId = element.patientId;
            const transactionId = element.transactionId;
            const transactionDate = element.transactionDate;

            this.PS.getOnePatient('getPatient/' + patientId).subscribe(
              result => {
                const patientName = result[0].lastName + ', ' + result[0].firstName + ' ' + result[0].middleName;
                const companyName = result[0].companyName;

                this.TS.getTransExt(transactionId).subscribe(
                  value => {
                    const packageName = value[0].packageName;

                    this.patients.push({
                      "transactionId": transactionId,
                      "patientId": patientId,
                      "transactionDate": transactionDate,
                      "companyName": companyName,
                      "patientName": patientName,
                      "packageName": packageName
                    });

                    this.dataSource = new MatTableDataSource(this.patients);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                  }
                );
              }
            );
          });

        }else {
          const patient = [{
            "transactionId": "No result(s)",
            "patientId": "No result(s)",
            "transactionDate": "No result(s)",
            "companyName": "No result(s)",
            "patientName": "No result(s)",
            "packageName": "No result(s)"
          }];

          this.dataSource = new MatTableDataSource(patient);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    );
  }

  monthChange(year: any) {
    this.patients = [];
    this.getData(year);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add(tid: any, pid: any) {

  }


}
