import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectItemAccountComponent } from './select-item-account.component';

describe('SelectItemAccountComponent', () => {
  let component: SelectItemAccountComponent;
  let fixture: ComponentFixture<SelectItemAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectItemAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectItemAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
