import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-qc',
  templateUrl: './qc.component.html',
  styleUrls: ['./qc.component.scss']
})
export class QcComponent implements OnInit {

  public listType = "qc";
  constructor(
    private math: MathService
  ) {
    this.math.navSubs("qc");
  }

  ngOnInit() {
  }

}
