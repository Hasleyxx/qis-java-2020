import { Component, OnInit, Input } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'doc-head',
  templateUrl: './doc-head.component.html',
  styleUrls: ['./doc-head.component.scss']
})
export class DocHeadComponent implements OnInit {

  @Input() patientId: number;
  public patientData: any;

  constructor(
    private PS: PatientService,
    public math: MathService
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.PS.getOnePatient('getPatient/' + this.patientId).subscribe(data => {
      this.patientData = data[0];
    });
  }

}
