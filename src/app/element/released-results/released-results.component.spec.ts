import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleasedResultsComponent } from './released-results.component';

describe('ReleasedResultsComponent', () => {
  let component: ReleasedResultsComponent;
  let fixture: ComponentFixture<ReleasedResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleasedResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleasedResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
