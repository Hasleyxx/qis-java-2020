import { Injectable } from '@angular/core';
import { Global } from '../global.variable';
import { HttpClient } from '@angular/common/http';
import { docTrans, addDocTrans, updateDocTransaction } from './service.interface';
import { retry, catchError } from 'rxjs/operators';
import { ErrorService } from './error.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class DocTransactionService {
  constructor(
    private global: Global,
    private http: HttpClient,
    private ehs: ErrorService
  ) { }

  getDocTransactions(date1: string, date2: string): Observable<docTrans[]> {
    return this.http.get<docTrans[]>(this.global.url + "/getDocTransactions/" + date1 + "/" + date2)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getOneDocTransaction(tid: number): Observable<docTrans> {
    return this.http.get<docTrans>(this.global.url + "/getOneDocTransaction/" + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getDocTransactionsCanceled(date1: string, date2: string): Observable<docTrans[]> {
    return this.http.get<docTrans[]>(this.global.url + "/getDocTransactionsCanceled/" + date1 + "/" + date2)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  addDocTransaction(addTrans: addDocTrans): Observable<addDocTrans> {
    return this.http
      .post<addDocTrans>(this.global.url + "/addDocTransaction", addTrans)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  updateDocTransaction(updateDocTransaction: any): Observable<updateDocTransaction> {
    return this.http
      .post<updateDocTransaction>(this.global.url + "/updateDocTransaction", updateDocTransaction)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

  cancelDocTransaction(transDocId: number): Observable<any> {
    return this.http
      .get<any>(this.global.url + "/cancelDocTransaction/" + transDocId)
      .pipe(retry(1), catchError(this.ehs.handleError));
  }

}
