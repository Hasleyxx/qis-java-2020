import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-imaging-summary',
  templateUrl: './imaging-summary.component.html',
  styleUrls: ['./imaging-summary.component.scss']
})
export class ImagingSummaryComponent implements OnInit {

  public summary = "imaging";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("imaging");
  }

  ngOnInit() {

  }
}
