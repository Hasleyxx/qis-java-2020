import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { MatDialog } from '@angular/material';
import { ImagingItemsComponent } from '../imaging-items/imaging-items.component';
import { MarkerOptionService } from 'src/app/services/marker-option.service';

@Component({
  selector: 'app-imaging-markers',
  templateUrl: './imaging-markers.component.html',
  styleUrls: ['./imaging-markers.component.scss']
})
export class ImagingMarkersComponent implements OnInit {

  public markers = 'markers';
  public markerOptions = [];

  public filmSizes: Array<any>[];
  public radTechs: Array<any>[];
  public bodyParts: Array<any>[];

  public filmSizeValue: any[];
  public radTechValue: any[];
  public bodyPartValue: any[];

  @ViewChild('filmSizeSelect') filmSizeSelect: ElementRef;
  @ViewChild('radTechSelect') radTechSelect: ElementRef;
  @ViewChild('bodyPartSelect') bodyPartSelect: ElementRef;

  constructor(
    private math: MathService,
    private dialog: MatDialog,
    private MOS: MarkerOptionService
  ) {
    this.math.navSubs("imaging")
  }

  ngOnInit() {
    this.getMarkerOptions();
  }
  manageMarkers() {
    const dialogRef = this.dialog.open(ImagingItemsComponent, {
      width: '120vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getMarkerOptions();
    });
  }

  getMarkerOptions() {
    // Film sizes
    this.MOS.getFilmSize().subscribe(
      data => {
        //let filmSizes = data;
        this.filmSizes = data;
        this.filmSizeValue = data[0].name;
        //this.markerOptions.push(filmSizes);
      }
    );

    // Radtech
    this.MOS.getRadtech().subscribe(
      data => {
        //let radTechs = data;
        this.radTechs = data;
        this.radTechValue = data[0].name;
        //this.markerOptions.push(radTechs);
      }
    );

    // Bodyparts
    this.MOS.getBodypart().subscribe(
      data => {
        //let bodyParts = data;
        this.bodyParts = data;
        this.bodyPartValue = data[0].name;
        //this.markerOptions.push(bodyParts);
      }
    );
  }

  onPrintMarker(event) {
    const filmSize = this.filmSizeSelect['value'];
    const radTech = this.radTechSelect['value'];
    const bodyPart = this.bodyPartSelect['value'];
    const tid = event.tid;
    const data = [filmSize, radTech, bodyPart, tid];

    this.math.printMarker('', data);
    if (window.onafterprint) {
      // console.log('closed');
    }
  }

}
