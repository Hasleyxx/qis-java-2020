import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import { PatientService } from "src/app/services/patient.service";
import { MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatPaginator, MatSort, MatDialogRef } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { MedcertDialogComponent } from "../medcert-dialog/medcert-dialog.component";
import { DoctorService } from 'src/app/services/doctor.service';
import { ClearanceDialogComponent } from '../clearance-dialog/clearance-dialog.component';
import { MathService } from 'src/app/services/math.service';
import * as moment from 'moment';
import { PatientRecComponent } from '../patient-rec/patient-rec.component';
import { EditPrintClearanceComponent } from '../edit-print-clearance/edit-print-clearance.component';

@Component({
  selector: 'app-clearance-history',
  templateUrl: './clearance-history.component.html',
  styleUrls: ['./clearance-history.component.scss']
})
export class ClearanceHistoryComponent implements OnInit {

  displayedColumns: string[] = ['date', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  heldData: any[] = [];

  public patientId: any;
  public type: any;

  public patientDatas: any = [];
  public transDatas: any = [];
  public availed: any;
  public description: any;
  public transactionType: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientService: PatientService,
    private doctor: DoctorService,
    public state: ActivatedRoute,
    private dialog: MatDialog,
    private math: MathService,
    private dialogRef: MatDialogRef<ClearanceHistoryComponent>
  ) {
    this.patientId = this.data.pid;
    if (this.data.status) {
      this.type = this.data.status;
    }
  }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData() {
    // Patient
    this.patientService.getOnePatient("getPatient/" + this.patientId).subscribe(data => {
      this.patientDatas = data[0];
    });

    // Clearances
    this.doctor.getPidDocClearance(this.patientId).subscribe(
      data => {
        if (data.length > 0) {
          this.heldData = [];
          data.forEach(element => {
            // console.log(element);
            let transData: any = {
              date: moment(element['dateCreated']).format("LLL"),
              id: element['docClearanceID']
            }
            this.heldData.push(transData);
          });
          this.dataSource = new MatTableDataSource(this.heldData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    )
  }

  clearance(pid) {
    const dialogRef = this.dialog.open(ClearanceDialogComponent, {
      data: {
        pid: pid
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.status == "print") {
        let dataRef = result.dataRef;
        this.doctor.getOneDocClearanceRef(dataRef).subscribe(data => {
          let id = data.id;
          this.printClearance(id, this.patientId);
        });
        /* const dialogRef = this.dialog.open(ClearanceHistoryComponent, {
          data: {
            pid: pid
          },
          disableClose: true
        }); */
      }
    });
  }

  medcert(pid) {
    this.dialog.open(MedcertDialogComponent, {
      data: {
        pid: pid
      }
    });
  }

  printClearance(id, pid) {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(EditPrintClearanceComponent, {
      data: {
        pid: pid,
        clearanceId: id
      },
      width: '70%'
    });
    /* const suffix = [pid, 'clearance'];
    sessionStorage.setItem("clearanceId", id);
    this.math.printDoc('', suffix); */
  }

  close() {
    this.dialogRef.close();
    this.dialog.open(PatientRecComponent, {
      data: {
        pid: this.patientId
      }
    });
  }
}
