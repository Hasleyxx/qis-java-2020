import { Component, OnInit, ViewChild, Inject, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { PatientService } from 'src/app/services/patient.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PeService } from 'src/app/services/pe.service';
import * as moment from 'moment';
import { MathService } from 'src/app/services/math.service';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';

@Component({
  selector: 'app-pe-add',
  templateUrl: './pe-add.component.html',
  styleUrls: ['./pe-add.component.scss']
})
export class PeAddComponent implements OnInit {

  public type?: any = "";
  public patientID: any = null;
  public transactionID: any = null;
  public patientData: any;
  public transactionData: any;

  public medicals = [
    { "category": "Asthma", "control": "asth" },
    { "category": "Tuberculosis", "control": "tb" },
    { "category": "Diabetes Mellitus", "control": "dia" },
    { "category": "High Blood Pressure", "control": "hb" },
    { "category": "Heart Problem", "control": "hp" },
    { "category": "Kidney Problem", "control": "kp" },
    { "category": "Abdominal/Hernia", "control": "ab" },
    { "category": "Joint/Back/Scoliosis", "control": "jbs" },
    { "category": "Psychiatric Problem", "control": "pp" },
    { "category": "Migraine/Headache", "control": "mh" },
    { "category": "Fainting/Seizures", "control": "fs" },
    { "category": "Allergies", "control": "alle" },
    { "category": "Cancer/Tumor", "control": "ct" },
    { "category": "Hepatitis", "control": "hep" },
    { "category": "STD/PLHIV", "control": "std" }
  ];

  public vitals = [
    { "category": "Ishihara Test:", "control": "cv" },
    { "category": "Hearing:", "control": "hearing" },
    { "category": "Hospitalization:", "control": "hosp" },
    { "category": "Operations:", "control": "opr" },
    { "category": "Medications:", "control": "pm" },
    { "category": "Smoker:", "control": "smoker" },
    { "category": "Alcoholic:", "control": "ad" },
    { "category": "Last Menstrual:", "control": "lmp" },
    { "category": "Others/Notes:", "control": "notes" }
  ];

  public areas = [
    { "area": "Skin", "control": "skin" },
    { "area": "Head and Neck", "control": "hn" },
    { "area": "Chest/Breast/Lungs", "control": "cbl" },
    { "area": "Cardiac/Heart", "control": "ch" },
    { "area": "Abdomen", "control": "abdo" },
    { "area": "Extremities", "control": "extre" }
  ]

  public doctors = [
    { "doctor": "FROILAN A. CANLAS, M.D.", "license": "82498" },
    { "doctor": "JOHN TANGLAO, M.D.", "license": "79549" },
    { "doctor": "MARIGOLD A. SIBAL, M.D.", "license": "104200" }
  ];
  public license = this.doctors[0].license;

  public peAddForm: FormGroup;

  @ViewChild('licenseInput') licenseInput: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private router: Router,
    private patientService: PatientService,
    private peService: PeService,
    private transactionService: TransactionService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private math: MathService,
    private dialogRef: MatDialogRef<PeAddComponent>
  ) {
    this.math.navSubs("pe");

    //this.transactionID = this.route.snapshot.paramMap.get('tid');
    this.transactionID = this.dialogData.tid;
    this.type = this.dialogData.type;
  }

  ngOnInit() {
    this.getPeData();
    this.peAddForm = this.formBuilder.group({
      medhisForm: this.formBuilder.group({
        transactionID: [''],
        patientID: [''],
        asth: ['NO'],
        tb: ['NO'],
        dia: ['NO'],
        hb: ['NO'],
        hp: ['NO'],
        kp: ['NO'],
        ab: ['NO'],
        jbs: ['NO'],
        pp: ['NO'],
        mh: ['NO'],
        fs: ['NO'],
        alle: ['NO'],
        ct: ['NO'],
        hep: ['NO'],
        std: ['NO'],
        patientIdRef: [''],
        userID: [''],
        creationDate: [''],
        dateUpdate: ['']
      }),
      vitalForm: this.formBuilder.group({
        transactionID: [''],
        patientID: [''],
        height: ['', Validators.required],
        weight: ['', Validators.required],
        bmi: [''],
        bp: ['', Validators.required],
        pr: ['', Validators.required],
        rr: ['', Validators.required],
        uod: [''],
        uos: [''],
        cod: [''],
        cos: [''],
        cv: [''],
        hearing: [''],
        hosp: [''],
        opr: [''],
        pm: [''],
        smoker: [''],
        ad: [''],
        lmp: [''],
        notes: [''],
        patientIdRef: [''],
        userID: [''],
        creationDate: [''],
        dateUpdate: ['']
      }),
      peForm: this.formBuilder.group({
        transactionID: [''],
        patientID: [''],
        skin: ['YES'],
        hn: ['YES'],
        cbl: ['YES'],
        ch: ['YES'],
        abdo: ['YES'],
        extre: ['YES'],
        ot: [''],
        find: [''],
        doctor: ['FROILAN A. CANLAS, M.D.'],
        license: [+this.license],
        patientIdRef: [''],
        userID: [''],
        creationDate: [''],
        dateUpdate: ['']
      })
    });

    this.peAddForm.get('vitalForm.weight')!.valueChanges.subscribe(data => {
      let tempHeight = this.peAddForm.get('vitalForm.height').value;
      if (tempHeight.includes(".")) {
        let f_height: any;
        let i_height: any;
        let weight: any;
        let bmi: any;

        tempHeight = tempHeight.split(".");
        f_height = tempHeight[0];
        i_height = tempHeight[1];

        weight = this.peAddForm.get('vitalForm.weight').value * 2.205;
        let i = f_height * 12 + i_height * 1.0;

        bmi = this.cal_bmi(weight, i);
        this.addBmiExt(bmi);
      }
    });

    this.peAddForm.get('vitalForm.height')!.valueChanges.subscribe(data => {
      let tempHeight = this.peAddForm.get('vitalForm.height').value;
      if (tempHeight.includes(".")) {
        let f_height: any;
        let i_height: any;
        let weight: any;
        let bmi: any;

        tempHeight = tempHeight.split(".");
        f_height = tempHeight[0];
        i_height = tempHeight[1];

        weight = this.peAddForm.get('vitalForm.weight').value * 2.205;
        let i = f_height * 12 + i_height * 1.0;

        bmi = this.cal_bmi(weight, i);
        this.addBmiExt(bmi);
      }
    });
  }

  addBmiExt(bmi) {
    if (bmi <= 18.5) {
      this.peAddForm.get('vitalForm.bmi').setValue(bmi + " / Underweight");
    } else if (bmi >= 18.6 && bmi <= 24.9) {
      this.peAddForm.get('vitalForm.bmi').setValue(bmi + " / Normal");
    } else if (bmi >= 25 && bmi <= 29.9) {
      this.peAddForm.get('vitalForm.bmi').setValue(bmi + " / Overweight");
    } else {
      this.peAddForm.get('vitalForm.bmi').setValue(bmi + " / Obese");
    }
  }

  cal_bmi(lbs, ins) {
    let h2 = ins * ins;
    let bmi: any = lbs / h2 * 703;
    let f_bmi = Math.floor(bmi);
    let diff = bmi - f_bmi;
    diff = diff * 10;
    diff = Math.round(diff);
    if (diff == 10) {
      f_bmi += 1;
      diff = 0;
    }
    bmi = f_bmi + "." + diff;
    return bmi;
  }

  getPeData() {
    this.transactionService.getOneTrans('getTransaction/' + this.transactionID).subscribe(
      (data: any) => {
        this.transactionData = data[0];
        this.patientID = data[0]['patientId'];

        this.patientService.getOnePatient('getPatient/' + this.patientID).subscribe(
          (data: any) => {
            this.patientData = data[0];
          }
        );
      }
    );
  }

  changeDoctor(event: any) {
    let doctor = event;
    for (let index = 0; index < this.doctors.length; index++) {
      if (this.doctors[index]['doctor'] === doctor) {
        this.peAddForm.patchValue({ peForm: { license: this.doctors[index]['license'] } });
      }
    }
  }

  back() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Cancel this operation!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result === 'ok') {
          // this.router.navigate(['/pe/list']);

          this.dialogRef.close(this.type);
        }
      }
    );
  }

  addPe() {
    let r_height = this.peAddForm.controls['vitalForm'].get('height').value;
    let r_weight = this.peAddForm.controls['vitalForm'].get('weight').value;
    if (r_height.includes(".")) {
      r_height = r_height.split(".");
      r_height = r_height[0] + "ft" + r_height[1] + "inch";
    }
    r_weight = r_weight + "kg";
    this.peAddForm.get('medhisForm').patchValue({
      transactionID: +this.transactionID,
      patientID: +this.patientID,
      patientIdRef: this.transactionData.patientIdRef,
      userID: parseInt(sessionStorage.getItem('token')),
      creationDate: moment().format('YYYY-MM-DD HH:mm:ss'),
      dateUpdate: moment().format('0000-00-00 00:00:00')
    });
    this.peAddForm.get('vitalForm').patchValue({
      transactionID: +this.transactionID,
      patientID: +this.patientID,
      patientIdRef: this.transactionData.patientIdRef,
      userID: parseInt(sessionStorage.getItem('token')),
      creationDate: moment().format('YYYY-MM-DD HH:mm:ss'),
      dateUpdate: moment().format('0000-00-00 00:00:00'),
      height: r_height,
      weight: r_weight,
    });
    this.peAddForm.get('peForm').patchValue({
      transactionID: +this.transactionID,
      patientID: +this.patientID,
      userID: parseInt(sessionStorage.getItem('token')),
      creationDate: moment().format('YYYY-MM-DD HH:mm:ss'),
      dateUpdate: moment().format('0000-00-00 00:00:00')
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        Title: "Are you sure?",
        Content: "Patient data will be saved!"
      },
      width: '20%'
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result == "ok") {
          this.peService.addMedhis(this.peAddForm.get('medhisForm').value).subscribe();
          this.peService.addVital(this.peAddForm.get('vitalForm').value).subscribe();
          this.peService.addPE(this.peAddForm.get('peForm').value).subscribe();

          this.openSnackBar("Record has been saved!", "close");
          // this.router.navigate(['/pe/list']);
          this.dialogRef.close();
        }
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
