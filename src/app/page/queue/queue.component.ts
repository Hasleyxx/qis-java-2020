import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import { QueueDialogComponent } from '../queue-dialog/queue-dialog.component';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.scss']
})
export class QueueComponent implements OnInit {

  d = new DatePipe('en-US');
  year: any = this.d.transform(new Date(), "yyyy");
  month: any = this.d.transform(new Date(), "MM");
  day: any = this.d.transform(new Date(), "dd");
  url: any = "getTransactionToday/" + this.year + "-" + this.month + "-" + this.day;

  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  openQueue(type: any) {
    this.dialog.open(QueueDialogComponent, {
      data: {
        type: type,
        url: this.url
      },
      width: '95%'
    });
  }

}
