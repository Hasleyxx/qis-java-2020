import { Component, OnInit } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { PatientRecComponent } from '../../patient-rec/patient-rec.component';
import { MedicineDialogComponent } from '../../medicine-dialog/medicine-dialog.component';
import { MedcertHistoryComponent } from '../../medcert-history/medcert-history.component';
import { ClearanceHistoryComponent } from '../../clearance-history/clearance-history.component';
import { RequestHistoryComponent } from '../../request-history/request-history.component';

@Component({
  selector: 'doc-body',
  templateUrl: './doc-body.component.html',
  styleUrls: ['./doc-body.component.scss']
})
export class DocBodyComponent implements OnInit {

  public type: string;
  public patientId: any;

  data: string[];
  dataDetails: Promise<any>[];

  constructor(
    public math: MathService,
    route: ActivatedRoute,
    private dialog: MatDialog
  ) {
    this.data = route.snapshot.params['data'].split(',');
    this.patientId = this.data[0];
    this.type = this.data[1];
  }

  ngOnInit() {
    this.dataDetails = this.data.map(data => this.getDocReady(data));

    Promise.all(this.dataDetails).then(() => {
      this.math.onDocReady();

      setTimeout(() => {
        if (this.type == "blank") {
          const dialogRef = this.dialog.open(PatientRecComponent, {
            data: {
              pid: this.patientId
            },
            disableClose: true
          });

        } else if (this.type == "medication") {
          const dialogRef = this.dialog.open(MedicineDialogComponent, {
            data: {
              pid: this.patientId
            },
            disableClose: true
          });

        } else if (this.type == "medcert") {
          console.log(this.type);
          const dialogRef = this.dialog.open(MedcertHistoryComponent, {
            data: {
              pid: this.patientId
            },
            disableClose: true
          });

        } else if (this.type == "clearance") {
          const dialogRef = this.dialog.open(ClearanceHistoryComponent, {
            data: {
              pid: this.patientId
            },
            disableClose: true
          });

        } else if (this.type == "request") {
          const dialogRef = this.dialog.open(RequestHistoryComponent, {
            data: {
              pid: this.patientId
            },
            disableClose: true
          });

        }
      });
    });
  }

  getDocReady(data) {
    const amount = Math.floor((Math.random() * 100));
    return new Promise(resolve =>
      setTimeout(() => resolve({ amount }), 1000)
    );
  }
}
