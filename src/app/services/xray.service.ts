import { Injectable } from '@angular/core';
import { Global } from '../global.variable';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class XrayService {

  constructor(
    private global: Global,
    private httpClient: HttpClient,
    private ehs: ErrorService,
  ) { }

  addMarker(type): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addMarker', type)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  addXray(type): Observable<any> {
    return this.httpClient.post<any>(this.global.url + '/addXray', type)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getMarker(tid: any): Observable<any> {
    return this.httpClient.get(this.global.url + '/getMarker/' + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      );
  }

  getOneXray(tid: any = ""): Observable<any[]> {
    return this.httpClient.get<any[]>(this.global.url + "/getOneXray/" + tid)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  updateXray(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/updateXray", data)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }

  addXrayScan(data): Observable<any> {
    return this.httpClient.post<any>(this.global.url + "/addXrayScanImage", data)
      .pipe(
        retry(1),
        catchError(this.ehs.handleError)
      )
  }
}
