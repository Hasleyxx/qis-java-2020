import { Component, OnInit, Inject } from '@angular/core';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-pe-summary',
  templateUrl: './pe-summary.component.html',
  styleUrls: ['./pe-summary.component.scss']
})
export class PeSummaryComponent implements OnInit {

  public peSummary = "peSummary";

  constructor(
    private math: MathService
  ) {
    this.math.navSubs("pe");
  }

  ngOnInit() {

  }

}
