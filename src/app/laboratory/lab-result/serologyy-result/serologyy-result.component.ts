import { Component, OnInit, Input } from '@angular/core';
import { serology, toxicology } from 'src/app/services/service.interface';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'serologyy-result',
  templateUrl: './serologyy-result.component.html',
  styleUrls: ['./serologyy-result.component.scss']
})
export class SerologyyResultComponent implements OnInit {

  @Input() serology: serology;
  @Input() toxicology: toxicology;

  constructor(
    public math: MathService
  ) { }

  ngOnInit() {
  }

}
