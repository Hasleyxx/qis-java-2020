import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MathService } from 'src/app/services/math.service';

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss']
})
export class CertificatesComponent implements OnInit {


  public peCertificate = "peCertificate";

  constructor(
    private router: Router,
    private math: MathService
  ) {
    if (this.router.url == '/nurse/certificate') {
      this.math.navSubs("nurse");
      this.peCertificate = "nurseCertificate";

    } else if (this.router.url == '/imaging/certificate') {
      this.math.navSubs("imaging");
      this.peCertificate = "imagingCertificate";

    } else {
      this.math.navSubs("qc");
      this.peCertificate = "peCertificate";

    }
  }

  ngOnInit() {
  }
}
