import { Component, OnInit, Input, Inject } from '@angular/core';
import { MathService } from 'src/app/services/math.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LaboratoryService } from 'src/app/services/laboratory.service';
import { medtech, transaction } from 'src/app/services/service.interface';
import { Router } from '@angular/router';
import { TransactionService } from 'src/app/services/transaction.service';
import { ConfirmComponent } from 'src/app/element/confirm/confirm.component';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: 'microscopy-form',
  templateUrl: './microscopy-form.component.html',
  styleUrls: ['./microscopy-form.component.scss']
})
export class MicroscopyFormComponent implements OnInit {

  id: any;
  medtech : medtech[];
  transaction: transaction;
  update: boolean = false;
  patient: any;
  @Input() isIndustrial: boolean = true;

  urineColor = ["STRAW", "LIGHT YELLOW", "YELLOW", "DARK YELLOW", "RED", "ORANGE", "AMBER"];
  urineTransparency = ["CLEAR", "HAZY", "SL. TURBID", "TURBID"];
  ucValue = ["NEGATIVE", "TRACE", "1+", "2+", "3+", "4+"];
  nitValue = ["POSITIVE", "NEGATIVE"];
  ucpH = ["5.0", "6.0", "6.5", "7.0", "7.5", "8.0", "8.5", "9.0", "9.5"];
  ucSPG = ["1.000", "1.005", "1.010", "1.015", "1.020", "1.025", "1.030",];
  microscopicVal = ["RARE", "FEW", "MODERATE", "MANY"];

  fecColor = ["GREEN", "YELLOW", "LIGHT BROWN", "BROWN", "DARK BROWN"];
  fecCons = ["FORMED", "SEMI-FORMED", "SOFT", "WATERY", "SLIGHTLY MUCOID", "MUCOID"];

  foo = ["NEGATIVE", "POSITIVE"];

  micro = new FormGroup({

    microID: new FormControl(""),
    transactionID: new FormControl(""),
    patientID: new FormControl(""),

    pathID: new FormControl('5',[Validators.required]),
    medID: new FormControl('1', [Validators.required]),
    qualityID: new FormControl('11', [Validators.required]),

    patientIdRef: new FormControl(''),
    userID: new FormControl(''),

    creationDate: new FormControl("0000-00-00 00:00:00"),
    dateUpdate: new FormControl("0000-00-00 00:00:00"),

    uriColor: new FormControl("-"),
    uriTrans: new FormControl("-"),
    uriOt: new FormControl(""),

    uriPh: new FormControl("-"),
    uriSp: new FormControl("-"),
    uriPro: new FormControl("-"),
    uriGlu: new FormControl("-"),
    le: new FormControl("-"),
    nit: new FormControl("-"),
    uro: new FormControl("-"),
    bld: new FormControl("-"),
    ket: new FormControl("-"),
    bil: new FormControl("-"),

    rbc: new FormControl(""),
    wbc: new FormControl(""),
    ecells: new FormControl("-"),
    mthreads: new FormControl("-"),
    bac: new FormControl("-"),
    amorph: new FormControl("-"),
    coAx: new FormControl("-"),

    fecColor: new FormControl("-"),
    fecCon: new FormControl("-"),
    fecOt: new FormControl(""),
    fecMicro: new FormControl(""),

    pregTest: new FormControl(""),

    occultBLD: new FormControl("-"),

    afbva1: new FormControl(""),
    afbva2: new FormControl(""),
    afbr1: new FormControl(""),
    afbr2: new FormControl(""),
    afbd: new FormControl("")
  })

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private math: MathService,
    private lab: LaboratoryService,
    private TS: TransactionService,
    private PS: PatientService,
    private router: Router,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<MicroscopyFormComponent>
  ) {
    this.math.navSubs("lab");

    // this.id = this.route.snapshot.paramMap.get('id');
    this.id = this.dialogData.tid;

    if(isNaN(this.id)){
      this.router.navigate(['error/404']);
    }
  }

  ngOnInit() {

    this.TS.getOneTrans("getTransaction/" + this.id).subscribe(
      data => {
        if(data[0].length == 0){
          this.router.navigate(['error/404']);
        }else{

          this.PS.getOnePatient('getPatient/' + data[0].patientId).subscribe(data => {
            this.patient = data[0];
          });

          this.transaction = data[0];

          this.micro.controls.transactionID.setValue(data[0].transactionId);
          this.micro.controls.patientID.setValue(data[0].patientId);
          this.micro.controls.patientIdRef.setValue(data[0].patientIdRef);

          this.lab.getMicroscopy(data[0].transactionId).subscribe(
            micro => {
              if(micro.length != 0){
                this.update = true;
                for(var i in micro[0]){
                 this.micro.get(i).setValue(micro[0][i]);
                }
              }
            }
          )
        }
      }
    )

    this.lab.getMedtech().subscribe(
      medtech => {
        this.medtech = medtech;
      }
    )

    // Auto-fill data
    this.micro.get("fecMicro").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.micro.get("fecMicro").setValue("NO OVA OR PARASITE SEEN");
      }
    });

    this.micro.get("fecOt").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.micro.get("fecOT").setValue("NORMAL");
      }
    });

    this.micro.get("uriOt").valueChanges.subscribe(data => {
      if (data == "NO") {
        this.micro.get("uriOt").setValue("NORMAL");
      }
      if (data == "PR") {
        this.micro.get("uriOt").setValue("PROTEINURIA");
      }
      if (data == "UT") {
        this.micro.get("uriOt").setValue("UTI");
      }
      if (data == "GL") {
        this.micro.get("uriOt").setValue("GLUCOSURIA");
      }
    });

    this.micro.get("fecOt").valueChanges.subscribe(data => {
      if (data == "SP") {
        this.micro.get("fecOt").setValue("NO SPX");
      }
      if (data == "NO") {
        this.micro.get("fecOt").setValue("NORMAL");
      }
    });

    this.micro.get("pregTest").valueChanges.subscribe(data => {
      if (data == "NE") {
        this.micro.get("pregTest").setValue("NEGATIVE");
      }
      if (data == "PO") {
        this.micro.get("pregTest").setValue("POSITIVE");
      }
    });

    this.micro.get("rbc").valueChanges.subscribe(data => {
      if (data.includes("-")) {
        let temp = data.split("-");
        if (temp[1] !== "") {
          if (temp[1] > 3) {
            let tempData = this.micro.controls['uriOt'].value;

            let concatData = "";
            if (tempData == "") {
              concatData = tempData.concat("HEMATURIA");
            } else {
              if (tempData == "NORMAL") {
                concatData = "HEMATURIA";
              } else {
                if (!tempData.includes("HEMATURIA")) {
                  concatData = tempData.concat(",HEMATURIA");
                } else {
                  concatData = tempData;
                }
              }
            }

            this.micro.get("uriOt").setValue(concatData);
          }

          if (temp[1] < 3) {
            let uriOtTemp = this.micro.get("uriOt").value;
            let uriOt = uriOtTemp.replace(",HEMATURIA");
            this.micro.get("uriOt").setValue(uriOt);
          }
        }
      }
    })

    this.micro.get("wbc").valueChanges.subscribe(data => {
      if (data.includes("-")) {
        let temp = data.split("-");
        if (temp[1] !== "") {
          if (temp[1] > 10) {
            if (this.patient.gender == "FEMALE") {
              let tempData = this.micro.controls['uriOt'].value;
              let concatData = "";
              if (tempData == "") {
                concatData = tempData.concat("UTI");
              } else {
                if (tempData == "NORMAL") {
                  concatData = "UTI";
                } else {
                  if (!tempData.includes("UTI")) {
                  concatData = tempData.concat(",UTI");
                }
                }
              }
              this.micro.get("uriOt").setValue(concatData);
            }
          }

          if (temp[1] > 5) {
            if (this.patient.gender == "MALE") {
              let tempData = this.micro.controls['uriOt'].value;
              let concatData = "";
              if (tempData == "") {
                concatData = tempData.concat("UTI");
              } else {
                if (tempData == "NORMAL") {
                  concatData = "UTI";
                } else {
                  if (!tempData.includes("UTI")) {
                    concatData = tempData.concat(",UTI");
                  }
                }
              }
              this.micro.get("uriOt").setValue(concatData);
            }
          }

          if (temp[1] < 11 || temp[1] < 6) {
            let uriOtTemp1 = this.micro.get("uriOt").value;
            let uriOt1 = uriOtTemp1.replace(",HEMATURIA");
            let uriOt2 = uriOt1.replace(",UTI");
            this.micro.get("uriOt").setValue(uriOt2);
          }
        }
      }
    });
  }

  changePro(event) {
    if (event == "TRACE" || event == "1+" || event == "2+" || event == "3+" || event == "4+") {
      let uriOt = this.micro.get("uriOt").value;

      if (!uriOt.includes(",PROTEINURIA")) {
        this.micro.get("uriOt").setValue(uriOt + " ,PROTEINURIA");
      }
    } else {
      let uriOtTemp = this.micro.get("uriOt").value;
      let uriOt = uriOtTemp.replace(",PROTEINURIA", "");
      this.micro.get("uriOt").setValue(uriOt);
    }
  }
  changeGlu(event) {
    if (event == "TRACE" || event == "1+" || event == "2+" || event == "3+" || event == "4+") {
      let uriOt = this.micro.get("uriOt").value;

      if (!uriOt.includes(",GLUCOSURIA")) {
        this.micro.get("uriOt").setValue(uriOt + " ,GLUCOSURIA");
      }
    } else {
      let uriOtTemp = this.micro.get("uriOt").value;
      let uriOt = uriOtTemp.replace(",GLUCOSURIA", "");
      this.micro.get("uriOt").setValue(uriOt);
    }
  }

  addMicro(){
    this.micro.controls.creationDate.setValue(this.math.dateNow());
    this.micro.controls.dateUpdate.setValue(this.math.dateNow());
    this.micro.controls.userID.setValue(parseInt(sessionStorage.getItem("token")));

    Object.keys(this.micro.controls).forEach((key: string) => {
      if (this.micro.get(key).value == "" || this.micro.get(key).value == null) {
        if (key !== "microID") {
          if (key !== "userID") {
            if (key == "rbc" || key == "wbc" || key == "uriOt" || key == "fecMicro" || key == "fecOt" || key == "pregTest" || key == "afbva1" || key == "afbva2" || key == "afbr1" || key == "afbr2" || key == "afbd") {
              this.micro.get(key).setValue("N/A");
            } else {
              this.micro.get(key).setValue("-");
            }
          }
        }
      }
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: {Title: "Are you sure?", Content: "Data will be saved to database!"}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == "ok"){
        this.lab.addMicroscopy(this.micro.value).subscribe(
          data => {
            if(data == 1){
              this.math.openSnackBar("Data successfuly saved","ok");
            }else{
              this.math.openSnackBar("Data not saved!!!","ok");
            }
            // this.router.navigate(['/laboratory/microscopy']);
            this.dialogRef.close();
          }
        )
       }

    })
  }

  updateMicro() {
    this.micro.controls.dateUpdate.setValue(this.math.dateNow());
    this.micro.controls.userID.setValue(parseInt(sessionStorage.getItem('token')));

    Object.keys(this.micro.controls).forEach((key: string) => {
      if (this.micro.get(key).value == "" || this.micro.get(key).value == null) {
        if (key !== "microID") {
          if (key !== "userID") {
            if (key == "rbc" || key == "wbc" || key == "uriOt" || key == "fecMicro" || key == "fecOt" || key == "pregTest" || key == "afbva1" || key == "afbva2" || key == "afbr1" || key == "afbr2" || key == "afbd") {
              this.micro.get(key).setValue("N/A");
            } else {
              this.micro.get(key).setValue("-");
            }
          }
        }
      }
    });

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '20%',
      data: {Title: "Are you sure?", Content: "Data will be updated!"}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == "ok"){
        this.lab.addMicroscopy(this.micro.value, "/updateMicroscopy").subscribe(
          data => {
            if(data == 1){
              this.math.openSnackBar("Data successfuly update","ok");
            }else{
              this.math.openSnackBar("Error Updating data!!!","ok");
            }
            // this.router.navigate(['/laboratory/microscopy']);
            this.dialogRef.close();
          }
        )
       }

    })

  }

}
